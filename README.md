#Aplikasi Perpustakaan

Aplikasi perpustakaan adalah sebuah aplikasi untuk mengelola perpustakaan. 


##Instalasi
1. Clone aplikasi perpustakaan dari gitlab.
	- Masuk kedalam folder *htdocs* menggunakan *command prompt* atau biasa disebut *cmd*. Lalu ketikkanperintah berikut:
	`cd c:/xampp/htdocs/`
	- Kemudian clone kode dari gitlab dengan perintah berikut:
	`git clone https://gitlab.com/aansuseno/aplikasi-perpustakaan`
2. Masukkan database
	- Buka internet browser dengan kondisi *mysql* dan *apache* aktif
	- Create database baru dengan nama perpustakaan
	- kemudian *import* database **perpustakaan.sql** yg ada di `c:/xampp/htdocs/aplikasi-perpustakaan`
3. Berhasil