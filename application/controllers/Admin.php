<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('role') != 'admin' && $this->session->userdata('role') != 'pustakawan') {
			redirect(base_url().'login/admin');
		}
	}

	public function index()
	{
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];

		// jml peminjam perbulan
		$peminjamBulan;
		for($i = 0; $i < 12; $i++) {
			$peminjamBulan[$i] = $this->m_perpus->jmlDataDgnKondisi('transaksi',
				['tgl_pinjam > ' => date('Y-m-d', strtotime('-'.($i+1).' month')), 'tgl_pinjam <= ' => date('Y-m-d', strtotime('-'.$i.' month'))]
			);
		}

		//transaksi 1 bulan
		$qSiswa1bulan = 'SELECT
			t.nis_siswa as nis,
			s.nama as nama,
			k.nama as kelas
			FROM transaksi t
			INNER JOIN siswa s ON t.nis_siswa = s.nis
			INNER JOIN kelas k ON s.id_kelas = k.id
			WHERE t.tgl_pinjam > "'.date('Y-m-d', strtotime('-6 month')).'"';
		$siswa1bulan = $this->m_perpus->sqlget($qSiswa1bulan.' group by t.nis_siswa');
		$pinjamTerbanyak = 0;
		for($i = 0; $i < count($siswa1bulan); $i++) {
			$yangDipinjam = count($this->m_perpus->sqlget($qSiswa1bulan.' and t.nis_siswa = '.$siswa1bulan[$i]->nis));
			$siswa1bulan[$i]->jmlBuku = $yangDipinjam;
			if($pinjamTerbanyak < $yangDipinjam) {$pinjamTerbanyak = $yangDipinjam;}
		}

		// mengurutkan hasil
		$ke0;
		for($i = 0; $i < count($siswa1bulan) - 1; $i++) {
			for($j = 0; $j < count($siswa1bulan) - 1; $j++) {
				if($siswa1bulan[$j]->jmlBuku < $siswa1bulan[$j+1]->jmlBuku) {
					$ke0 = $siswa1bulan[$j];
					$siswa1bulan[$j] = $siswa1bulan[$j+1];
					$siswa1bulan[$j+1] = $ke0;
				}
			}
		}

		$data_user = $this->m_perpus->getsatu('admin', $kondisi);

		// aktivitas siswa
		$data_siswa = $this->m_perpus->sqlget('select * from siswa order by terakhir_aktif desc limit 5');
		$datahalaman = [
			'judul_halaman' => 'Beranda',
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'halaman' => 'beranda',
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'peminjamBulan' => $peminjamBulan,
			'data_siswa' => $data_siswa,
			'detailSiswa1Bln' => $siswa1bulan,
			'pinjamTerbanyak' => $pinjamTerbanyak,
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/home.php');
		$this->load->view('admin/footer.php');
	}

	function profil() {
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'judul_halaman' => 'Profil',
			'halaman' => 'akun',
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'username' => $data_user->username,
			'no_wa' => $data_user->no_wa,
			'alamat' => $data_user->alamat,
			'bio' => $data_user->bio,
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/akun/akun');
		$this->load->view('admin/footer.php');
	}

	function editprofilProses() {
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$dataupdate = [
			'nama' => $_POST['nama'],
			'username' => $_POST['username'],
			'no_wa' => $_POST['no_wa'],
			'alamat' => $_POST['alamat'],
			'bio' => $_POST['bio'],
		];

		// update data
		$this->m_perpus->update('admin', $dataupdate, $kondisi);

		redirect(base_url().'admin/profil');
	}

	function logout() {
		$this->session->sess_destroy();
		redirect(base_url().'login/admin');
	}

	function pengaturan() {
		$pesan = '';
		if(isset($_GET['pesan'])) {
			$pesan = $_GET['pesan'];
		}

		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$dataSekolah = $this->m_perpus->getsatuTanpaKondisi('pengaturan');
		$datahalaman = [
			'judul_halaman' => 'Profil',
			'perpus' => $dataSekolah,
			'halaman' => 'pengaturan',
			'nama_sekolah' => $dataSekolah->nama_perpus,
			'nama_user' => $data_user,
			'pesan' => $pesan,
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/pengaturan');
		$this->load->view('admin/footer.php');
	}

	function editPengaturanProses() {
		$dataupdate = [
			'nama_perpus' => $_POST['nama_perpus'],
			'moto' => $_POST['moto'],
			'deskripsi' => $_POST['deskripsi'],
			'denda' => $_POST['denda'],
			'lama_peminjaman' => $_POST['lama_peminjaman'],
		];

		// update data
		$this->m_perpus->update('pengaturan', $dataupdate, ['id' => $_POST['id']]);

		redirect(base_url('admin/pengaturan?pesan=berhasil'));
	}

	function gantiLogo() {
		$data = [
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
		];
		$this->load->view('admin/gantiLogo', $data);
	}

	function gantiLogoProses() {
		$P = $this->m_perpus->getsatuTanpaKondisi('pengaturan');

		$config['upload_path'] = './gambar/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
		$config['max_size'] = 10000;
		$config['overwrite'] = FALSE;

		$this->load->library('upload', $config);
		if (! $this->upload->do_upload('logo')) {
			redirect(base_url('admin/pengaturan?pesan='.$this->upload->display_errors()));
		} else {
			// hapus logo
			unlink('./gambar/'.$P->logo);
			$data_input = [
				'logo' => $this->upload->file_name,
			];
			$this->m_perpus->update('pengaturan', $data_input, ['id' => $_POST['id']]);
			redirect(base_url('admin/pengaturan?pesan=berhasilfoto'));
		}
	}

	function gantiPassword() {
		$pesan = '';
		if(isset($_GET['pesan'])) {
			$pesan = $_GET['pesan'];
		}
		$this->load->view('admin/akun/gantiPassword', ['pesan' => $pesan]);
	}

	function gantiPasswordProses() {
		$pw = $_POST['pw'];
		$pwKonf = $_POST['pwKonf'];
		if (strlen($pw) < 8) {
			redirect(base_url('admin/gantiPassword?pesan=kurang'));
		} else if($pw == $pwKonf) {
			//$this->m_perpus->update('admin', ['pw' => $pw], ['id' => $this->session->userdata('id')]);
			redirect(base_url('admin/gantiPassword?pesan=oke'));
		} else {
			redirect(base_url('admin/gantiPassword?pesan=tidakOke'));
		}
	}

	function aktivitasSiswa() {
		$data_siswa = $this->m_perpus->sqlget('select * from siswa order by terakhir_aktif desc limit '.$_GET['jmlAktivitas']);
		$datahalaman = [
			'data_siswa' => $data_siswa,
		];
		$this->load->view('admin/listAktivitas', $datahalaman);
	}

	function terbanyakMeminjam() {
		$bln = $_GET['bln'];
		//transaksi 1 bulan
		$qSiswa1bulan = 'SELECT
			t.nis_siswa as nis,
			s.nama as nama,
			k.nama as kelas
			FROM transaksi t
			INNER JOIN siswa s ON t.nis_siswa = s.nis
			INNER JOIN kelas k ON s.id_kelas = k.id
			WHERE t.tgl_pinjam > "'.date('Y-m-d', strtotime('-'.$bln.' month')).'"';
		$siswa1bulan = $this->m_perpus->sqlget($qSiswa1bulan.' group by t.nis_siswa');
		$pinjamTerbanyak = 0;
		for($i = 0; $i < count($siswa1bulan); $i++) {
			$yangDipinjam = count($this->m_perpus->sqlget($qSiswa1bulan.' and t.nis_siswa = '.$siswa1bulan[$i]->nis));
			$siswa1bulan[$i]->jmlBuku = $yangDipinjam;
			if($pinjamTerbanyak < $yangDipinjam) {$pinjamTerbanyak = $yangDipinjam;}
		}

		// mengurutkan hasil
		$ke0;
		for($i = 0; $i < count($siswa1bulan) - 1; $i++) {
			for($j = 0; $j < count($siswa1bulan) - 1; $j++) {
				if($siswa1bulan[$j]->jmlBuku < $siswa1bulan[$j+1]->jmlBuku) {
					$ke0 = $siswa1bulan[$j];
					$siswa1bulan[$j] = $siswa1bulan[$j+1];
					$siswa1bulan[$j+1] = $ke0;
				}
			}
		}
		$data['detailSiswa1Bln'] = $siswa1bulan;
		$data['pinjamTerbanyak'] = $pinjamTerbanyak;
		$this->load->view('admin/terbanyakMeminjam', $data);
	}

	function editPengaturanSuratBebasProses() {
		$dataupdate = [
			'nama_penanda_tangan' => $_POST['nama_penanda_tangan'],
			'jabatan_penanda_tangan' => $_POST['jabatan_penanda_tangan'],
			'nip_penanda_tangan' => $_POST['nip_penanda_tangan'],
			'alamat' => $_POST['alamat'],
			'kota' => $_POST['kota'],
		];

		// update data
		$this->m_perpus->update('pengaturan', $dataupdate, ['id' => $_POST['id']]);

		redirect(base_url('admin/pengaturan?pesan=berhasil'));
	}
}
