<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bibliografi extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('role') != 'admin' && $this->session->userdata('role') != 'pustakawan') {
			redirect(base_url().'login/admin');
		}
	}

	public function tambah()
	{
		$pesan = '';
		if (isset($_GET['pesan'])) {
			if ($_GET['pesan'] == 'berhasil') {
				$pesan = 'Data berhasil ditambah.';
			} else {
				$pesan = $_GET['pesan'];
			}
		}
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'judul_halaman' => 'Tambah Bibliografi',
			'halaman' => 'tambahBibliografi',
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'lemari' => $this->m_perpus->getUrut('lemari', 'nama'),
			'tahun' => $this->m_perpus->getUrut('tahun', 'nama'),
			'penerbit' => $this->m_perpus->getUrut('penerbit', 'nama'),
			'pesan' => $pesan,
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/bibliografi/tambah');
		$this->load->view('admin/footer.php');
	}

	public function daftar()
	{
		$query = 'SELECT * from bibliografi order by judul';
		$total = $this->m_perpus->jmlDataDgnQuery($query);
		$jmlDataDiambil = 20;
		$jml_halaman = 0;
		$halaman = 0;
		$pesan = '';
		if (isset($_GET['pesan'])) {
			$pesan =$_GET['pesan'];
		}
		if (isset($_GET['halaman'])) {
			$halaman =$_GET['halaman'];
		}
		$mulai = $jmlDataDiambil * $halaman;

		if ($total % $jmlDataDiambil == 0) {
			$jml_halaman = $total / $jmlDataDiambil;
		} else {
			$jml_halaman = $total / $jmlDataDiambil + 1;
		}
		$jml_halaman = (int)$jml_halaman;

		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'jmlData' => $jmlDataDiambil,
			'total' => $total,
			'pesan' => $pesan,
			'jml_halaman' => $jml_halaman,
			'halamanSekarang' => $halaman,
			'judul_halaman' => 'Daftar Bibliografi',
			'halaman' => 'daftarBibliografi',
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'buku' => $this->m_perpus->sqlget($query.' LIMIT '.$mulai.', '.$jmlDataDiambil),
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/bibliografi/tampil');
		$this->load->view('admin/footer.php');
	}

	// penulis
	function filterPenulis() {
		$data = [
			'penulis' => $this->m_perpus->cariUrut('penulis', 'nama', $_GET['cari'], 'nama'),
		];
		$this->load->view('admin/bibliografi/listPenulis', $data);
	}

	function tambahPenulis() {
		$this->m_perpus->input('penulis', ['nama' => $_GET['nama']]);
	}

	// kategori
	function filterKategori() {
		$data = [
			'kategori' => $this->m_perpus->cariUrut('kategori', 'nama', $_GET['cari'], 'nama'),
		];
		$this->load->view('admin/bibliografi/listKategori', $data);
	}

	function tambahKategori() {
		$this->m_perpus->input('kategori', ['nama' => $_GET['nama']]);
	}

	// tambah lemari
	function tambahLemari() {
		$this->load->view('admin/bibliografi/tambahLemari');
	}

	function tambahLemariProses() {
		$input = [
			'nama' => $_GET['nama'],
			'deskripsi' => $_GET['deskripsi']
		];
		$this->m_perpus->input('lemari', $input);
		$data = [
			'lemari' => $this->m_perpus->getUrut('lemari', 'nama'),
		];
		$this->load->view('admin/bibliografi/lemari',$data);
	}

	// tambah penerbit
	function tambahPenerbit() {
		$this->load->view('admin/bibliografi/tambahPenerbit');
	}

	function tambahPenerbitProses() {
		$input = [
			'nama' => $_GET['nama'],
			'deskripsi' => $_GET['deskripsi']
		];
		$this->m_perpus->input('penerbit', $input);
		$data = [
			'penerbit' => $this->m_perpus->getUrut('penerbit', 'nama'),
		];
		$this->load->view('admin/bibliografi/penerbit',$data);
	}

	// tambah tahun
	function tambahTahun() {
		$this->load->view('admin/bibliografi/tambahTahun');
	}

	function tambahTahunProses() {
		$input = [
			'nama' => $_GET['nama'],
			'deskripsi' => $_GET['deskripsi']
		];
		$this->m_perpus->input('tahun', $input);
		$data = [
			'tahun' => $this->m_perpus->getUrut('tahun', 'nama'),
		];
		$this->load->view('admin/bibliografi/tahun',$data);
	}

	function tambahProses() {
		$config['upload_path'] = './gambar/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
		$config['max_size'] = 10000;
		$config['overwrite'] = FALSE;

		$this->load->library('upload', $config);
		if (! $this->upload->do_upload('sampul')) {
			redirect(base_url('bibliografi/tambah?pesan='.$this->upload->display_errors()));
		} else {
			$data_input = [
				'judul' => $_POST['judul'],
				'penulis' => $_POST['penulis'],
				'id_tahun' => $_POST['id_tahun'],
				'id_penerbit' => $_POST['id_penerbit'],
				'kategori' => $_POST['kategori'],
				'id_lemari' => $_POST['id_lemari'],
				'jumlah_halaman' => $_POST['jumlah_halaman'],
				'deskripsi' => $_POST['deskripsi'],
				'tgl_buat' => date('Y-m-d'),
				'tgl_edit' => date('Y-m-d'),
				'sampul' => $this->upload->file_name,
			];
			$this->m_perpus->input('bibliografi', $data_input);
			redirect(base_url('bibliografi/tambah?pesan=berhasil'));
		}
	}

	public function detailBuku()
	{
		$buku = $this->m_perpus->getSatu('bibliografi', ['id' => $_GET['id']]);
		$penerbit = $this->m_perpus->getSatu('penerbit', ['id' => $buku->id_penerbit]);
		$tahun = $this->m_perpus->getSatu('tahun', ['id' => $buku->id_tahun]);
		$lemari = $this->m_perpus->getSatu('lemari', ['id' => $buku->id_lemari]);
		$data = [
			'buku' => $buku,
			'penerbit' => $penerbit,
			'tahun' => $tahun,
			'lemari' => $lemari,
		];
		$this->load->view('admin/bibliografi/detail', $data);
	}

	// hapus
	public function hapusBuku()
	{
		$data = [
			'buku' => $this->m_perpus->getsatu('bibliografi', ['id' => $_GET['id']])
		];
		$this->load->view('admin/bibliografi/hapus', $data);
	}

	public function hapusbukuProses() {
		$buku = $this->m_perpus->getsatu('bibliografi', ['id' => $_GET['id']]);

		// hapus sampul
		unlink('./gambar/'.$buku->sampul);
		$this->m_perpus->hapus('bibliografi', ['id' => $_GET['id']]);
		redirect(base_url('bibliografi/listbuku?pesan=berhasilHapus'));
	}
	// rampung hapus

	function listbuku() {
		$query = 'SELECT * from bibliografi order by judul';
		$total = $this->m_perpus->jmlDataDgnQuery($query);
		$jmlDataDiambil = 20;
		$jml_halaman = 0;
		$halaman = 0;
		$pesan = '';
		if (isset($_GET['pesan'])) {
			$pesan =$_GET['pesan'];
		}
		if (isset($_GET['halaman'])) {
			$halaman =$_GET['halaman'];
		}
		$mulai = $jmlDataDiambil * $halaman;

		if ($total % $jmlDataDiambil == 0) {
			$jml_halaman = $total / $jmlDataDiambil;
		} else {
			$jml_halaman = $total / $jmlDataDiambil + 1;
		}
		$jml_halaman = (int)$jml_halaman;

		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$data = [
			'buku' => $this->m_perpus->getUrut('bibliografi', 'judul'),
			'jmlData' => $jmlDataDiambil,
			'total' => $total,
			'pesan' => $pesan,
			'jml_halaman' => $jml_halaman,
			'halamanSekarang' => $halaman,
			'judul_halaman' => 'Daftar Bibliografi',
			'halaman' => 'daftarBibliografi',
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'buku' => $this->m_perpus->sqlget($query.' LIMIT '.$mulai.', '.$jmlDataDiambil),
		];
		$this->load->view('admin/bibliografi/listbuku', $data);
	}

	// edit
	public function editBuku()
	{
		$buku = $this->m_perpus->getSatu('bibliografi', ['id' => $_GET['id']]);
		$penerbit = $this->m_perpus->getSatu('penerbit', ['id' => $buku->id_penerbit]);
		$tahun = $this->m_perpus->getSatu('tahun', ['id' => $buku->id_tahun]);
		$lemari = $this->m_perpus->getSatu('lemari', ['id' => $buku->id_lemari]);
		$daftar_lemari = $this->m_perpus->getUrut('lemari', 'nama');
		$daftar_tahun = $this->m_perpus->getUrut('tahun', 'nama');
		$daftar_penerbit = $this->m_perpus->getUrut('penerbit', 'nama');
		$data = [
			'buku' => $buku,
			'penerbit' => $penerbit,
			'tahun' => $tahun,
			'lemari' => $lemari,
			'daftar_lemari' => $daftar_lemari,
			'daftar_tahun' => $daftar_tahun,
			'daftar_penerbit' => $daftar_penerbit,
		];
		$this->load->view('admin/bibliografi/edit', $data);
	}

	function editBukuProses() {
		$dataedit = [
			'judul' => $_GET['judul'],
			'penulis' => $_GET['penulis'],
			'id_penerbit' => $_GET['id_penerbit'],
			'jumlah_halaman' => $_GET['jumlah_halaman'],
			'id_tahun' => $_GET['id_tahun'],
			'kategori' => $_GET['kategori'],
			'id_lemari' => $_GET['id_lemari'],
			'deskripsi' => $_GET['deskripsi'],
			'tgl_edit' => date('Y-m-d')
		];
		$this->m_perpus->update('bibliografi', $dataedit, ['id' => $_GET['id']]);
		redirect(base_url('bibliografi/listbuku?pesan=berhasilEdit'));
	}

	function informasiDaftarBibliografi() {
		$this->load->view('admin/bibliografi/informasiDaftarBibliografi');
	}

	function gantiSampul() {
		$pesan = '';
		if(isset($_GET['pesan'])) {
			$pesan = $_GET['pesan'];
		}
		$data['b'] = $this->m_perpus->getsatu('bibliografi', ['id' => $_GET['id']]);
		$data['pesan'] = $pesan;
		$this->load->view('admin/bibliografi/gantiSampul', $data);
	}

	function gantiSampulProses() {
		$config['upload_path'] = './gambar/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg|svg';
		$config['max_size'] = 10000;
		$config['overwrite'] = FALSE;

		$this->load->library('upload', $config);
		if (! $this->upload->do_upload('sampul')) {
			redirect(base_url('bibliografi/gantiSampul?pesan='.$this->upload->display_errors().'&id='.$_POST['id']));
		} else {
			$buku = $this->m_perpus->getsatu('bibliografi', ['id' => $_POST['id']]);

			// hapus sampul
			unlink('./gambar/'.$buku->sampul);

			// Masukkan sampul
			$data_input = [
				'tgl_edit' => date('Y-m-d'),
				'sampul' => $this->upload->file_name,
			];
			$this->m_perpus->update('bibliografi', $data_input, ['id' => $_POST['id']]);
			redirect(base_url('bibliografi/gantiSampul?pesan=berhasil&id='.$_POST['id']));
		}
	}

	function cariBuku() {
		$c =$_GET['cari'];
		$query = 'SELECT * from bibliografi WHERE
			judul LIKE "%'.$c.'%" or
			penulis LIKE "%'.$c.'%" or
			kategori LIKE "%'.$c.'%"
			order by judul
		';
		$total = $this->m_perpus->jmlDataDgnQuery($query);
		$jmlDataDiambil = 20;
		$jml_halaman = 0;
		$halaman = 0;
		$pesan = '';
		if (isset($_GET['pesan'])) {
			$pesan =$_GET['pesan'];
		}
		if (isset($_GET['halaman'])) {
			$halaman =$_GET['halaman'];
		}
		$mulai = $jmlDataDiambil * $halaman;

		if ($total % $jmlDataDiambil == 0) {
			$jml_halaman = $total / $jmlDataDiambil;
		} else {
			$jml_halaman = $total / $jmlDataDiambil + 1;
		}
		$jml_halaman = (int)$jml_halaman;

		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$data = [
			'jmlData' => $jmlDataDiambil,
			'total' => $total,
			'pesan' => $pesan,
			'cari' => $c,
			'jml_halaman' => $jml_halaman,
			'halamanSekarang' => $halaman,
			'buku' => $this->m_perpus->sqlget($query.' LIMIT '.$mulai.', '.$jmlDataDiambil),
		];
		$this->load->view('admin/bibliografi/hasilCariBuku', $data);

	}
}
