<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cari extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('login')) {
			if ($this->session->userdata('login') == 'user') {
				redirect(base_url('user'));
			} else if($this->session->userdata('login') == 'pustakawan') {
				redirect(base_url('admin'));
			} else if($this->session->userdata('login') == 'admin') {
				redirect(base_url('admin'));
			}
		}
		$this->load->model('m_perpus');
	}

	public function index() {
		$data['perpus'] = $this->m_perpus->getsatuTanpaKondisi('pengaturan');
		$jmlPengunjung = $data['perpus']->jmlPengunjung + 1;
		if ($this->session->userdata('berkunjung') == true) {} else {
			$this->m_perpus->update('pengaturan', ['jmlPengunjung' => $jmlPengunjung], ['id' => $data['perpus']->id]);
		}
		$session = [
			'berkunjung' => true
		];
		$this->session->set_userdata($session);
		$this->load->view('cari', $data);
	}

	function cari() {
		$c = '';
		if (isset($_GET['cari'])) {
			$c = $_GET['cari'];
		}
		if (strlen($c) <= 2) {
			echo "Pencarian gagal. Silakan masukkan pencarian yang lebih spesifik. Seperti judul dan pengarang.";
		} else {
			$query = 'SELECT * from bibliografi WHERE judul LIKE "%'.$c.'%" or penulis LIKE "%'.$c.'%" or kategori LIKE "%'.$c.'%" or deskripsi LIKE "%'.$c.'%" ORDER BY rating  desc';
			$total = $this->m_perpus->jmlDataDgnQuery($query);
			$jmlDataDiambil = 10;
			$jml_halaman = 0;
			$halaman = 0;
			if (isset($_GET['halaman'])) {
				$halaman =$_GET['halaman'];
			}
			$mulai = $jmlDataDiambil * $halaman;


			if ($total % $jmlDataDiambil == 0) {
				$jml_halaman = $total / $jmlDataDiambil;
			} else {
				$jml_halaman = $total / $jmlDataDiambil + 1;
			}
			$jml_halaman = (int)$jml_halaman;
			$data = [
				'j' => $this->m_perpus->sqlget($query.' LIMIT '.$mulai.', '.$jmlDataDiambil),
				'jmlData' => $jmlDataDiambil,
				'cari' => $c,
				'total' => $total,
				'jml_halaman' => $jml_halaman,
				'halamanSekarang' => $halaman,
			];
			$this->load->view('hasilCari', $data);
			$this->load->view('pagination');
		}
	}

	function detailBuku() {
		$id = $_GET['id'];
		$b = $this->m_perpus->getsatu('bibliografi', ['id' => $id]);
		$t = $this->m_perpus->getsatu('tahun', ['id' => $b->id_tahun]);
		$p = $this->m_perpus->getsatu('penerbit', ['id' => $b->id_penerbit]);
		$l = $this->m_perpus->getsatu('lemari', ['id' => $b->id_lemari]);
		$data = [
			'b' => $b,
			'l' => $l,
			'p' => $p,
			't' => $t,
		];
		$this->load->view('detailBuku', $data);
	}
}
