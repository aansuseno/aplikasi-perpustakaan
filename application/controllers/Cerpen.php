<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cerpen extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('role') != 'admin' && $this->session->userdata('role') != 'pustakawan') {
			redirect(base_url().'login/admin');
		}
	}

	public function aktif()
	{
		$query = 'SELECT * from cerpen where status = 1 order by id desc';
		$total = $this->m_perpus->jmlDataDgnQuery($query);
		$jmlDataDiambil = 20;
		$jml_halaman = 0;
		$halaman = 0;
		if (isset($_GET['halaman'])) {
			$halaman =$_GET['halaman'];
		}
		$mulai = $jmlDataDiambil * $halaman;
		if ($total % $jmlDataDiambil == 0) {
			$jml_halaman = $total / $jmlDataDiambil;
		} else {
			$jml_halaman = $total / $jmlDataDiambil + 1;
		}
		$jml_halaman = (int)$jml_halaman;
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'judul_halaman' => 'Daftar Cerpen',
			'halaman' => 'daftarCerpenAktif',
			'jml_halaman' => $jml_halaman,
			'jmlData' => $jmlDataDiambil,
			'total' => $total,
			'halamanSekarang' => $halaman,
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'cerpen' => $this->m_perpus->sqlget($query.' LIMIT '.$mulai.', '.$jmlDataDiambil),
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/cerpen/aktif');
		$this->load->view('admin/footer.php');
	}

	function listAktif() {
		$query = 'SELECT * from cerpen where status = 1 order by id desc';
		$total = $this->m_perpus->jmlDataDgnQuery($query);
		$jmlDataDiambil = 20;
		$jml_halaman = 0;
		$halaman = 0;
		$pesan = '';
		if (isset($_GET['halaman'])) {
			$halaman =$_GET['halaman'];
		}
		if (isset($_GET['pesan'])) {
			$pesan =$_GET['pesan'];
		}
		$mulai = $jmlDataDiambil * $halaman;
		if ($total % $jmlDataDiambil == 0) {
			$jml_halaman = $total / $jmlDataDiambil;
		} else {
			$jml_halaman = $total / $jmlDataDiambil + 1;
		}
		$jml_halaman = (int)$jml_halaman;
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'jml_halaman' => $jml_halaman,
			'jmlData' => $jmlDataDiambil,
			'total' => $total,
			'halamanSekarang' => $halaman,
			'pesan' => $pesan,
			'cerpen' => $this->m_perpus->sqlget($query.' LIMIT '.$mulai.', '.$jmlDataDiambil),
		];
		$this->load->view('admin/cerpen/listaktif', $datahalaman);
	}

	function hapus() {
		$data['cerpen'] = $this->m_perpus->getsatu('cerpen', ['id' => $_GET['id']]);
		$this->load->view('admin/cerpen/hapus', $data);
	}

	function hapusProses() {
		$this->m_perpus->hapus('bintang_cerpen', ['id_cerpen' => $_GET['id']]);
		$this->m_perpus->hapus('cerpen', ['id' => $_GET['id']]);
		redirect(base_url('cerpen/listAktif?pesan=berhasilHapus'));
	}

	function hentikan() {
		$data['cerpen'] = $this->m_perpus->getsatu('cerpen', ['id' => $_GET['id']]);
		$this->load->view('admin/cerpen/hentikan', $data);
	}

	function hentikanProses() {
		$this->m_perpus->update('cerpen', ['status' => 0], ['id' => $_GET['id']]);
		redirect(base_url('cerpen/listAktif?pesan=berhasilHentikan'));
	}

	function baca() {
		$data['cerpen'] = $this->m_perpus->getsatu('cerpen', ['id' => $_GET['id']]);
		$this->load->view('admin/cerpen/baca', $data);
	}

	public function nonaktif()
	{
		$query = 'SELECT * from cerpen where status = 0 order by id desc';
		$total = $this->m_perpus->jmlDataDgnQuery($query);
		$jmlDataDiambil = 20;
		$jml_halaman = 0;
		$halaman = 0;
		if (isset($_GET['halaman'])) {
			$halaman =$_GET['halaman'];
		}
		$mulai = $jmlDataDiambil * $halaman;
		if ($total % $jmlDataDiambil == 0) {
			$jml_halaman = $total / $jmlDataDiambil;
		} else {
			$jml_halaman = $total / $jmlDataDiambil + 1;
		}
		$jml_halaman = (int)$jml_halaman;
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'judul_halaman' => 'Daftar Cerpen',
			'halaman' => 'daftarCerpenNonAktif',
			'jml_halaman' => $jml_halaman,
			'jmlData' => $jmlDataDiambil,
			'total' => $total,
			'halamanSekarang' => $halaman,
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'cerpen' => $this->m_perpus->sqlget($query.' LIMIT '.$mulai.', '.$jmlDataDiambil),
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/cerpen/nonaktif');
		$this->load->view('admin/footer.php');
	}

	function listNonAktif() {
		$query = 'SELECT * from cerpen where status = 0 order by id desc';
		$total = $this->m_perpus->jmlDataDgnQuery($query);
		$jmlDataDiambil = 20;
		$jml_halaman = 0;
		$halaman = 0;
		$pesan = '';
		if (isset($_GET['halaman'])) {
			$halaman =$_GET['halaman'];
		}
		if (isset($_GET['pesan'])) {
			$pesan =$_GET['pesan'];
		}
		$mulai = $jmlDataDiambil * $halaman;
		if ($total % $jmlDataDiambil == 0) {
			$jml_halaman = $total / $jmlDataDiambil;
		} else {
			$jml_halaman = $total / $jmlDataDiambil + 1;
		}
		$jml_halaman = (int)$jml_halaman;
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'jml_halaman' => $jml_halaman,
			'jmlData' => $jmlDataDiambil,
			'total' => $total,
			'halamanSekarang' => $halaman,
			'pesan' => $pesan,
			'cerpen' => $this->m_perpus->sqlget($query.' LIMIT '.$mulai.', '.$jmlDataDiambil),
		];
		$this->load->view('admin/cerpen/listnonaktif', $datahalaman);
	}

	function tayang() {
		$data['cerpen'] = $this->m_perpus->getsatu('cerpen', ['id' => $_GET['id']]);
		$this->load->view('admin/cerpen/tayang', $data);
	}

	function tayangProses() {
		$this->m_perpus->update('cerpen', ['status' => 1], ['id' => $_GET['id']]);
		redirect(base_url('cerpen/listNonAktif?pesan=berhasilTayang'));
	}
}
