<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('role') == 'admin' && $this->session->userdata('role') == 'pustakawan') {
			redirect(base_url().'/admin');
		}
	}

	function index() {
		$pesan = '';
		if (isset($_GET['pesan'])) {
			$pesan = $_GET['pesan'];
		}
		$data = [
			'pesan' =>$pesan,
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
		];
		$this->load->view('siswa/daftar', $data);
	}

	public function admin()
	{
		$pesan = '';
		if (isset($_GET['pesan'])) {
			if ($_GET['pesan'] == 'username-sudah-digunakan') {
				$pesan = "Maaf username sudah digunakan user lain";
			} else if ($_GET['pesan'] == 'berhasil') {
				$pesan = "Pendaftaran berhasil. Permintaan Anda akan segera kami proses. Anda akan segera mendapat pesan WA dari Admin.";
			}
		}
		$data = [
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'pesan' => $pesan,
		];
		$this->load->view('daftarAdmin', $data);
	}

	public function adminProses() {
		$nama = $_POST['nama'];
		$wa = $_POST['no-wa'];
		$username = $_POST['username'];
		$pass = $_POST['password'];
		$role = $_POST['wewenang'];

		// validasi
		$this->form_validation->set_rules('nama','Nama','required');
		$this->form_validation->set_rules('no-wa','Nomor WA','required');
		$this->form_validation->set_rules('username','username','required');
		if ($this->form_validation->run() == false) {
			redirect(base_url().'daftar/admin');
		} else if($this->m_perpus->jmlDataDgnKondisi('admin', ['username' => $username]) >= 1) {
			redirect(base_url().'daftar/admin?pesan=username-sudah-digunakan');
		}else {

			// input
			$input = array(
				'username' => $username,
				'password' => md5($pass),
				'nama' => $nama,
				'no_wa' => $wa,
				'wewenang' => $role,
				'status' => 'n',
				'tgl_daftar' => date('Y-m-d'),
				'tgl_edit' => date('Y-m-d')
			);
			$this->m_perpus->input('admin', $input);
			redirect(base_url().'daftar/admin?pesan=berhasil');
		}
	}

	function daftarSiswaProses() {
		$nis = $_POST['nis'];
		$pw = $_POST['pw'];
		$pwKonf = $_POST['pwKonf'];

		$s = $this->m_perpus->getsatu('siswa', ['nis' => $nis]);
		if ($this->m_perpus->jmlDataDgnKondisi('siswa', ['nis' => $nis]) < 1) {
			redirect(base_url().'daftar?pesan=NIS '.$nis.' tidak tersedia.');
		} else if($s->status != 'n') {
			redirect(base_url().'daftar?pesan=NIS tidak valid.');
		} else if(strlen($pw) < 8) {
			redirect(base_url().'daftar?pesan=Password harus lebih dari 8 karakter.');
		} else if($pw != $pwKonf) {
			redirect(base_url().'daftar?pesan=Konfirmasi password salah.');
		} else {
			$data = [
				'status' => 'y',
				'password' => md5($pw),
			];
			$this->m_perpus->update('siswa', $data, ['nis' => $nis]);
			redirect(base_url().'login?pesan=Berhasil mendaftar, sekarang silakan masuk.');
		}
	}
}
