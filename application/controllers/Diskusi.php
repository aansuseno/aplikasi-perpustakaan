<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diskusi extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		if ($this->session->userdata('role') != 'admin' && $this->session->userdata('role') != 'pustakawan') {
			redirect(base_url().'login/admin');
		}

		// hapus pesan lama
		$this->m_perpus->hapus('diskusi', ['hapus <' => date("Y-m-d H:i:s")]);
	}

	public function index(){
		$pesan = $this->m_perpus->sqlget('select * from diskusi order by id desc limit 20');

		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'judul_halaman' => 'Diskusi',
			'halaman' => 'semuaDiskusi',
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'pesan' => $pesan,
			'pengaturan' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/diskusi/semua');
		$this->load->view('admin/footer.php');
	}

	function semuaDiskusi() {
		$pesan = $this->m_perpus->sqlget('select * from diskusi order by id desc limit '.$_GET['jmlPesan']);
		$this->load->view('admin/diskusi/semuaList', ['pesan' => $pesan, 'pengaturan' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')]);
	}

	function kirim() {
		$data['pengaturan'] = $this->m_perpus->getsatuTanpaKondisi('pengaturan');
		$data['pengirim'] = $this->m_perpus->getsatu('admin', ['id' => $this->session->userdata('id')]);
		$this->load->view('admin/diskusi/kirim', $data);
	}

	function kirimProses() {
		date_default_timezone_set('Asia/Jakarta');

		$data['id_pengirim'] = $_GET['id_pengirim'];
		$data['nama_pengirim'] = $_GET['nama_pengirim'];
		$data['admin_kah'] = 1;
		$data['pesan'] = str_replace("00100110", "&", $_GET['pesan']);
		$data['dikirim'] = date('Y-m-d H:i:s');
		$data['hapus'] = '';
		if($_GET['rentang'] == 'n') {
			$data['hapus'] = '9999-12-12 00:00:00';
		} else if($_GET['rentang'] == 'o') {
			$data['hapus'] = $_GET['sampai'];
		} else if($_GET['rentang'] == 'x') {
			$data['hapus'] = date('Y-m-d H:i:s', strtotime("+".$_GET['sampai']." hours"));
		}
		$data['membalas'] = 'n';
		$this->m_perpus->input('diskusi', $data);
		redirect(base_url('diskusi/semuaDiskusi?jmlPesan='.$_GET['jmlPesan']));
	}

	function detailSiswa() {
		$data['p'] = $this->m_perpus->getSatuQuery('
			SELECT
			d.nama_pengirim AS nama_pengirim,
			k.nama as kelas,
			s.nis as nis,
			d.pesan as pesan,
			d.dikirim as dikirim,
			d.hapus as hapus,
			d.membalas as membalas
			FROM diskusi d
			INNER JOIN siswa s ON d.id_pengirim=s.id
			INNER JOIN kelas k ON s.id_kelas=k.id
			WHERE d.id = '.$_GET['id']);
		$this->load->view('admin/diskusi/detailSiswa', $data);
	}

	function detailAdmin() {
		$data['p'] = $this->m_perpus->getSatuQuery('
			SELECT
			d.nama_pengirim AS nama_pengirim,
			a.nama as nama_admin,
			a.no_wa as no_wa,
			d.pesan as pesan,
			d.dikirim as dikirim,
			d.hapus as hapus,
			d.membalas as membalas
			FROM diskusi d
			INNER JOIN admin a ON d.id_pengirim=a.id
			WHERE d.id = '.$_GET['id']);
		$this->load->view('admin/diskusi/detailAdmin', $data);
	}

	function hapus() {
		$this->m_perpus->hapus('diskusi', ['id' =>$_GET['id']]);
		redirect(base_url('diskusi/semuaDiskusi?jmlPesan='.$_GET['jmlPesan']));
	}

	function balas() {
		$data['pengaturan'] = $this->m_perpus->getsatuTanpaKondisi('pengaturan');
		$data['pengirim'] = $this->m_perpus->getsatu('admin', ['id' => $this->session->userdata('id')]);
		$data['pesan'] = $this->m_perpus->getsatu('diskusi', ['id' => $_GET['id']]);
		$this->load->view('admin/diskusi/balas', $data);
	}

	function balasProses() {
		date_default_timezone_set('Asia/Jakarta');

		$pengaturan = $this->m_perpus->getsatuTanpaKondisi('pengaturan');

		$data['id_pengirim'] = $_GET['id_pengirim'];
		$data['nama_pengirim'] = $_GET['nama_pengirim'];
		$data['admin_kah'] = 1;
		$data['pesan'] = str_replace("00100110", "&", $_GET['pesan']);
		$data['dikirim'] = date('Y-m-d H:i:s');
		$data['hapus'] = date('Y-m-d H:i:s', strtotime("+".$pengaturan->lama_pesan_diskusi." hours"));
		$data['membalas'] = $_GET['membalas'];
		$this->m_perpus->input('diskusi', $data);
		redirect(base_url('diskusi/semuaDiskusi?jmlPesan='.$_GET['jmlPesan']));
	}

	function pengaturan() {
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'judul_halaman' => 'Pengaturan Diskusi',
			'halaman' => 'pengaturanDiskusi',
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/diskusi/pengaturan');
		$this->load->view('admin/footer.php');
	}

	function editPengaturan() {
		$pengaturan = $this->m_perpus->getsatuTanpaKondisi('pengaturan');

		$data['lama_pesan_diskusi'] = $_POST['lama_pesan_diskusi'];
		$data['max_pesan_diskusi'] = $_POST['max_pesan_diskusi'];
		$data['panggil_pustakawan'] = $_POST['panggil_pustakawan'];
		$this->m_perpus->update('pengaturan', $data, ['id' => $pengaturan->id]);
		redirect(base_url('diskusi/pengaturan'));
	}
}
