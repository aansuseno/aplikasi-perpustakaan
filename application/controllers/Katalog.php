<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Katalog extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('role') != 'admin' && $this->session->userdata('role') != 'pustakawan') {
			redirect(base_url().'login/admin');
		}
	}

	public function tambah()
	{
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'judul_halaman' => 'Tambah Katalog',
			'halaman' => 'tambahKatalog',
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/katalog/tambah');
		$this->load->view('admin/footer.php');
	}

	function cariBuku() {
		$data = [
			'buku' => $this->m_perpus->sqlget('select * from bibliografi where judul like "%'.$_GET['judul'].'%" limit 10'),
		];
		$this->load->view('admin/katalog/listbuku', $data);
	}

	function cariBukuId() {
		$data = [
			'buku' => $this->m_perpus->cariUrut('bibliografi', 'id', $_GET['id'], 'judul'),
		];
		$this->load->view('admin/katalog/listbuku', $data);
	}

	function tambahProses() {
		$buku = $this->m_perpus->getsatu('bibliografi', ['id' => $_GET['id_buku']]);
		$jml;
		if ($buku->jumlah_buku == null) {
			$jml = 1;
		} else {
			$jml = $buku->jumlah_buku + 1;
		}
		$tersedia = $buku->tersedia + 1;
		$dataInput = [
			'kode_katalog' => $_GET['kode_katalog'],
			'id_buku' => $_GET['id_buku'],
			'kondisi' => $_GET['kondisi'],
			'deskripsi' => $_GET['deskripsi'],
			'kode_katalog' => $_GET['kode_katalog'],
			'tgl_buat_katalog' => date('Y-m-d'),
			'tgl_edit_katalog' => date('Y-m-d'),
		];
		$this->m_perpus->input('katalog', $dataInput);
		$this->m_perpus->update('bibliografi', ['jumlah_buku' => $jml, 'tersedia' => $tersedia], ['id' => $_GET['id_buku']]);
		$this->load->view('admin/katalog/tambahKatalog');
	}

	public function daftar()
	{
		$query = 'select * from katalog inner join bibliografi ON katalog.id_buku=bibliografi.id order by tgl_edit_katalog desc';
		$total = $this->m_perpus->jmlDataDgnQuery($query);
		$jmlDataDiambil = 15;
		$jml_halaman = 0;
		$halaman = 0;
		$mulai = $jmlDataDiambil * $halaman;
		if ($total % $jmlDataDiambil == 0) {
			$jml_halaman = $total / $jmlDataDiambil;
		} else {
			$jml_halaman = $total / $jmlDataDiambil + 1;
		}
		$jml_halaman = (int)$jml_halaman;
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'judul_halaman' => 'Daftar Katalog',
			'halaman' => 'daftarKatalog',
			'jml_halaman' => $jml_halaman,
			'jmlData' => $jmlDataDiambil,
			'total' => $total,
			'halamanSekarang' => $halaman,
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'katalog' => $this->m_perpus->sqlget($query.' LIMIT '.$mulai.', '.$jmlDataDiambil),
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/katalog/tampil');
		$this->load->view('admin/footer.php');
	}

	public function info(){
		$k =  $this->m_perpus->getsatu('katalog', ['id_katalog' => $_GET['id']]);
		$buku = $this->m_perpus->getsatu('bibliografi', ['id' => $k->id_buku]);
		$data = [
			'katalog1' => $k,
			'buku1' => $buku,
			'lemari1' => $this->m_perpus->getsatu('lemari', ['id' => $buku->id_lemari])
		];
		$this->load->view('admin/katalog/detail', $data);
	}

	function edit() {
		$k =  $this->m_perpus->getsatu('katalog', ['id_katalog' => $_GET['id']]);
		$data = [
			'katalog1' => $k,
			'buku' => $this->m_perpus->getsatu('bibliografi', ['id' =>$k->id_buku]),
		];
		$this->load->view('admin/katalog/edit', $data);
	}

	function editproses() {
		$ktlg = $this->m_perpus->getsatu('katalog', ['id_katalog' => $_GET['id']]);
		$kode = count($this->m_perpus->sqlget('select * from katalog where id_katalog = '.$ktlg->id_katalog.' and kode_katalog = "'.$_GET['kode_katalog'].'"'));
		if($kode == 0) {
			$dataInput = [
				'kode_katalog' => $_GET['kode_katalog'],
				'id_buku' => $_GET['id_buku'],
				'kondisi' => $_GET['kondisi'],
				'deskripsi' => $_GET['deskripsi'],
				'kode_katalog' => $_GET['kode_katalog'],
				'tgl_edit_katalog' => date('Y-m-d'),
			];

			//update transaksi
			$this->m_perpus->update('transaksi', ['kode_katalog' => $_GET['kode_katalog']], ['kode_katalog' => $ktlg->kode_katalog]);

			$this->m_perpus->update('katalog', $dataInput, ['id_katalog' => $_GET['id']]);
			redirect(base_url('katalog/list?pesan=berhasilEdit'));
		} else {
			redirect(base_url('katalog/list?pesan=sudahAda'));
		}
	}

	public function hapus(){
		$data=[
			'k1'=> $this->m_perpus->getsatu('katalog', ['id_katalog' => $_GET['id']])
		];
		$this->load->view('admin/katalog/hapus', $data);
	}

	function proseshapus() {
		$k =  $this->m_perpus->getsatu('katalog', ['id_katalog' => $_GET['id']]);
		$buku = $this->m_perpus->getsatu('bibliografi', ['id' => $k->id_buku]);
		$jml = $buku->jumlah_buku - 1;
		$tersedia = $buku->tersedia - 1;
		$this->m_perpus->update('bibliografi', ['jumlah_buku' => $jml, 'tersedia' => $tersedia], ['id' => $k->id_buku]);
		$this->m_perpus->hapus('katalog', ['id_katalog' => $_GET['id']]);
		redirect(base_url('katalog/list?pesan=berhasilHapus'));
	}

	function list () {
		$query = 'select * from katalog inner join bibliografi ON katalog.id_buku=bibliografi.id order by tgl_edit_katalog desc';
		$total = $this->m_perpus->jmlDataDgnQuery($query);
		$jmlDataDiambil = 15;
		$jml_halaman = 0;
		$halaman = 0;
		$pesan = '';
		if (isset($_GET['pesan'])) {
			$pesan =$_GET['pesan'];
		}
		if (isset($_GET['halaman'])) {
			$halaman =$_GET['halaman'];
		}
		$mulai = $jmlDataDiambil * $halaman;

		if ($total % $jmlDataDiambil == 0) {
			$jml_halaman = $total / $jmlDataDiambil;
		} else {
			$jml_halaman = $total / $jmlDataDiambil + 1;
		}
		$jml_halaman = (int)$jml_halaman;
		$data = [
			'katalog' => $this->m_perpus->sqlget($query.' LIMIT '.$mulai.', '.$jmlDataDiambil),
			'jmlData' => $jmlDataDiambil,
			'total' => $total,
			'pesan' => $pesan,
			'jml_halaman' => $jml_halaman,
			'halamanSekarang' => $halaman,
		];
		$this->load->view('admin/katalog/listkatalog', $data);
	}

	function informasiTambahKatalog() {
		$this->load->view('admin/katalog/informasiTambahKatalog');
	}

	function informasiKatalog() {
		$this->load->view('admin/katalog/informasiKatalog');
	}

	function cariKatalog () {
		$c = $_GET['cari'];
		$query = 'select * from katalog
			inner join bibliografi ON katalog.id_buku=bibliografi.id
			where katalog.kode_katalog LIKE "%'.$c.'%"
			or bibliografi.judul LIKE "%'.$c.'%"
			order by tgl_edit_katalog desc';
		$total = $this->m_perpus->jmlDataDgnQuery($query);
		$jmlDataDiambil = 15;
		$jml_halaman = 0;
		$halaman = 0;
		$pesan = '';
		if (isset($_GET['pesan'])) {
			$pesan =$_GET['pesan'];
		}
		if (isset($_GET['halaman'])) {
			$halaman =$_GET['halaman'];
		}
		$mulai = $jmlDataDiambil * $halaman;

		if ($total % $jmlDataDiambil == 0) {
			$jml_halaman = $total / $jmlDataDiambil;
		} else {
			$jml_halaman = $total / $jmlDataDiambil + 1;
		}
		$jml_halaman = (int)$jml_halaman;
		$data = [
			'katalog' => $this->m_perpus->sqlget($query.' LIMIT '.$mulai.', '.$jmlDataDiambil),
			'jmlData' => $jmlDataDiambil,
			'total' => $total,
			'pesan' => $pesan,
			'cari' => $c,
			'jml_halaman' => $jml_halaman,
			'halamanSekarang' => $halaman,
		];
		$this->load->view('admin/katalog/hasilCariKatalog', $data);
	}
}
