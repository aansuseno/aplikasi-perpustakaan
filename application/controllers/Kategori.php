<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('role') != 'admin' && $this->session->userdata('role') != 'pustakawan') {
			redirect(base_url().'login/admin');
		}
	}

	public function index(){
		$query = 'SELECT * from kategori order by nama';
		$total = $this->m_perpus->jmlDataDgnQuery($query);
		$jmlDataDiambil = 15;
		$jml_halaman = 0;
		$halaman = 0;
		$mulai = $jmlDataDiambil * $halaman;
		if ($total % $jmlDataDiambil == 0) {
			$jml_halaman = $total / $jmlDataDiambil;
		} else {
			$jml_halaman = $total / $jmlDataDiambil + 1;
		}
		$jml_halaman = (int)$jml_halaman;
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'judul_halaman' => 'Kategori',
			'halaman' => 'daftarKategori',
			'jml_halaman' => $jml_halaman,
			'jmlData' => $jmlDataDiambil,
			'total' => $total,
			'halamanSekarang' => $halaman,
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'kategori' => $this->m_perpus->sqlget($query.' LIMIT '.$mulai.', '.$jmlDataDiambil),
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/kategori/tampil');
		$this->load->view('admin/footer.php');
	}

	public function tambah(){
      $this->load->view('admin/kategori/tambah');
	}

	public function prosesTambahKategori(){
		$data=[
			'nama'=>$_GET['nama'],
			'deskripsi'=>$_GET['deskripsi']
		];
		$this->m_perpus->input('kategori',$data);
		redirect(base_url('kategori/list?pesan=berhasilTambah'));
	}

	public function proseshapus(){
		$this->m_perpus->hapus('kategori', ['id'=>$_GET['id']]);
		redirect(base_url('kategori/list?pesan=berhasilHapus'));
	}

	public function hapus(){
		$data=[
			'nama'=>$_GET['nama'],
			'id'=>$_GET['id']
		];
		$this->load->view('admin/kategori/hapus', $data);
	}

	public function info(){
		$data = [
			'kategori1' => $this->m_perpus->getsatu('kategori', ['id' =>$_GET['id']]),
		];
		$this->load->view('admin/kategori/info', $data);
	}

	public function edit(){
		$data = [
			'kategori1' => $this->m_perpus->getsatu('kategori', ['id' =>$_GET['id']]),
		];
		$this->load->view('admin/kategori/edit', $data);
	}

	public function editproses(){
		$dataedit=[
			'nama'=>$_GET['nama'],
			'deskripsi'=>$_GET['deskripsi']
		];
		$this->m_perpus->update('kategori', $dataedit,['id'=>$_GET['id']]);
		redirect(base_url('kategori/list?pesan=berhasilHapus'));
	}

	function list() {
		$query = 'SELECT * from kategori order by nama';
		$total = $this->m_perpus->jmlDataDgnQuery($query);
		$jmlDataDiambil = 15;
		$jml_halaman = 0;
		$halaman = 0;
		$pesan = '';
		if (isset($_GET['pesan'])) {
			$pesan =$_GET['pesan'];
		}
		if (isset($_GET['halaman'])) {
			$halaman =$_GET['halaman'];
		}
		$mulai = $jmlDataDiambil * $halaman;

		if ($total % $jmlDataDiambil == 0) {
			$jml_halaman = $total / $jmlDataDiambil;
		} else {
			$jml_halaman = $total / $jmlDataDiambil + 1;
		}
		$jml_halaman = (int)$jml_halaman;
		$data = [
			'kategori' => $this->m_perpus->sqlget($query.' LIMIT '.$mulai.', '.$jmlDataDiambil),
			'jmlData' => $jmlDataDiambil,
			'total' => $total,
			'pesan' => $pesan,
			'jml_halaman' => $jml_halaman,
			'halamanSekarang' => $halaman,
		];
		$this->load->view('admin/kategori/listkategori', $data);
	}

	function informasi() {
		$this->load->view('admin/kategori/informasi');
	}
}
