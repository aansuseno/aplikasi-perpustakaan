<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('role') != 'admin' && $this->session->userdata('role') != 'pustakawan') {
			redirect(base_url().'login/admin');
		}
	}

	public function tambah()
	{
		$pesan = '';
		if (isset($_GET['pesan'])) {
			$pesan = $_GET['pesan'];
		}
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'judul_halaman' => 'Tambah kelas',
			'halaman' => 'tambahKelas',
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'pesan'=> $pesan,
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/kelas/tambah');
		$this->load->view('admin/footer.php');
	}

	public function tambahProses(){
		$datainput = [
			'nama' => $_POST['nama_kelas'],
			'kepanjangan' => $_POST['kepanjangan_kelas'],
			'tgl_buat' => date('Y-m-d'),
			'tgl_edit' => date('Y-m-d'),
		];
		$this->m_perpus->input('kelas', $datainput);
		redirect(base_url().'kelas/tambah?pesan=berhasil');
	}

	public function daftar(){
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$pesan = '';
		if(isset($_GET['pesan'])) {
			$pesan = $_GET['pesan'];
		}
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'judul_halaman' => 'Daftar Kelas',
			'halaman' => 'daftarKelas',
			'pesan' => $pesan,
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'kelas' => $this->m_perpus->get('kelas'),
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/kelas/tampil');
		$this->load->view('admin/footer.php');
	}

	public function detail() {
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'judul_halaman' => 'Detail kelas',
			'halaman' => 'detailKelas',
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'kelas' => $this->m_perpus->getsatu('kelas', ['id'=> $_GET['id']])
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/kelas/detail');
		$this->load->view('admin/footer.php');
	}
	public function edit()
	{
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'judul_halaman' => 'Edit Kelas',
			'halaman' => 'detailKelas',
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'kelas' => $this->m_perpus->getsatu('kelas', ['id' => $_GET['id']])
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/kelas/edit');
		$this->load->view('admin/footer.php');

	}
	public function editProses(){
		$datainput = [
			'nama' => $_POST['nama_kelas'],
			'kepanjangan' => $_POST['kepanjangan_kelas'],
			'deskripsi' => $_POST['deskripsi'],
			'tgl_edit' => date('Y-m-d'),
		];
		$this->m_perpus->update('kelas', $datainput, ['id' =>$_POST['id']]);
		redirect(base_url().'kelas/daftar?pesan=berhasilEdit');
	}

	public function hapus() {
		$kelasTerkait = count($this->m_perpus->sqlget('select * from siswa where id_kelas = '.$_GET['id']));
		if($kelasTerkait == 0) {
			$this->m_perpus->hapus('kelas', ['id' => $_GET['id']]);
			redirect(base_url().'kelas/daftar?pesan=hapusSukses');
		} else {
			redirect(base_url().'kelas/daftar?pesan=hapusGagal');
		}
	}
}
