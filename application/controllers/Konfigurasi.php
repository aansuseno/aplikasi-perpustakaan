<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konfigurasi extends CI_Controller {

	function __construct() {
		parent::__construct();
		// if ($this->m_perpus->jumlahData('admin') > 0 && $this->m_perpus->jumlahData('pengaturan') > 0) {
		// 	redirect(base_url());
		// }
	}

	function index() {
		$this->load->view('konfigurasi');
	}

	function adminProses() {
		$nama = $_POST['nama'];
		$wa = $_POST['no-wa'];
		$username = $_POST['username'];
		$pass = $_POST['password'];

		// validasi
		$this->form_validation->set_rules('nama','Nama','required');
		$this->form_validation->set_rules('no-wa','Nomor WA','required');
		$this->form_validation->set_rules('username','username','required');
		if ($this->form_validation->run() == false) {
			$this->load->view('konfigurasi');
		} else {

			// input
			$input = array(
				'username' => $username,
				'password' => md5($pass),
				'nama' => $nama,
				'no_wa' => $wa,
				'wewenang' => 'admin',
				'status' => 'y',
				'dikonfirmasi_oleh' => -1,
				'tgl_daftar' => date('Y-m-d')
			);
			$this->m_perpus->input('admin', $input);
			
			redirect(base_url().'konfigurasi/aplikasi');
		}
	}

	function aplikasi() {
		$this->load->view('konfigurasiAplikasi');
	}

	function aplikasiProses() {
		$nama = $_POST['nama'];
		$moto = $_POST['moto'];

		$input = array(
			'nama_perpus' => $nama,
			'moto' => $moto,
			'tgl_buat' => date('Y-m-d')
		);
		$this->m_perpus->input('pengaturan', $input);
		redirect(base_url().'login/admin');
	}
}
