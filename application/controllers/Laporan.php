<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('role') != 'admin' && $this->session->userdata('role') != 'pustakawan') {
			redirect(base_url().'login/admin');
		}
	}

	function transaksi() {
		$pesan = '';
		if (isset($_GET['pesan'])) {
			$pesan = $_GET['pesan'];
		}
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$petugas = $this->m_perpus->sqlget('select * from admin where status = "y" or status = "b"');
		$datahalaman = [
			'judul_halaman' => 'Laporan Transaksi',
			'halaman' => 'laporanTransaksi',
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'pesan'=> $pesan,
			'petugas' => $petugas,
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/laporan/transaksi');
		$this->load->view('admin/footer.php');
	}

	function transaksi30hari() {
		if(isset($_GET['donlot'])) {
			if($_GET['donlot'] == 'iya') {
				header("Content-type: application/vnd-ms-excel");
				header("Content-disposition: attachment; filename=Laporan transaksi perpustakaan.xls");
			}
		}
		$query = 'select
			t.tgl_pinjam as tgl_pinjam,
			t.nis_siswa as nis,
			s.nama as nama_siswa,
			k.kode_katalog as kode_katalog,
			b.judul as judul_buku,
			a.nama as nama_petugas,
			t.kembali as kembali,
			t.tepat_waktu as tepat_waktu,
			kls.nama as kelas
			FROM transaksi t
			INNER JOIN katalog k ON k.kode_katalog = t.kode_katalog
			INNER JOIN bibliografi b ON b.id = k.id_buku
			INNER JOIN siswa s ON s.nis = t.nis_siswa
			INNER JOIN admin a ON t.id_petugas = a.id
			INNER JOIN kelas kls ON kls.id = s.id_kelas
			WHERE t.tgl_pinjam >= "'.date('Y-m-d', strtotime('-30 days')).'"';
		$peminjam = $this->m_perpus->sqlget($query.'  GROUP by t.nis_siswa ORDER BY t.id DESC');
		$transaksi = $this->m_perpus->sqlget($query.' ORDER BY t.id DESC');

		// menghitung jumlah buku yang dipinjam
		for($i = 0; $i < count($peminjam); $i++) {
			$kondisi = [
				'nis_siswa' => $peminjam[$i]->nis,
				'tgl_pinjam >=' => date('Y-m-d', strtotime('-30 days')),
			];
			$peminjam[$i]->meminjam_buku = $this->m_perpus->jmlDataDgnKondisi('transaksi', $kondisi);
			$kondisiTerlambat = [
				'nis_siswa' => $peminjam[$i]->nis,
				'tgl_pinjam >=' => date('Y-m-d', strtotime('-30 days')),
				'tepat_waktu' => 0,
			];
			$peminjam[$i]->terlambat = $this->m_perpus->jmlDataDgnKondisi('transaksi', $kondisiTerlambat);
		}

		// mengurutkan hasil siswa
		$ke0;
		for($i = 0; $i < count($peminjam) - 1; $i++) {
			for($j = 0; $j < count($peminjam) - 1; $j++) {
				if($peminjam[$j]->meminjam_buku < $peminjam[$j+1]->meminjam_buku) {
					$ke0 = $peminjam[$j];
					$peminjam[$j] = $peminjam[$j+1];
					$peminjam[$j+1] = $ke0;
				}
			}
		}

		// data ke view
		$data = [
			'transaksi' => $transaksi,
			'siswa' => $peminjam,
			'lama' => '30 Hari',
		];
		$this->load->view('admin/laporan/print_transaksi30hari', $data);
	}

	function transaksi1tahun() {
		if(isset($_GET['donlot'])) {
			if($_GET['donlot'] == 'iya') {
				header("Content-type: application/vnd-ms-excel");
				header("Content-disposition: attachment; filename=Laporan transaksi perpustakaan.xls");
			}
		}
		$query = 'select
			t.tgl_pinjam as tgl_pinjam,
			t.nis_siswa as nis,
			s.nama as nama_siswa,
			k.kode_katalog as kode_katalog,
			b.judul as judul_buku,
			a.nama as nama_petugas,
			t.kembali as kembali,
			t.tepat_waktu as tepat_waktu,
			kls.nama as kelas
			FROM transaksi t
			INNER JOIN katalog k ON k.kode_katalog = t.kode_katalog
			INNER JOIN bibliografi b ON b.id = k.id_buku
			INNER JOIN siswa s ON s.nis = t.nis_siswa
			INNER JOIN admin a ON t.id_petugas = a.id
			INNER JOIN kelas kls ON kls.id = s.id_kelas
			WHERE t.tgl_pinjam >= "'.date('Y-m-d', strtotime('-1 year')).'"';
		$peminjam = $this->m_perpus->sqlget($query.'  GROUP by t.nis_siswa ORDER BY t.id DESC');
		$transaksi = $this->m_perpus->sqlget($query.' ORDER BY t.id DESC');

		// menghitung jumlah buku yang dipinjam
		for($i = 0; $i < count($peminjam); $i++) {
			$kondisi = [
				'nis_siswa' => $peminjam[$i]->nis,
				'tgl_pinjam >=' => date('Y-m-d', strtotime('-1 year')),
			];
			$peminjam[$i]->meminjam_buku = $this->m_perpus->jmlDataDgnKondisi('transaksi', $kondisi);
			$kondisiTerlambat = [
				'nis_siswa' => $peminjam[$i]->nis,
				'tgl_pinjam >=' => date('Y-m-d', strtotime('-1 year')),
				'tepat_waktu' => 0,
			];
			$peminjam[$i]->terlambat = $this->m_perpus->jmlDataDgnKondisi('transaksi', $kondisiTerlambat);
		}

		// mengurutkan hasil siswa
		$ke0;
		for($i = 0; $i < count($peminjam) - 1; $i++) {
			for($j = 0; $j < count($peminjam) - 1; $j++) {
				if($peminjam[$j]->meminjam_buku < $peminjam[$j+1]->meminjam_buku) {
					$ke0 = $peminjam[$j];
					$peminjam[$j] = $peminjam[$j+1];
					$peminjam[$j+1] = $ke0;
				}
			}
		}

		// data ke view
		$data = [
			'transaksi' => $transaksi,
			'siswa' => $peminjam,
			'lama' => '1 Tahun',
		];
		$this->load->view('admin/laporan/print_transaksi30hari', $data);
	}

	function transaksiOpsional() {
		header("Content-type: application/vnd-ms-excel");
		header("Content-disposition: attachment; filename=Laporan transaksi perpustakaan.xls");
		$mulai = $_POST['mulai'];
		$sampai = $_POST['sampai'];
		$kategori = $_POST['kategori'];
		$petugas = $_POST['petugas'];

		$query = 'select
			t.tgl_pinjam as tgl_pinjam,
			t.nis_siswa as nis,
			s.nama as nama_siswa,
			k.kode_katalog as kode_katalog,
			b.judul as judul_buku,
			a.nama as nama_petugas,
			t.kembali as kembali,
			t.tepat_waktu as tepat_waktu,
			kls.nama as kelas
			FROM transaksi t
			INNER JOIN katalog k ON k.kode_katalog = t.kode_katalog
			INNER JOIN bibliografi b ON b.id = k.id_buku
			INNER JOIN siswa s ON s.nis = t.nis_siswa
			INNER JOIN admin a ON t.id_petugas = a.id
			INNER JOIN kelas kls ON kls.id = s.id_kelas
			WHERE t.tgl_pinjam >= "'.$mulai.'" and t.tgl_pinjam <= "'.$sampai.'"';
		if($kategori == 'dipinjam') {
			$query .= " and t.kembali = 0";
		} else if($kategori == 'kembali') {
			$query .= " and t.kembali = 1";
		}else if($kategori == 'terlambat') {
			$query .= " and t.kembali = 1 and t.tepat_waktu = 0";
		}
		if($petugas != 'semua') {
			$query .= ' and t.id_petugas = '.$petugas;
		}
		$transaksi = $this->m_perpus->sqlget($query.' ORDER BY t.id DESC');

		// data ke view
		if($petugas == 'semua') {
			$petugas = 'Semua petugas';
		} else {
			$petugas = $this->m_perpus->getsatu('admin', ['id' => $petugas])->nama;
		}
		$data = [
			'transaksi' => $transaksi,
			'kategori' => $kategori,
			'petugas' => $petugas,
			'mulai' => $mulai,
			'sampai' => $sampai,
		];
		$this->load->view('admin/laporan/print_transaksiOps', $data);
	}

	function buku() {
		$pesan = '';
		if (isset($_GET['pesan'])) {
			$pesan = $_GET['pesan'];
		}
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$petugas = $this->m_perpus->sqlget('select * from admin where status = "y" or status = "b"');
		$datahalaman = [
			'judul_halaman' => 'Laporan Transaksi',
			'halaman' => 'laporanBuku',
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'pesan'=> $pesan,
			'petugas' => $petugas,
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/laporan/buku');
		$this->load->view('admin/footer.php');
	}

	function bukuSemua() {
		if(isset($_GET['donlot'])) {
			if($_GET['donlot'] == 'iya') {
				header("Content-type: application/vnd-ms-excel");
				header("Content-disposition: attachment; filename=Laporan transaksi perpustakaan.xls");
			}
		}

		$query = 'SELECT
		b.judul AS judul,
		b.penulis AS penulis,
		p.nama AS penerbit,
		b.jumlah_halaman AS jumlah_halaman,
		t.nama AS tahun,
		b.kategori AS kategori,
		l.nama AS lemari,
		b.jumlah_buku AS jumlah_buku,
		b.tersedia AS tersedia,
		b.rating AS rating
		FROM bibliografi b
		INNER JOIN tahun t ON b.id_tahun = t.id
		INNER JOIN penerbit p ON b.id_penerbit = p.id
		INNER JOIN lemari l ON b.id_lemari = l.id
		ORDER BY b.judul';

		$buku = $this->m_perpus->sqlget($query);
		$data['buku'] = $buku;
		$this->load->view('admin/laporan/print_buku', $data);
	}

	function katalogSemua() {
		if(isset($_GET['donlot'])) {
			if($_GET['donlot'] == 'iya') {
				header("Content-type: application/vnd-ms-excel");
				header("Content-disposition: attachment; filename=Laporan transaksi perpustakaan.xls");
			}
		}

		$query = 'SELECT
		b.judul AS judul,
		k.kode_katalog AS kode_katalog
		FROM katalog k
		INNER JOIN bibliografi b ON b.id = k.id_buku
		ORDER BY k.kode_katalog';

		$buku = $this->m_perpus->sqlget($query);
		$data['buku'] = $buku;
		$this->load->view('admin/laporan/print_katalog', $data);
	}

	function siswa() {
		$pesan = '';
		if (isset($_GET['pesan'])) {
			$pesan = $_GET['pesan'];
		}
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$kelas = $this->m_perpus->sqlget('select * from kelas order by nama');
		$datahalaman = [
			'judul_halaman' => 'Laporan Siswa',
			'halaman' => 'laporanSiswa',
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'pesan'=> $pesan,
			'kelas'=> $kelas,
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/laporan/siswa');
		$this->load->view('admin/footer.php');
	}

	function siswaP() {
		header("Content-type: application/vnd-ms-excel");
		header("Content-disposition: attachment; filename=Laporan transaksi perpustakaan.xls");

		$kategori = $_POST['kategori'];
		$kelas = $_POST['kelas'];
		$jk = $_POST['jk'];
		$sort = $_POST['sort'];

		$query = 'SELECT
		s.nis AS nis,
		s.nama AS nama,
		s.jenis_kelamin AS jk,
		s.status AS status,
		k.nama AS kelas
		FROM siswa s
		INNER JOIN kelas k ON s.id_kelas = k.id';

		if($kategori == 'semua') {
			$query .= ' WHERE (status = "y" OR status = "n")';
		} else if($kategori == 'lulus') {
			$query .= ' WHERE status = "x"';
		} else if($kategori == 'aktif') {
			$query .= ' WHERE status = "y"';
		} else if($kategori == 'tidakAktif') {
			$query .= ' WHERE status = "n"';
		}

		if($kelas != 'semua') {
			$query .= ' and id_kelas = '.$kelas;
		}

		if($jk != 'semua') {
			$query .= ' and jenis_kelamin = "'.$jk.'"';
		}

		if($sort == 'nis') {
			$query .= ' ORDER BY nis';
		} else if($sort == 'nama') {
			$query .= ' ORDER BY nama';
		} if($sort == 'kelas') {
			$query .= ' ORDER BY kelas';
		}

		$siswa = $this->m_perpus->sqlget($query);
		for($i = 0; $i < count($siswa); $i++) {
			$siswa[$i]->meminjam_buku = $this->m_perpus->jmlDataDgnKondisi('transaksi', ['nis_siswa' => $siswa[$i]->nis]);
		}
		$data['siswa'] = $siswa;
		$this->load->view('admin/laporan/print_siswa', $data);
	}

	function bebasPerpus() {
		$pesan = '';
		if (isset($_GET['pesan'])) {
			$pesan = $_GET['pesan'];
		}
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$kelas = $this->m_perpus->sqlget('SELECT * FROM kelas ORDER BY nama');
		$datahalaman = [
			'judul_halaman' => 'Laporan Bebas Perpustakaan',
			'halaman' => 'bebasPerpus',
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'pesan'=> $pesan,
			'kelas' => $kelas,
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/laporan/bebasPerpus');
		$this->load->view('admin/footer.php');
	}

	function cekBebasPerpus() {
		$id = $_GET['id'];
		$siswa = $this->m_perpus->sqlget('SELECT * FROM siswa WHERE id_kelas='.$id.' and status != "x" ORDER BY nis');
		foreach($siswa as $s) {
			$s->yangSedangDipinjam = count($this->m_perpus->sqlget('select *  from transaksi where nis_siswa='.$s->nis.' and kembali=0'));
		}

		$data['siswa'] = $siswa;
		$this->load->view('admin/laporan/cekBebasPerpus', $data);
	}

	function printSurat() {
		$s = $this->m_perpus->getsatu('siswa', ['id' => $_GET['id']]);
		$perpus = $this->m_perpus->getsatuTanpaKondisi('pengaturan');

		//tgl print
		date_default_timezone_set('Asia/Jakarta');
		$tgl = date('d');
		$blnAngka = date('m');
		$bln = '';
		if($blnAngka == 1) {
			$bln = 'Januari';
		} else if($blnAngka == 2) {
			$bln = 'Februari';
		} else if($blnAngka == 3) {
			$bln = 'Maret';
		} else if($blnAngka == 4) {
			$bln = 'April';
		} else if($blnAngka == 5) {
			$bln = 'Mei';
		} else if($blnAngka == 6) {
			$bln = 'Juni';
		} else if($blnAngka == 7) {
			$bln = 'Juli';
		} else if($blnAngka == 8) {
			$bln = 'Agustus';
		} else if($blnAngka == 9) {
			$bln = 'September';
		} else if($blnAngka == 10) {
			$bln = 'Oktober';
		} else if($blnAngka == 11) {
			$bln = 'November';
		} else if($blnAngka == 12) {
			$bln = 'Desember';
		}
		$tahun = date('Y');

		// buat logo
		$this->load->library('pdfgenerator');
		$path = base_url().'gambar/'.$perpus->logo;
		$type = pathinfo($path, PATHINFO_EXTENSION);
		$data = file_get_contents($path);
		$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

		$html = '
			<style type="text/css">
			body {
				margin: 10px;
				font-family: sans;
			}
			</style>
			<img src="'.$base64.'" style="position: absolute; height: 80px;">
			<div style="text-align: center;">
				<h3 style="margin: 5px">PERPUSTAKAAN</h3>
				<h3 style="margin: 5px">'.$perpus->nama_perpus.'</h3>
				<span>'.$perpus->alamat.'</span>
				<hr>
				<br>
				<h3>SURAT KETERANGAN<br>BEBAS PERPUSTAKAAN</h3>
				<br>
				<br>
			</div>
			<p>Yang bertanda tangan dibawah ini, '.$perpus->jabatan_penanda_tangan.' '.$perpus->nama_perpus.', menerangkan bahwa:</p>
			<br>
			<div>
			<table>
				<tr>
					<td>NIS</td>
					<td>: '.$s->nis.'</td>
				</tr>
				<tr>
					<td style="padding-right: 20px;">Nama Lengkap</td>
					<td>: '.$s->nama.'</td>
				</tr>
			</table>
			</div>
			<br>
			<p>Siswa tersebut tidak memiliki pinjaman koleksi milik Perpustakaan '.$perpus->nama_perpus.'.</p>
			<p>Demikian surat ini dibuat untuk dipergunakan sebagaimana mestinya.</p>
			<br><br><br><br>
			<div style="position: absolute; right: 0;">
			<span>'.$perpus->kota.', '.$tgl.' '.$bln.' '.$tahun.'
			<br>
			'.$perpus->jabatan_penanda_tangan.'
			<br>
			'.$perpus->nama_perpus.'
			<br>
			<br>
			<br>
			<br>
			<br>
			<b>'.$perpus->nama_penanda_tangan.'</b><br>
			NIP: '.$perpus->nip_penanda_tangan.'
			</span>
			</div>
		';
		$this->pdfgenerator->generate($html, $s->nis, 'A4', 'portrait');
	}
}
