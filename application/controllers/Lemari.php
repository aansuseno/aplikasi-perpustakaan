 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lemari extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('role') != 'admin' && $this->session->userdata('role') != 'pustakawan') {
			redirect(base_url().'login/admin');
		}
	}

	public function tambah()
	{
		$pesan = '';
		if (isset($_GET['pesan'])) {
			$pesan = $_GET['pesan'];
		}
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'judul_halaman' => 'Tambah lemari',
			'halaman' => 'tambahLemari',
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'pesan'=> $pesan,
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/lemari/tambah');
		$this->load->view('admin/footer.php');
	}

	public  function tambahProses()
	{
		$nama = $_POST['nama_lemari'];
		$deskripsi = $_POST['deskripsi'];

		$input = array(
			'nama' => $nama,
			'deskripsi' => $deskripsi,
		);
		$this->m_perpus->input('lemari', $input);
		redirect(base_url().'lemari/tambah?pesan=berhasil');
	}

	public function tampil() {
		$pesan = '';
		if(isset($_GET['pesan'])) {
			$pesan = $_GET['pesan'];
		}
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'judul_halaman' => 'Daftar Lemari',
			'halaman' => 'daftarLemari',
			'pesan' => $pesan,
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'lemari' => $this->m_perpus->get('lemari'),
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/lemari/tampil');
		$this->load->view('admin/footer.php');
	}

	public function detail() {
		$pesan = '';
		if(isset($_GET['pesan'])) {
			$pesan = $_GET['pesan'];
		}
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'judul_halaman' => 'Detail lemari',
			'halaman' => 'detailLemari',
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'pesan' => $pesan,
			'lemari' => $this->m_perpus->getsatu('lemari', ['id'=> $_GET['id']])
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/lemari/detail');
		$this->load->view('admin/footer.php');
	}

	public function hapus() {
		$lemariTerkait = count($this->m_perpus->sqlget('select * from bibliografi where id_lemari = '.$_GET['id']));
		if($lemariTerkait == 0) {
			$this->m_perpus->hapus('lemari', ['id' => $_GET['id']]);
			redirect(base_url().'lemari/tampil?pesan=berhasilHapus');
		} else {
			redirect(base_url().'lemari/tampil?pesan=gagalHapus');
		}
	}

	function editProses() {
		$nama = $_POST['nama-lemari'];
		$deskripsi = $_POST['deskripsi-lemari'];

		$input = array(
			'nama' => $nama,
			'deskripsi' => $deskripsi,
		);
		$this->m_perpus->update('lemari', $input, ['id' => $_POST['id']]);
		redirect(base_url().'lemari/detail?pesan=berhasilEdit&id='.$_POST['id']);
	}
}
