<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct() {
		parent::__construct();
		if ($this->session->userdata('login')) {
			if ($this->session->userdata('login') == 'user') {
				redirect(base_url('user'));
			} else if($this->session->userdata('login') == 'admin') {
				redirect(base_url('admin'));
			}
		}
	}

	function index() {
		$pesan = '';
		if (isset($_GET['pesan'])) {
			$pesan = $_GET['pesan'];
		}
		$data = [
			'pesan' =>$pesan,
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
		];
		$this->load->view('siswa/login', $data);
	}

	function siswaProses() {
		$nis = $_POST['nis'];
		$pw = $_POST['pw'];

		$kondisi = [
			'nis' => $nis,
			'password' => md5($pw),
			'status' => 'y',
		];
		if ($this->m_perpus->jmlDataDgnKondisi('siswa', $kondisi) < 1) {
			redirect(base_url().'login?pesan=Password atau NIS salah.');
		} else {
			$data['perpus'] = $this->m_perpus->getsatuTanpaKondisi('pengaturan');
			$jmlPengunjung = $data['perpus']->jmlPengunjung + 1;
			if ($this->session->userdata('berkunjung') == true) {} else {
				$this->m_perpus->update('pengaturan', ['jmlPengunjung' => $jmlPengunjung], ['id' => $data['perpus']->id]);
			}
			$user = $this->m_perpus->getsatu('siswa', $kondisi);
			$session = [
				'id' => $user->id,
				'status' => 'loginSiswa',
				'berkunjung' => true,
				'login' => 'user',
			];
			$this->session->set_userdata($session);
			//update aktivitas
			date_default_timezone_set('Asia/Jakarta');
			$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Login'], ['id' => $this->session->userdata('id')]);
			redirect(base_url('user'));
		}
	}

	function admin() {
		$pesan = '';
		if (isset($_GET['pesan'])) {
			$pesan = $_GET['pesan'];
		}
		$data = [
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'pesan' => $pesan,
		];
		$this->load->view('loginAdmin', $data);
	}

	function adminProses() {
		$username = $_POST['username'];
		$pass = $_POST['pass'];

		$kondisi = [
			'username' => $username,
			'password' => md5($pass),
			'status' => 'y',
		];

		if ($this->m_perpus->jmlDataDgnKondisi('admin', $kondisi) >= 1) {
			$user = $this->m_perpus->getsatu('admin', $kondisi);
			$session = [
				'id' => $user->id,
				'role' => $user->wewenang,
			];
			$this->session->set_userdata($session);
			redirect(base_url().'admin');
		} else {
			redirect(base_url().'login/admin?pesan=salah');
		}


	}
}
