<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penerbit extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('role') != 'admin' && $this->session->userdata('role') != 'pustakawan') {
			redirect(base_url().'login/admin');
		}
	}

	public function index(){
		$query = 'SELECT * from penerbit order by nama';
		$total = $this->m_perpus->jmlDataDgnQuery($query);
		$jmlDataDiambil = 15;
		$jml_halaman = 0;
		$halaman = 0;
		$mulai = $jmlDataDiambil * $halaman;
		if ($total % $jmlDataDiambil == 0) {
			$jml_halaman = $total / $jmlDataDiambil;
		} else {
			$jml_halaman = $total / $jmlDataDiambil + 1;
		}
		$jml_halaman = (int)$jml_halaman;
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'judul_halaman' => 'Daftar Penerbit',
			'halaman' => 'daftarPenerbit',
			'jml_halaman' => $jml_halaman,
			'jmlData' => $jmlDataDiambil,
			'total' => $total,
			'halamanSekarang' => $halaman,
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'penerbit' => $this->m_perpus->sqlget($query.' LIMIT '.$mulai.', '.$jmlDataDiambil),
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/penerbit/tampil');
		$this->load->view('admin/footer.php');
	}

	public function tambah(){
      $this->load->view('admin/penerbit/tambah');
	}

	public function prosesTambahPenerbit(){
		$data=[
			'nama'=>$_GET['nama'],
			'deskripsi'=>$_GET['deskripsi']
		];
		$this->m_perpus->input('penerbit',$data);
		redirect(base_url('penerbit/list?pesan=berhasilTambah'));
	}

	public function proseshapus(){
		$penerbitTerkait = count($this->m_perpus->sqlget('select * from bibliografi where id_penerbit = '.$_GET['id']));
		if($penerbitTerkait == 0) {
			$this->m_perpus->hapus('penerbit', ['id'=>$_GET['id']]);
			redirect(base_url('penerbit/list?pesan=berhasilHapus'));
		} else {
			redirect(base_url('penerbit/list?pesan=gagalHapus'));
		}
	}

	public function hapus(){
		$data=[
			'nama'=>$_GET['nama'],
			'id'=>$_GET['id']
		];
		$this->load->view('admin/penerbit/hapus', $data);
	}

	public function info(){
		$data = [
			'penerbit1' => $this->m_perpus->getsatu('penerbit', ['id' =>$_GET['id']]),
		];
		$this->load->view('admin/penerbit/info', $data);
	}

	public function edit(){
		$data = [
			'penerbit1' => $this->m_perpus->getsatu('penerbit', ['id' =>$_GET['id']]),
		];
		$this->load->view('admin/penerbit/edit', $data);
	}

	public function editproses(){
		$dataedit=[
			'nama'=>$_GET['nama'],
			'deskripsi'=>$_GET['deskripsi']
		];
		$this->m_perpus->update('penerbit', $dataedit,['id'=>$_GET['id']]);
		redirect(base_url('penerbit/list?pesan=berhasilEdit'));
	}

	function list() {
		$query = 'SELECT * from penerbit order by nama';
		$total = $this->m_perpus->jmlDataDgnQuery($query);
		$jmlDataDiambil = 15;
		$jml_halaman = 0;
		$halaman = 0;
		$pesan = '';
		if (isset($_GET['pesan'])) {
			$pesan =$_GET['pesan'];
		}
		if (isset($_GET['halaman'])) {
			$halaman =$_GET['halaman'];
		}
		$mulai = $jmlDataDiambil * $halaman;

		if ($total % $jmlDataDiambil == 0) {
			$jml_halaman = $total / $jmlDataDiambil;
		} else {
			$jml_halaman = $total / $jmlDataDiambil + 1;
		}
		$jml_halaman = (int)$jml_halaman;
		$data = [
			'penerbit' => $this->m_perpus->sqlget($query.' LIMIT '.$mulai.', '.$jmlDataDiambil),
			'jmlData' => $jmlDataDiambil,
			'total' => $total,
			'pesan' => $pesan,
			'jml_halaman' => $jml_halaman,
			'halamanSekarang' => $halaman,
		];
		$this->load->view('admin/penerbit/listpenerbit', $data);
	}

	function cari() {
		$c = '';
		if (isset($_GET['cari'])) {
			$c = $_GET['cari'];
		}
		if (strlen($c) <= 2) {
			echo "Pencarian gagal. Silakan masukkan pencarian yang lebih spesifik.";
		} else {
			$query = 'SELECT * from penerbit WHERE nama LIKE "%'.$c.'%" or deskripsi LIKE "%'.$c.'%" order by nama';
			$total = $this->m_perpus->jmlDataDgnQuery($query);
			$jmlDataDiambil = 15;
			$jml_halaman = 0;
			$halaman = 0;
			if (isset($_GET['halaman'])) {
				$halaman =$_GET['halaman'];
			}
			$mulai = $jmlDataDiambil * $halaman;


			if ($total % $jmlDataDiambil == 0) {
				$jml_halaman = $total / $jmlDataDiambil;
			} else {
				$jml_halaman = $total / $jmlDataDiambil + 1;
			}
			$jml_halaman = (int)$jml_halaman;
			$data = [
				'penerbit' => $this->m_perpus->sqlget($query.' LIMIT '.$mulai.', '.$jmlDataDiambil),
				'jmlData' => $jmlDataDiambil,
				'cari' => $c,
				'total' => $total,
				'jml_halaman' => $jml_halaman,
				'halamanSekarang' => $halaman,
			];
			$this->load->view('admin/penerbit/hasilCari.php', $data);
		}
	}

	function informasi() {
		$this->load->view('admin/penerbit/informasi');
	}
}
