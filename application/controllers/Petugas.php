<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Petugas extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('role') != 'admin' && $this->session->userdata('role') != 'pustakawan') {
			redirect(base_url().'login/admin');
		}
	}

	public function baru(){
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'judul_halaman' => 'Daftar Petugas',
			'halaman' => 'daftarPetugas',
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'admin' => $this->m_perpus->getkondisi('admin', ['status' => 'n']),
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/petugas/baru');
		$this->load->view('admin/footer.php');
	}

	public function detail() {
		$pesan = '';
		if (isset($_GET['pesan'])) {
			$pesan = "Berhasil";
		}
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'judul_halaman' => 'Detail Petugas',
			'halaman' => 'detailPetugas',
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'petugas' => $this->m_perpus->getsatu('admin', ['id' => $_GET['id']]),
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/petugas/detail');
		$this->load->view('admin/footer.php');
	}

	public function terima() {
		$this->m_perpus->update('admin', ['status' => 'y', 'dikonfirmasi_oleh' => $this->session->userdata('id')], ['id' => $_GET['id']]);
		$petugas = $this->m_perpus->getsatu('admin', ['id' => $_GET['id']]);
		redirect('https://api.whatsapp.com/send?phone='.$petugas->no_wa.'&text=*Selamat%20*%0AAnda%20sekarang%20sudah%20dikonfirmasi%20sebagai%20pengurus%20aplikasi%20perpustakaan.%0A%0ALink%20login%20bisa%20diakses%20melalui%20'.base_url().'admin'.'%0A%0A*Admin*');
	}

	function hapusAdminPradaftar() {
		$this->m_perpus->hapus('admin', ['id' => $_GET['id']]);
	}

	function pradaftar() {
		$data = [
			'admin' => $this->m_perpus->getkondisi('admin', ['status' => 'n']),
		];
		$this->load->view('admin/petugas/tampilDataAdminBelumDiKonfirmasi', $data);
	}

	// halaman petugas yang sudah disetujui
	function aktif() {
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'judul_halaman' => 'Daftar Petugas Aktif',
			'halaman' => 'aktifPetugas',
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'id_user' => $data_user->id,
			'admin' => $this->m_perpus->getkondisi('admin', ['status' => 'y']),
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/petugas/aktif');
		$this->load->view('admin/footer.php');
	}

	function infoAdmin() {
		$dataAdmin = $this->m_perpus->getsatu('admin', ['id' => $_GET['id']]);
		$data = [
			'admin' => $dataAdmin,
			'disetujui' => $this->m_perpus->getsatu('admin', ['id' => $dataAdmin->dikonfirmasi_oleh]),
		];
		$this->load->view('admin/petugas/infoadmin', $data);
	}

	function bekukan() {
		$this->m_perpus->update('admin', ['status' => 'b', 'dibekukan_oleh' => $this->session->userdata('id')], ['id' => $_GET['id']]);
		redirect(base_url().'petugas/sudahAktif');
	}

	function sudahAktif() {
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$data = [
			'nama_user' => $data_user,
			'admin' => $this->m_perpus->getkondisi('admin', ['status' => 'y']),
			'id_user' => $data_user->id,
		];
		$this->load->view('admin/petugas/tampilDataAdminSudahDiKonfirmasi', $data);
	}

	// petugas dibekukan
	function beku() {
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'judul_halaman' => 'Daftar Petugas Dibekukan',
			'halaman' => 'bekuPetugas',
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'id_user' => $data_user->id,
			'admin' => $this->m_perpus->getkondisi('admin', ['status' => 'b']),
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/petugas/beku');
		$this->load->view('admin/footer.php');
	}

	function buka_beku() {
		$this->m_perpus->update('admin', ['status' => 'y', 'dibekukan_oleh' => $this->session->userdata('id')], ['id' => $_GET['id']]);
		redirect(base_url().'petugas/bukaBeku');
	}

	function bukaBeku() {
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$data = [
			'nama_user' => $data_user,
			'admin' => $this->m_perpus->getkondisi('admin', ['status' => 'b']),
			'id_user' => $data_user->id,
		];
		$this->load->view('admin/petugas/tampilDataAdminSudahDiBukaBeku', $data);
	}
}
