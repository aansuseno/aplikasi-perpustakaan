<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('role') != 'admin' && $this->session->userdata('role') != 'pustakawan') {
			redirect(base_url().'login/admin');
		}
	}

	function tambah() {
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'judul_halaman' => 'Tambah Siswa',
			'halaman' => 'tambah_siswa',
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/siswa/tambah');
		$this->load->view('admin/footer.php');
	}

	function tampilTambah() {
		$pesan = '';
		if (isset($_GET['pesan'])) {
			$pesan = $_GET['pesan'];
		}
		$data = [
			'kelas' => $this->m_perpus->get('kelas'),
			'pesan' => $pesan,
		];
		$this->load->view('admin/siswa/tambahSiswa', $data);
	}

	function tampilTambahKelas() {
		$this->load->view('admin/siswa/tambahKelas');
	}

	function tambahKelasProses() {
		$nama = $_GET['nama'];
		$deskripsi = $_GET['deskripsi'];
		$input = [
			'nama' => $nama,
			'deskripsi' => $deskripsi,
		];
		$this->m_perpus->input('kelas', $input);
		redirect(base_url().'siswa/tampilTambah');
	}

	function tambahSiswaProses() {
		$jmlsiswa = $this->m_perpus->jmlDataDgnKondisi('siswa', ['nis' => $_GET['nis']]);
		if($jmlsiswa == 0) {
			$input = [
				'nama' => $_GET['nama'],
				'nis' => $_GET['nis'],
				'jenis_kelamin' => $_GET['jns_kelamin'],
				'id_kelas' => $_GET['kelas'],
				'status' => 'n'
			];
			$this->m_perpus->input('siswa', $input);
			redirect(base_url().'siswa/tambahJumlahSiswaKelas?id='.$_GET['kelas'].'&pesan=tampil');
		} else {
			if ($_GET['pesan'] == 'tampil') {
				redirect(base_url().'siswa/tampilSiswaKelas?id='.$_GET['kelas'].'&pesan=nisAda');
			} else {
				redirect(base_url().'siswa/tampilTambah?pesan=nisAda');
			}
		}
	}

	function tampil() {
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'judul_halaman' => 'Tampil Siswa',
			'halaman' => 'tampilsiswa',
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'kelas' => $this->m_perpus->get('kelas'),
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/siswa/tampil');
		$this->load->view('admin/footer.php');
	}

	function tambahJumlahSiswaKelas() {
		$updatekelas = [
			'jml_siswa' => $this->m_perpus->jmlDataDgnKondisi('siswa', ['id_kelas' => $_GET['id'], 'status !=' => 'x']),
		];
		$this->m_perpus->update('kelas', $updatekelas, ['id' => $_GET['id']]);
		if ($_GET['pesan'] == 'tampil') {
			redirect(base_url().'siswa/tampilSiswaKelas?id='.$_GET['id'].'&pesan=berhasilTambah');
		} else {
			redirect(base_url().'siswa/tampilTambah?pesan=berhasil');
		}
	}

	function tampilKelas() {
//		jml siswas
		$jmlAktif;
		$kelas= $this->m_perpus->sqlget('select * from kelas order by nama');
		for($i = 0; $i < count($kelas); $i++) {
			$jmlAktif[$i] = count($this->m_perpus->sqlget('select * from siswa where status = "y" and id_kelas = '.$kelas[$i]->id));
		}

		$data = [
			'kelas' => $kelas,
			'jmlAktif' => $jmlAktif,
		];
		$this->load->view('admin/siswa/tampilKelas', $data);
	}

	function tampilSiswaKelas() {
		$idkelas = $_GET['id'];
		$query = 'SELECT * from siswa WHERE id_kelas = '.$idkelas.' and status != "x"';
		$total = $this->m_perpus->jmlDataDgnQuery($query);
		$jmlDataDiambil = 15;
		$jml_halaman = 0;
		$halaman = 0;
		$pesan = '';
		if (isset($_GET['pesan'])) {
			$pesan =$_GET['pesan'];
		}
		if (isset($_GET['halaman'])) {
			$halaman =$_GET['halaman'];
		}
		$mulai = $jmlDataDiambil * $halaman;


		if ($total % $jmlDataDiambil == 0) {
			$jml_halaman = $total / $jmlDataDiambil;
		} else {
			$jml_halaman = $total / $jmlDataDiambil + 1;
		}
		$jml_halaman = (int)$jml_halaman;
		$data = [
			'siswa' => $this->m_perpus->sqlget($query.' LIMIT '.$mulai.', '.$jmlDataDiambil),
			'jmlData' => $jmlDataDiambil,
			'total' => $total,
			'pesan' => $pesan,
			'jml_halaman' => $jml_halaman,
			'halamanSekarang' => $halaman,
			'kelas' => $this->m_perpus->get('kelas'),
			'idkelas' => $idkelas,
			'namakelassiswa' => $this->m_perpus->getsatu('kelas', ['id' => $idkelas]),
		];
		$this->load->view('admin/siswa/tampilSiswaPerKelas', $data);
	}

	function tambahKelasProsesTampilSIswa() {
		$nama = $_GET['nama'];
		$deskripsi = $_GET['deskripsi'];
		$input = [
			'nama' => $nama,
			'deskripsi' => $deskripsi,
		];
		$this->m_perpus->input('kelas', $input);
		redirect(base_url().'siswa/tampilKelas');
	}

	function hapusSiswaProses() {
		$siswa = $this->m_perpus->getsatu('siswa', ['id' => $_GET['siswa']]);
		$meminjamkah = $this->m_perpus->jmlDataDgnKondisi('transaksi', ['nis_siswa' => $siswa->nis]);
		if($meminjamkah == 0) {
			$this->m_perpus->hapus('siswa', ['id' => $_GET['siswa']]);
			redirect(base_url().'siswa/kurangJmlSiswa?id='.$_GET['kelas']);
		} else {
			redirect(base_url().'siswa/tampilSiswaKelas?id='.$_GET['kelas'].'&pesan=gagalHapus');
		}
	}

	function kurangJmlSiswa() {
		$updatekelas = [
			'jml_siswa' => $this->m_perpus->jmlDataDgnKondisi('siswa', ['id_kelas' => $_GET['id'], 'status !=' => 'x']),
		];
		$this->m_perpus->update('kelas', $updatekelas, ['id' => $_GET['id']]);
		redirect(base_url().'siswa/tampilSiswaKelas?id='.$_GET['id'].'&pesan=berhasilHapus');
	}

	function editSiswaProses() {
		$update = [
			'nama' => $_GET['nama'],
			'nis' => $_GET['nis'],
			'jenis_kelamin' => $_GET['jk'],
			'id_kelas' => $_GET['kelas'],
		];
		$this->m_perpus->update('siswa', $update, ['id' => $_GET['id']]);
		redirect(base_url().'siswa/tampilSiswaKelas?id='.$_GET['kelas'].'&pesan=berhasilEdit');
	}

	function prosesNaikKelas() {
		if($_GET['ke'] == 'x') {
			$this->m_perpus->update('siswa', ['status' => 'x'], ['id_kelas' => $_GET['dari'], 'status !=' => 'x']);
			$updatekelas = [
				'jml_siswa' => $this->m_perpus->jmlDataDgnKondisi('siswa', ['id_kelas' => $_GET['dari'], 'status !=' => 'x']),
			];
			$this->m_perpus->update('kelas', $updatekelas, ['id' => $_GET['dari']]);
			redirect(base_url().'siswa/tampilSiswaKelas?id='.$_GET['dari'].'&pesan=berhasilLulus');
		} else {
			$update = [
				'id_kelas' => $_GET['ke'],
			];
			$this->m_perpus->update('siswa', $update, ['id_kelas' => $_GET['dari'], 'status !=' => 'x']);
			redirect(base_url().'siswa/naikKelasJmlSiswa?ke='.$_GET['ke'].'&dari='.$_GET['dari']);
		}
	}

	function naikKelasJmlSiswa() {
		$updatekelas = [
			'jml_siswa' => $this->m_perpus->jmlDataDgnKondisi('siswa', ['id_kelas' => $_GET['dari'], 'status !=' => 'x']),
		];
		$this->m_perpus->update('kelas', $updatekelas, ['id' => $_GET['dari']]);
		redirect(base_url().'siswa/naikKelasJmlSiswa2?ke='.$_GET['ke'].'&dari='.$_GET['dari']);
	}

	function naikKelasJmlSiswa2() {
		$updatekelas = [
			'jml_siswa' => $this->m_perpus->jmlDataDgnKondisi('siswa', ['id_kelas' => $_GET['ke'], 'status !=' => 'x']),
		];
		$this->m_perpus->update('kelas', $updatekelas, ['id' => $_GET['ke']]);
		redirect(base_url().'siswa/tampilSiswaKelas?pesan=berhasilNaik&id='.$_GET['ke']);
	}

	function cari() {
		$c = '';
		if (isset($_GET['cari'])) {
			$c = $_GET['cari'];
		}
		if (strlen($c) <= 0) {
			echo "Pencarian gagal. Silakan masukkan pencarian yang lebih spesifik.";
		} else {
			$query = 'SELECT s.nama as nama, s.id as id, s.nis as nis, k.nama as nama_kelas, s.id_kelas as id_kelas, s.jenis_kelamin as jenis_kelamin, s.status as status, s.no_wa as no_wa
				from siswa s
				INNER JOIN kelas k on s.id_kelas=k.id
				WHERE (s.nama LIKE "%'.$c.'%" or s.nis = '.$c.') and status != "x" order by nis desc';
			$total = $this->m_perpus->jmlDataDgnQuery($query);
			$jmlDataDiambil = 15;
			$jml_halaman = 0;
			$halaman = 0;
			if (isset($_GET['halaman'])) {
				$halaman =$_GET['halaman'];
			}
			$mulai = $jmlDataDiambil * $halaman;


			if ($total % $jmlDataDiambil == 0) {
				$jml_halaman = $total / $jmlDataDiambil;
			} else {
				$jml_halaman = $total / $jmlDataDiambil + 1;
			}
			$jml_halaman = (int)$jml_halaman;
			$data = [
				'siswa' => $this->m_perpus->sqlget($query.' LIMIT '.$mulai.', '.$jmlDataDiambil),
				'jmlData' => $jmlDataDiambil,
				'cari' => $c,
				'total' => $total,
				'jml_halaman' => $jml_halaman,
				'halamanSekarang' => $halaman,
			];
			$this->load->view('admin/siswa/hasilCari.php', $data);
		}
	}

	function arsipkan() {
		$siswa = $this->m_perpus->getsatu('siswa', ['id' => $_GET['id']]);
		$meminjamkah = $this->m_perpus->jmlDataDgnKondisi('transaksi', ['nis_siswa' => $siswa->nis, 'kembali' => 0]);
		if($meminjamkah > 0) {
			redirect(base_url().'siswa/tampilSiswaKelas?pesan=gagalArsip&id='.$siswa->id_kelas);
		} else {
			// update siswa
			$this->m_perpus->update('siswa', ['status' => 'x'], ['id' => $_GET['id']]);

			// update jumlah siswa
			$updatekelas = [
				'jml_siswa' => $this->m_perpus->jmlDataDgnKondisi('siswa', ['id_kelas' => $siswa->id_kelas, 'status !=' => 'x']),
			];
			$this->m_perpus->update('kelas', $updatekelas, ['id' => $siswa->id_kelas]);
			redirect(base_url().'siswa/tampilSiswaKelas?pesan=berhasilArsip&id='.$siswa->id_kelas);
		}
	}

	function reset() {
		$siswa = $this->m_perpus->getsatu('siswa', ['id' => $_GET['id']]);
		$this->m_perpus->update('siswa', ['status' => 'n', 'password' => ''], ['id' => $_GET['id']]);
		redirect(base_url().'siswa/tampilSiswaKelas?pesan=berhasilReset&id='.$siswa->id_kelas);
	}
}
