<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tahun extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('role') != 'admin' && $this->session->userdata('role') != 'pustakawan') {
			redirect(base_url().'login/admin');
		}
	}


	public function index() {
		$query = 'SELECT * from tahun order by nama desc';
		$total = $this->m_perpus->jmlDataDgnQuery($query);
		$jmlDataDiambil = 15;
		$jml_halaman = 0;
		$halaman = 0;
		$mulai = $jmlDataDiambil * $halaman;
		if ($total % $jmlDataDiambil == 0) {
			$jml_halaman = $total / $jmlDataDiambil;
		} else {
			$jml_halaman = $total / $jmlDataDiambil + 1;
		}
		$jml_halaman = (int)$jml_halaman;
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'judul_halaman' => 'tahun',
			'halaman' => 'tahun',
			'jml_halaman' => $jml_halaman,
			'jmlData' => $jmlDataDiambil,
			'total' => $total,
			'halamanSekarang' => $halaman,
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'tahun' => $this->m_perpus->sqlget($query.' LIMIT '.$mulai.', '.$jmlDataDiambil),
		];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/tahun/tampil');
		$this->load->view('admin/footer.php');
	}

	public function tambah() {
		$this->load->view('admin/tahun/tambah');
	}

	public function prosesTambahTahun() {
 		$data=[
 			'nama'=>$_GET['nama'],
 			'deskripsi'=>$_GET['deskripsi']
 		];
 		$this->m_perpus->input('tahun',$data);
 		redirect(base_url('tahun/list?pesan=berhasilTambah&halaman=0'));
 	}

 	public function hapus(){
 		$data=[
 			'nama'=>$_GET['nama'],
 			'id'=>$_GET['id']
 		];
 		$this->load->view('admin/tahun/hapus', $data);
 	}

 	public function proseshapus(){
		$tahunTerkait = count($this->m_perpus->sqlget('select * from bibliografi where id_tahun = '.$_GET['id']));
		if($tahunTerkait == 0) {
	 		$this->m_perpus->hapus('tahun', ['id'=>$_GET['id']]);
	 		redirect(base_url('tahun/list?pesan=berhasilHapus&halaman=0'));
	 	} else {
	 		redirect(base_url('tahun/list?pesan=gagalHapus&halaman=0'));
	 	}
 	}

 	public function info(){
		$data = [
			'tahun1' => $this->m_perpus->getsatu('tahun', ['id' =>$_GET['id']]),
		];
		$this->load->view('admin/tahun/info', $data);
	}

	public function edit(){
		$data = [
			'tahun1' => $this->m_perpus->getsatu('tahun', ['id' =>$_GET['id']]),
		];
		$this->load->view('admin/tahun/edit', $data);
	}

	function list() {
		$query = 'SELECT * from tahun order by nama desc';
		$total = $this->m_perpus->jmlDataDgnQuery($query);
		$jmlDataDiambil = 15;
		$jml_halaman = 0;
		$halaman = 0;
		$pesan = '';
		if (isset($_GET['pesan'])) {
			$pesan =$_GET['pesan'];
		}
		if (isset($_GET['halaman'])) {
			$halaman =$_GET['halaman'];
		}
		$mulai = $jmlDataDiambil * $halaman;

		if ($total % $jmlDataDiambil == 0) {
			$jml_halaman = $total / $jmlDataDiambil;
		} else {
			$jml_halaman = $total / $jmlDataDiambil + 1;
		}
		$jml_halaman = (int)$jml_halaman;
		$data = [
			'tahun' => $this->m_perpus->sqlget($query.' LIMIT '.$mulai.', '.$jmlDataDiambil),
			'jmlData' => $jmlDataDiambil,
			'total' => $total,
			'pesan' => $pesan,
			'jml_halaman' => $jml_halaman,
			'halamanSekarang' => $halaman,
		];
		$this->load->view('admin/tahun/listtahun', $data);
	}

	public function editproses(){
		$dataedit=[
			'nama'=>$_GET['nama'],
			'deskripsi'=>$_GET['deskripsi']
		];
		$this->m_perpus->update('tahun', $dataedit,['id'=>$_GET['id']]);
		redirect(base_url('tahun/list?pesan=berhasilEdit&halaman=0'));
	}

	function informasi() {
		$this->load->view('admin/tahun/informasi');
	}
}
