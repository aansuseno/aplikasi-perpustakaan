<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('role') != 'admin' && $this->session->userdata('role') != 'pustakawan') {
			redirect(base_url().'login/admin');
		}
	}

	public function index()
	{
		$page = 'anu';
		$judul_halaman = 'Semua Transaksi';
		$halaman = 'transaksi';
		$transaksi = Transaksi::getSemuaTransaksi(0);
		if (isset($_GET['page'])) {
			if ($_GET['page'] == 'blmkembali') {
				$judul_halaman = 'Belum Dikembalikan';
				$halaman = 'belumkembali';
				$page = $_GET['page'];
				$transaksi = Transaksi::getTransaksiBlmKembali();
			}
		}
		$kondisi = [
			'id' => $this->session->userdata('id'),
		];
		$data_user = $this->m_perpus->getsatu('admin', $kondisi);
		$datahalaman = [
			'judul_halaman' => $judul_halaman,
			'halaman' => $halaman,
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'nama_user' => $data_user,
			'transaksi' => $transaksi,
			'page' => $page,
			];
		$this->load->view('admin/header.php', $datahalaman);
		$this->load->view('admin/transaksi/index');
		$this->load->view('admin/footer.php');
	}

	function getSemuaTransaksi($limit) {
		$tgl = new DateTime('now');
		$tgl->modify($limit.' month');
		$tgl = $tgl->format('Y-m-d');

		return $this->m_perpus->transaksi('t.id as id, t.kode_katalog as katalog, t.nis_siswa as nis, t.tgl_pinjam as pinjam, b.judul as judul, t.target_kembali as target_kembali, s.nama as nama, t.kembali as kembali, t.minta_tambah_lama_pinjam as minta_tambah_lama_pinjam', $tgl);
	}

	function getTransaksiBlmKembali() {
		$where = [
			'kembali' => 0
		];
		return $this->m_perpus->transaksiDgnKondisi('t.id as id, t.kode_katalog as katalog, t.nis_siswa as nis, t.tgl_pinjam as pinjam, b.judul as judul, t.target_kembali as target_kembali, s.nama as nama, t.kembali as kembali, t.minta_tambah_lama_pinjam as minta_tambah_lama_pinjam', $where);
	}

	function tambah() {
		$data = [
			'page' => $_GET['page'],
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
		];
		$this->load->view('admin/transaksi/tambah', $data);
	}

	function tambahProses() {
		// update katalog
		$this->m_perpus->update('katalog', ['status' => 'dipinjam'], ['kode_katalog' => $_GET['kode_katalog']]);

		// update bibliografi
		$katalog = $this->m_perpus->getsatu('katalog', ['kode_katalog' => $_GET['kode_katalog']]);
		$buku = $this->m_perpus->getsatu('bibliografi', ['id' => $katalog->id_buku]);
		$tersedia = $buku->tersedia - 1;
		$this->m_perpus->update('bibliografi', ['tersedia' => $tersedia], ['id' => $katalog->id_buku]);

		// insert ke transaksi
		$dataInput = [
			'kode_katalog' => $_GET['kode_katalog'],
			'nis_siswa' => $_GET['nis_siswa'],
			'id_petugas' => $this->session->userdata('id'),
			'tgl_pinjam' => date('Y-m-d'),
			'target_kembali' => $_GET['target_kembali'],
			'kembali' => 0
		];
		$this->m_perpus->input('transaksi', $dataInput);

		// tampil data
		redirect(base_url('transaksi/tampilListTransaksi?page='.$_GET['page'].'&pesan=berhasilTambah'));
	}

	function cekKodeKatalog() {
		$jml = $this->m_perpus->jmlDataDgnKondisi('katalog',['kode_katalog' => $_GET['kode'], 'status' => 'ada']);
		if ($jml > 0) {
			echo "Data tersedia.";
		} elseif ($jml == 0) {
			echo "Tidak valid";
		}
	}

	function cekNisSiswa() {
		$jml = $this->m_perpus->jmlDataDgnKondisi('siswa',['nis' => $_GET['nis']]);
		if ($jml > 0) {
			echo "Data tersedia.";
		} elseif ($jml == 0) {
			echo "Tidak valid";
		}
	}

	function hapus() {
		$data = [
			'id_transaksi_hapus' => $_GET['id'],
			'page' => $_GET['page'],
		];
		$this->load->view('admin/transaksi/hapus', $data);
	}

	function hapusproses() {
		$t = $this->m_perpus->getsatu('transaksi', ['id' => $_GET['id']]);

		// update katalog
		$this->m_perpus->update('katalog', ['status' => 'ada'], ['kode_katalog' => $t->kode_katalog]);

		// update bibliografi
		$katalog = $this->m_perpus->getsatu('katalog', ['kode_katalog' => $t->kode_katalog]);
		$buku = $this->m_perpus->getsatu('bibliografi', ['id' => $katalog->id_buku]);
		$tersedia = $buku->tersedia + 1;
		$this->m_perpus->update('bibliografi', ['tersedia' => $tersedia], ['id' => $katalog->id_buku]);

		// hapus transaksi
		$this->m_perpus->hapus('transaksi', ['id' => $_GET['id']]);

		// tampil data
		redirect(base_url('transaksi/tampilListTransaksi?page='.$_GET['page'].'&pesan=berhasilHapus'));
	}

	function tampilListTransaksi() {
		$bulan = 0;
		if(isset($_GET['bulan'])) {
			$bulan = $_GET['bulan'];
		}
		if ($_GET['page'] == 'blmkembali') {
			$transaksi = Transaksi::getTransaksiBlmKembali();
		} else {
			$transaksi = Transaksi::getSemuaTransaksi($bulan);
		}
		$pesan = '';
		if(isset($_GET['pesan'])) {
			$pesan = $_GET['pesan'];
		}
		$data = [
			'transaksi' => $transaksi,
			'page' => $_GET['page'],
			'pesan' => $pesan,
			'bulan' => $bulan,
		];
		$this->load->view('admin/transaksi/listtransaksi', $data);
	}

	function info() {
		$transaksi = $this->m_perpus->getsatu('transaksi', ['id' => $_GET['id']]);
		$katalog = $this->m_perpus->getsatu('katalog', ['kode_katalog' => $transaksi->kode_katalog]);
		$bibliografi = $this->m_perpus->getsatu('bibliografi', ['id' => $katalog->id_buku]);
		$lemari = $this->m_perpus->getsatu('lemari', ['id' => $bibliografi->id_lemari]);
		$petugas = $this->m_perpus->getsatu('admin', ['id' => $transaksi->id_petugas]);
		$siswa = $this->m_perpus->getsatu('siswa', ['nis' => $transaksi->nis_siswa]);
		$kelas = $this->m_perpus->getsatu('kelas', ['id' => $siswa->id_kelas]);

		$data = [
			'transaksi_info' => $transaksi,
			'katalog_info' => $katalog,
			'bibliografi_info' => $bibliografi,
			'lemari_info' => $lemari,
			'petugas_info' => $petugas,
			'siswa_info' => $siswa,
			'kelas_info' => $kelas,
		];
		$this->load->view('admin/transaksi/info', $data);
	}

	function kembali() {
		$transaksi = $this->m_perpus->getsatu('transaksi', ['id' => $_GET['id']]);
		if ($transaksi->target_kembali < date('Y-m-d')) {
			$data = [
				'transaksi_kembali_terlambat' => $transaksi,
				'page' => $_GET['page'],
				'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			];
			$this->load->view('admin/transaksi/kembaliTerlambat', $data);
		} else {
			$data = [
				'id_transaksi_kembali' => $_GET['id'],
				'page' => $_GET['page'],
			];
			$this->load->view('admin/transaksi/kembali', $data);
		}
	}

	function kembaliProses() {
		$transaksi = $this->m_perpus->getsatu('transaksi', ['id' => $_GET['id']]);

		// update katalog
		$this->m_perpus->update('katalog', ['status' => 'ada'], ['kode_katalog' => $transaksi->kode_katalog]);

		// update bibliografi
		$katalog = $this->m_perpus->getsatu('katalog', ['kode_katalog' => $transaksi->kode_katalog]);
		$buku = $this->m_perpus->getsatu('bibliografi', ['id' => $katalog->id_buku]);
		$tersedia = $buku->tersedia + 1;
		$this->m_perpus->update('bibliografi', ['tersedia' => $tersedia], ['id' => $katalog->id_buku]);

		// update transaksi
		$tepat_waktu = 1;
		if($transaksi->target_kembali < date('Y-m-d')) {
			$tepat_waktu = 0;
		}
		$updateTransaksi = [
			'kembali' => 1,
			'tgl_kembali' => date('Y-m-d'),
			'tepat_waktu' => $tepat_waktu,
		];
		$this->m_perpus->update('transaksi', $updateTransaksi, ['id' => $transaksi->id]);

		// tampil data
		redirect(base_url('transaksi/tampilListTransaksi?page=blmKembali&pesan=berhasilKembali'));
	}

	function informasiTransaksi() {
		$data = [
				'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			];
		$this->load->view('admin/transaksi/informasiTransaksi', $data);
	}

	function tambahLamaPeminjaman() {
		$data = [
			'transaksi' => $this->m_perpus->getsatu('transaksi', ['id' => $_GET['id']]),
			'page' => $_GET['page'],
		];
		$this->load->view('admin/transaksi/tambahLamaPeminjaman', $data);
	}

	function tambahLamaPeminjamanProses() {
		$updateTransaksi = [
			'target_kembali' => $_GET['target_kembali'],
			'minta_tambah_lama_pinjam' => 0,
		];
		$this->m_perpus->update('transaksi', $updateTransaksi, ['id' => $_GET['id']]);
		redirect(base_url('transaksi/tampilListTransaksi?page='.$_GET['page'].'&pesan=berhasilTambahLamaPeminjaman'));
	}
}
