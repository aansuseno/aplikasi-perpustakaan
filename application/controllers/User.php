<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct() {
		parent::__construct();
		if ($this->session->userdata('status') != 'loginSiswa') {
			redirect(base_url().'login');
		}
	}

	function index() {
		$data = [
			'nama_sekolah' => $this->m_perpus->getsatuTanpaKondisi('pengaturan')->nama_perpus,
			'siswa' => $this->m_perpus->getsatu('siswa', ['id' => $this->session->userdata('id')]),
			'urut_rating' => $this->m_perpus->sqlget('SELECT * from bibliografi ORDER BY rating  desc LIMIT 0, 5'),
			'urut_baru' => $this->m_perpus->sqlget('SELECT * from bibliografi ORDER BY id  desc LIMIT 0, 5'),
			'urut_terbit' => $this->m_perpus->sqlget('SELECT b.judul as judul, b.id as id, b.sampul as sampul from bibliografi b INNER JOIN tahun t ON b.id_tahun=t.id ORDER BY t.nama desc, b.id desc LIMIT 0, 5'),
		];
		$this->load->view('siswa/header', $data);
		$this->load->view('siswa/home', $data);
		$this->load->view('siswa/footer');
	}

	function beranda() {
		//update aktivitas
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Membuka beranda'], ['id' => $this->session->userdata('id')]);

		$data = [
			'urut_rating' => $this->m_perpus->sqlget('SELECT * from bibliografi ORDER BY rating  desc LIMIT 0, 5'),
			'urut_baru' => $this->m_perpus->sqlget('SELECT * from bibliografi ORDER BY id  desc LIMIT 0, 5'),
			'urut_terbit' => $this->m_perpus->sqlget('SELECT b.judul as judul, b.id as id, b.sampul as sampul from bibliografi b INNER JOIN tahun t ON b.id_tahun=t.id ORDER BY t.nama desc, b.id desc LIMIT 0, 5'),
		];
		$this->load->view('siswa/home', $data);
	}

	function cari() {
		//update aktivitas
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Membuka halaman cari'], ['id' => $this->session->userdata('id')]);

		$this->load->view('siswa/cari');
	}

	function detailBuku() {
		$b = $this->m_perpus->getsatu('bibliografi', ['id' => $_GET['id']]);
		$t = $this->m_perpus->getsatu('tahun', ['id' => $b->id_tahun]);
		$p = $this->m_perpus->getsatu('penerbit', ['id' => $b->id_penerbit]);
		$l = $this->m_perpus->getsatu('lemari', ['id' => $b->id_lemari]);
		$data = [
			'b' => $b,
			'l' => $l,
			'p' => $p,
			't' => $t,
		];
		//update aktivitas
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Melihat detail buku '.$b->judul], ['id' => $this->session->userdata('id')]);

		$this->load->view('siswa/detailBuku', $data);
	}

	function cariBuku()	{
		//update aktivitas
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Mencari buku'], ['id' => $this->session->userdata('id')]);

		$c = '';
		if (isset($_GET['cari'])) {
			$c = $_GET['cari'];
		}
		if (strlen($c) <= 2) {
			echo "Pencarian gagal. Silakan masukkan pencarian yang lebih spesifik. Seperti judul dan pengarang.";
		} else {
			$query = 'SELECT * from bibliografi WHERE judul LIKE "%'.$c.'%" or penulis LIKE "%'.$c.'%" or kategori LIKE "%'.$c.'%" ORDER BY rating  desc';
			$total = $this->m_perpus->jmlDataDgnQuery($query);
			$jmlDataDiambil = 10;
			$jml_halaman = 0;
			$halaman = 0;
			if (isset($_GET['halaman'])) {
				$halaman =$_GET['halaman'];
			}
			$mulai = $jmlDataDiambil * $halaman;


			if ($total % $jmlDataDiambil == 0) {
				$jml_halaman = $total / $jmlDataDiambil;
			} else {
				$jml_halaman = $total / $jmlDataDiambil + 1;
			}
			$jml_halaman = (int)$jml_halaman;
			$data = [
				'j' => $this->m_perpus->sqlget($query.' LIMIT '.$mulai.', '.$jmlDataDiambil),
				'jmlData' => $jmlDataDiambil,
				'cari' => $c,
				'total' => $total,
				'jml_halaman' => $jml_halaman,
				'halamanSekarang' => $halaman,
			];
			$this->load->view('siswa/hasilCari', $data);
			$this->load->view('siswa/pagination');
		}
	}

	function riwayat() {
		//update aktivitas
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Melihat riwayat peminjaman'], ['id' => $this->session->userdata('id')]);

		$siswa = $this->m_perpus->getsatu('siswa', ['id' => $this->session->userdata('id')]);
		$riwayat = $this->m_perpus->sqlget('
			SELECT b.judul as judul, t.tgl_pinjam as tgl_pinjam, t.target_kembali as target_kembali,
			t.kembali as kembali, t.id as id
			FROM transaksi t
			INNER JOIN katalog k ON k.kode_katalog=t.kode_katalog
			INNER JOIN bibliografi b ON b.id=k.id_buku
			WHERE t.nis_siswa='.$siswa->nis.'
			ORDER BY t.tgl_pinjam DESC
			');
		$data = [
			'riwayat' => $riwayat,
		];
		$this->load->view('siswa/riwayat', $data);
	}

	function detailRiwayat() {
		//update aktivitas
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Melihat detail riwayat peminjaman'], ['id' => $this->session->userdata('id')]);

		$id = $_GET['id'];
		$detail = $this->m_perpus->getSatuQuery('
			SELECT b.judul as judul,
			t.tgl_pinjam as tgl_pinjam,
			t.target_kembali as target_kembali,
			t.kembali as kembali,
			b.sampul as sampul,
			b.penulis as penulis,
			p.nama as penerbit,
			b.kategori as kategori,
			th.nama as tahun,
			b.deskripsi as deskripsi_buku,
			k.kode_katalog as kode_katalog,
			t.tgl_pinjam as tgl_pinjam,
			t.tgl_kembali as tgl_kembali,
			t.target_kembali as target_kembali,
			t.minta_tambah_lama_pinjam as lamakan,
			t.id as id,
			b.id as id_buku,
			s.nis as nis
			FROM transaksi t
			INNER JOIN katalog k ON k.kode_katalog=t.kode_katalog
			INNER JOIN bibliografi b ON b.id=k.id_buku
			INNER JOIN penerbit p ON p.id=b.id_penerbit
			INNER JOIN tahun th ON th.id=b.id_tahun
			INNER JOIN siswa s ON s.nis=t.nis_siswa
			WHERE t.id='.$id.'
			');

		$rating = 0;
		$kondisi = [
			'nis_siswa' => $detail->nis,
			'id_buku' => $detail->id_buku,
		];
		$ratingkah = $this->m_perpus->jmlDataDgnKondisi('rating', $kondisi);
		if($ratingkah > 0) {
			$rate = $this->m_perpus->getsatu('rating', $kondisi);
			$rating = $rate->rating;
		}

		$data = [
			'detail' => $detail,
			'rating' => $rating,
			'perpus' => $this->m_perpus->getsatuTanpaKondisi('pengaturan'),
			'siswa' => $this->m_perpus->getsatu('siswa', ['id' => $this->session->userdata('id')]),
		];
		$this->load->view('siswa/detailRiwayat', $data);
	}

	function editProfil() {
		//update aktivitas
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Membuka halaman edit password'], ['id' => $this->session->userdata('id')]);

		$this->load->view('siswa/editProfil');
	}

	function editProfilProses() {
		//update aktivitas
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Mengganti password'], ['id' => $this->session->userdata('id')]);

		$pw = $_GET['pw'];
		$pwKonf = $_GET['pwKonf'];

		if(strlen($pw) < 8) {
			$data['pesan'] = 'Password harus lebih dari 8 karakter';
			$this->load->view('siswa/editProfil', $data);
		} else if($pw != $pwKonf) {
			$data['pesan'] = 'Konfirmasi password salah.';
			$this->load->view('siswa/editProfil', $data);
		} else {
			$this->m_perpus->update('siswa', ['password' => md5($pw)], ['id' => $this->session->userdata('id')]);
			$data['pesan'] = 'Password berhasil diganti.';
			$this->load->view('siswa/editProfil', $data);
		}
	}

	function keluar() {
		//update aktivitas
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Keluar/Logout'], ['id' => $this->session->userdata('id')]);

		$this->session->sess_destroy();
		redirect(base_url().'login?pesan=Berhasil keluar.');
	}

	function rating() {
		$nis = $_GET['nis'];
		$id = $_GET['id'];
		$bintang = $_GET['bintang'];

		$kondisi = [
			'nis_siswa' => $nis,
			'id_buku' => $id,
		];
		$jmlData = $this->m_perpus->jmlDataDgnKondisi('rating', $kondisi);
		$buku = $this->m_perpus->getsatu('bibliografi', ['id' => $id]);
		if($jmlData <= 0) {
			// uppdate buku
			$rtBaru = (($buku->rating * $buku->jml_siswa_pemberi_nilai) + $bintang)/($buku->jml_siswa_pemberi_nilai+1);
			$updateBuku = [
				'rating' => $rtBaru,
				'jml_siswa_pemberi_nilai' => $buku->jml_siswa_pemberi_nilai+1,
			];
			$this->m_perpus->update('bibliografi', $updateBuku, ['id' => $id]);

			//tambah rating
			$kondisi['rating'] = $bintang;
			$this->m_perpus->input('rating', $kondisi);
		} else {
			// update buku
			$lama = $this->m_perpus->getsatu('rating', $kondisi);
			$rtBaru = (($buku->rating * $buku->jml_siswa_pemberi_nilai) - $lama->rating + $bintang)/($buku->jml_siswa_pemberi_nilai);
			$updateBuku = [
				'rating' => $rtBaru,
			];
			$this->m_perpus->update('bibliografi', $updateBuku, ['id' => $id]);

			//update aktivitas
			date_default_timezone_set('Asia/Jakarta');
			$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Memberi rating '.$bintang.' ke buku '.$buku->judul], ['id' => $this->session->userdata('id')]);


			// update rating
			$this->m_perpus->update('rating', ['rating' => $bintang], $kondisi);
		}
	}

	function koleksi() {
		//update aktivitas
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Membuka halaman koleksi'], ['id' => $this->session->userdata('id')]);

		$siswa = $this->m_perpus->getsatu('siswa', ['id' => $this->session->userdata('id')]);
		$data = [
			'koleksi' => $this->m_perpus->getUrutKondisi('cerpen', 'id', ['nis' => $siswa->nis]),
		];
		$this->load->view('siswa/koleksi', $data);
	}

	function tambahCerpen() {
		//update aktivitas
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Membuka halaman tambah cerpen'], ['id' => $this->session->userdata('id')]);

		$this->load->view('siswa/tambahCerpen');
	}

	function tambahCerpenProses() {
		$siswa = $this->m_perpus->getsatu('siswa', ['id' => $this->session->userdata('id')]);
		$kelas = $this->m_perpus->getsatu('kelas', ['id' => $siswa->id_kelas]);

        // edit simbol
		$isi = str_replace('
', '<br>',  $_POST['isi']);

        //input
		$data = [
			'judul' => $_POST['judul'],
			'kategori' => $_POST['kategori'],
			'isi' => $isi,
			'nama_pengarang' => $siswa->nama,
			'nis' => $siswa->nis,
			'kelas_pengarang' => $kelas->nama,
		];
		$this->m_perpus->input('cerpen', $data);

		//update aktivitas
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Membuka cerpen dengan judul '.$data['judul']], ['id' => $this->session->userdata('id')]);


		redirect(base_url('user'));
	}

	function editCerpen() {
		$cerpen = $this->m_perpus->getsatu('cerpen', ['id' => $_GET['id']]);
		$data = [
			'cerpen' => $cerpen,
		];

		//update aktivitas
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Membuka halaman edit cerpen dengan judul '.$cerpen->judul], ['id' => $this->session->userdata('id')]);

		$this->load->view('siswa/editCerpen', $data);
	}

	function editCerpenProses() {
		$isi = str_replace('
', '<br>',  $_POST['isi']);
		$data = [
			'judul' => $_POST['judul'],
			'kategori' => $_POST['kategori'],
			'isi' => $isi,
		];

		//update aktivitas
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Mengedit cerpen dengan judul '.$data['judul']], ['id' => $this->session->userdata('id')]);

		$this->m_perpus->update('cerpen', $data, ['id' => $_POST['id']]);
		redirect(base_url('user'));
	}

	function cerpen() {
		//update aktivitas
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Membuka halaman cerpen'], ['id' => $this->session->userdata('id')]);
		$this->load->view('siswa/cerpen');
	}

	function cariCerpen() {
		//update aktivitas
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Membuka halaman cari cerpen'], ['id' => $this->session->userdata('id')]);
		$this->load->view('siswa/cerpen/cari');
	}

	function cariCerpenProses() {
		//update aktivitas
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Mencari cerpen'], ['id' => $this->session->userdata('id')]);
		$c = '';
		if (isset($_GET['s'])) {
			$c = $_GET['s'];
		}
		if (strlen($c) == 0) {
			echo "Pencarian gagal. Silakan masukkan pencarian yang lebih spesifik. Seperti judul dan pengarang.";
		} else {
			$query = 'SELECT * from cerpen
			WHERE status = 1
			and (judul LIKE "%'.$c.'%"
			or id = '.$c.'
			or nama_pengarang LIKE "%'.$c.'%"
			or kategori LIKE "%'.$c.'%"
			or kelas_pengarang LIKE "%'.$c.'%")';
			$total = $this->m_perpus->jmlDataDgnQuery($query);
			$jmlDataDiambil = 12;
			$jml_halaman = 0;
			$halaman = 0;
			if (isset($_GET['halaman'])) {
				$halaman =$_GET['halaman'];
			}
			$mulai = $jmlDataDiambil * $halaman;


			if ($total % $jmlDataDiambil == 0) {
				$jml_halaman = $total / $jmlDataDiambil;
			} else {
				$jml_halaman = $total / $jmlDataDiambil + 1;
			}
			$jml_halaman = (int)$jml_halaman;
			$data = [
				'cerpen' => $this->m_perpus->sqlget($query.' LIMIT '.$mulai.', '.$jmlDataDiambil),
				'jmlData' => $jmlDataDiambil,
				'cari' => $c,
				'total' => $total,
				'jml_halaman' => $jml_halaman,
				'halamanSekarang' => $halaman,
			];
			$this->load->view('siswa/cerpen/hasilCari', $data);
		}
	}

	function bacaCerpen() {
		$cerpen = $this->m_perpus->getsatu('cerpen', ['id' => $_GET['id']]);

		//update aktivitas
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Membaca cerpen dengan judul '.$cerpen->judul], ['id' => $this->session->userdata('id')]);

		// update dilihat
		$kondisiDilihat = [
			'id_cerpen' => $_GET['id'],
			'id_siswa' => $this->session->userdata('id'),
		];
		$dilihat = $this->m_perpus->jmlDataDgnKondisi('baca_cerpen', $kondisiDilihat);
		if($dilihat < 1) {
			$this->m_perpus->input('baca_cerpen', $kondisiDilihat);
			$this->m_perpus->update('cerpen', ['dilihat' => $cerpen->dilihat + 1], ['id' => $_GET['id']]);
		}

		$sudahBeriBintangKah = $this->m_perpus->jmlDataDgnKondisi('bintang_cerpen', ['id_siswa' => $this->session->userdata('id'), 'id_cerpen' => $cerpen->id]);
		$berbintang = 0;
		if ($sudahBeriBintangKah > 0) {
			$berbintang = 1;
		}
		$data = [
			'cerpen' => $cerpen,
			'berbintangkah' => $berbintang,
		];
		$this->load->view('siswa/cerpen/bacaCerpen', $data);
	}

	function cerpenTerbaru() {
		//update aktivitas
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Membuka halaman cerpen terbaru'], ['id' => $this->session->userdata('id')]);

		$query = 'SELECT * from cerpen
		WHERE status = 1
		order by id DESC';
		$total = $this->m_perpus->jmlDataDgnQuery($query);
		$jmlDataDiambil = 12;
		$jml_halaman = 0;
		$halaman = 0;
		if (isset($_GET['halaman'])) {
			$halaman =$_GET['halaman'];
		}
		$mulai = $jmlDataDiambil * $halaman;


		if ($total % $jmlDataDiambil == 0) {
			$jml_halaman = $total / $jmlDataDiambil;
		} else {
			$jml_halaman = $total / $jmlDataDiambil + 1;
		}
		$jml_halaman = (int)$jml_halaman;
		$data = [
			'cerpen' => $this->m_perpus->sqlget($query.' LIMIT '.$mulai.', '.$jmlDataDiambil),
			'jmlData' => $jmlDataDiambil,
			'total' => $total,
			'jml_halaman' => $jml_halaman,
			'halamanSekarang' => $halaman,
		];
		$this->load->view('siswa/cerpen/terbaru', $data);
	}

	function beriCerpenBintang() {
		$id_siswa = $this->session->userdata('id');
		$id_cerpen = $_GET['id'];
		$cerpen = $this->m_perpus->getsatu('cerpen', ['id' => $id_cerpen]);
		$berbintangkah = $_GET['sudahBerbintang'];
		$kondisi = ['id_siswa' => $id_siswa, 'id_cerpen' => $id_cerpen];
		if($berbintangkah == 1) {
			$jmlbintang = $cerpen->bintang - 1;
			$this->m_perpus->update('cerpen', ['bintang' => $jmlbintang], ['id' => $id_cerpen]);
			$this->m_perpus->hapus('bintang_cerpen', $kondisi);
		} else {
			$jmlbintang = $cerpen->bintang + 1;
			$this->m_perpus->update('cerpen', ['bintang' => $jmlbintang], ['id' => $id_cerpen]);
			$this->m_perpus->input('bintang_cerpen', $kondisi);
		}

		//update aktivitas
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Memberi cerpen bintang dengan judul '.$cerpen->judul], ['id' => $this->session->userdata('id')]);

		redirect(base_url('user/bacaCerpen?id='.$id_cerpen));
	}

	function cerpenBerbintang() {
		//update aktivitas
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Membuka halaman cerpen dengan bintang terbanyak'], ['id' => $this->session->userdata('id')]);
		$query = 'SELECT * from cerpen
		WHERE status = 1
		order by bintang DESC';
		$total = $this->m_perpus->jmlDataDgnQuery($query);
		$jmlDataDiambil = 12;
		$jml_halaman = 0;
		$halaman = 0;
		if (isset($_GET['halaman'])) {
			$halaman =$_GET['halaman'];
		}
		$mulai = $jmlDataDiambil * $halaman;


		if ($total % $jmlDataDiambil == 0) {
			$jml_halaman = $total / $jmlDataDiambil;
		} else {
			$jml_halaman = $total / $jmlDataDiambil + 1;
		}
		$jml_halaman = (int)$jml_halaman;
		$data = [
			'cerpen' => $this->m_perpus->sqlget($query.' LIMIT '.$mulai.', '.$jmlDataDiambil),
			'jmlData' => $jmlDataDiambil,
			'total' => $total,
			'jml_halaman' => $jml_halaman,
			'halamanSekarang' => $halaman,
		];
		$this->load->view('siswa/cerpen/berbintang', $data);
	}

	function cerpenPopuler() {
		//update aktivitas
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Membuka halaman cerpen populer'], ['id' => $this->session->userdata('id')]);
		$query = 'SELECT * from cerpen
		WHERE status = 1
		order by dilihat DESC';
		$total = $this->m_perpus->jmlDataDgnQuery($query);
		$jmlDataDiambil = 12;
		$jml_halaman = 0;
		$halaman = 0;
		if (isset($_GET['halaman'])) {
			$halaman =$_GET['halaman'];
		}
		$mulai = $jmlDataDiambil * $halaman;


		if ($total % $jmlDataDiambil == 0) {
			$jml_halaman = $total / $jmlDataDiambil;
		} else {
			$jml_halaman = $total / $jmlDataDiambil + 1;
		}
		$jml_halaman = (int)$jml_halaman;
		$data = [
			'cerpen' => $this->m_perpus->sqlget($query.' LIMIT '.$mulai.', '.$jmlDataDiambil),
			'jmlData' => $jmlDataDiambil,
			'total' => $total,
			'jml_halaman' => $jml_halaman,
			'halamanSekarang' => $halaman,
		];
		$this->load->view('siswa/cerpen/banyakDilihat', $data);
	}

	function modeGelapTerang() {
		//update aktivitas
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Mengganti mode'], ['id' => $this->session->userdata('id')]);

		$s = $this->m_perpus->getsatu('siswa', ['id' => $this->session->userdata('id')]);
		if($s->mode_gelap == 0) {
			$this->m_perpus->update('siswa', ['mode_gelap' => 1], ['id' => $s->id]);
		} else {
			$this->m_perpus->update('siswa', ['mode_gelap' => 0], ['id' => $s->id]);
		}

		redirect(base_url('user'));
	}

	function hapusCerpen() {
		$dt = [
			'cerpen' => $this->m_perpus->getsatu('cerpen', ['id' => $_GET['id']]),
		];
		$this->load->view('siswa/cerpen/hapus', $dt);
	}

	function hapusCerpenProses() {
		//update aktivitas
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Menghapus cerpen'], ['id' => $this->session->userdata('id')]);
		$id = $_GET['id'];
		$this->m_perpus->hapus('cerpen', ['id' => $id]);
		$this->m_perpus->hapus('bintang_cerpen', ['id_cerpen' => $id]);
		$this->m_perpus->hapus('baca_cerpen', ['id_cerpen' => $id]);

		redirect(base_url('user/koleksi'));
	}

	function mintaPerlama() {
		//update aktivitas
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Meminta perpanjang peminjaman'], ['id' => $this->session->userdata('id')]);

		$transaksi = $this->m_perpus->getsatu('transaksi', ['id' => $_GET['id']]);
		if($transaksi->minta_tambah_lama_pinjam == 0) {
			$this->m_perpus->update('transaksi', ['minta_tambah_lama_pinjam' => 1], ['id' => $transaksi->id]);
			echo "Sedang diproses.. Klik lagi untuk membatalkan";
		} else {
			$this->m_perpus->update('transaksi', ['minta_tambah_lama_pinjam' => 0], ['id' => $transaksi->id]);
			echo "Tambah lama peminjaman";
		}
	}

	function diskusi() {
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Membuka halaman diskusi.'], ['id' => $this->session->userdata('id')]);

		// hapus pesan lama
		$this->m_perpus->hapus('diskusi', ['hapus <' => date("Y-m-d H:i:s")]);

		$pengaturan = $this->m_perpus->getsatuTanpaKondisi('pengaturan');
		$data = [
			'p' => $this->m_perpus->sqlget('select * from diskusi order by id desc'),
			'siswa' => $this->m_perpus->getsatu('siswa', ['id' => $this->session->userdata('id')]),
			'pengaturan' => $pengaturan,
		];
		$this->load->view('siswa/diskusi', $data);
	}

	function diskusiKirim() {
		date_default_timezone_set('Asia/Jakarta');
		$this->m_perpus->update('siswa', ['terakhir_aktif' => date("Y-m-d H:i:s"), 'aktivitas' => 'Diskusi mengirim pesan.'], ['id' => $this->session->userdata('id')]);

		$pesan = $_GET['pesan'];
		$siswa = $this->m_perpus->getsatu('siswa', ['id' => $this->session->userdata('id')]);
		$pengaturan = $this->m_perpus->getsatuTanpaKondisi('pengaturan');
		$pesan = str_replace("00100110", "&", $pesan);
		$data = [
			'id_pengirim' => $this->session->userdata('id'),
			'admin_kah' => 0,
			'pesan' => $pesan,
			'dikirim' => date('Y-m-d H:i:s'),
			'nama_pengirim' => $siswa->nama,
			'hapus' => date('Y-m-d H:i:s', strtotime("+".$pengaturan->lama_pesan_diskusi." hours")),
			'membalas' => $_GET['idBalas'],
		];
		$this->m_perpus->input('diskusi', $data);
		redirect(base_url().'user/diskusi');
	}

	function hapusPesan() {
		$this->m_perpus->hapus('diskusi', ['id' => $_GET['id']]);
		redirect(base_url().'user/diskusi');
	}
}
