<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('login')) {
			if ($this->session->userdata('login') == 'user') {
				redirect(base_url('user'));
			} else if($this->session->userdata('login') == 'pustakawan') {
				redirect(base_url('pustakawan'));
			} else if($this->session->userdata('login') == 'admin') {
				redirect(base_url('admin'));
			}
		}
		$this->load->model('m_perpus');
	}

	public function index() {
		if ($this->m_perpus->jumlahData('admin') == 0) {
			redirect(base_url().'konfigurasi');
		} else {
			$data['perpus'] = $this->m_perpus->getsatuTanpaKondisi('pengaturan');
			$data['jmlBuku'] = $this->m_perpus->jumlahData('bibliografi');
			$data['jmlExsemplar'] = $this->m_perpus->sqlget('SELECT SUM(tersedia) as ttl FROM bibliografi;')[0]->ttl;
			$jmlPengunjung = $data['perpus']->jmlPengunjung + 1;
			if ($this->session->userdata('berkunjung') == true) {} else {
				$this->m_perpus->update('pengaturan', ['jmlPengunjung' => $jmlPengunjung], ['id' => $data['perpus']->id]);
			}
			$session = [
				'berkunjung' => true
			];
			$this->session->set_userdata($session);
			$this->load->view('halamanTamu', $data);
		}
	}
}
