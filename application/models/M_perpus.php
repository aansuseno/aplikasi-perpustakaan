<?php

class M_perpus extends CI_Model{

	function jumlahData($table) {
		return $this->db->count_all($table);
	}

	function jmlDataDgnKondisi($table, $kondisi){
		return $this->db->get_where($table, $kondisi)->num_rows();
	}

	function jmlDataDgnQuery($anu) {
		return $this->db->query($anu)->num_rows();
	}

	function jmlDataLike($table, $kondisi){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->like($kondisi);
		return $this->db->get()->num_rows();
	}

	function input($table, $isi) {
		$this->db->insert($table, $isi);
	}

	function getsatu($table, $kondisi) {
		return $this->db->get_where($table, $kondisi)->row();
	}

	function getUrut($table, $urut) {
		return $this->db->query("select * from ".$table." order by ".$urut.";")->result();
	}

	function getPagi($table, $urut, $limit, $mulai) {
		$this->db->order_by($urut);
		return $this->db->get($table, $limit, $mulai)->result();
	}

	function getUrutKondisi($table, $urut, $kondisi) {
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($kondisi);
		$this->db->order_by($urut);
		return $this->db->get()->result();
	}

	function getUrutBalik($table, $kolom) {
		return $this->db->query("select * from ".$table." order by ".$kolom." asc;")->result();
	}

	function getsatuTanpaKondisi($table) {
		return $this->db->get($table)->row();
	}

	function getSatuQuery($query) {
		return $this->db->query($query)->row();
	}

	function update($table, $data, $where) {
		$this->db->update($table, $data, $where);
	}

	function get($table) {
		return $this->db->get($table)->result();
	}

	function getkondisi($table, $kondisi) {
		return $this->db->get_where($table, $kondisi)->result();
	}

	function hapus($table, $kondisi) {
		$this->db->delete($table, $kondisi);
	}

	function cariUrut($table, $kolom, $cari, $urut) {
		return $this->db->query('select * from '.$table.' where '.$kolom.' like "%'.$cari.'%" order by '.$urut)->result();
	}

	function sqlget($sql) {
		return $this->db->query($sql)->result();
	}

	function transaksi($dipilih, $limit) {
		$this->db->select($dipilih);
		$this->db->from('transaksi t');
		$this->db->join('siswa s', 's.nis=t.nis_siswa', 'inner');
		$this->db->join('katalog k', 'k.kode_katalog=t.kode_katalog', 'inner');
		$this->db->join('bibliografi b', 'b.id=k.id_buku', 'left');
		$this->db->where('t.tgl_pinjam >= "'.$limit.'" && t.kembali = 1');
		$this->db->order_by('id', 'desc');
		return $this->db->get()->result();
	}

	function transaksiDgnKondisi($dipilih, $kondisi) {
		$this->db->select($dipilih);
		$this->db->from('transaksi t');
		$this->db->join('siswa s', 's.nis=t.nis_siswa', 'inner');
		$this->db->join('katalog k', 'k.kode_katalog=t.kode_katalog', 'inner');
		$this->db->join('bibliografi b', 'b.id=k.id_buku', 'left');
		$this->db->where($kondisi);
		$this->db->order_by('id', 'desc');
		return $this->db->get()->result();
	}

	function getLikeKondisi($table, $kondisi, $urut){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->like($kondisi);
		$this->db->order_by($urut, 'asc');
		return $this->db->get()->result();
	}

	function getLikeLimit($table, $kondisi, $urut, $limit) {
		$this->db->select('*');
		$this->db->from($table);
		$this->db->like($kondisi);
		$this->db->limit($limit);
		$this->db->order_by($urut, 'asc');
		return $this->db->get()->result();
	}

	function getLimit($table, $kondisi, $mulai, $jumlah, $urut) {
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($kondisi);
		$this->db->limit($jumlah, $mulai);
		$this->db->order_by($urut, 'asc');
		return $this->db->get()->result();
	}
}
