<div class="container">
	<form action="<?= base_url().'admin/editprofilProses' ?>" method="post">
		<table class="table">
			<tr>
				<th>Nama Lengkap</th>
				<td id="nama">: <?= $nama_user->nama ?></td>
				<td id="editnama" style="display: none;">
					<input type="text" name="nama" id="namainput" oninput="janganKosong('namainput')" class="form-control" value="<?= $nama_user->nama ?>">
				</td>
				<td>
					<a
					 href="#"
					 class="btn btn-warning"
					 onclick="nonblok('nama','editnama')"
					 title="Klik untuk mengedit nama">
					 	<i class="fas fa-edit"></i>
					 </a>
				 </td>
			</tr>
			<tr>
				<th>Username</th>
				<td id="username">: <?= $username ?></td>
				<td id="editusername" style="display: none;">
					<input
					 type="text"
					 name="username"
					 id="usernameinput"
					 oninput="janganKosong('usernameinput')"
					 value="<?= $username ?>"
					 class="form-control">
				</td>
				<td>
					<a
					 href="#"
					 class="btn btn-warning"
					 onclick="nonblok('username','editusername')">
						<i class="fas fa-edit"></i>
					</a>
				</td>
			</tr>
			<tr>
				<th>Password</th>
				<td>: ***</td>
				<td>
					<a
					 href="<?= base_url() ?>admin/gantiPassword"
					 target="_blank"
					 class="btn btn-warning"
					 title="Klik untuk membuka tab baru untuk edit password">
					 	<i class="fas fa-edit"></i>
					 </a>
				</td>
			</tr>
			<tr>
				<th>No. WA</th>
				<td id="no_wa">: <?= $no_wa ?></td>
				<td
				 id="editno_wa"
				 style="display: none;">
					<input
					 type="text"
					 id="no_wainput"
					 name="no_wa"
					 oninput="janganKosong('no_wainput')"
					 value="<?= $no_wa ?>"
					 class="form-control">
				</td>
				<td>
					<a
					 href="#"
					 class="btn
					 btn-warning"
					 onclick="nonblok('no_wa','editno_wa')">
						<i class="fas fa-edit"></i>
					</a>
				</td>
			</tr>
			<tr>
				<th>Alamat</th>
				<td id="alamat">: <?= $alamat ?></td>
				<td
				 id="editalamat"
				 style="display: none;">
					<textarea
					 name="alamat"
					 class="form-control">
						<?= $alamat ?>
					</textarea>
				</td>
				<td>
					<a
					 href="#"
					 class="btn btn-warning"
					 onclick="nonblok('alamat','editalamat')"
					 title="Edit alamat(optional)">
						<i class="fas fa-edit"></i>
					</a>
				</td>
			</tr>
			<tr>
				<th>Bio</th>
				<td id="bio">: <?= $bio ?></td>
				<td
				 id="editbio"
				 style="display: none;">
					<textarea
					 name="bio"
					 class="form-control">
						<?= $bio ?>
					</textarea>
				</td>
				<td>
					<a
					 href="#"
					 class="btn btn-warning"
					 onclick="nonblok('bio','editbio')"
					 title="Edit bio, bisa diisi latar belakang(optional)">
						<i class="fas fa-edit"></i>
					</a>
				</td>
			</tr>
		</table>
		<button
		 type="submit"
		 id="submit"
		 class="btn btn-warning"
		 title="Klik untuk menyimpan perubahan">
		 	Edit
		 </button>
	</form>
</div>
