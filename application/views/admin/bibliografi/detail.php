<div class="modal-body">
	<center>
		<h4><?= $buku->judul ?></h4>
	</center>
	<img src="<?= base_url('gambar/'.$buku->sampul) ?>" style="max-width: 200px;border-radius: 10px; box-shadow: 0 2px 5px rgba(0,0,0,0.6)" class="sampul-detail">
	<br><br>
	<table class="table-detail">
		<tr>
			<th>Pengarang</th>
			<td>: <?= $buku->penulis ?></td>
		</tr>
		<tr>
			<th>Penerbit</th>
			<td>: <?= $penerbit->nama ?></td>
		</tr>
		<tr>
			<th>Kategori</th>
			<td>: <?= $buku->kategori ?></td>
		</tr>
		<tr>
			<th>Tahun Terbit</th>
			<td>: <?= $tahun->nama ?></td>
		</tr>
		<tr>
			<th>Lemari</th>
			<td>: Lemari <?= $lemari->nama ?></td>
		</tr>
		<tr>
			<th>Halaman</th>
			<td>: <?= $buku->jumlah_halaman ?></td>
		</tr>
		<tr>
			<th>Jumlah</th>
			<td>: <?= $buku->jumlah_buku ?></td>
		</tr>
		<tr>
			<th>Deskripsi</th>
			<td>: <?= $buku->deskripsi ?></td>
		</tr>
	</table>
</div>
<div class="modal-footer">
	<span class="btn btn-secondary float-right" data-dismiss="modal">Tutup &times;</span>
</div>