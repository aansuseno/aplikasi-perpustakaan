<div class="modal-body">
	<table class="table-detail">
		<tr>
			<th>Judul</th>
			<td><input type="text" autocomplete="off" value="<?= $buku->judul ?>" class="form-control" id="nilai-judul"></td>
		</tr>
		<tr>
			<th>Pengarang</th>
			<td>
				<input type="text" autocomplete="off" id="filter-penulis" oninput="filterPenulis('<?= base_url() ?>')" placeholder="filter pengarang" class="form-control">
			</td>
		</tr>
		<tr>
			<td> </td>
			<td>
				<div class="input-group">
					<select class="form-control" id="list-penulis">
						<option selected="selected">Null</option>
					</select>
					<div class="input-group-append">
						<span class="btn btn-outline-secondary" id="button-addon2" onclick="setPenulis()">+</span>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td> </td>
			<td>
				<input type="text" autocomplete="off" value="<?= $buku->penulis ?>" class="form-control" id="penulis">
			</td>
		</tr>
		<tr>
			<th>Penerbit</th>
			<td>
				<select class="form-control" id="id_penerbit">
					<?php foreach($daftar_penerbit as $t) { ?>
					<option <?php if ($t->id == $buku->id_penerbit) { echo "selected='selected'";} ?> value="<?= $t->id ?>"><?= $t->nama ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<th>Kategori</th>
				<td>
					<input type="text" autocomplete="off" id="filter-kategori" oninput="filterKategori('<?= base_url() ?>')" placeholder="filter kategori" class="form-control">
				</td>
			</tr>
			<tr>
				<td> </td>
				<td>
					<div class="input-group">
						<select class="form-control" id="list-kategori">
							<option selected="selected">Null</option>
						</select>
						<div class="input-group-append">
							<span class="btn btn-outline-secondary" onclick="setKategori()" id="button-addon2">+</span>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td> </td>
				<td>
					<input type="text" autocomplete="off" id="kategori" value="<?= $buku->kategori ?>" class="form-control">
				</td>
			</tr>
		</tr>
		<tr>
			<th>Lemari</th>
			<td>
				<select class="form-control" id="id_lemari">
					<?php foreach($daftar_lemari as $t) { ?>
					<option value="<?= $t->id ?>" <?php if ($t->id == $buku->id_lemari): ?>
						selected="selected"
					<?php endif ?>><?= $t->nama ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<th>Tahun Terbit</th>
			<td>
				<select class="form-control" id="id_tahun">
					<?php foreach($daftar_tahun as $t) { ?>
					<option value="<?= $t->id ?>" <?php if ($t->id == $buku->id_tahun): ?>
						selected="selected"
					<?php endif ?>><?= $t->nama ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<th>Halaman</th>
			<td><input type="text" autocomplete="off" value="<?= $buku->jumlah_halaman ?>" id="jumlah_halaman" class="form-control"></td>
		</tr>
		<tr>
			<th>Deskripsi</th>
			<td><input type="text" autocomplete="off" value="<?= $buku->deskripsi ?>" id="deskripsi" class="form-control"></td>
		</tr>
	</table>
	<br>
	<a target="_blank" href="<?= base_url() ?>bibliografi/gantiSampul?id=<?= $buku->id ?>" class="btn btn-warning" title="Klik untuk mengganti sampul buku">Edit sampul buku</a>
</div>
<div class="modal-footer">
	<span class="btn btn-secondary float-right" data-dismiss="modal">Tutup &times;</span>
	<span class="btn btn-primary" data-dismiss="modal" onclick="simpanEditBuku('<?= base_url() ?>', <?= $buku->id ?>)">Edit</span>
</div>
