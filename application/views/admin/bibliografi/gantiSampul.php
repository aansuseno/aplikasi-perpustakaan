<form method="post" action="<?= base_url() ?>bibliografi/gantiSampulProses" enctype="multipart/form-data">
	<?php if($pesan == 'berhasil') { ?>
	<b>Sampul buku berhasil diedit.</b>
	<?php } else { ?>
		<?= $pesan ?>
	<?php } ?>
	<table>
		<tr>
			<td>Sampul dulu</td>
			<td>:
				<img src="<?= base_url() ?>gambar/<?= $b->sampul ?>" style="max-width: 200px;">
			</td>
		</tr>
		<tr>
			<td>Sampul baru</td>
			<td>: <input type="file" name="sampul">
			</td>
		</tr>
	</table>
	<br>
	<br>
	<input type="hidden" name="id" value="<?= $b->id ?>">
	<input type="submit" value="Ganti sampul">
</form>
