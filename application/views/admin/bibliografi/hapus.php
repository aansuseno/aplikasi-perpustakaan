<div class="modal-body">
	Anda yakin akan menghapus buku <b><?= $buku->judul ?></b>
</div>
<div class="modal-footer">
	<span class="btn btn-outline-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Tutup</span>
	<span class="btn btn-danger float-right" data-dismiss="modal" onclick="hapusBukuProses('<?= base_url() ?>', <?= $buku->id ?>, <?= $buku->jumlah_buku ?>)"><i class="fas fa-trash"></i> Hapus</span>
</div>