<div class="modal-body" style="max-height: 50vh; overflow:auto;">
	<ol>
		<li>
			<h4>Info</h4>
			<p>Klik icon <i>i</i> untuk melihat informasi lebaih detail dari buku.</p>
		</li>
		<li>
			<h4>Hapus</h4>
			<ol>
				<li>Klik ikon tempat sampah untuk mengahapus buku <b>permanen</b></li>
				<li><b>Tidak disarankan untuk menghapus buku</b> yang sudah terhubung dengan katalog</li>
				<li>Hapuslah buku jika baru ditambahkan</li>
				<li>Sistem akan error jika buku dihapus dan terhubung dengan katalog yang sudah pernah melakukan transaksi</li>
			</ol>
		</li>
		<li>
			<h4>Edit</h4>
			<ol>
				<li>Klik ikon pensil orange untuk mengedit buku</li>
				<li>
					<ol>
						<li>
							<h5>Judul</h5>
							<p>Masukkan judul dengan batas karakter 255 karakter</p>
						</li>
						<li>
							<h5>Penulis(Pengarang)</h5>
							<ol>
								<li>Cari dahulu pengarang didalam kolom <i>filter pengarang</i></li>
								<li>Hasil pencarian akan muncul disebelah kanan kolom.</li>
								<li>Klik ikon plus untuk mengirim nama pengarang ke input nama pengarang</li>
								<li><b>Pengarang bisa lebih dari 1</b></li>
							</ol>
						</li>
						<li>
							<h5>Tahun Terbit</h5>
							<ol>
								<li>Klik pada bagian input tahun terbit</li>
								<li>Pilih salah satu tahun</li>
								<li>Jika tahun yang dimaksud belu tersedia bisa ditambahkan melalui <a href="<?= base_url() ?>tahun">halaman tahun</a></li>
							</ol>
						</li>
						<li>
							<h5>Penerbit</h5>
							<ol>
								<li>Penerbit yaitu PT atau perusahaan yang mencetak atau menerbitkan buku</li>
								<li>Klik pada bagian input penerbit</li>
								<li>Pilih salah satu penerbit</li>
								<li>Jika tahun yang dimaksud belu tersedia bisa ditambahkan melalui <a href="<?= base_url() ?>penerbit">halaman tahun</a></li>
							</ol>
						</li>
						<li>
							<h5>Kategori</h5>
							<ol>
								<li>Cari dahulu kategori didalam kolom <i>filter kategori</i></li>
								<li>Hasil pencarian akan muncul disebelah kanan kolom.</li>
								<li>Klik ikon plus untuk mengirim kategori ke input kategori</li>
								<li><b>Kategori bisa lebih dari 1</b></li>
								<li>Jika kategori yang dimaksud belum tersedia bisa ditambahkan melalui <a href="<?= base_url() ?>tahun">halaman kategori</a></li>
							</ol>
						</li>
						<li>
							<h5>Lemari</h5>
							<ol>
								<li>Lemari atau rak buku adalah dimana buku akan disimpan</li>
								<li>Klik pada bagian input lemari</li>
								<li>Pilih salah satu lemari</li>
								<li>Jika lemari yang dimaksud belum tersedia bisa ditambahkan melalui <a href="<?= base_url() ?>lemari">halaman lemari</a></li>
							</ol>
						</li>
						<li>
							<h5>Jumlah</h5>
							<p>Jumlah halaman harus berupa bilangan bulat</p>
						</li>
						<li>
							<h5>Deskripsi</h5>
							<ol>
								<li>Deskripsi bisa berupa sinopsis atau keterangan buku atau bisa juga keterangan dari admin</li>
								<li>Deskripsi berupa optional(boleh tidak diisi)</li>
							</ol>
						</li>
						<li>
							<h5>Sampul</h5>
							<ol>
								<li>Klik ganti sampul</li>
								<li>Browser akan membuka tab baru</li>
								<li>Masukkan gambar di input gambar baru</li>
								<li>Sampul harus berformat: gif, jpg, png, jpeg, atau svg</li>
								<li>Sebaiknya sampul berresolusi Potret</li>
							</ol>
						</li>
					</ol>
				</li>
			</ol>
		</li>
		<li>
			<h4>Cari</h4>
			<ol>
				<li>Masukkan kata kunci di dalam kolom cari, berdasar judul, nama pengarang, dan kategori</li>
				<li>Klik ikon cari untuk menampilkan hasil cari. Enter tidak menampilkan hasil pencarian</li>
			</ol>
		</li>
	</ol>
</div>
<div class="modal-footer">
	<button class="btn btn-secondary" data-dismiss="modal">Tutup</button>
</div>
