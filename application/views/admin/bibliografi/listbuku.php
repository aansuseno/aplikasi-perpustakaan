<?php if($pesan == 'berhasilHapus') { ?>
	<div class="alert alert-primary">Buku berhasil dihapus</div>
<?php } else if($pesan == 'berhasilEdit') { ?>
	<div class="alert alert-primary">Buku berhasil diedit</div>
<?php } ?>
<br>

<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th scope="col">Id</th>
				<th scope="col">Judul</th>
				<th scope="col">Aksi</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($buku as $b) : ?>
				<tr>
					<td><?= $b->id ?></td>
					<td><?= $b->judul ?></td>
					<td>
						<span
						 data-toggle="modal"
						 onclick="detailBuku('<?= base_url() ?>', <?= $b->id ?>)"
						 data-target="#popupDetail"
						 class="btn btn-info"
						 title="Info detail buku <?= $b->judul?>">
							<i class="fas fa-info-circle"></i>
						</span>
						<span
						 data-toggle="modal"
						 onclick="editBuku('<?= base_url() ?>', <?= $b->id ?>)"
						 data-target="#popupDetail"
						 class="btn btn-warning"
						 title="Edit buku <?= $b->judul ?>">
							<i class="fas fa-edit"></i>
						</span>
						<span
						 data-toggle="modal"
						 onclick="hapusBuku('<?= base_url() ?>', <?= $b->id ?>)"
						 data-target="#popupDetail"
						 class="btn btn-danger"
						 title="Hapus buku <?= $b->judul ?>">
							<i class="fas fa-trash"></i>
						</span>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
	<!-- pagination -->
	<div style="display: flex; flex-wrap: wrap; justify-content: space-around;">
	<?php if ($jml_halaman > 1): ?>
		<ul class="pagination">
			<?php if ($halamanSekarang > 0): ?>
				<li class="page-item">
					<span
					class="page-link"
					style="cursor: pointer;"
					onclick="setAjak('listBuku', '<?= base_url() ?>bibliografi/listbuku?halaman=0')"
					>First
					</span>
				</li>
				<li class="page-item">
					<span
					class="page-link"
					style="cursor: pointer;"
					<?php if ($halamanSekarang > 0): ?>
						onclick="setAjak('listBuku', '<?= base_url() ?>bibliografi/listbuku?halaman='+<?= $halamanSekarang-1 ?>)"
					<?php endif ?>
					>&lt;
					</span>
				</li>
			<?php endif ?>
			<?php for ($i = $halamanSekarang - 2; $i <= $halamanSekarang + 4; $i++): ?>
				<?php if ($i > 0 && $i <= $jml_halaman): ?>
					<li
					class="page-item <?php if($halamanSekarang == $i - 1) { echo 'active'; }?>"
					style="cursor: pointer;">
						<span
						class="page-link"
						onclick="setAjak('listBuku', '<?= base_url() ?>bibliografi/listbuku?halaman='+<?= $i-1 ?>)">
							<?= $i ?>
						</span>
					</li>
				<?php endif ?>
			<?php endfor ?>
			<?php if ($halamanSekarang < $jml_halaman-1): ?>
				<li class="page-item">
					<span
					class="page-link"
					style="cursor: pointer;"
					<?php if ($halamanSekarang < $jml_halaman-1): ?>
						onclick="setAjak('listBuku', '<?= base_url() ?>bibliografi/listbuku?halaman='+<?= $halamanSekarang+1 ?>)"
					<?php endif ?>
					>&gt;
					</span>
				</li>
				<li class="page-item">
					<span
					class="page-link"
					style="cursor: pointer;"
					onclick="setAjak('listBuku', '<?= base_url() ?>bibliografi/listbuku?halaman='+<?= $jml_halaman-1 ?>)"
					>Last
					</span>
				</li>
			<?php endif ?>
		</ul>
	<?php endif ?>
	</div>
	<!-- end pagination -->
