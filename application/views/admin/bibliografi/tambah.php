<script src="<?= base_url().'assets/js/tambahBibliografi.js' ?>"></script>

<div class="container">
	<div class="judul-halaman">
		<h1>Tambah Bibliografi</h1>
	</div>
	<br>
	<button
	 class="btn btn-info"
	 data-toggle="modal"
	 data-target="#popupInfo"
	 title="Informasi tentang tambah bibliografi.">
		<i class="fas fa-info"></i>
	</button>

	<?php if ($pesan != '') { ?>
		<div class="alert alert-secondary" role="alert">
			<?= $pesan ?>
		</div>
	<?php } ?>
	<br>
	<form method="post" action="<?= base_url() ?>bibliografi/tambahProses" enctype="multipart/form-data">
		<!-- judul buku -->
		<div class="form-group row">
			<label for="judul" class="col-sm-2 col-form-label">Judul Buku</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" autocomplete="off" id="judul" name="judul" placeholder="Judul Buku">
			</div>
		</div>

		<!-- pengarang -->
		<div class="form-group row">
			<label for="pengarang" class="col-sm-2 col-form-label">Pengarang</label>
			<div class="col-sm-10">
				<div class="input-group">
					<input type="text" id="filter-penulis" oninput="filterPenulis('<?= base_url() ?>')" class="form-control" placeholder="Filter pengarang">
					<div class="input-group-append col-sm-4">
						<select class="form-control" id="list-penulis">
							<option selected="selected">Null</option>
						</select>
					</div>
					<span
					 class="btn btn-secondary mb-2"
					 onclick="setPenulis()"
					 title="Pilih pengarang">+</span>
					<br>
				</div>
			</div>
		</div>
		<div class="form-group row">
			<label for="tambahPengarang" class="col-sm-2 col-form-label"> </label>
			<div class="input-group col-sm-10">
				<input type="text" class="form-control" id="tambah-penulis" placeholder="Tambah pengarang" aria-label="Recipient's username" aria-describedby="button-addon2">
				<div class="input-group-append">
					<span
					 class="btn btn-outline-secondary"
					 onclick="tambahPenulis('<?= base_url() ?>')"
					 id="button-addon2"
					 title="Simpan pengarang baru">+</span>
				</div>
			</div>
		</div>

		<!-- tambah penulis -->
		<div class="form-group row">
			<label class="col-sm-2 col-form-label"></label>
			<div class="col-sm-5">
				<textarea class="form-control" id="penulis" name="penulis"> </textarea>
			</div>
		</div>

		<!-- tahun terbit -->
		<div class="form-group row">
			<label for="Tahunterbit" class="col-sm-2 col-form-label">Tahun terbit</label>
			<div class="col-sm-10">
				<div class="input-group">
					<div class="input-group-append col-sm-4">
						<select class="form-control" id="tahun" name="id_tahun">
							<option selected="selected" value="null">Null</option>
							<?php foreach($tahun as $t) { ?>
							<option value="<?= $t->id ?>"><?= $t->nama ?></option>
							<?php } ?>
						</select>
					</div>
					<span
					 onclick="tambahTahun('<?= base_url() ?>')"
					 data-toggle="modal"
					 data-target="#popupTambah"
					 class="btn btn-outline-success"
					 title="Tambah tahun terbit baru"s>+</span>
				</div>
			</div>
		</div>

		<!-- penerbit -->
		<div class="form-group row">
			<label for="penerbit" class="col-sm-2 col-form-label">Penerbit</label>
			<div class="col-sm-10">
				<div class="input-group">
					<div class="input-group-append col-sm-4">
						<select class="form-control" id="penerbit" name="id_penerbit">
							<option selected="selected" value="null">Null</option>
							<?php foreach($penerbit as $p) { ?>
							<option value="<?= $p->id ?>"><?= $p->nama ?></option>
							<?php } ?>
						</select>
					</div>
					<span
					 onclick="tambahPenerbit('<?= base_url() ?>')"
					 data-toggle="modal"
					 data-target="#popupTambah"
					 class="btn btn-outline-success"
					 title="Tambah penerbit baru">+</span>
				</div>
			</div>
		</div>

		<!-- kategori -->
		<div class="form-group row">
			<label for="kategori" class="col-sm-2 col-form-label">Kategori</label>
			<div class="col-sm-10">
				<div class="input-group">
					<input type="text" id="filter-kategori" oninput="filterKategori('<?= base_url() ?>')" class="form-control" placeholder="Filter kategori">
					<div class="input-group-append col-sm-4">
						<select class="form-control" id="list-kategori">
							<option selected="selected">Null</option>
						</select>
					</div>
					<span
					 class="btn btn-secondary mb-2"
					 onclick="setKategori()"
					 title="Tambahkan kategori keinput">+</span>
					<br>
				</div>
			</div>
		</div>
		<div class="form-group row">
			<label for="tambahKategori" class="col-sm-2 col-form-label"> </label>
			<div class="input-group col-sm-10">
				<input type="text" class="form-control" id="tambah-kategori" placeholder="Tambah kategori" aria-label="Recipient's username" aria-describedby="button-addon2">
				<div class="input-group-append">
					<span
					 class="btn btn-outline-secondary"
					 onclick="tambahKategori('<?= base_url() ?>')"
					 id="button-addon2"
					 title="Simpan kategori baru">+</span>
				</div>
			</div>
		</div>

		<!-- tambah kategori -->
		<div class="form-group row">
			<label class="col-sm-2 col-form-label"></label>
			<div class="col-sm-5">
				<textarea class="form-control" id="kategori" name="kategori"> </textarea>
			</div>
		</div>

		<!-- lemari -->
		<div class="form-group row">
			<label for="lemari" class="col-sm-2 col-form-label">Lemari</label>
			<div class="col-sm-10">
				<div class="input-group">
					<div class="input-group-append col-sm-4">
						<select class="form-control" id="lemari" name="id_lemari">
							<option selected="selected" value="null">Null</option>
							<?php foreach($lemari as $l) :?>
								<option value="<?= $l->id ?>"> <?= $l->nama ?></option>
							<?php endforeach ?>
						</select>
					</div>
					<span
					 onclick="tambahLemari('<?= base_url() ?>')"
					 data-toggle="modal"
					 data-target="#popupTambah"
					 class="btn btn-outline-success"
					 title="Tambah lemari baru">+</span>
				</div>
			</div>
		</div>

		<!-- Jumlah halaman -->
		<div class="form-group row">
			<label for="halaman" class="col-sm-2 col-form-label">Jumlah halaman</label>
			<div class="col-sm-5">
				<input type="text" class="form-control" id="halaman" placeholder="Jumlah halaman" name="jumlah_halaman">
			</div>
		</div>

		<!-- deskripsi -->
		<div class="form-group row">
			<label for="deskripsi" class="col-sm-2 col-form-label">Deskripsi</label>
			<div class="col-sm-5">
				<textarea class="form-control" id="deskripsi" name="deskripsi"></textarea>
			</div>
		</div>

		<!-- sampul -->
		<div class="form-group row">
			<label for="sampul" class="col-sm-2 col-form-label">Sampul</label>
			<div class="col-sm-5">
				<div class="custom-file">
					<input type="file" class="custom-file-input" id="customFile" name="sampul">
					<label class="custom-file-label" for="customFile">Pilih gambar</label>
				</div>
			</div>
		</div>

		<small class="text-danger" id="pesan_gagal"></small>

		<!-- simpan -->
		<div id="submit"><span onclick="cekSimpan('<?= base_url() ?>')" class="btn btn-primary float-right">Cek</span></div>
	</form>
</div>

<!-- pop up tambah -->
<div class="modal fade" id="popupTambah" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title">Tambah <span id="dataYgAkanDitambah"></span></h3>
				<button class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div id="isiTambah"></div>
		</div>
	</div>
</div>

<!-- pop up tambah -->
<div class="modal fade" id="popupInfo" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title">Informasi tentang tambah bibliografi</h3>
				<button class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="max-height: 50vh; overflow:auto;">
				<ol>
					<li>
						<h5>Judul</h5>
						<p>Masukkan judul dengan batas karakter 255 karakter</p>
					</li>
					<li>
						<h5>Penulis(Pengarang)</h5>
						<ol>
							<li>Cari dahulu pengarang didalam kolom <i>filter pengarang</i></li>
							<li>Hasil pencarian akan muncul disebelah kanan kolom.</li>
							<li>Klik ikon plus untuk mengirim nama pengarang ke input nama pengarang</li>
							<li><b>Pengarang bisa lebih dari 1</b></li>
							<li>Jika nama pengarang belum tersedia, masukkan nama pengarang kedalam kolom <i>tambah pengarang</i>.</li>
							<li>Kemudian klik ikon plus garis tepi</li>
							<li>Maka nama pengarang akan automatis terindex kedatabase.</li>
						</ol>
					</li>
					<li>
						<h5>Tahun Terbit</h5>
						<ol>
							<li>Klik pada bagian input tahun terbit</li>
							<li>Pilih salah satu tahun</li>
							<li>Ikon plus di gunakan apabila tahun yang dimaksud tidak ada</li>
							<li>Jika tahun tidak ada, klik ikon plus bergaris tepi</li>
							<li>Pop up akan muncul. Dan masukkan tahun dimaksud di dalam kolom nama tahun</li>
							<li>Untuk deskripsi bersifat optional</li>
							<li>Simpan tahun</li>
							<li>Lalu klik lagi input tahun terbit, dan pilih tahun yang baru ditambah</li>
						</ol>
					</li>
					<li>
						<h5>Penerbit</h5>
						<ol>
							<li>Penerbit yaitu PT atau perusahaan yang mencetak atau menerbitkan buku</li>
							<li>Klik pada bagian input penerbit</li>
							<li>Pilih salah satu penerbit</li>
							<li>Ikon plus di gunakan apabila penerbit yang dimaksud tidak ada</li>
							<li>Jika penerbit tidak ada, klik ikon plus bergaris tepi</li>
							<li>Pop up akan muncul. Dan masukkan nama penerbit dimaksud di dalam kolom nama penerbit</li>
							<li>Untuk deskripsi bersifat optional</li>
							<li>Simpan penerbit</li>
							<li>Lalu klik lagi input penerbit, dan pilih penerbit yang baru ditambah</li>
						</ol>
					</li>
					<li>
						<h5>Kategori</h5>
						<ol>
							<li>Cari dahulu kategori didalam kolom <i>filter kategori</i></li>
							<li>Hasil pencarian akan muncul disebelah kanan kolom.</li>
							<li>Klik ikon plus untuk mengirim kategori ke input kategori</li>
							<li><b>Kategori bisa lebih dari 1</b></li>
							<li>Jika kategori belum tersedia, masukkan kategori baru kedalam kolom <i>tambah kategori</i>.</li>
							<li>Kemudian klik ikon plus garis tepi</li>
							<li>Maka kategori akan automatis terindex kedatabase.</li>
						</ol>
					</li>
					<li>
						<h5>Lemari</h5>
						<ol>
							<li>Lemari atau rak buku adalah dimana buku akan disimpan</li>
							<li>Klik pada bagian input lemari</li>
							<li>Pilih salah satu lemari</li>
							<li>Ikon plus di gunakan apabila lemari yang dimaksud tidak ada</li>
							<li>Jika lemari tidak ada, klik ikon plus bergaris tepi</li>
							<li>Pop up akan muncul. Dan masukkan nama lemari dimaksud di dalam kolom nama lemari</li>
							<li>Untuk deskripsi bersifat optional</li>
							<li>Simpan lemari</li>
							<li>Lalu klik lagi input lemari, dan pilih lemari yang baru ditambah</li>
						</ol>
					</li>
					<li>
						<h5>Jumlah</h5>
						<p>Jumlahalman harus berupa bilangan bulat</p>
					</li>
					<li>
						<h5>Deskripsi</h5>
						<ol>
							<li>Deskripsi bisa berupa sinopsis atau keterangan buku atau bisa juga keterangan dari admin</li>
							<li>Deskripsi berupa optional(boleh tidak diisi)</li>
						</ol>
					</li>
					<li>
						<h5>Sampul</h5>
						<ol>
							<li>Sampul harus dicantumkan</li>
							<li>Sampul harus berformat: gif, jpg, png, jpeg, atau svg</li>
							<li>Sebaiknya sampul berresolusi Potret</li>
						</ol>
					</li>
					<li>
						<h5>Cek</h5>
						<ol>
							<li>Jika sudah selesai menginput klik tombol <b>Cek</b></li>
							<li>Jika ada error akan muncul pemberitahuan dibawah input sampul</li>
							<li>Jika sudah berhasil maka tombol cek akan berubah menjadi tombol simpan</li>
							<li>Klik tombol simpan dan berhasil menambah bibliografi
							 baru</li>
						</ol>
					</li>
				</ol>
			</div>
			<div class="modal-footer">
				<button
				 class="btn btn-secondary"
				 data-dismiss="modal">
					Oke, paham.
				</button>
			</div>
		</div>
	</div>
</div>
