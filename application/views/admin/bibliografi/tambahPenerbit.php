<div class="modal-body">
	<table class="table borderless">
		<tr>
			<th>Nama</th>
			<td><input id="namaPenerbitBaru" type="text" autocomplete="off" class="form-control"></td>
		</tr>
		<tr>
			<th>Deskripsi</th>
			<td><input id="deskripsiPenerbitBaru" type="text" autocomplete="off" class="form-control"></td>
		</tr>
	</table>
</div>
<div class="modal-footer">
	<button class="btn btn-success float-right" data-dismiss="modal" onclick="tambahPenerbitProses('<?= base_url() ?>')">Simpan</button>
</div>
