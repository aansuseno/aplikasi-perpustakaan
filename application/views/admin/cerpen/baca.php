<div class="modal-body" style="max-height: 50vh; overflow: auto;">
	<h4><?= $cerpen->judul ?></h4>
	<?= $cerpen->isi ?>
</div>
<div class="modal-footer">
	<button class="btn btn-secondary" data-dismiss="modal">Tutup</button>
</div>
