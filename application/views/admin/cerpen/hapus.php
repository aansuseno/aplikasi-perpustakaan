<div class="modal-body">
	<p>Anda yakin ingin menghapus cerpen <?= $cerpen->judul ?>? Pengarang juga akan kehilangan cerpennya secara permanen, sebaiknya minta izin dahulu ke <?= $cerpen->nama_pengarang ?>.</p>
</div>
<div class="modal-footer">
	<button class="btn btn-secondary" data-dismiss="modal">Batal</button>
	<button
	 class="btn btn-danger"
	 data-dismiss="modal"
	 onclick="setAjak('datacerpen', '<?= base_url() ?>cerpen/hapusProses?id=<?= $cerpen->id ?>')">Hapus</button>
</div>
