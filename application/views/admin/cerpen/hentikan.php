<div class="modal-body">
	<p>Anda yakin ingin menonaktifkan cerpen <?= $cerpen->judul ?>?</p>
</div>
<div class="modal-footer">
	<button class="btn btn-secondary" data-dismiss="modal">Batal</button>
	<button
	 class="btn btn-danger"
	 data-dismiss="modal"
	 onclick="setAjak('datacerpen', '<?= base_url() ?>cerpen/hentikanProses?id=<?= $cerpen->id ?>')">Nonaktifkan</button>
</div>
