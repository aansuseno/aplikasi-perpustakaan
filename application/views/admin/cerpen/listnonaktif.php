<?php if($pesan == 'berhasilTayang') {?>
<div class="alert alert-primary">Cerpen berhasil ditayangkan</div>
<?php } ?>
<br><br>
<table class="table table-bordered table-hover">
<thead>
		<th scope="col">No.</th>
		<th scope="col">Judul</th>
		<th scope="col">Pengarang</th>
		<th scope="col">Kelas</th>
		<th scope="col">Aksi</th>
	</tr>
</thead>
<tbody>
	<?php $no = $halamanSekarang * $jmlData; foreach ($cerpen as $k):?>
	<tr>
		<td><?php $no++; echo $no ?></td>
		<td><?= $k->judul ?></td>
		<td><?= $k->nama_pengarang ?></td>
		<td><?= $k->kelas_pengarang ?></td>
		<td>
			<!-- hapus cerpen -->
			<span
			 class="btn btn-danger"
			 onclick="
			 	document.getElementById('judulpopup').innerHTML = 'Hapus cerpen'
			 	setAjak('isipopup', '<?= base_url() ?>cerpen/hapus?id=<?= $k->id ?>')
			 "
			 data-toggle="modal"
			 data-target="#popup"
			 title="Hapus cerpen secara permanen">
				<i class="fas fa-trash"></i>
			</span>
			<!-- end hapus cerpen -->

			<!-- tayangkan cerpen -->
			<span
			 class="btn btn-primary"
			 onclick="
			 	document.getElementById('judulpopup').innerHTML = 'Tayangkan cerpen'
			 	setAjak('isipopup', '<?= base_url() ?>cerpen/tayang?id=<?= $k->id ?>')
			 "
			 data-toggle="modal"
			 data-target="#popup"
			 title="Tayangkan cerpen. Klik untuk menayangkan cerpen">
				<i class="fas fa-play"></i>
			</span>
			<!-- end tayangkan cerpen -->

			<!-- hentikan baca -->
			<span
			 class="btn btn-info"
			 onclick="
			 	document.getElementById('judulpopup').innerHTML = 'Baca cerpen'
			 	setAjak('isipopup', '<?= base_url() ?>cerpen/baca?id=<?= $k->id ?>')
			 "
			 data-toggle="modal"
			 data-target="#popup"
			 title="Baca cerpen.">
				<i class="fas fa-book-open"></i>
			</span>
			<!-- end baca cerpen -->
		</td>
	</tr>
	<?php endforeach ?>
</tbody>
</table>

<!-- pagination -->
<div style="display: flex; flex-wrap: wrap; justify-content: space-around;">
<?php if ($jml_halaman > 1): ?>
	<ul class="pagination">
		<?php if ($halamanSekarang > 0): ?>
			<li class="page-item">
				<span
				class="page-link"
				style="cursor: pointer;"
				onclick="setAjak('datacerpen', '<?= base_url() ?>cerpen/listNonAktif?halaman=0')"
				>First
				</span>
			</li>
			<li class="page-item">
				<span
				class="page-link"
				style="cursor: pointer;"
				<?php if ($halamanSekarang > 0): ?>
					onclick="setAjak('datacerpen', '<?= base_url() ?>cerpen/listNonAktif?halaman='+<?= $halamanSekarang-1 ?>)"
				<?php endif ?>
				>&lt;
				</span>
			</li>
		<?php endif ?>
		<?php for ($i = $halamanSekarang - 2; $i <= $halamanSekarang + 4; $i++): ?>
			<?php if ($i > 0 && $i <= $jml_halaman): ?>
				<li
				class="page-item <?php if($halamanSekarang == $i - 1) { echo 'active'; }?>"
				style="cursor: pointer;">
					<span
					class="page-link"
					onclick="setAjak('datacerpen', '<?= base_url() ?>cerpen/listNonAktif?halaman='+<?= $i-1 ?>)">
						<?= $i ?>
					</span>
				</li>
			<?php endif ?>
		<?php endfor ?>
		<?php if ($halamanSekarang < $jml_halaman-1): ?>
			<li class="page-item">
				<span
				class="page-link"
				style="cursor: pointer;"
				<?php if ($halamanSekarang < $jml_halaman-1): ?>
					onclick="setAjak('datacerpen', '<?= base_url() ?>cerpen/listNonAktif?halaman='+<?= $halamanSekarang+1 ?>)"
				<?php endif ?>
				>&gt;
				</span>
			</li>
			<li class="page-item">
				<span
				class="page-link"
				style="cursor: pointer;"
				onclick="setAjak('datacerpen', '<?= base_url() ?>cerpen/listNonAktif?halaman='+<?= $jml_halaman-1 ?>)"
				>Last
				</span>
			</li>
		<?php endif ?>
	</ul>
<?php endif ?>
</div>
