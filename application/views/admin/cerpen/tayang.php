<div class="modal-body">
	<p>Anda yakin ingin menayangkan cerpen <?= $cerpen->judul ?>? Yang nantinya bisa dilihat oleh semua siswas.</p>
</div>
<div class="modal-footer">
	<button class="btn btn-secondary" data-dismiss="modal">Batal</button>
	<button
	 class="btn btn-primary"
	 data-dismiss="modal"
	 onclick="setAjak('datacerpen', '<?= base_url() ?>cerpen/tayangProses?id=<?= $cerpen->id ?>')">Tayang</button>
</div>
