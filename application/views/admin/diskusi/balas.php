<div class="modal-body" style="max-height: 70vh; overflow: auto">
	<div class="card bg-gray-dark">
		<div class="card-header"><h5><?= $pesan->nama_pengirim ?></h5></div>
		<div class="card-body">
		<?= $pesan->pesan ?>
		</div>
	</div>
	<b>Kirim sebagai :</b>
	<select class="form-control" id="nama_pengirim">
		<option value="Pustakawan">Pustakawan</option>
		<option value="Admin">Admin</option>
		<option value="Perpustakaan <?= $pengaturan->nama_perpus ?>">Perpustakaan <?= $pengaturan->nama_perpus ?></option>
		<option value="<?= $pengirim->nama ?>"><?= $pengirim->nama ?></option>
	</select>
	<b>Balas :</b><br>
	<input type="text" class="form-control" id="pesanBalas" placeholder="Tulis pesan disini.">
</div>
<div class="modal-footer">
	<button class="btn btn-secondary" data-dismiss="modal">Batal</button>
	<button class="btn btn-primary" data-dismiss="modal" onclick="
		var nama_pengirim = document.getElementById('nama_pengirim').value
		var pesan = document.getElementById('pesanBalas').value
		setAjak('data-aktivitas', '<?= base_url() ?>diskusi/balasProses?id_pengirim=<?= $pengirim->id ?>&nama_pengirim='+nama_pengirim+'&pesan='+pesan+'&jmlPesan='+jmlAktivitas+'&membalas=p<?= $pesan->id ?>')
	">Kirim</button>
</div>
