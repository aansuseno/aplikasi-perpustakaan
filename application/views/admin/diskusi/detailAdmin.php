<div class="modal-body" style="max-height: 70vh; overflow: auto">
	<table class="table table-borderless">
		<tr>
			<td>Pesan</td>
			<td>: <?= $p->pesan ?></td>
		</tr>
		<tr>
			<td>Dikirim Sebagai</td>
			<th>: <?= $p->nama_pengirim ?></th>
		</tr>
		<tr>
			<td>Nama Pengirim</td>
			<th>: <?= $p->nama_admin ?></th>
		</tr>
		<tr>
			<td>WA Pengirim</td>
			<th>: <?= $p->no_wa ?></th>
		</tr>
		<tr>
			<td>Waktu kirim</td>
			<th>: <?= $p->dikirim ?></th>
		</tr>
		<tr>
			<td>Waktu hapus</td>
			<th>: <?= $p->hapus ?></th>
		</tr>
		<tr>
			<td>Membalas pesan</td>
			<th>:
			<?php
			if($p->membalas == 'n') {
				echo '-';
			} else {
				echo $p->membalas;
			}
			 ?>
			<th>
		</tr>
	</table>
</div>
<div class="modal-footer">
	<button class="btn btn-secondary" data-dismiss="modal">Tutup</button>
</div>
