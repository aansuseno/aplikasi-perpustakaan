<div class="modal-body" style="max-height: 70vh; overflow: auto">
	<table class="table table-borderless">
		<tr>
			<td>Kirim sebagai :</td>
			<td>
				<select class="form-control" id="nama_pengirim">
					<option value="Pustakawan">Pustakawan</option>
					<option value="Admin">Admin</option>
					<option value="Perpustakaan <?= $pengaturan->nama_perpus ?>">Perpustakaan <?= $pengaturan->nama_perpus ?></option>
					<option value="<?= $pengirim->nama ?>"><?= $pengirim->nama ?></option>
				</select>
			</td>
		</tr>

		<!-- pesan -->
		<tr>
			<td>Pesan : </td>
			<td><textarea class="form-control" id="pesan"></textarea></td>
		</tr>
		<!-- end pesan -->

		<!-- rentang -->
		<tr>
			<td>Rentang pesan<i class="fas fa-info-circle" title="Waktu lama pesan sebelum pesan dihapus secara otomatis."></i> : </td>
			<td>
				<select class="form-control" id="rentang">
					<option
					 value="n"
					 title="Pesan akan selalu ada selama belum dihapus secara manual."
					 onclick="document.getElementById('hapusTgl').style.opacity = 0
					 document.getElementById('hapusJam').style.opacity = 0"
					 >
						Tak kan pernah
					</option>
					<option
					 value="o"
					 title="Pilih untuk memasukkan rentang pesan dalam input tanggal."
					 onclick="document.getElementById('hapusTgl').style.opacity = 1
					 document.getElementById('hapusJam').style.opacity = 0">
					 	Tenggat tanggal
					 </option>
					<option value="x" title="Pilih untuk memasukkan rentang pesan dalam input jam."
					 onclick="document.getElementById('hapusTgl').style.opacity = 0
					 document.getElementById('hapusJam').style.opacity = 1">Tenggat jam</option>
				</select>
			</td>
		</tr>
		<tr id="hapusTgl" style="opacity: 0">
			<td>Sampai tanggal : </td>
			<td>
				<input type="date" class="form-control" id="tglHapus">
			</td>
		</tr>
		<tr id="hapusJam" style="opacity: 0">
			<td>Sampai berapa jam : </td>
			<td>
				<input type="text" title="Pesan akan dihapus setelah sekian jam seperti yang diinputkan" class="form-control" id="jamHapus">
			</td>
		</tr>
		<!-- end rentang -->

	</table>
</div>
<div class="modal-footer">
	<button class="btn btn-secondary" data-dismiss="modal">Batal</button>
	<button class="btn btn-primary" data-dismiss="modal" onclick="
		var nama_pengirim = document.getElementById('nama_pengirim').value
		var pesanNya = document.getElementById('pesan').value
		var rentang = document.getElementById('rentang').value
		var sampai
		pesan = pesanNya.split('&').join('00100110')
		if(rentang == 'n') {
			sampai = rentang
		} else if (rentang == 'o') {
			sampai = document.getElementById('tglHapus').value
		} else if (rentang == 'x') {
			sampai = document.getElementById('jamHapus').value
		}
		setAjak('data-aktivitas', '<?= base_url() ?>diskusi/kirimProses?id_pengirim=<?= $pengirim->id ?>&nama_pengirim='+nama_pengirim+'&pesan='+pesan+'&rentang='+rentang+'&sampai='+sampai+'&jmlPesan='+jmlAktivitas)
	">Kirim</button>
</div>
