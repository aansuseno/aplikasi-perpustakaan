<div class="container">
	<div class="judul-halaman">
		<h1>Pengaturan Diskusi</h1>
	</div>
	<br>
	<form method="post" action="<?= base_url() ?>diskusi/editPengaturan">
		<table class="table table-borderless table-hover">
			<tr title="Lama pesan sebelum dihapus secara otomatis dalam satuan jam.">
				<td>Rentang pesan</td>
				<td><input class="form-control" value="<?= $perpus->lama_pesan_diskusi ?>"  name="lama_pesan_diskusi">
			</tr>
			<tr title="Jumlah maksimal karakter yang bisa siswa kirim.">
				<td>Panjang karakter pesan</td>
				<td><input class="form-control" value="<?= $perpus->max_pesan_diskusi ?>"  name="max_pesan_diskusi">
			</tr>
			<tr title="Cara siswa untuk menandai pustakawan.">
				<td>Tandai pustakawan</td>
				<td><input class="form-control" value="<?= $perpus->panggil_pustakawan ?>"  name="panggil_pustakawan">
			</tr>
		</table>
		<br>
		<button class="btn btn-warning" type="submit">Edit</button>
	</form>
</div>

<!-- pop up -->
<div class="modal fade" id="popup" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="judulPopup"></h3>
				<button class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div id="isiPopup"></div>
		</div>
	</div>
</div>
