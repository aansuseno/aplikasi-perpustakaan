<div class="container">
	<div class="judul-halaman">
		<h1>Diskusi</h1>
	</div>
	<br>
	<button
	 class="btn btn-success float-right"
	 title="Buat pesan baru."
	 data-toggle="modal"
	 data-target="#popup"
	 onclick="
	 	document.getElementById('judulPopup').innerHTML = 'Kirim pesan'
	 	setAjak('isiPopup', '<?= base_url() ?>diskusi/kirim')
	 ">
		<i class="fas fa-comment"></i>
	</button>
	<br><br>
	<!-- diskusi -->
	<div class="row">
		<div class="col-12">
			<div class="card card-primary card-outline">
				<div class="card-header">
					<h3 class="card-title">
						<i class="far fa-chart-bar"></i>
						Semua Diskusi
					</h3>
				</div>
				<script>
					ulang = 0;
					jmlAktivitas=20;
					kecepatan=200;
					sebelum='data-aktivitas';
					function ulangulang1() {
						setAjak('data-aktivitas', '<?= base_url() ?>diskusi/semuaDiskusi?jmlPesan='+jmlAktivitas)
						if(ulang == 1) {
							ulangulang()
						}
				 	}

				 	function ulangulang() {
						setTimeout(ulangulang1, kecepatan)
				 	}
				</script>
				<div class="card-body">
					<button
					 class="btn btn-primary
					 float-right"
					 title="Klik untuk melihat aktivitas diskusi secara live"
					 onclick="
					 	if(ulang == 1) {
					 		ulang = 0
					 		document.getElementById('tombolPlay').className = 'fas fa-play'
					 	} else {
					 		ulang = 1
					 		document.getElementById('tombolPlay').className = 'fas fa-pause'
					 		ulangulang1();
					 	}
					 ">
						<i id="tombolPlay" class="fas fa-play"></i>
					</button>
					<br>
					<br>
					<table class="table" id="data-aktivitas">
						<tr>
							<th>Id Pesan</th>
							<th>Role</th>
							<th>Nama Pengirim</th>
							<th>Isi Pesan</th>
							<th>Aksi</th>
						</tr>
						<?php foreach($pesan as $p) { ?>
						<tr id="p<?= $p->id ?>" class="trtrtr <?php if(strpos(strtolower($p->pesan), $pengaturan->panggil_pustakawan) !== false) { echo 'bg-primary'; }?>">
							<td>p<?= $p->id ?></td>
							<td>
								<?php
								if($p->admin_kah == 1) {
									echo 'Pustakawan';
								} else {
									echo 'Siswa';
								}
								?>
							</td>
							<td>
								<?php
								if(strlen($p->nama_pengirim) > 10) {
									echo substr($p->nama_pengirim, 0, 10).'...';
								} else {
									echo $p->nama_pengirim;
								}
								?>
							</td>
							<td>
								<?php
								if(strlen($p->pesan) > 50) {
									echo substr($p->pesan, 0, 50).'...';
								} else {
									echo $p->pesan;
								}
								?>
							</td>
							<td>
								<!-- detail -->
								<button
								 class="btn btn-info"
								 title="Info detail pesan"
								 data-toggle="modal"
								 data-target="#popup"
								 onclick="
								 	document.getElementById('judulPopup').innerHTML = 'Kirim pesan'
								 	<?php if($p->admin_kah == 0) { ?>
								 	setAjak('isiPopup', '<?= base_url() ?>diskusi/detailSiswa?id=<?= $p->id ?>')
								 	<?php } else { ?>
								 	setAjak('isiPopup', '<?= base_url() ?>diskusi/detailAdmin?id=<?= $p->id ?>')
								 	<?php } ?>
								 ">
									<i class="fas fa-info-circle"></i>
								</button>

								<!-- hapus -->
								<button
								 class="btn btn-danger"
								 title="Hapus pesan permanen. Sekali klik pesan akan langsung hapus tanpa konfirmasi!"
								 onclick="setAjak('data-aktivitas', '<?= base_url() ?>diskusi/hapus?id=<?= $p->id ?>&jmlPesan='+jmlAktivitas)">
									<i class="fas fa-times-circle"></i>
								</button>

								<!-- balas -->
								<button
								 class="btn btn-dark"
								 title="Balas pesan."
								 data-toggle="modal"
								 data-target="#popup"
								 onclick="
								 	document.getElementById('judulPopup').innerHTML = 'Balas pesan p<?= $p->id ?>'
								 	setAjak('isiPopup', '<?= base_url() ?>diskusi/balas?id=<?= $p->id ?>')
								 ">
									<i class="fas fa-comment-alt"></i>
								</button>

								<!-- membalas -->
								<?php if($p->membalas != 'n') {?>
								<a
								 class="btn"
								 title="Pesan yang dibalas."
								 href="#<?= $p->membalas ?>"
								 onclick="
								 	document.getElementById(sebelum).classList.remove('bg-warning')
								 	document.querySelector('#<?= $p->membalas ?>').classList.add('bg-warning')
								 	sebelum = '<?= $p->membalas ?>'
								 ">
									<?= $p->membalas ?>
								</a>
								<?php } ?>
							</td>
						</tr>
						<?php } ?>
					</table>
					<hr>
					<div class=" float-right">
						<button class="btn btn-secondary" style="border-radius: 50%;" onclick="kecepatan -= 200" title="Kurangi kecepatan sebesar 200 milidetik. Klik ikon play untuk melihat hasilnya."><i class="fas fa-minus"></i></button>
						<button class="btn btn-secondary" style="border-radius: 50%;" onclick="jmlAktivitas += 5" title="Tambah kecepatan sebesar 200 milidetik. Klik ikon play untuk melihat hasilnya."><i class="fas fa-plus"></i></button>

						<button class="btn btn-dark" style="border-radius: 50%;" onclick="jmlAktivitas -= 5" title="Kurangi 5 daftar pesan untuk dilihat. Klik ikon play untuk melihat hasilnya."><i class="fas fa-minus"></i></button>
						<button class="btn btn-dark" style="border-radius: 50%;" onclick="jmlAktivitas += 5" title="Tambah 5 daftar pesan untuk dilihat. Klik ikon play untuk melihat hasilnya."><i class="fas fa-plus"></i></button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end diskusi -->
</div>

<!-- pop up -->
<div class="modal fade" id="popup" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="judulPopup"></h3>
				<button class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div id="isiPopup"></div>
		</div>
	</div>
</div>
