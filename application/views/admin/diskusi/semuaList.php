<tr>
	<th>Id Pesan</th>
	<th>Role</th>
	<th>Nama Pengirim</th>
	<th>Isi Pesan</th>
	<th>Aksi</th>
</tr>
<?php foreach($pesan as $p) { ?>
<tr id="p<?= $p->id ?>" class="trtrtr <?php if(strpos(strtolower($p->pesan), $pengaturan->panggil_pustakawan) !== false) { echo 'bg-primary'; }?>">
	<td>p<?= $p->id ?></td>
	<td>
		<?php
		if($p->admin_kah == 1) {
			echo 'Pustakawan';
		} else {
			echo 'Siswa';
		}
		?>
	</td>
	<td>
		<?php
		if(strlen($p->nama_pengirim) > 10) {
			echo substr($p->nama_pengirim, 0, 10).'...';
		} else {
			echo $p->nama_pengirim;
		}
		?>
	</td>
	<td>
		<?php
		if(strlen($p->pesan) > 50) {
			echo substr($p->pesan, 0, 50).'...';
		} else {
			echo $p->pesan;
		}
		?>
	</td>
	<td>
		<!-- detail -->
		<button
		 class="btn btn-info"
		 title="Info detail pesan"
		 data-toggle="modal"
		 data-target="#popup"
		 onclick="
		 	document.getElementById('judulPopup').innerHTML = 'Kirim pesan'
		 	<?php if($p->admin_kah == 0) { ?>
		 	setAjak('isiPopup', '<?= base_url() ?>diskusi/detailSiswa?id=<?= $p->id ?>')
		 	<?php } else { ?>
		 	setAjak('isiPopup', '<?= base_url() ?>diskusi/detailAdmin?id=<?= $p->id ?>')
		 	<?php } ?>
		 ">
			<i class="fas fa-info-circle"></i>
		</button>

		<!-- hapus -->
		<button
		 class="btn btn-danger"
		 title="Hapus pesan permanen. Sekali klik pesan akan langsung hapus tanpa konfirmasi!"
		 onclick="setAjak('data-aktivitas', '<?= base_url() ?>diskusi/hapus?id=<?= $p->id ?>&jmlPesan='+jmlAktivitas)">
			<i class="fas fa-times-circle"></i>
		</button>

		<!-- balas -->
		<button
		 class="btn btn-dark"
		 title="Balas pesan."
		 data-toggle="modal"
		 data-target="#popup"
		 onclick="
		 	document.getElementById('judulPopup').innerHTML = 'Balas pesan p<?= $p->id ?>'
		 	setAjak('isiPopup', '<?= base_url() ?>diskusi/balas?id=<?= $p->id ?>')
		 ">
			<i class="fas fa-comment-alt"></i>
		</button>

		<!-- membalas -->
		<?php if($p->membalas != 'n') {?>
		<a
		 class="btn"
		 title="Pesan yang dibalas."
		 href="#<?= $p->membalas ?>"
		 onclick="
		 	document.getElementById(sebelum).classList.remove('bg-warning')
		 	document.querySelector('#<?= $p->membalas ?>').classList.add('bg-warning')
		 	sebelum = '<?= $p->membalas ?>'
		 ">
			<?= $p->membalas ?>
		</a>
		<?php } ?>
	</td>
</tr>
<?php } ?>
