</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
	<!-- Control sidebar content goes here -->
	<div class="p-3">
	<h5>Title</h5>
	<p>Sidebar content</p>
	</div>
</aside>
<!-- /.control-sidebar -->

<!-- Main Footer -->
<footer class="main-footer">
	<!-- To the right -->
	<div class="float-right d-none d-sm-inline">
	Anything you want
	</div>
	<!-- Default to the left -->
	<strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
</footer>
</div>
<!-- ./wrapper -->

	<!-- REQUIRED SCRIPTS -->

	<!-- jQuery -->
	<script src="<?= base_url()?>/assets/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="<?= base_url()?>/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- AdminLTE App -->
	<script src="<?= base_url()?>/assets/dist/js/adminlte.min.js"></script>
	<!-- js tambahan -->
	<script type="text/javascript" src="<?= base_url()?>/assets/js/js.js"></script>
	<!-- js ajax -->
	<script type="text/javascript" src="<?= base_url()?>/assets/js/ajax.js"></script>
	<!-- FLOT CHARTS -->
	<script src="<?= base_url()?>/assets/plugins/flot/jquery.flot.js"></script>
	<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
	<script src="<?= base_url()?>/assets/plugins/flot/plugins/jquery.flot.resize.js"></script>
	<!-- FLOT PIE PLUGIN - also used to draw donut charts -->
	<script src="<?= base_url()?>/assets/plugins/flot/plugins/jquery.flot.pie.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="<?= base_url()?>/assets/dist/js/demo.js"></script>

	<?php if($halaman == 'beranda') : ?>
	<script>
		$(function () {

			/*
			 * BAR CHART
			 * ---------
			 */

			var bar_data = {
				data : [[1,<?= $peminjamBulan[11] ?>], [2,<?= $peminjamBulan[10] ?>], [3,<?= $peminjamBulan[9] ?>], [4,<?= $peminjamBulan[8] ?>], [5,<?= $peminjamBulan[7] ?>], [6,<?= $peminjamBulan[6] ?>], [7,<?= $peminjamBulan[5] ?>], [8,<?= $peminjamBulan[4] ?>], [9,<?= $peminjamBulan[3] ?>], [10,<?= $peminjamBulan[2] ?>], [11,<?= $peminjamBulan[1] ?>], [12,<?= $peminjamBulan[0] ?>]],
				bars: { show: true }
			}
			$.plot('#bar-chart', [bar_data], {
				grid	: {
					borderWidth: 1,
					borderColor: '#f3f3f3',
					tickColor: '#f3f3f3'
				},
				series: {
					 bars: {
						show: true, barWidth: 0.5, align: 'center',
					},
				},
				colors: ['#3c8dbc'],
				xaxis : {
					ticks: [[1,'12 BT'], [2,'11 BT'], [3,'10 BT'], [4,'9 BT'], [5,'8 BT'], [6,'7 BT'], [7,'6 BT'], [8,'5 BT'], [9,'4 BT'], [10,'3 BT'], [11,'2 BT'], [12,'1 BT']]
				}
			})
			/* END BAR CHART */

		})
	</script>
	<?php endif ?>
</body>
</html>
