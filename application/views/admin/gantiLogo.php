<form method="post" action="<?= base_url() ?>admin/gantiLogoProses" enctype="multipart/form-data">
	<table class="table table-borderless" style="max-width: 200px;margin: auto">
		<tr>
			<td>
				Logo lama
			</td>
			<td>
				: <img src="<?= base_url() ?>gambar/<?= $perpus->logo ?>" style="max-width: 100px;">
			</td>
		</tr>
		<tr>
			<td>
				Logo baru
			</td>
			<td>
				: <input type="file" name="logo">
			</td>
		</tr>
	</table>
	<hr>
	<input type="hidden" name="id" value="<?= $perpus->id ?>">
	<input type="submit" class="btn btn-success" value="Edit Logo">
</form>
