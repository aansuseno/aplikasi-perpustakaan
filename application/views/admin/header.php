<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title><?= $judul_halaman ?> Admin - Perpustakaan <?= $nama_sekolah?></title>
	<!-- Google Font: Source Sans Pro -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	<!-- Font Awesome Icons -->
	<link rel="stylesheet" href="<?= base_url()?>/assets/plugins/fontawesome-free/css/all.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?= base_url()?>/assets/dist/css/adminlte.min.css">
	<!-- css tambahan -->
	<link rel="stylesheet" href="<?= base_url()?>/assets/css/style.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

	<!-- Navbar -->
	<nav class="main-header navbar navbar-expand navbar-white navbar-light">
		<!-- Left navbar links -->
		<ul class="navbar-nav">
			<li class="nav-item">
				<a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
			</li>
		</ul>

		<!-- Right navbar links -->
		<ul class="navbar-nav ml-auto">
			<li class="nav-item" title="Tampil dalam layar penuh">
				<a class="nav-link" data-widget="fullscreen" href="#" role="button">
					<i class="fas fa-expand-arrows-alt"></i>
				</a>
			</li>

			<!-- tombol logout -->
			<li class="nav-item" title="Keluar dari akun">
				<a class="nav-link text text-danger" href="<?= base_url().'admin/logout' ?>" role="button">
					<i class="fas fa-sign-out-alt"></i>
				</a>
			</li>
		</ul>
	</nav>
	<!-- /.navbar -->

	<!-- Main Sidebar Container -->
	<aside class="main-sidebar sidebar-dark-primary elevation-4" style="height: 100%;overflow: auto;">
		<!-- Logo Sekolah -->
		<a
		 href="<?= base_url('admin/pengaturan') ?>"
		 class="brand-link"
		 title="Halaman edit data sekolah">
			<img src="<?= base_url()?>gambar/<?= $perpus->logo ?>" alt="Logo" class="brand-image">
			<span class="brand-text font-weight-light"><?= $nama_sekolah ?></span>
		</a>

		<!-- Sidebar -->
		<div class="sidebar">
			<!-- Sidebar user panel -->
			<div
			 class="user-panel mt-3 pb-3 mb-3 d-flex"
			 title="Halaman edit profil. Seperti edit: username, password, bio, dan lainnya.">
				<div class="image">
					<img src="<?= base_url()?>/assets/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
				</div>
				<div class="info">
					<a href="<?= base_url().'admin/profil' ?>" class="d-block"><?= $nama_user->nama ?></a>
				</div>
			</div>

			<!-- Sidebar Menu -->
			<nav class="mt-2">
				<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
					<!-- Add icons to the links using the .nav-icon class
					with font-awesome or any other icon font library -->
					<li
					 class="nav-item"
					 title="Halaman beranda">
						<a href="<?= base_url() ?>admin" class="nav-link <?php if($halaman == 'beranda') echo'active' ?>">
							<i class="nav-icon fas fa-home"></i>
							<p>Beranda</p>
						</a>
					</li>

					<!-- transaksi -->
					<li
					 class="nav-item <?php if($halaman == 'transaksi' || $halaman == 'belumkembali') echo'menu-open' ?>"
					 title="Halaman transaksi">
						<a href="#" class="nav-link <?php if($halaman == 'transaksi' || $halaman == 'belumkembali') echo'active' ?>">
							<i class="nav-icon fas fa-exchange-alt" style="transform: rotate(90deg);"></i>
							<p>
								Transaksi
								<i class="right fas fa-angle-left"></i>
							</p>
						</a>
						<ul class="nav nav-treeview">
							<li class="nav-item">
								<a
								 href="<?= base_url() ?>transaksi/index?page=blmkembali"
								 class="nav-link <?php if($halaman == 'belumkembali') echo'active' ?>"
								 title="Menampilkan semua transaksi peminjaman yg sedang dipinjam">
									<i class="far fa-circle nav-icon"></i>
									<p>Sedang Dipinjam</p>
								</a>
								<a
								 href="<?= base_url() ?>transaksi/"
								 class="nav-link <?php if($halaman == 'transaksi') echo'active' ?>"
								 title="Menampilkan semua transaksi yang sudah kembali.">
									<i class="far fa-circle nav-icon"></i>
									<p>Sudah Kembali</p>
								</a>
							</li>
						</ul>
					</li>
					<!-- rampung transaksi -->

					<!-- diskusi -->
					<li class="nav-item <?php if($halaman == 'semuaDiskusi' || $halaman == 'pengaturanDiskusi') echo'menu-open' ?>" title="Halaman diskusi">
						<a href="#" class="nav-link <?php if($halaman == 'semuaDiskusi' || $halaman == 'pengaturanDiskusi') echo'active' ?>">
							<i class="nav-icon fas fa-comment"></i>
							<p>
								Diskusi
								<i class="right fas fa-angle-left"></i>
							</p>
						</a>
						<ul class="nav nav-treeview">
							<li class="nav-item">
								<a
								 href="<?= base_url() ?>diskusi"
								 class="nav-link <?php if($halaman == 'semuaDiskusi') echo'active' ?>"
								 title="Semua diskusi">
									<i class="far fa-circle nav-icon"></i>
									<p>Semua Obrolan</p>
								</a>
							</li>
							<li class="nav-item">
								<a
								 href="<?= base_url() ?>diskusi/pengaturan"
								 class="nav-link <?php if($halaman == 'pengaturanDiskusi') echo'active' ?>"
								 title="Halaman Pengaturan diskusi">
									<i class="far fa-circle nav-icon"></i>
									<p>Pengaturan</p>
								</a>
							</li>
						</ul>
					</li>
					<!-- rampung diskusi -->

					<!-- katalog -->
					<li class="nav-item <?php if($halaman == 'daftarCerpenAktif'|| $halaman == 'daftarCerpenNonAktif') echo'menu-open' ?>" title="Halaman pengelolaan cerpen">
						<a href="#" class="nav-link <?php if($halaman == 'daftarCerpenAktif' || $halaman == 'daftarCerpenNonAktif') echo'active' ?>">
							<i class="nav-icon fas fa-scroll"></i>
							<p>
								Cerpen
								<i class="right fas fa-angle-left"></i>
							</p>
						</a>
						<ul class="nav nav-treeview">
							<li class="nav-item">
								<a
								 href="<?= base_url() ?>cerpen/nonaktif"
								 class="nav-link <?php if($halaman == 'daftarCerpenNonAktif') echo'active' ?>"
								 title="Daftar cerpen yang belum atau tidak aktif">
									<i class="far fa-circle nav-icon"></i>
									<p>Cerpen Belum Aktif</p>
								</a>
							</li>
							<li class="nav-item">
								<a
								 href="<?= base_url() ?>cerpen/aktif"
								 class="nav-link <?php if($halaman == 'daftarCerpenAktif') echo'active' ?>"
								 title="Daftar cerpen yang sudah aktif">
									<i class="far fa-circle nav-icon"></i>
									<p>Cerpen Aktif</p>
								</a>
							</li>
						</ul>
					</li>
					<!-- rampung katalog -->

					<!-- bibliografi -->
					<li
					 class="nav-item <?php if($halaman == 'tambahBibliografi' || $halaman == 'daftarBibliografi' || $halaman == 'detailBibliografi') echo'menu-open' ?>"
					 title="Kelola Buku/Biblografi">
						<a
						 href="#"
						 class="nav-link <?php if($halaman == 'tambahBibliografi' || $halaman == 'daftarBibliografi' || $halaman == 'detailBibliografi') echo'active' ?>">
							<i class="nav-icon fas fa-book"></i>
							<p>
								Bibliografi
								<i class="right fas fa-angle-left"></i>
							</p>
						</a>
						<ul class="nav nav-treeview">
							<li class="nav-item">
								<a href="<?= base_url() ?>bibliografi/tambah" class="nav-link <?php if($halaman == 'tambahBibliografi') echo'active' ?>"
								 title="Tambah Bibliografi baru">
									<i class="far fa-circle nav-icon"></i>
									<p>Tambah Bibliografi</p>
								</a>
							</li>
							<li class="nav-item">
								<a
								 href="<?= base_url() ?>bibliografi/daftar" class="nav-link <?php if($halaman == 'daftarBibliografi' || $halaman == 'detailBibliografi') echo'active' ?>"
								 title="Daftar semua bibliografi">
									<i class="far fa-circle nav-icon"></i>
									<p>Daftar Bibliografi</p>
								</a>
							</li>
						</ul>
					</li>
					<!-- rampung bibliografi -->

					<!-- katalog -->
					<li class="nav-item <?php if($halaman == 'tambahKatalog' || $halaman == 'daftarKatalog' || $halaman == 'detailKatalog') echo'menu-open' ?>" title="Halaman katalog">
						<a href="#" class="nav-link <?php if($halaman == 'tambahKatalog' || $halaman == 'daftarKatalog' || $halaman == 'detailKatalog') echo'active' ?>">
							<i class="nav-icon fas fa-swatchbook"></i>
							<p>
								Katalog
								<i class="right fas fa-angle-left"></i>
							</p>
						</a>
						<ul class="nav nav-treeview">
							<li class="nav-item">
								<a
								 href="<?= base_url() ?>katalog/tambah"
								 class="nav-link <?php if($halaman == 'tambahKatalog') echo'active' ?>"
								 title="Buat katalog baru">
									<i class="far fa-circle nav-icon"></i>
									<p>Tambah Katalog</p>
								</a>
							</li>
							<li class="nav-item">
								<a
								 href="<?= base_url() ?>katalog/daftar"
								 class="nav-link <?php if($halaman == 'daftarKatalog' || $halaman == 'detailKatalog') echo'active' ?>"
								 title="Semua katalog">
									<i class="far fa-circle nav-icon"></i>
									<p>Daftar Katalog</p>
								</a>
							</li>
						</ul>
					</li>
					<!-- rampung katalog -->

					<!-- lemari -->
					<li class="nav-item <?php if($halaman == 'tambahLemari' || $halaman == 'daftarLemari' || $halaman == 'detailLemari') echo'menu-open' ?>" title="Kelola lemari.">
						<a href="#" class="nav-link <?php if($halaman == 'tambahLemari' || $halaman == 'daftarLemari' || $halaman == 'detailLemari') echo'active' ?>">
							<i class="nav-icon fas fa-swatchbook"></i>
							<p>
								Lemari
								<i class="right fas fa-angle-left"></i>
							</p>
						</a>
						<ul class="nav nav-treeview">
							<li class="nav-item">
								<a
								 href="<?= base_url() ?>lemari/tambah"
								 class="nav-link <?php if($halaman == 'tambahLemari') echo'active' ?>"
								 title="Tambah lemari baru">
									<i class="far fa-circle nav-icon"></i>
									<p>Tambah Lemari</p>
								</a>
							</li>
							<li class="nav-item">
								<a
								 href="<?= base_url() ?>lemari/tampil"
								 class="nav-link <?php if($halaman == 'daftarLemari' || $halaman == 'detailLemari') echo'active' ?>"
								 title="Tampil semua lemari">
									<i class="far fa-circle nav-icon"></i>
									<p>Daftar Lemari</p>
								</a>
							</li>
						</ul>
					</li>
					<!-- rampung Lemari -->

					<!-- Penulis -->
					<li class="nav-item <?php if($halaman == 'daftarPenulis') echo'menu-open' ?>" title="Halaman kelola penulis/pengarang">
						<a href="<?php echo base_url() ?>penulis" class="nav-link <?php if($halaman == 'daftarPenulis') echo'active' ?>">
							<i class="nav-icon fas fa-feather-alt"></i>
							<p>
								Penulis
								<i class="right fas fa-angle-left"></i>
							</p>
						</a>
					</li>
					<!-- rampung Penulis -->

					<!-- Penerbit -->
					<li class="nav-item <?php if($halaman == 'daftarPenerbit') echo'menu-open' ?>" title="Halaman kelola penerbit">
						<a href="<?php echo base_url() ?>penerbit" class="nav-link <?php if($halaman == 'daftarPenerbit') echo'active' ?>">
							<i class="nav-icon fas fa-industry"></i>
							<p>
								Penerbit
								<i class="right fas fa-angle-left"></i>
							</p>
						</a>
					</li>
					<!-- rampung Penerbit -->

					<!-- Kategori -->
					<li class="nav-item <?php if($halaman == 'daftarKategori') echo'menu-open' ?>" title="Halaman kelola kategori">
						<a href="<?php echo base_url() ?>kategori" class="nav-link <?php if($halaman == 'daftarKategori') echo'active' ?>">
							<i class="nav-icon fas fa-tags"></i>
							<p>
								Kategori
								<i class="right fas fa-angle-left"></i>
							</p>
						</a>
					</li>
					<!-- rampung Kategori -->

					<!-- tahun -->
					<li class="nav-item <?php if($halaman == 'tahun') echo'menu-open' ?>" title="Halaman kelola tahun terbit">
						<a href="<?php echo base_url() ?>tahun" class="nav-link <?php if($halaman == 'tahun') echo'active' ?>">
							<i class="nav-icon fas fa-table"></i>
							<p>
								Tahun
								<i class="right fas fa-angle-left"></i>
							</p>
						</a>
					</li>
					<!-- rampung tahun -->


					<!-- siswa -->
					<li class="nav-item <?php if($halaman == 'tambah_siswa' || $halaman == 'tampilsiswa') echo'menu-open' ?>" title="Kelola siswa">
						<a href="#" class="nav-link <?php if($halaman == 'tambah_siswa' || $halaman == 'tampilsiswa') echo'active' ?>">
							<i class="nav-icon fas fa-user-graduate"></i>
							<p>
								Siswa
								<i class="right fas fa-angle-left"></i>
							</p>
						</a>
						<ul class="nav nav-treeview">
							<li class="nav-item" title="Tambah siswa">
								<a href="<?= base_url() ?>siswa/tambah" class="nav-link <?php if($halaman == 'tambah_siswa') echo'active' ?>">
									<i class="far fa-circle nav-icon"></i>
									<p>Tambah Siswa</p>
								</a>
							</li>
							<li class="nav-item" title="Tampil siswa">
								<a href="<?= base_url() ?>siswa/tampil" class="nav-link <?php if($halaman == 'tampilsiswa') echo'active' ?>">
									<i class="far fa-circle nav-icon"></i>
									<p>Tampil Siswa</p>
								</a>
							</li>
						</ul>
					</li>
					<!-- rampung siswa -->

					<!-- kelas -->
					<li class="nav-item <?php if($halaman == 'tambahKelas' || $halaman == 'daftarKelas' || $halaman == 'detailKelas') echo'menu-open' ?>" title="Halaman kelola kelas">
						<a href="#" class="nav-link <?php if($halaman == 'tambahKelas' || $halaman == 'daftarKelas' || $halaman == 'detailKelas') echo'active' ?>">
							<i class="nav-icon fas fa-graduation-cap"></i>
							<p>
								Kelas
								<i class="right fas fa-angle-left"></i>
							</p>
						</a>
						<ul class="nav nav-treeview">
							<li class="nav-item" title="Tambah kelas baru">
								<a href="<?= base_url() ?>kelas/tambah" class="nav-link <?php if($halaman == 'tambahKelas') echo'active' ?>">
									<i class="far fa-circle nav-icon"></i>
									<p>Tambah kelas</p>
								</a>
							</li>
							<li class="nav-item" title="Tampil semua kelas">
								<a href="<?= base_url() ?>kelas/daftar" class="nav-link <?php if($halaman == 'daftarKelas' || $halaman == 'detailKelas') echo'active' ?>">
									<i class="far fa-circle nav-icon"></i>
									<p>Daftar kelas</p>
								</a>
							</li>
						</ul>
					</li>
					<!-- rampung kelas -->

					<!-- petugas -->
					<?php if($nama_user->wewenang == 'admin') { ?>
					<li class="nav-item <?php if($halaman == 'daftarPetugas' || $halaman == 'detailPetugas' || $halaman == 'aktifPetugas' || $halaman == 'bekuPetugas') echo'menu-open' ?>" title="Kelola petugas">
						<a href="#" class="nav-link <?php if($halaman == 'daftarPetugas' || $halaman == 'daftarPengurus' || $halaman == 'detailPengurus' || $halaman == 'aktifPetugas' || $halaman == 'bekuPetugas') echo'active' ?>">
							<i class="nav-icon fas fa-user-tag"></i>
							<p>
								Petugas
								<i class="right fas fa-angle-left"></i>
							</p>
						</a>
						<ul class="nav nav-treeview">
							<li class="nav-item" title="Petugas belum aktif">
								<a href="<?= base_url() ?>petugas/baru" class="nav-link <?php if($halaman == 'daftarPetugas') echo'active' ?>">
									<i class="far fa-circle nav-icon"></i>
									<p>Petugas baru</p>
								</a>
							</li>
							<li class="nav-item" title="Petugas aktif">
								<a href="<?= base_url() ?>petugas/aktif" class="nav-link <?php if($halaman == 'aktifPetugas') echo'active' ?>">
									<i class="far fa-circle nav-icon"></i>
									<p>Petugas aktif</p>
								</a>
							</li>
							<li class="nav-item" title="Petugas yang dibekukan">
								<a href="<?= base_url() ?>petugas/beku" class="nav-link <?php if($halaman == 'bekuPetugas') echo'active' ?>">
									<i class="far fa-circle nav-icon"></i>
									<p>Petugas Dibekukan</p>
								</a>
							</li>
						</ul>
					</li>
					<?php } ?>
					<!-- rampung petugas -->

					<!-- laporan -->
					<li class="nav-item <?php if($halaman == 'laporanTransaksi' || $halaman == 'laporanBuku' || $halaman == 'laporanSiswa' || $halaman == 'bebasPerpus') echo'menu-open' ?>">
						<a href="#" class="nav-link <?php if($halaman == 'laporanTransaksi' || $halaman == 'laporanBuku' || $halaman == 'laporanSiswa' || $halaman == 'bebasPerpus') echo'active' ?>">
							<i class="nav-icon fas fa-file-excel"></i>
							<p>
								Laporan
								<i class="right fas fa-angle-left"></i>
							</p>
						</a>
						<ul class="nav nav-treeview">
							<li class="nav-item" title="Laporan tentang transaksi">
								<a href="<?= base_url() ?>laporan/transaksi" class="nav-link <?php if($halaman == 'laporanTransaksi') echo'active' ?>">
									<i class="far fa-circle nav-icon"></i>
									<p>Transaksi</p>
								</a>
							</li>
							<li class="nav-item" title="Laporan tentang buku">
								<a href="<?= base_url() ?>laporan/buku" class="nav-link <?php if($halaman == 'laporanBuku') echo'active' ?>">
									<i class="far fa-circle nav-icon"></i>
									<p>Buku</p>
								</a>
							</li>
							<li class="nav-item" title="Laporan tentang siswa">
								<a href="<?= base_url() ?>laporan/siswa" class="nav-link <?php if($halaman == 'laporanSiswa') echo'active' ?>">
									<i class="far fa-circle nav-icon"></i>
									<p>Siswa</p>
								</a>
							</li>
							<li class="nav-item" title="Laporan bebas perpustakaan">
								<a href="<?= base_url() ?>laporan/bebasPerpus" class="nav-link <?php if($halaman == 'bebasPerpus') echo'active' ?>">
									<i class="far fa-circle nav-icon"></i>
									<p>Bebas Perpustakaan</p>
								</a>
							</li>
						</ul>
					</li>
					<!-- rampung laporan -->
				</ul>
			</nav>
			<!-- /.sidebar-menu -->
		</div>
		<!-- /.sidebar -->
	</aside>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
