<div class="container">
	<div class="judul-halaman">
		<h1>Beranda</h1>
	</div>

	<!-- aktivitas siswa -->
	<div class="row">
		<div class="col-12">
			<div class="card card-primary card-outline">
				<div class="card-header">
					<h3 class="card-title">
						<i class="far fa-chart-bar"></i>
						Rekaman dalam 12 bulan terakhir
					</h3>
				</div>
				<div class="card-body">
					<div id="bar-chart" style="height: 300px;"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.row -->
	<small>* BT: Bulan terakhir</small>

	<!-- raking meminjam dalam 1 bulan -->
	<div class="row">
		<div class="col-12">
			<div class="card card-primary card-outline">
				<div class="card-header">
					<h3 class="card-title">
						<i class="far fa-chart-bar"></i>
						Rekaman siswa meminjam
						<input
						 type="number"
						 id="jmlBulan"
						 title="Masukkan jml bulan antara 1 sampai 36 bulan"
						 style="max-width: 50px;"
						 value="6"
						 oninput="
						 blnV = document.getElementById('jmlBulan').value
						 if(blnV >= 1 && blnV <= 36) {
							setAjak('terbanyakMeminjam', '<?= base_url() ?>admin/terbanyakMeminjam?bln='+blnV)
						 }
						 ">
						bulan terakhir
					</h3>
				</div>
				<div class="card-body" id="terbanyakMeminjam">
					<table class="table">
						<tr>
							<th>Peringkat</th>
							<th>NIS</th>
							<th>Nama</th>
							<th>Kelas</th>
							<th>Jumlah buku yang dipinjam</th>
						</tr>
						<?php $no = 1;foreach($detailSiswa1Bln as $s) { ?>
							<tr>
								<th style="text-align: center">
									<?php
									if($no <= 1) {
										echo '<i class="fas fa-star" style="color: gold;"></i><i class="fas fa-star" style="color: gold;"></i><i class="fas fa-star" style="color: gold;"></i>';
									} else if($no == 2) {
										echo '<i class="fas fa-star" style="color: gold;"></i><i class="fas fa-star" style="color: gold;"></i>';
									} else if($no == 3) {
										echo '<i class="fas fa-star" style="color: gold;"></i>';
									} else {
										echo $no;
									}
									$no++; ?>
								</th>
								<td><?= $s->nis ?></td>
								<td><?= $s->nama ?></td>
								<td><?= $s->kelas ?></td>
								<td>
									<div class="progress mt-3 mb-1 rounded" style="height: 20px;" title="Sudah meminjam <?= $s->jmlBuku ?> buku">
									<div class="progress-bar bg-primary" role="progressbar" style="width:
										<?php
										echo ($s->jmlBuku*100.00)/$pinjamTerbanyak;
										?>%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
								</td>
							</tr>
						<?php } ?>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- /.row -->

	<!-- aktivitas siswa -->
	<?php if($nama_user->wewenang == 'admin') { ?>
	<div class="row">
		<div class="col-12">
			<div class="card card-primary card-outline">
				<div class="card-header">
					<h3 class="card-title">
						<i class="far fa-chart-bar"></i>
						Aktivitas siswa
					</h3>
				</div>
				<script>
					ulang = 0;
					jmlAktivitas=5;
					function ulangulang1() {
						setAjak('data-aktivitas', '<?= base_url() ?>admin/aktivitasSiswa?jmlAktivitas='+jmlAktivitas)
						if(ulang == 1) {
							ulangulang()
						}
				 	}

				 	function ulangulang() {
						setTimeout(ulangulang1, 200)
				 	}
				</script>
				<div class="card-body">
					<button
					 class="btn btn-primary
					 float-right"
					 title="Klik untuk melihat aktivitas secara live"
					 onclick="
					 	if(ulang == 1) {
					 		ulang = 0
					 	} else {
					 		ulang = 1
					 		ulangulang1();
					 	}
					 ">
						<i class="fas fa-play"></i>
					</button>
					<br>
					<br>
					<table class="table" id="data-aktivitas">
						<tr>
							<th>Nama</th>
							<th>Waktu</th>
							<th>Aktivitas</th>
						</tr>
						<?php foreach($data_siswa as $s) { ?>
						<tr>
							<td><?= $s->nama ?></td>
							<td><?= $s->terakhir_aktif ?></td>
							<td><?= $s->aktivitas ?></td>
						</tr>
						<?php } ?>
					</table>
					<div class=" float-right">
						<button class="btn btn-dark" style="border-radius: 50%;" onclick="jmlAktivitas -= 5" title="Kurangi 5 daftar siswa untuk dilihat aktivittasnya. Jangan lupa klik ikon play untuk melihat hasilnya."><i class="fas fa-minus"></i></button>
						<button class="btn btn-dark" style="border-radius: 50%;" onclick="jmlAktivitas += 5" title="Tambah 5 daftar siswa untuk dilihat aktivittasnya. Jangan lupa klik ikon play untuk melihat hasilnya."><i class="fas fa-plus"></i></button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.row -->
	<?php } ?>

	
</div>
