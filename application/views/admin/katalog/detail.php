<div class="modal-body">
	<table class="table borderless">
		<img src="<?= base_url() ?>gambar/<?= $buku1->sampul ?>"  style="max-width: 200px;border-radius: 10px; box-shadow: 0 2px 5px rgba(0,0,0,0.6)" class="sampul-detail">
		<tr>
			<th>Kode Katalog</th>
			<th><?= $katalog1->kode_katalog ?></th>
		</tr>
		<tr>
			<th>Judul</th>
			<td><?= $buku1->judul ?></td>
		</tr>
		<tr>
			<th>Lemari</th>
			<td><?= $lemari1->nama ?></td>
		</tr>
		<tr>
			<th>Deskripsi</th>
			<td><?= $katalog1->deskripsi ?></td>
		</tr>
	</table>
</div>
<div class="modal-footer">
	<span class="btn btn-secondary float-right" data-dismiss="modal">Tutup &times;</span>
</div>