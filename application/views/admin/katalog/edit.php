<div class="modal-body">
	<span id="pesanerror"></span>
	<table class="table borderless">
		<tr>
			<th>Kode katalog</th>
			<td>
				<input type="text" autocomplete="off" value="<?= $katalog1->kode_katalog ?>" class="form-control" id="kode-katalog" placeholder="kode katalog">
			</td>
		</tr>
		<!-- kode buku -->
		<tr>
			<th>Kode buku</th>
			<td>
				<input type="text" autocomplete="off" oninput="cariBuku('<?= base_url() ?>')" class="form-control" id="cari-buku" placeholder="cari buku">
			</td>
		</tr>
		<tr>
			<td> </td>
			<td>
				<input type="text" autocomplete="off" oninput="cariBukuId('<?= base_url() ?>')" class="form-control" id="cari-buku-id" placeholder="id buku">
			</td>
		</tr>
		<tr>
			<td> </td>
			<td>
				<select class="form-control" id="list-buku">
					<option value="<?= $buku->id ?>"><?= $buku->judul ?></option>
				</select>
			</td>
		</tr>
		<!-- kondisi -->
		<tr>
			<td>Kondisi</td>
			<td>
				<select class="form-control" id="kondisi">
					<option value="Baik" <?php if ($katalog1->kondisi == 'Baik'): ?>
						selected="selected"
					<?php endif ?>>Baik</option>
					<option value="Baru" <?php if ($katalog1->kondisi == 'Baru'): ?>
						selected="selected"
					<?php endif ?>>Baru</option>
					<option value="Rusak" <?php if ($katalog1->kondisi == 'Rusak'): ?>
						selected="selected"
					<?php endif ?>>Rusak</option>
				</select>
			</td>
		</tr>
		<!-- deskripsi -->
		<tr>
			<td>Deskripsi</td>
			<td>
				<textarea id="deskripsi" class="form-control"></textarea>
			</td>
		</tr>
	</table>
</div>

<div class="modal-footer">
	<!-- simpan -->
	<div class="sumit">
		<span class="btn btn-primary float-right" data-dismiss="modal" onclick="cekUpdate('<?= base_url() ?>', <?= $katalog1->id_katalog ?>)">Simpan</span>
	</div>
</div>