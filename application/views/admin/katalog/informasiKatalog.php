<div class="modal-body" style="max-height: 50vh; overflow: auto;">
	<ol>
		<li>
			<h4>Info</h4>
			<p>Klik icon <i>i</i> untuk melihat informasi lebaih detail dari katalog.</p>
		</li>
		<li>
			<h4>Hapus</h4>
			<ol>
				<li>Klik ikon tempat sampah untuk mengahapus katalog <b>permanen</b></li>
				<li><b>Tidak disarankan untuk menghapus katalog</b> yang sudah terhubung dengan transaksi</li>
				<li>Hapuslah katalog jika baru ditambahkan</li>
				<li>Sistem akan error jika katalog dihapus dan sudah pernah melakukan transaksi</li>
			</ol>
		</li>
		<li>
			<h4>Edit</h4>
			<ol>
				<li>Klik ikon edit untuk mengedit</li>
				<li>Edit kode katalog. Dan tidak boleh menggunakan tanda <b>&</b></li>
				<li>
					<h4>Kode buku</h4>
					<ul>
						<li>Kode buku yang dimaksud yaitu id buku.</li>
						<li>
							Jika ingat judul buku:
							<ol>
								<li>Inputkan judul buku di kolom pertama input kode buku</li>
								<li>Di kolom ketika akan muncul dropdown. Pilih salah satu buku.</li>
								<li>Hasil pencarian hanya akan menampilkan 10 buku</li>
							</ol>
						</li>
						<li>
							Jika merasa ribet, bisa menggunakan id buku:
							<ol>
								<li>Id buku bisa dilihat di <a href="<?= base_url() ?>bibliografi/daftar">halaman daftar buku</a></li>
								<li>Maasukkan id buku di kolom kedua input kode buku</li>
								<li>Pastikan id ditulis dengan benar</li>
								<li>Judul buku akan muncul di kolom ketiga</li>
							</ol>
						</li>
					</ul>
				</li>
				<li>Kondisi buku sesuaikan sesuai dengan buku yang berkode katalog
				</li>
				<li>Deskripsi bersifat optional</li>
				<li>Simpan</li>
			</ol>
		</li>
	</ol>
</div>
<div class="modal-footer">
	<button class="btn btn-secondary" data-dismiss="modal">Tutup</button>
</div>
