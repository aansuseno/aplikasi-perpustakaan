<div class="modal-body" style="max-height: 50vh; overflow: auto;">
	<h4>Kode katalog</h4>
	<ul>
		<li>Kode katalog bebas.</li>
		<li>Sebaiknya jika buku sama membunyai kode yang sama hanya menambahkan angka saja</li>
		<li>Tidak boleh ada tanda baca <b>&</b></li>
	</ul>
	<hr>
	<h4>Kode buku</h4>
	<ul>
		<li>Kode buku yang dimaksud yaitu id buku.</li>
		<li>
			Jika ingat judul buku:
			<ol>
				<li>Inputkan judul buku di kolom pertama input kode buku</li>
				<li>Di kolom ketika akan muncul dropdown. Pilih salah satu buku.</li>
				<li>Hasil pencarian hanya akan menampilkan 10 buku</li>
			</ol>
		</li>
		<li>
			Jika merasa ribet, bisa menggunakan id buku:
			<ol>
				<li>Id buku bisa dilihat di <a href="<?= base_url() ?>bibliografi/daftar">halaman daftar buku</a></li>
				<li>Maasukkan id buku di kolom kedua input kode buku</li>
				<li>Pastikan id ditulis dengan benar</li>
				<li>Judul buku akan muncul di kolom ketiga</li>
			</ol>
		</li>
	</ul>
	<hr>
	<h4>Kondisi buku</h4>
	<p>Kondisi buku sesuaikan sesuai dengan buku yang berkode katalog</p>
	<hr>
	<h4>Deskripsi (opsional)</h4>
	<p>Deskripsi bisa berupa keterangan dari pustakawan</p>
	<hr>
	<h4>Simpan</h4>
	<p>Klik tombol simpan untuk menyimpan</p>
</div>
<div class="modal-footer">
	<button class="btn btn-secondary" data-dismiss="modal">Tutup</button>
</div>
