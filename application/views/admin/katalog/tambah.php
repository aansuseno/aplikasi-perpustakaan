<script type="text/javascript" src="<?= base_url('assets/js/adminkatalog.js') ?>"></script>

<div class="container">
	<div class="judul-halaman">
		<h1>Tambah Katalog</h1>
	</div>
	<br>
		<button
		 class="btn btn-info"
		 onclick="
			document.getElementById('judulPopup').innerHTML = 'Informasi Tambah katalog'
			setAjak('isiDetail', '<?= base_url() ?>katalog/informasiTambahKatalog')
		 "
		 data-toggle="modal"
		 data-target="#popupDetail"
		 title="Informasi tambah katalog">
			<i class="fas fa-info"></i>
		</button>
	<br>
	<br>
	<div id="tambahKatalog">
		<div id="pesanerror"></div>
		<table class="table borderless">
			<tr>
				<th>Kode katalog</th>
				<td>
					<input type="text" autocomplete="off" class="form-control" id="kode-katalog" placeholder="kode katalog">
				</td>
			</tr>
			<!-- kode buku -->
			<tr>
				<th>Kode buku</th>
				<td>
					<input type="text" autocomplete="off" oninput="cariBuku('<?= base_url() ?>')" class="form-control" id="cari-buku" placeholder="cari buku">
				</td>
			</tr>
			<tr>
				<td> </td>
				<td>
					<input type="text" autocomplete="off" oninput="cariBukuId('<?= base_url() ?>')" class="form-control" id="cari-buku-id" placeholder="id buku">
				</td>
			</tr>
			<tr>
				<td> </td>
				<td>
					<select class="form-control" id="list-buku">
						<option value="">null</option>
					</select>
				</td>
			</tr>
			<!-- kondisi -->
			<tr>
				<td>Kondisi</td>
				<td>
					<select class="form-control" id="kondisi">
						<option value="Baik">Baik</option>
						<option value="Baru">Baru</option>
						<option value="Rusak">Rusak</option>
					</select>
				</td>
			</tr>
			<!-- deskripsi -->
			<tr>
				<td>Deskripsi</td>
				<td>
					<textarea id="deskripsi" class="form-control"></textarea>
				</td>
			</tr>
		</table>
	</div>

	<!-- simpan -->
	<div class="sumit">
		<span class="btn btn-primary float-right" onclick="cekInputan('<?= base_url() ?>')">Simpan</span>
	</div>
</div>

<!-- pop up -->
<div class="modal fade" id="popupDetail" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="judulPopup"></h3>
				<button class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div id="isiDetail"></div>
		</div>
	</div>
</div>
