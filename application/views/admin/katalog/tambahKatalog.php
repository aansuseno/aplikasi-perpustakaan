<div id="pesanerror"><span class="alert alert-primary">Data berhasil ditambah.</span></div>
<br><br>
<table class="table borderless">
	<tr>
		<th>Kode katalog</th>
		<td>
			<input type="text" autocomplete="off" class="form-control" id="kode-katalog" placeholder="kode katalog">
		</td>
	</tr>
	<!-- kode buku -->
	<tr>
		<th>Kode buku</th>
		<td>
			<input type="text" autocomplete="off" oninput="cariBuku('<?= base_url() ?>')" class="form-control" id="cari-buku" placeholder="cari buku">
		</td>
	</tr>
	<tr>
		<td> </td>
		<td>
			<input type="text" autocomplete="off" oninput="cariBukuId('<?= base_url() ?>')" class="form-control" id="cari-buku-id" placeholder="id buku">
		</td>
	</tr>
	<tr>
		<td> </td>
		<td>
			<select class="form-control" id="list-buku">
				<option value="">null</option>
			</select>
		</td>
	</tr>
	<!-- kondisi -->
	<tr>
		<td>Kondisi</td>
		<td>
			<select class="form-control" id="kondisi">
				<option value="Baik">Baik</option>
				<option value="Baru">Baru</option>
				<option value="Rusak">Rusak</option>
			</select>
		</td>
	</tr>
	<!-- deskripsi -->
	<tr>
		<td>Deskripsi</td>
		<td>
			<textarea id="deskripsi" class="form-control"></textarea>
		</td>
	</tr>
</table>