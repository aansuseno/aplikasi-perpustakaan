<script type="text/javascript" src="<?= base_url() ?>assets/js/adminkatalog.js"></script>

<div class="container">
	<div class="judul-halaman">
		<h1>Daftar Katalog</h1>
	</div>
	<br>
	<button
	 class="btn btn-info"
	 title="Informasi pengelolaan katalog"
	 data-target="#popup"
	 data-toggle="modal"
	 onclick="
	 	document.getElementById('judulpopup').innerHTML = 'Informasi pengelolaan katalog'
		setAjak('isipopup', '<?= base_url() ?>katalog/informasiKatalog')
	 ">
		<i class="fas fa-info"></i>
	</button>
	<br>
	<br>
	<div class="input-group">
		<input
		 type="text"
		 class="form-control"
		 id="cari-katalog"
		 placeholder="Cari berdasar judul atau kode katalog">
		<div class="input-group-append">
			<span
			 class="btn btn-outline-primary tombol-cari"
			 onclick="setAjak('data_katalog', '<?= base_url() ?>katalog/cariKatalog?cari='+document.getElementById('cari-katalog').value)">
				<i class="fas fa-search"></i>
			</span>
		</div>
	</div>
	<br>
	<div id="data_katalog">
		<table class="table table-bordered table-hover">
			<thead>
					<th scope="col">Kode Katalog</th>
					<th scope="col">Judul</th>
					<th scope="col">Tgl Edit</th>
					<th scope="col">Aksi</th>
				</tr>
			</thead>
			<tbody >
				<?php foreach ($katalog as $k): ?>
					<tr>
						<td><?= $k->kode_katalog ?></td>
						<td><?= $k->judul ?></td>
						<td><?= $k->tgl_edit_katalog ?></td>
						<td>
							<span
							 class="btn btn-info"
							 onclick="infokatalog('<?= base_url() ?>',<?= $k->id_katalog; ?>)"
							 data-toggle="modal"
							 data-target="#popup"
							 title="Detail katalog">
								<i class="fas fa-info-circle"></i>
							</span>
							<span
							 class="btn btn-warning"
							 onclick="editkatalog('<?=base_url() ?>',<?php echo $k->id_katalog; ?>)"
							 data-toggle="modal"
							 data-target="#popup"
							 title="Edit katalog">
								<i class="fas fa-edit"></i>
							</span>
							<span
							 data-toggle="modal"
							 onclick="hapusKatalog('<?= base_url() ?>', <?= $k->id_katalog ?>)"
							 data-target="#popup"
							 class="btn btn-danger"
							 title="Hapus katalog">
								<i class="fas fa-trash"></i>
							</span>
						</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>

		<!-- pagination -->
		<div style="display: flex; flex-wrap: wrap; justify-content: space-around;">
		<?php if ($jml_halaman > 1): ?>
			<ul class="pagination">
				<?php if ($halamanSekarang > 0): ?>
					<li class="page-item">
						<span
						class="page-link"
						style="cursor: pointer;"
						onclick="setAjak('data_katalog', '<?= base_url() ?>katalog/list?halaman=0')"
						>First
						</span>
					</li>
					<li class="page-item">
						<span
						class="page-link"
						style="cursor: pointer;"
						<?php if ($halamanSekarang > 0): ?>
							onclick="setAjak('data_katalog', '<?= base_url() ?>katalog/list?halaman='+<?= $halamanSekarang-1 ?>)"
						<?php endif ?>
						>&lt;
						</span>
					</li>
				<?php endif ?>
				<?php for ($i = $halamanSekarang - 2; $i <= $halamanSekarang + 4; $i++): ?>
					<?php if ($i > 0 && $i <= $jml_halaman): ?>
						<li
						class="page-item <?php if($halamanSekarang == $i - 1) { echo 'active'; }?>"
						style="cursor: pointer;">
							<span
							class="page-link"
							onclick="setAjak('data_katalog', '<?= base_url() ?>katalog/list?halaman='+<?= $i-1 ?>)">
								<?= $i ?>
							</span>
						</li>
					<?php endif ?>
				<?php endfor ?>
				<?php if ($halamanSekarang < $jml_halaman-1): ?>
					<li class="page-item">
						<span
						class="page-link"
						style="cursor: pointer;"
						<?php if ($halamanSekarang < $jml_halaman-1): ?>
							onclick="setAjak('data_katalog', '<?= base_url() ?>katalog/list?halaman='+<?= $halamanSekarang+1 ?>)"
						<?php endif ?>
						>&gt;
						</span>
					</li>
					<li class="page-item">
						<span
						class="page-link"
						style="cursor: pointer;"
						onclick="setAjak('data_katalog', '<?= base_url() ?>katalog/list?halaman='+<?= $jml_halaman-1 ?>)"
						>Last
						</span>
					</li>
				<?php endif ?>
			</ul>
		<?php endif ?>
		</div>
	</div>
</div>


<!-- popup -->
<div class="modal fade" id="popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="judulpopup"></h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div id="isipopup"></div>
		</div>
	</div>
</div>
