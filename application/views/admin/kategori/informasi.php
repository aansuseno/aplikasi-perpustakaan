<div class="modal-body" style="max-height: 50vh; overflow:auto;">
	<ol>
		<li>
			<h4>Tambah Kategori</h4>
			<ol>
				<li>Klik ikon <i class="fas fa-plus"></i></li>
				<li>Akan muncul popup</li>
				<li>Masukkan nama kategori</li>
				<li>Masukkan deskripsi(opsional)</li>
				<li>Aturan: dilarang menggunakan simbol <b>&</b></li>
				<li>Klik tombol tambah untuk menyimpan</li>
			</ol>
		</li>
		<li>
			<h4>Detail Kategori</h4>
			<ol>
				<li>Untuk melihat detail kategori bisa klik ikon <i class="fas fa-info-circle"></i></li>
			</ol>
		</li>
		<li>
			<h4>Edit Kategori</h4>
			<ol>
				<li>Untuk mengedit kategori bisa klik ikon <i class="fas fa-edit"></i></li>
				<li>Ganti nama kategori dan deskripsi</li>
				<li>Klik tombol edit untuk menyimpan</li>
			</ol>
		</li>
		<li>
			<h4>Hapus Kategori</h4>
			<ol>
				<li>Untuk menghapus kategori bisa klik ikon <i class="fas fa-trash"></i></li>
				<li>Tidak disarankan untuk mnghapus kategori</li>
			</ol>
		</li>
	</ol>
</div>
<div class="modal-footer">
	<button class="btn btn-secondary" data-dismiss="modal">tutup</button>
</div>
