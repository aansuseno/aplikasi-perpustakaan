<?php if($pesan == 'berhasilTambah') { ?>
<div class="alert alert-primary">Kategori berhasil tambah.</div>
<br>
<?php } else if($pesan == 'berhasilHapus') { ?>
<div class="alert alert-primary">Kategori berhasil dihapus.</div>
<br>
<?php } else if($pesan == 'berhasilEdit') { ?>
<div class="alert alert-primary">Kategori berhasil diedit.</div>
<br>
<?php } ?>

<div class="btn btn-success float-right" onclick="tambahkategori('<?= base_url() ?>')" data-toggle="modal" data-target="#popup"><i class="fas fa-plus"></i></div>
<br><br>
<table class="table table-bordered table-hover">
	<thead>
			<th scope="col">#</th>
			<th scope="col">kategori</th>
			<th scope="col">Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($kategori as $k):?>
		<tr>
			<td><?= $k->id ?></td>
			<td><?= $k->nama ?></td>
			<td>
				<span
				 class="btn btn-info"
				 onclick="infokategoridatamaster('<?=base_url() ?>',<?php echo $k->id; ?>)"
				 data-toggle="modal"
				 data-target="#popup"
				 title="Detail kategori">
					<i class="fas fa-info-circle"></i>
				</span>
				<span
				 class="btn btn-warning"
				 onclick="editkategoridatamaster('<?=base_url() ?>',<?php echo $k->id; ?>)"
				 data-toggle="modal"
				 data-target="#popup"
				 title="Edit kategori">
					<i class="fas fa-edit"></i>
				</span>
				<span
				 class="btn btn-danger"
				 onclick="hapuskategoridatamaster('<?= base_url() ?>', <?php echo $k->id ?>, '<?= $k->nama ?>')"
				 data-toggle="modal"
				 data-target="#popup"
				 title="Hapus kategori">
					<i class="fas fa-trash"></i>
				</span>
			</td>
		</tr>
		<?php endforeach ?>
	</tbody>
</table>

<!-- pagination -->
<div style="display: flex; flex-wrap: wrap; justify-content: space-around;">
<?php if ($jml_halaman > 1): ?>
	<ul class="pagination">
		<?php if ($halamanSekarang > 0): ?>
			<li class="page-item">
				<span
				class="page-link"
				style="cursor: pointer;"
				onclick="setAjak('datakategori', '<?= base_url() ?>kategori/list?halaman=0')"
				>First
				</span>
			</li>
			<li class="page-item">
				<span
				class="page-link"
				style="cursor: pointer;"
				<?php if ($halamanSekarang > 0): ?>
					onclick="setAjak('datakategori', '<?= base_url() ?>kategori/list?halaman='+<?= $halamanSekarang-1 ?>)"
				<?php endif ?>
				>&lt;
				</span>
			</li>
		<?php endif ?>
		<?php for ($i = $halamanSekarang - 2; $i <= $halamanSekarang + 4; $i++): ?>
			<?php if ($i > 0 && $i <= $jml_halaman): ?>
				<li
				class="page-item <?php if($halamanSekarang == $i - 1) { echo 'active'; }?>"
				style="cursor: pointer;">
					<span
					class="page-link"
					onclick="setAjak('datakategori', '<?= base_url() ?>kategori/list?halaman='+<?= $i-1 ?>)">
						<?= $i ?>
					</span>
				</li>
			<?php endif ?>
		<?php endfor ?>
		<?php if ($halamanSekarang < $jml_halaman-1): ?>
			<li class="page-item">
				<span
				class="page-link"
				style="cursor: pointer;"
				<?php if ($halamanSekarang < $jml_halaman-1): ?>
					onclick="setAjak('datakategori', '<?= base_url() ?>kategori/list?halaman='+<?= $halamanSekarang+1 ?>)"
				<?php endif ?>
				>&gt;
				</span>
			</li>
			<li class="page-item">
				<span
				class="page-link"
				style="cursor: pointer;"
				onclick="setAjak('datakategori', '<?= base_url() ?>kategori/list?halaman='+<?= $jml_halaman-1 ?>)"
				>Last
				</span>
			</li>
		<?php endif ?>
	</ul>
<?php endif ?>
</div>
