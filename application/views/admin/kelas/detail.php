<div class="container">
	<div class="judul-halaman">
		<h1>Detail Kelas <?php echo $kelas->nama; ?></h1>
	</div>
	<br><br>
	<table class="table-detail">
		<tr>
			<th>Kelas</th>
			<td>: <?php echo $kelas->nama; ?></td>
		</tr>
		<tr>
			<th>Kepanjangan</th>
			<td>: <?php echo $kelas->kepanjangan; ?></td>
		</tr>
		<tr>
			<th>Deskripsi</th>
			<td>: <?php echo $kelas->deskripsi; ?></td>
		</tr>

	</table>
	<a href="<?= base_url() ?>kelas/daftar"><button class="btn btn-dark" title="Kembali ke halaman daftar kelas"><i class="fas fa-arrow-circle-left"></i> Kembali</button></a>
	<a href="<?= base_url() ?>kelas/edit?id=<?php echo $kelas->id; ?>"><button title="Edit kelas" class="btn btn-warning"><i class="fas fa-edit"></i> Edit</button></a>
	<button title="Hapus kelas" class="btn btn-danger" data-toggle="modal" data-target="#hapus"><i class="fas fa-trash"></i> Hapus</button>
</div>

<!-- pop up hapus-->
<div class="modal fade" id="hapus" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title text-danger" id="exampleModalLongTitle">Peringatan !!</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
			Anda yakin untuk menghapus?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<a href="<?= base_url() ?>kelas/hapus?id=<?php echo $kelas->id?>">
					<button type="button" class="btn btn-danger"><i class="fas fa-trash"></i> Hapus</button>
				</a>
			</div>
		</div>
	</div>
</div>
