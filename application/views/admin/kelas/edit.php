<div class="container">
	<div class="judul-halaman">
		<h1>Edit Kelas</h1>
	</div>
	<br>
	<form action="<?= base_url() ?>kelas/editProses" method="post">
		<!-- nama kelas -->
		<div class="form-group row">
			<label for="nama-kelas" class="col-sm-2 col-form-label">Nama Kelas</label>
			<div class="col-sm-10">
				<input type="text" name="nama_kelas" class="form-control" id="nama-kelas" placeholder="nama kelas" value="<?php echo  $kelas->nama; ?>">
			</div>
		</div>

		<!-- kepanjangan kelas -->
		<div class="form-group row">
			<label for="kepanjangan-kelas" class="col-sm-2 col-form-label">Kepanjangan Kelas</label>
			<div class="col-sm-10">
				<input type="text"name="kepanjangan_kelas" class="form-control" id="kepanjangan-kelas" placeholder="kepanjangan kelas" value="<?php echo  $kelas->kepanjangan; ?>">
			</div>
		</div>

		<!-- deskripsi -->
		<div class="form-group row">
			<label for="deskripsi" class="col-sm-2 col-form-label">deskripsi</label>
			<div class="col-sm-10">
				<input type="text"name="deskripsi" class="form-control" id="deskripsi" placeholder=" deskripsi"value="<?php echo  $kelas->deskripsi; ?>">
			</div>
		</div>

		<input type="hidden" name="id"value="<?php echo  $kelas->id; ?>">

		<!-- simpan -->
		<input title="Simpan perubahan" type="submit" value="Simpan" class="btn btn-success">
	</form>
</div>
