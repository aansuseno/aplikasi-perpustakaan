<div class="container">
	<div class="judul-halaman">
		<h1>Tambah Kelas</h1>
	</div>
	<br>
	<?php if ($pesan == 'berhasil') { ?>
		<div class="alert alert-primary" role="alert">
			Kelas berhasil ditambah.
		</div>
	<?php } ?>
	<form action="<?= base_url() ?>kelas/tambahProses" method="post">
		<!-- nama kelas -->
		<div class="form-group row">
			<label for="nama-kelas" class="col-sm-2 col-form-label">Nama Kelas</label>
			<div class="col-sm-10">
				<input type="text" name="nama_kelas" class="form-control" id="nama-kelas" placeholder="nama kelas" title="nama kelas, seperti X TKJ">
			</div>
		</div>

		<!-- kepanjangan kelas -->
		<div class="form-group row">
			<label for="kepanjangan-kelas" class="col-sm-2 col-form-label">Kepanjangan Kelas</label>
			<div class="col-sm-10">
				<input type="text"name="kepanjangan_kelas" class="form-control" id="kepanjangan-kelas" placeholder="kepanjangan kelas" title="Kepanjangan kelas, 10 Teknik Komputer dan Jaringan">
			</div>
		</div>

		<!-- simpan -->
		<input type="submit" value="Simpan" title="Simpan kelas" class="btn btn-primary">
	</form>
</div>
