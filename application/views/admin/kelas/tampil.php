<div class="container">
	<div class="judul-halaman">
		<h1>Daftar Kelas</h1>
	</div>
	<br>
	<?php if($pesan == 'hapusSukses') {?>
		<div class="alert alert-primary">Data berhasil dihapus</div>
	<?php } else if($pesan == 'berhasilEdit') {?>
		<div class="alert alert-primary">Data berhasil diedit</div>
	<?php } else if($pesan == 'hapusGagal') {?>
		<div class="alert alert-primary">Data gagal dihapus karena kelas terkait dengan beberapa siswa</div>
	<?php } ?>
	<br>
	<table class="table table-bordered table-hover">
		<thead>
				<th scope="col">#</th>
				<th scope="col">Kelas</th>
				<th scope="col">Aksi</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($kelas as $k):?>
			<tr>
				<td><?= $k->id ?></td>
				<td><?= $k->nama ?></td>
				<td><a href="<?= base_url() ?>kelas/detail?id=<?= $k->id ?>" class="btn btn-info btn-sm"><i class="fas fa-info-circle" title="Kelola kelas lebih lanjut"></i></a></td>
			</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>
