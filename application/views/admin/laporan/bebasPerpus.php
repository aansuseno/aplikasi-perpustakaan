<div class="container">
	<div class="judul-halaman">
		<h1>Laporan Bebas Perpustakaan</h1>
	</div>
	<br><br>
	<i class="fas fa-info-circle" title="Klik logo selolah dikiri atas untuk mengedit alamat sekolah, nama penanda tangan, dan lainnya."></i>
	<table class="table table-borderless">
		<tr>
			<td>Kelas</td>
			<td>
				<select class="form-control" title="Pilih kelas." id="idKelasCetak" oninput="setAjak('kelasCetak', '<?= base_url() ?>laporan/cekBebasPerpus?id='+document.getElementById('idKelasCetak').value)">
					<?php foreach($kelas as $k) { ?>
					<option value="<?= $k->id ?>"><?= $k->nama ?></option>
					<?php } ?>
				</select>
			</td>
			<td><button
			 class="btn btn-primary"
			 title="Klik untuk melihat siswa yang sudah bebas perpus atau belum."
			 onclick="setAjak('kelasCetak', '<?= base_url() ?>laporan/cekBebasPerpus?id='+document.getElementById('idKelasCetak').value)">Cek</button></td>
		</tr>
	</table>
	<br>
	<br>
	<div id="kelasCetak"></div>
</div>
