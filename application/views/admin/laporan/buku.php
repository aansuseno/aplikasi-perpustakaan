<div class="container">
	<div class="judul-halaman">
		<h1>Laporan Buku</h1>
	</div>
	<br><br><br>
	<table class="table table-hover">
		<tr>
			<td>Semua buku dengan detail</td>
			<td>
				<a href="<?= base_url() ?>laporan/bukuSemua?donlot=iya" target="_blank">
					<button class="btn btn-success" title="Download semua data buku.">Download</button>
				</a>
				<a href="<?= base_url() ?>laporan/bukuSemua" target="_blank">
					<button class="btn btn-info" title="Lihat semua data buku.">Lihat</button>
				</a>
			</td>
		</tr>
		<tr>
			<td>Semua katalog</td>
			<td>
				<a href="<?= base_url() ?>laporan/katalogSemua?donlot=iya" target="_blank">
					<button class="btn btn-success" title="Download semua data katalog.">Download</button>
				</a>
				<a href="<?= base_url() ?>laporan/katalogSemua" target="_blank">
					<button class="btn btn-info" title="Lihat semua data katalog.">Lihat</button>
				</a>
			</td>
		</tr>
	</table>
</div>
