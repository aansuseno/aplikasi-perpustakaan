<table class="table table-hover">
	<tr>
		<th>No</th>
		<th>NIS</th>
		<th>Nama</th>
		<th>Aksi</th>
	</tr>
	<?php $no=1; foreach($siswa as $s) {?>
	<tr>
		<td><?php echo $no; $no++; ?></td>
		<td><?= $s->nis ?></td>
		<td><?= $s->nama ?></td>
		<td>
			<?php if($s->yangSedangDipinjam != 0) {?>
				Siswa sedang meminjam <?= $s->yangSedangDipinjam ?> buku. Silahkan dicek di menu transaksi.
			<?php } else {?>
				<a href="<?= base_url() ?>laporan/printSurat?id=<?= $s->id ?>" target="_blank" class="btn btn-success" title="Download surat bebas perpustakaan.">Download</a>
			<?php } ?>
		</td>
	</tr>
	<?php } ?>
</table>
