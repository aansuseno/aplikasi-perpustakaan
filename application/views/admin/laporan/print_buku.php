<table border="1">
	<tr>
		<th colspan="11">Daftar Semua Buku</th>
	</tr>
	<tr>
		<th>No.</th>
		<th>Judul</th>
		<th>Pengarang</th>
		<th>Penerbit</th>
		<th>Jumlah Halaman</th>
		<th>Tahun Terbit</th>
		<th>Kategori</th>
		<th>Lemari</th>
		<th>Jumlah Buku</th>
		<th>Buku Tersedia</th>
		<th>Rating</th>
	</tr>
	<?php $no = 1;foreach($buku as $b) { ?>
		<tr>
			<td><?php echo $no; $no++ ?></td>
			<td><?= $b->judul ?></td>
			<td><?= $b->penulis ?></td>
			<td><?= $b->penerbit ?></td>
			<td><?= $b->jumlah_halaman ?></td>
			<td><?= $b->tahun ?></td>
			<td><?= $b->kategori ?></td>
			<td><?= $b->lemari ?></td>
			<td><?= $b->jumlah_buku ?></td>
			<td><?= $b->tersedia ?></td>
			<td><?= $b->rating ?></td>
		</tr>
	<?php } ?>
</table>
