<table border="1">
	<tr>
		<th colspan="7">Daftar Siswa</th>
	</tr>
	<tr>
		<th>No.</th>
		<th>NIS</th>
		<th>Nama</th>
		<th>Kelas</th>
		<th>Jenis Kelamin</th>
		<th>Status</th>
		<th>Jumlah buku yang dipinjam</th>
	</tr>
	<?php $no = 1;foreach($siswa as $b) { ?>
		<tr>
			<td><?php echo $no; $no++ ?></td>
			<td><?= $b->nis ?></td>
			<td><?= $b->nama ?></td>
			<td><?= $b->kelas ?></td>
			<td><?= $b->jk ?></td>
			<td>
				<?php if($b->status == 'y') { ?>
					Aktif
				<?php } else if($b->status == 'n') { ?>
					Belum mendaftar
				<?php } else if($b->status == 'x') { ?>
					Lulus
				<?php } ?>
			</td>
			<td><?= $b->meminjam_buku ?></td>
		</tr>
	<?php } ?>
</table>
