<table border="1">
	<tr>
		<th colspan="8">Daftar Transaksi Selama <?= $lama ?> Terakhir</th>
	<tr>
	<tr>
		<th>No.</th>
		<th>Tgl Pinjam</th>
		<th>NIS Siswa</th>
		<th>Nama Siswa</th>
		<th>Kode Katalog</th>
		<th>Judul Buku</th>
		<th>Meminjam Dengan</th>
		<th>Status</th>
	</tr>
	<?php $no = 1;foreach($transaksi as $t) { ?>
		<tr>
			<td><?php echo $no; $no += 1 ?>.</td>
			<td><?= $t->tgl_pinjam ?></td>
			<td><?= $t->nis ?></td>
			<td><?= $t->nama_siswa ?></td>
			<td><?= $t->kode_katalog ?></td>
			<td><?= $t->judul_buku ?></td>
			<td><?= $t->nama_petugas ?></td>
			<?php if($t->kembali == 0) { ?>
				<td style="background: orange;">Sedang dipinjam</td>
			<?php } else if($t->kembali == 1 && $t->tepat_waktu == 0) {?>
				<td style="background: red;">Terlambat dikembalikan</td>
			<?php } else if($t->kembali == 1 && $t->tepat_waktu == 1) {?>
				<td style="background: lightgreen;">Sudah dikembalikan</td>
			<?php } ?>
		</tr>
	<?php } ?>
</table>
<br>
<table border="1">
	<tr>
		<th colspan="5">Daftar Siswa Peminjam Dalam <?= $lama ?> Terakhir</th>
	<tr>
	<tr>
		<th>No.</th>
		<th>Nama</th>
		<th>Kelas</th>
		<th>Jml yang terlambat dikembalikan</th>
		<th>Jumlah Buku Yang Dipinjam</th>
	</tr>
	<?php $no = 1;foreach($siswa as $t) { ?>
		<tr>
			<td><?php echo $no; $no += 1 ?>.</td>
			<td><?= $t->nama_siswa ?></td>
			<td><?= $t->kelas ?></td>
			<td><?= $t->terlambat ?></td>
			<td><?= $t->meminjam_buku ?></td>
		</tr>
	<?php } ?>
</table>
