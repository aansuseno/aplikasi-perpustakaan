<table border="1">
	<tr>
		<th>No.</th>
		<th>Tgl Pinjam</th>
		<th>NIS Siswa</th>
		<th>Nama Siswa</th>
		<th>Kode Katalog</th>
		<th>Judul Buku</th>
		<th>Meminjam Dengan</th>
		<th>Status</th>
	</tr>
	<?php $no = 1;foreach($transaksi as $t) { ?>
		<tr>
			<td><?php echo $no; $no += 1 ?>.</td>
			<td><?= $t->tgl_pinjam ?></td>
			<td><?= $t->nis ?></td>
			<td><?= $t->nama_siswa ?></td>
			<td><?= $t->kode_katalog ?></td>
			<td><?= $t->judul_buku ?></td>
			<td><?= $t->nama_petugas ?></td>
			<?php if($t->kembali == 0) { ?>
				<td style="background: orange;">Sedang dipinjam</td>
			<?php } else if($t->kembali == 1 && $t->tepat_waktu == 0) {?>
				<td style="background: red;">Terlambat dikembalikan</td>
			<?php } else if($t->kembali == 1 && $t->tepat_waktu == 1) {?>
				<td style="background: lightgreen;">Sudah dikembalikan</td>
			<?php } ?>
		</tr>
	<?php } ?>
</table>

<table>
	<tr>
		<th colspan="2">Keterangan</th>
	</tr>
	<tr>
		<td>Dari Tanggal</td>
		<td>: <?= $mulai ?></td>
	</tr>
	<tr>
		<td>Sampai Tanggal</td>
		<td>: <?= $sampai ?></td>
	</tr>
	<tr>
		<td>Kategori</td>
		<td>:
		<?php if($kategori == 'semua') {
			echo "Semua Transaksi";
		} else if($kategori == 'dipinjam') {
			echo "Sedang Dipinjam";
		} else if($kategori == 'kembali') {
			echo "Sudah Dikembalikan";
		} else if($kategori == 'terlambat') {
			echo "Terlambat Dikembalikan";
		}?>
		</td>
	</tr>
	<tr>
		<td>Petugas</td>
		<td>:
		<?= $petugas ?>
		</td>
	</tr>
</table>
