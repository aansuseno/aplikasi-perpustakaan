<div class="container">
	<div class="judul-halaman">
		<h1>Laporan Siswa</h1>
	</div>
	<br><br><br>
	<form action="<?= base_url() ?>laporan/siswaP?donlot=iya" target="_blank" method="post">
		<table class="table table-boderless" style="max-width: 400px;">
			<tr>
				<td>Kategori</td>
				<td>
					<select name="kategori" class="form-control">
						<option value="semua">Semua yang belum lulus</option>
						<option value="aktif" title="Semua siswa yang belum lulus dan sudah mendaftar.">Aktif</option>
						<option value="tidakAktif" title="Semua siswa yang belum lulus dan belum mendaftar.">Tidak Aktif</option>
						<option value="lulus">Semua siswa yang sudah lulus</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Kelas</td>
				<td>
					<select name="kelas" class="form-control">
						<option value="semua">Semua</option>
						<?php foreach($kelas as $k) : ?>
							<option value="<?= $k->id ?>"><?= $k->nama ?></option>
						<?php endforeach ?>
						<option value="lulus">Semua siswa yang sudah lulus</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Jenis kelamin</td>
				<td>
					<select name="jk" class="form-control">
						<option value="semua">Semua</option>
						<option value="l">Laki-laki</option>
						<option value="p">Perempuan</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Urutkan</td>
				<td>
					<select name="sort" class="form-control">
						<option value="nis">NIS</option>
						<option value="nama">Nama</option>
						<option value="kelas">Kelas</option>
					</select>
				</td>
			</tr>
			<tr>
				<td> </td>
				<td style="display: flex;justify-content: end;">
					<input type="submit" value="Download" class="btn btn-success" title="Download semua siswa berdasar pilihan di atas.">
				</td>
			</tr>
		</table>
	</form>
</div>
