<div class="container">
	<div class="judul-halaman">
		<h1>Laporan Transaksi</h1>
	</div>
	<br><br><br>
	<table class="table table-hover">
		<tr>
			<td>Semua transaksi dalam jangka waktu 30 hari terakhir</td>
			<td>
				<a href="<?= base_url() ?>laporan/transaksi30hari?donlot=iya" target="_blank">
					<button class="btn btn-success" title="Download semua transaksi dengan tanggal pinjam pertama 30 hari yang lalu.">Download</button>
				</a>
				<a href="<?= base_url() ?>laporan/transaksi30hari" target="_blank">
					<button class="btn btn-info" title="Lihat semua transaksi dengan tanggal pinjam pertama 30 hari yang lalu.">Lihat</button>
				</a>
			</td>
		</tr>
		<tr>
			<td>Semua transaksi dalam jangka waktu 1 tahun terakhir</td>
			<td>
				<a href="<?= base_url() ?>laporan/transaksi1tahun?donlot=iya" target="_blank">
					<button class="btn btn-success" title="Download semua transaksi dengan tanggal pinjam pertama 1 tahun yang lalu.">Download</button>
				</a>
				<a href="<?= base_url() ?>laporan/transaksi1tahun" target="_blank">
					<button class="btn btn-info" title="Lihat semua transaksi dengan tanggal pinjam pertama 1 tahun yang lalu.">Lihat</button>
				</a>
			</td>
		</tr>
	</table>

	<br>
	<hr>

	<h4>Lebih spesifik</h4>
	<form action="<?= base_url() ?>laporan/transaksiOpsional" target="_blank" method="post">
		<table class="table" style="max-width: 500px">
			<tr>
				<td>Dari tanggal</td>
				<td>
					<input name="mulai" type="date" value="<?= date('Y-m-d', strtotime('-7 days')) ?>" class="form-control">
				</td>
			</tr>
			<tr>
				<td>Hingga tanggal</td>
				<td>
					<input name="sampai" type="date" value="<?= date('Y-m-d') ?>" class="form-control">
				</td>
			</tr>
			<tr>
				<td>Kategori</td>
				<td>
					<select name="kategori" class="form-control">
						<option value="semua">Semua Transaksi</option>
						<option value="dipinjam">Sedang Dipinjam</option>
						<option value="kembali">Sudah Dikembalikan</option>
						<option value="terlambat">Terlambat Dikembalikan</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Meminjam Dengan</td>
				<td>
					<select name="petugas" class="form-control">
						<option value="semua">Semua Petugas</option>
						<?php foreach($petugas as $p) : ?>
						<option value="<?= $p->id ?>"><?= $p->nama ?></option>
						<?php endforeach ?>
					</select>
				</td>
			</tr>
			<tr>
				<td> </td>
				<td><button class="btn btn-success float-right">Download</button></td>
			</tr>
		</table>
	</form>
</div>

<!-- pop up -->
<div class="modal fade" id="popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="judulpopup"></h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div id="isipopup"></div>
		</div>
	</div>
</div>
