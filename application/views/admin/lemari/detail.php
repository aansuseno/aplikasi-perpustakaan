<div class="container">
	<div class="judul-halaman">
		<h1>Detail Lemari <?php echo $lemari->nama; ?></h1>
	</div>
	<?php if($pesan == 'berhasilEdit') {?>
	<div class="alert alert-primary">
		Data berhasil diedit
	</div>
	<?php } ?>
	<br>
	<br>
	<table class="table-detail">
		<tr>
			<th>Lemari</th>
			<td>: <?php echo $lemari->nama; ?></td>
		</tr>
		<tr>
			<th>Deskripsi</th>
			<td>: <?php echo $lemari->deskripsi; ?></td>
		</tr>

	</table>
	<br>
	<a href="<?= base_url() ?>lemari/tampil" title="Kembali"><button class="btn btn-dark"><i class="fas fa-arrow-circle-left"></i> Kembali</button></a>
	<button
	 class="btn btn-warning"
	 data-toggle="modal"
	 data-target="#popupDetail"
	 title="Edit lemari">
		<i class="fas fa-edit"></i> Edit
	</button>
	<button
	 class="btn btn-danger"
	 data-toggle="modal"
	 data-target="#hapus"
	 title="Hapus lemari">
		<i class="fas fa-trash"></i> Hapus
	</button>
</div>

<!-- pop up hapus-->
<div class="modal fade" id="hapus" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title text-danger" id="exampleModalLongTitle">Peringatan !!</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
			Anda yakin untuk menghapus?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<a href="<?= base_url() ?>lemari/hapus?id=<?php echo $lemari->id?>">
					<button type="button" class="btn btn-danger"><i class="fas fa-trash"></i> Hapus</button>
				</a>
			</div>
		</div>
	</div>
</div>

<!-- pop up hapus-->
<div class="modal fade" id="popupDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title text-danger" id="exampleModalLongTitle">Edit !!</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="<?= base_url() ?>lemari/editProses" method="post">
			<div class="modal-body">
				<table class="table table-borderless">
					<tr>
						<td>Nama lemari</td>
						<td><input
						 type="text"
						 auto-complete="off"
						 class="form-control"
						 name="nama-lemari"
						 value="<?= $lemari->nama ?>"></td>
					</tr>
					<tr>
						<td>Deskripsi lemari</td>
						<td><input
						 type="text"
						 auto-complete="off"
						 class="form-control"
						 name="deskripsi-lemari"
						 value="<?= $lemari->deskripsi ?>"></td>
					</tr>
				</table>
				<input type="hidden" value="<?= $lemari->id ?>" name="id">
			</div>
			<div class="modal-footer">
				<span type="button" class="btn btn-secondary" data-dismiss="modal">Batal</span>
				<input
				 type="submit"
				 class="btn btn-primary"
				 value="edit">
			</div>
			</form>
		</div>
	</div>
</div>
