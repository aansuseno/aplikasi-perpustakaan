<div class="container">
	<div class="judul-halaman">
		<h1>Tambah Lemari</h1>
	</div>
	<br>
	<?php if ($pesan == 'berhasil') { ?>
		<div class="alert alert-primary" role="alert">
			Lemari berhasil ditambah.
		</div>
	<?php } ?>
	<br>
		<button
		 class="btn btn-info"
		 data-toggle="modal"
		 data-target="#popupDetail"
		 title="Informasi tambah lemari">
			<i class="fas fa-info"></i>
		</button>
	<br>
	<br>
	<form action="<?= base_url() ?>lemari/tambahProses" method="post">
		<!-- nama lemari -->
		<div class="form-group row">
			<label for="nama-lemari" class="col-sm-2 col-form-label">Nama Lemari</label>
			<div class="col-sm-10">
				<input type="text" name="nama_lemari" class="form-control" id="nama-lemari" placeholder="nama lemari">
			</div>
		</div>

		<!-- kepanjangan lemari -->
		<div class="form-group row">
			<label for="deskripsi" class="col-sm-2 col-form-label">deskripsi</label>
			<div class="col-sm-10">
				<input type="text"name="deskripsi" class="form-control" id="deskripsi" placeholder="deskripsi">
			</div>
		</div>

		<!-- simpan -->
		<input type="submit" value="Simpan" class="btn btn-primary" title="simpan lemari">
	</form>
</div>

<!-- pop up -->
<div class="modal fade" id="popupDetail" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="judulPopup">Informasi tambah lemari</h3>
				<button class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Nama lemari bisa berupa angka atau pun huruf. Tanda baca <b>&</b> jangan digunakan</p>
				<p>DEskripsi bersifat opsional</p>
				<p>Klik simpan untuk menyinpan</p>
			</div>
			<div class="modal-footer">
				<button class="btn btn-secondary" data-dismiss="modal">tutup</button>
			</div>
		</div>
	</div>
</div>
