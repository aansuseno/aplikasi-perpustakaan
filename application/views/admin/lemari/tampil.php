<div class="container">
	<div class="judul-halaman">
		<h1>Daftar Lemari</h1>
	</div>
	<?php if($pesan == 'berhasilHapus') {?>
	<div class="alert alert-primary">
		Data berhasil dihapus
	</div>
	<?php } else if($pesan == 'gagalHapus') {?>
	<div class="alert alert-primary">
		Data gagal dihapus karena data tertaut dengan beberapa bibliografi.
	</div>
	<?php } ?>
	<br>
	<br>
		<button
		 class="btn btn-info"
		 data-toggle="modal"
		 data-target="#popupDetail"
		 title="Informasi pengelolaan">
			<i class="fas fa-info"></i>
		</button>
	<br>
	<br>

	<table class="table table-bordered table-hover">
			<thead>
					<th scope="col">No</th>
					<th scope="col">Nama</th>
					<th scope="col">Deskripsi</th>
					<th scope="col">aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php  $no=0;foreach ($lemari as $l) {?>
				<tr>
					<?php $no++ ?>
					<td><?php echo $no ?></td>
					<td><?php echo $l->nama ?></td>
					<td><?php echo $l->deskripsi ?></td>
					<td><a href="<?= base_url() ?>lemari/detail?id=<?php echo $l->id ?>"><button class="btn btn-info btn-sm"><i class="fas fa-info-circle"></i></button></a></td>
				</tr>
				<?php }	 ?>
			</tbody>
		</table>
</div>

<!-- pop up -->
<div class="modal fade" id="popupDetail" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="judulPopup">Informasi kelola lemari</h3>
				<button class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<ul>
					<li>Klik ikon lingkaran i<li>
					<li>Akan  membuka halaman baru<li>
					<li>Operasional edit dan hapus ada disana</li>
				</ul>
			</div>
			<div class="modal-footer">
				<button class="btn btn-secondary" data-dismiss="modal">tutup</button>
			</div>
		</div>
	</div>
</div>
