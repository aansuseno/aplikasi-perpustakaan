<?php if($pesan == 'berhasilTambah') { ?>
<div class="alert alert-primary">Penerbit berhasil tambah.</div>
<br>
<?php } else if($pesan == 'berhasilHapus') { ?>
<div class="alert alert-primary">Penerbit berhasil dihapus.</div>
<br>
<?php } else if($pesan == 'berhasilEdit') { ?>
<div class="alert alert-primary">Penerbit berhasil diedit.</div>
<br>
<?php } else if($pesan == 'gagalHapus') { ?>
<div class="alert alert-primary">Penerbit gagal dihapus karena tertaut dengan beberapa bibliografi.</div>
<br>
<?php } ?>

<div class="btn btn-success float-right" onclick="tambahpenerbit('<?= base_url() ?>')" data-toggle="modal" data-target="#popup"><i class="fas fa-plus"></i></div>
<br><br>
<table class="table table-bordered table-hover">
	<thead>
			<th scope="col">No.</th>
			<th scope="col">Penerbit</th>
			<th scope="col">Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = $halamanSekarang * $jmlData; foreach ($penerbit as $k):?>
		<tr>
			<td><?php $no++; echo $no ?></td>
			<td><?= $k->nama ?></td>
			<td>
				<span
				 class="btn btn-info"
				 onclick="infopenerbitdatamaster('<?=base_url() ?>',<?php echo $k->id; ?>)"
				 data-toggle="modal"
				 data-target="#popup"
				 title="Detail penerbit">
					<i class="fas fa-info-circle"></i>
				</span>
				<span
				 class="btn btn-warning"
				 onclick="editpenerbitdatamaster('<?=base_url() ?>',<?php echo $k->id; ?>)"
				 data-toggle="modal"
				 data-target="#popup"
				 title="Edit penerbit">
					<i class="fas fa-edit"></i>
				</span>
				<span
				 class="btn btn-danger"
				 onclick="hapuspenerbitdatamaster('<?= base_url() ?>', <?php echo $k->id ?>, '<?= $k->nama ?>')"
				 data-toggle="modal"
				 data-target="#popup"
				 title="Hapus penerbit">
					<i class="fas fa-trash"></i>
				</span>
			</td>
		</tr>
		<?php endforeach ?>
	</tbody>
</table>

<!-- pagination -->
<div style="display: flex; flex-wrap: wrap; justify-content: space-around;">
<?php if ($jml_halaman > 1): ?>
	<ul class="pagination">
		<?php if ($halamanSekarang > 0): ?>
			<li class="page-item">
				<span
				class="page-link"
				style="cursor: pointer;"
				onclick="setAjak('datapenerbit', '<?= base_url() ?>penerbit/list?halaman=0')"
				>First
				</span>
			</li>
			<li class="page-item">
				<span
				class="page-link"
				style="cursor: pointer;"
				<?php if ($halamanSekarang > 0): ?>
					onclick="setAjak('datapenerbit', '<?= base_url() ?>penerbit/list?halaman='+<?= $halamanSekarang-1 ?>)"
				<?php endif ?>
				>&lt;
				</span>
			</li>
		<?php endif ?>
		<?php for ($i = $halamanSekarang - 2; $i <= $halamanSekarang + 4; $i++): ?>
			<?php if ($i > 0 && $i <= $jml_halaman): ?>
				<li
				class="page-item <?php if($halamanSekarang == $i - 1) { echo 'active'; }?>"
				style="cursor: pointer;">
					<span
					class="page-link"
					onclick="setAjak('datapenerbit', '<?= base_url() ?>penerbit/list?halaman='+<?= $i-1 ?>)">
						<?= $i ?>
					</span>
				</li>
			<?php endif ?>
		<?php endfor ?>
		<?php if ($halamanSekarang < $jml_halaman-1): ?>
			<li class="page-item">
				<span
				class="page-link"
				style="cursor: pointer;"
				<?php if ($halamanSekarang < $jml_halaman-1): ?>
					onclick="setAjak('datapenerbit', '<?= base_url() ?>penerbit/list?halaman='+<?= $halamanSekarang+1 ?>)"
				<?php endif ?>
				>&gt;
				</span>
			</li>
			<li class="page-item">
				<span
				class="page-link"
				style="cursor: pointer;"
				onclick="setAjak('datapenerbit', '<?= base_url() ?>penerbit/list?halaman='+<?= $jml_halaman-1 ?>)"
				>Last
				</span>
			</li>
		<?php endif ?>
	</ul>
<?php endif ?>
</div>
