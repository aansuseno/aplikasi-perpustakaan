<div class="container" id="container">
	<br>
	<?php if ($pesan == 'berhasil') { ?>
		<div class="alert alert-primary">
			Berhasil diedit
		</div>
	<?php } ?>
	<form action="<?= base_url().'admin/editPengaturanProses' ?>" method="post">
		<table class="table table-borderless">
			<tr>
				<td>
					Nama Sekolah
				</td>
				<td>
					<input type="text" name="nama_perpus" class="form-control" value="<?= $perpus->nama_perpus ?>">
				</td>
			</tr>
			<tr>
				<td>
					Moto
				</td>
				<td>
					<input type="text" name="moto" class="form-control" value="<?= $perpus->moto ?>">
				</td>
			</tr>
			<tr>
				<td>
					Deskripsi
				</td>
				<td>
					<textarea class="form-control" name="deskripsi"><?= $perpus->deskripsi ?></textarea>
				</td>
			</tr>
			<tr>
				<td>
					Denda
				</td>
				<td>
					<input type="text" name="denda" class="form-control" value="<?= $perpus->denda ?>">
				</td>
			</tr>
			<tr>
				<td>
					Lama peminjaman buku bawaan(dalam hari)
				</td>
				<td>
					<input type="text" name="lama_peminjaman" class="form-control" value="<?= $perpus->lama_peminjaman ?>">
				</td>
			</tr>
		</table>
		<input type="hidden" value="<?= $perpus->id ?>" name="id">
		<button type="submit" title="Edit perubahan" id="submit" class="btn btn-warning">Edit</button>
	</form>
	<br>
	<span onclick="setAjak('container', '<?= base_url() ?>admin/gantiLogo')" class="alert alert-success" style="cursor: pointer;">Klik untuk mengganti logo</span>
	<br>
	<br>
	<hr>


	<h3>Surat bebas perpustakaan</h3>
	<form action="<?= base_url() ?>admin/editPengaturanSuratBebasProses" method="post">
	<table class="table table-borderless">
		<tr>
			<td title="Nama yang nantinya akan tanda tangan dibagian surat keterangan bebas perpustakaan."><i class="fas fa-info-circle"></i>Nama Penanda Tangan</td>
			<td><input type="text" name="nama_penanda_tangan" class="form-control" value="<?= $perpus->nama_penanda_tangan ?>"></td>
		</tr>
		<tr>
			<td title="Jabatan yang bertanda tangan."><i class="fas fa-info-circle"></i>Jabatan</td>
			<td><input type="text" name="jabatan_penanda_tangan" class="form-control" value="<?= $perpus->jabatan_penanda_tangan ?>"></td>
		</tr>
		<tr>
			<td title="NIP penanda tangan yang akan dicetak dibawah surat."><i class="fas fa-info-circle"></i>NIP</td>
			<td><input type="text" name="nip_penanda_tangan" class="form-control" value="<?= $perpus->nip_penanda_tangan ?>"></td>
		</tr>
		<tr>
			<td title="Alamat sekolah yang akan tampil di kop surat."><i class="fas fa-info-circle"></i>Alamat</td>
			<td><input type="text" name="alamat" class="form-control" value="<?= $perpus->alamat ?>"></td>
		</tr>
		<tr>
			<td title="Kota perpustakaan."><i class="fas fa-info-circle"></i>Kota</td>
			<td><input type="text" name="kota" class="form-control" value="<?= $perpus->kota ?>"></td>
		</tr>
	</table>
	<input type="hidden" value="<?= $perpus->id ?>" name="id">
	<button class="btn btn-warning" title="Edit bagian surat saja.">Edit</button>
	</form>
</div>
