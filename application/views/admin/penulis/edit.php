<div class="modal-body">
	<table class="table borderless">
		<tr>
			<td>Nama</td>
			<td><input type="text" id="namapenulisedit" class="form-control" value="<?= $penulis1->nama ?>"></td>
		</tr>
		<tr>
			<td>Deskripsi</td>
			<td><input type="text" id="deskripsipenulisedit"class="form-control" value="<?= $penulis1->deskripsi ?>"></td>
		</tr>
	</table>
	<span class="btn btn-success" onclick="proseseditpenulis('<?php echo base_url() ?>', <?= $penulis1->id ?>)" data-dismiss="modal">Edit</span>
</div>