<div class="modal-body" style="max-height: 50vh; overflow:auto;">
	<ol>
		<li>
			<h4>Tambah Penulis</h4>
			<ol>
				<li>Penulis yang dimaksut yaitu pengarang buku</li>
				<li>Klik ikon <i class="fas fa-plus"></i></li>
				<li>Akan muncul popup</li>
				<li>Masukkan nama penulis</li>
				<li>Masukkan deskripsi(opsional)</li>
				<li>Aturan: dilarang menggunakan simbol <b>&</b></li>
				<li>Klik tombol tambah untuk menyimpan</li>
			</ol>
		</li>
		<li>
			<h4>Detail Penulis</h4>
			<ol>
				<li>Untuk melihat detail penulis bisa klik ikon <i class="fas fa-info-circle"></i></li>
			</ol>
		</li>
		<li>
			<h4>Edit Penulis</h4>
			<ol>
				<li>Untuk mengedit penulis bisa klik ikon <i class="fas fa-edit"></i></li>
				<li>Ganti nama penulis dan deskripsi</li>
				<li>Klik tombol edit untuk menyimpan</li>
			</ol>
		</li>
		<li>
			<h4>Hapus Penulis</h4>
			<ol>
				<li>Untuk menghapus penulis bisa klik ikon <i class="fas fa-trash"></i></li>
				<li>Tidak disarankan untuk mnghapus penulis</li>
			</ol>
		</li>
	</ol>
</div>
<div class="modal-footer">
	<button class="btn btn-secondary" data-dismiss="modal">tutup</button>
</div>
