<script type="text/javascript" src="<?= base_url() ?>assets/js/adminPenulis.js"></script>

<div class="container">
	<div class="judul-halaman">
		<h1>Daftar Penulis</h1>
	</div>
	<br>
	<input
	 type="text"
	 id="cari-siswa"
	 class="form-control"
	 oninput="setAjak('datapenulis', '<?= base_url() ?>penulis/cari?cari='+document.getElementById('cari-siswa').value)"
	 style="border-radius: 24px;"
	 placeholder="Cari berdasar nama penulis">
	<br>
	<br>

	<!-- tutorial -->
	<button
	 class="btn btn-info"
	 data-toggle="modal"
	 data-target="#popup"
	 onclick="
		document.getElementById('judulpopup').innerHTML = 'Informasi pengelolaan penulis'
		setAjak('isipopup', '<?= base_url() ?>penulis/informasi')
	 "
	 title="Informasi pengelolaan">
		<i class="fas fa-info"></i>
	</button>
	<br>
	<br>
	<div id="datapenulis">

		<!-- tambah penulis -->
		<div
		 class="btn btn-success float-right"
		 onclick="tambahpenulis('<?= base_url() ?>')"
		 data-toggle="modal"
		 data-target="#popup"
		 title="Tambah pengarang baru">
			<i class="fas fa-plus"></i>
		</div>
		<!-- end tambah penulis -->

		<br><br>
		<table class="table table-bordered table-hover">
			<thead>
					<th scope="col">No.</th>
					<th scope="col">penulis</th>
					<th scope="col">Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php $no = $halamanSekarang * $jmlData; foreach ($penulis as $k):?>
				<tr>
					<td><?php $no++; echo $no ?></td>
					<td><?= $k->nama ?></td>
					<td>
						<!-- info penulis -->
						<span
						 class="btn btn-info"
						 onclick="infopenulisdatamaster('<?=base_url() ?>',<?php echo $k->id; ?>)"
						 data-toggle="modal"
						 data-target="#popup"
						 title="Tampilkan lebih detail">
							<i class="fas fa-info-circle"></i>
						</span>
						<!-- end info penulis -->
						<!-- edit penulis -->
						<span
						 class="btn btn-warning"
						 onclick="editpenulisdatamaster('<?=base_url() ?>',<?php echo $k->id; ?>)"
						 data-toggle="modal"
						 data-target="#popup"
						 title="Edit penulis">
							<i class="fas fa-edit"></i>
						</span>
						<!-- end edit penulis -->
						<!-- hapus penulis -->
						<span
						 class="btn btn-danger"
						 onclick="hapuspenulisdatamaster('<?= base_url() ?>', <?php echo $k->id ?>, '<?= $k->nama ?>')"
						 data-toggle="modal"
						 data-target="#popup"
						 title="Hapus penulis">
							<i class="fas fa-trash"></i>
						</span>
						<!-- end hapus penulis -->
					</td>
				</tr>
				<?php endforeach ?>
			</tbody>
		</table>

		<!-- pagination -->
		<div style="display: flex; flex-wrap: wrap; justify-content: space-around;">
		<?php if ($jml_halaman > 1): ?>
			<ul class="pagination">
				<?php if ($halamanSekarang > 0): ?>
					<li class="page-item">
						<span
						class="page-link"
						style="cursor: pointer;"
						onclick="setAjak('datapenulis', '<?= base_url() ?>penulis/list?halaman=0')"
						>First
						</span>
					</li>
					<li class="page-item">
						<span
						class="page-link"
						style="cursor: pointer;"
						<?php if ($halamanSekarang > 0): ?>
							onclick="setAjak('datapenulis', '<?= base_url() ?>penulis/list?halaman='+<?= $halamanSekarang-1 ?>)"
						<?php endif ?>
						>&lt;
						</span>
					</li>
				<?php endif ?>
				<?php for ($i = $halamanSekarang - 2; $i <= $halamanSekarang + 4; $i++): ?>
					<?php if ($i > 0 && $i <= $jml_halaman): ?>
						<li
						class="page-item <?php if($halamanSekarang == $i - 1) { echo 'active'; }?>"
						style="cursor: pointer;">
							<span
							class="page-link"
							onclick="setAjak('datapenulis', '<?= base_url() ?>penulis/list?halaman='+<?= $i-1 ?>)">
								<?= $i ?>
							</span>
						</li>
					<?php endif ?>
				<?php endfor ?>
				<?php if ($halamanSekarang < $jml_halaman-1): ?>
					<li class="page-item">
						<span
						class="page-link"
						style="cursor: pointer;"
						<?php if ($halamanSekarang < $jml_halaman-1): ?>
							onclick="setAjak('datapenulis', '<?= base_url() ?>penulis/list?halaman='+<?= $halamanSekarang+1 ?>)"
						<?php endif ?>
						>&gt;
						</span>
					</li>
					<li class="page-item">
						<span
						class="page-link"
						style="cursor: pointer;"
						onclick="setAjak('datapenulis', '<?= base_url() ?>penulis/list?halaman='+<?= $jml_halaman-1 ?>)"
						>Last
						</span>
					</li>
				<?php endif ?>
			</ul>
		<?php endif ?>
		</div>
	</div>
</div>
<!-- pop up -->
<div class="modal fade" id="popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="judulpopup">Tambah Kelas!!</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div id="isipopup"></div>
		</div>
	</div>
</div>
