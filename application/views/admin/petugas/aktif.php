<div class="container">
	<div class="judul-halaman" id="judul">
		<h1>Daftar Petugas Belum Terdaftar</h1>
	</div>
	<br>
	<form>
		<br><br>
		<table class="table table-bordered table-hover">
			<thead>
					<th scope="col">No. </th>
					<th scope="col">Nama</th>
					<th scope="col">Wewenang</th>
					<th scope="col">Aksi</th>
				</tr>
			</thead>
			<tbody id="table-data">
				<?php $no = 0; foreach ($admin as $k):?>
				<tr>
					<?php $no++; ?>
					<td><?= $no ?></td>
					<td><?= $k->nama ?></td>
					<td><?php if ($k->wewenang == 'admin') { echo "Admin";} elseif ($k->wewenang == 'pustakawan') { echo "Pustakawan";} ?></td>
					<td>
						<?php if ($k->dikonfirmasi_oleh != -1 && $k->id != $id_user) { ?>
							<a
							 href="#"
							 data-toggle="modal"
							 data-target="#info-petugas-detail"
							 onclick="setAjak('info-admin', '<?= base_url() ?>petugas/infoadmin?id=<?= $k->id ?>')"
							 class="btn btn-info"
							 title="Detail petugas">
								<i class="fas fa-info-circle"></i>
							</a>

							<a
							 href="?id=<?= $k->id ?>"
							 onclick="bekuAdmin(<?= $k->id ?>, '<?= $k->nama ?>')"
							 data-toggle="modal"
							 data-target="#beku"
							 class="btn btn-danger"
							 title="Berhentikan petugas">
								<i class="fas fa-stop-circle"></i>
							</a>
						<?php } ?>
					</td>
				</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</form>
</div>

<!-- pop up beku-->
<div class="modal fade" id="beku" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title text-danger" id="exampleModalLongTitle">Peringatan !!</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
			Anda yakin akan mencabut izin <b id="nama-petugas-terpilih"></b>? Yang berarti tidak akan bisa mengkases halaman Administator, kecuali izin dikembalikan lagi.
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<a onclick="setAjak('table-data', '<?= base_url() ?>petugas/bekukan?id='+idA)" href="#">
					<button class="btn btn-danger"  data-toggle="modal" data-target="#beku"><i class="fas fa-stop-circle"></i> Ya</button>
				</a>
			</div>
		</div>
	</div>
</div>

<!-- pop up info -->
<div class="modal fade bd-example-modal-lg" id="info-petugas-detail" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-body" id="info-admin">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>
