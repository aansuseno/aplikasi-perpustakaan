<div class="container">
	<div class="judul-halaman" id="judul">
		<h1>Daftar Petugas Belum Terdaftar</h1>
	</div>
	<br>
	<form>
		<br><br>
		<table class="table table-bordered table-hover">
			<thead>
					<th scope="col">No. </th>
					<th scope="col">Nama</th>
					<th scope="col">Wewenang</th>
					<th scope="col">Aksi</th>
				</tr>
			</thead>
			<tbody id="table-data">
				
			</tbody>
		</table>
	</form>
</div>

<!-- pop up setuju-->
<div class="modal fade" id="setuju" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title text-danger" id="exampleModalLongTitle">Peringatan !!</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
			Dengan hak izin yang Anda berikan, pendaftar akan mempunyai hak akses untuk mengedit dan menghapus data-data. <b>Anda yakin?</b>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<a id="terima" target="_blank" href="">
					<button class="btn btn-danger"  data-toggle="modal" data-target="#setuju"><i class="fas fa-handshake"></i> Terima</button>
				</a>
			</div>
		</div>
	</div>
</div>

<!-- pop up tolak -->
<div class="modal fade" id="tolak" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title text-danger" id="exampleModalLongTitle">Peringatan !!</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
			Anda akan menghapus secara permanen.
			</div>
			<div class="modal-footer">
				<a href="#" target="_blank" onclick="tolakAdmin(idA)">
					<button class="btn btn-danger" data-dismiss="modal" data-target="#setuju"><i class="fas fa-times-circle"></i> Tolak</button>
				</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	let idA
	function setId(id) {
		idA = id
		document.getElementById('terima').href = "<?= base_url().'petugas/terima?id=' ?>"+idA
	}
	function klikTolakAdmin(id) {
		idA = id
	}
	function tolakAdmin(id) {
		var xhr = new XMLHttpRequest()

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				document.getElementById('tolak').tabindex = -1
				tampilDataPradaftar()
			}
		}

		xhr.open('GET', '<?= base_url()?>petugas/hapusAdminPradaftar?id='+id, true)
		xhr.send()
	}
	function tampilDataPradaftar() {
		var xhr = new XMLHttpRequest()

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				document.getElementById('table-data').innerHTML = this.responseText
			}
		}

		xhr.open('GET', '<?= base_url()?>petugas/pradaftar', true)
		xhr.send()
	}

	window.onload = tampilDataPradaftar()
	// document.getElementById("judul").onclick = function() {console.log('ok')}
</script>