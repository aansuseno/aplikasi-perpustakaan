<div class="container">
	<div class="judul-halaman">
		<h1>Detail Petugas Baru</h1>
	</div>
	<a href="<?= base_url() ?>petugas/baru"><button class="btn btn-dark"><i class="fas fa-arrow-circle-left"></i> Kembali</button></a>
	<br><br>
	<table>
		<tr>
			<th>Nama Lengkap</th>
			<td>: <?= $petugas->nama?></td>
		</tr>
		<tr>
			<th>WA</th>
			<td>: <?= $petugas->no_wa?></td>
		</tr>
		<tr>
			<th>Alamat</th>
			<td>: <?= $petugas->alamat?></td>
		</tr>
		<tr>
			<th>bio</th>
			<td>: <?= $petugas->bio?></td>
		</tr>
		<tr>
			<th>Wewenang</th>
			<td>: <?= $petugas->wewenang?></td>
		</tr>
		<tr>
			<th>Tanggal daftar</th>
			<td>: <?= $petugas->tgl_daftar ?></td>
		</tr>
	</table>
</div>