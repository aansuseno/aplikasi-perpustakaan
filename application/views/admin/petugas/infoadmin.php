<table class="table borderless">
	<tr>
		<th>Nama</th>
		<td>: <?= $admin->nama ?></td>
	</tr>
	<tr>
		<th>No. Wa</th>
		<td>: <?= $admin->no_wa ?> <a target="_blank" href="https://api.whatsapp.com/send?phone=<?= $admin->no_wa ?>" class="btn btn-sm" style="background-color: #25D366"><i class="fab fa-whatsapp"></i></a></td>
	</tr>
	<tr>
		<th>Alamat</th>
		<td>: <?= $admin->alamat ?></td>
	</tr>
	<tr>
		<th>Wewenang</th>
		<td>: <?= $admin->wewenang ?></td>
	</tr>
	<tr>
		<th>Tgl daftar</th>
		<td>: <?= $admin->tgl_daftar ?></td>
	</tr>
	<tr>
		<th>Dikonfirmasi oleh</th>
		<td>: <?= $disetujui->nama ?> <a target="_blank" href="https://api.whatsapp.com/send?phone=<?= $disetujui->no_wa ?>" class="btn btn-sm" style="background-color: #25D366"><i class="fab fa-whatsapp"></i></a></td>
	</tr>
</table>