<?php $no = 0; foreach ($admin as $k):?>
<tr>
	<?php $no++; ?>
	<td><?= $no ?></td>
	<td><?= $k->nama ?></td>
	<td><?php if ($k->wewenang == 'admin') { echo "Admin";} elseif ($k->wewenang == 'pustakawan') { echo "Pustakawan";} ?></td>
	<td>
		<a
		 href="#"
		 onclick="setId(<?= $k->id ?>)"
		 data-toggle="modal"
		 data-target="#setuju"
		 class="btn btn-primary"
		 title="Aktifkan petugas">
			<i class="fas fa-handshake"></i>
		</a>

		<a
		 href="<?= base_url() ?>petugas/detail?id=<?= $k->id?>"
		 class="btn btn-info"
		 title="Detail petugas">
			<i class="fas fa-info-circle"></i>
		</a>

		<a
		 href="#"
		 onclick="klikTolakAdmin(<?= $k->id ?>)"
		 data-toggle="modal"
		 data-target="#tolak"
		 class="btn btn-danger"
		 title="Hapus permanen petugas pradaftar">
			<i class="fas fa-times-circle"></i>
		</a>
	</td>
</tr>
<?php endforeach ?>
