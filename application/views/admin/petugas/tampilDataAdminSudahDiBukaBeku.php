<?php $no = 0; foreach ($admin as $k):?>
<tr>
	<?php $no++; ?>
	<td><?= $no ?></td>
	<td><?= $k->nama ?></td>
	<td><?php if ($k->wewenang == 'admin') { echo "Admin";} elseif ($k->wewenang == 'pustakawan') { echo "Pustakawan";} ?></td>
	<td>
		<?php if ($k->dikonfirmasi_oleh != -1 && $k->id != $id_user) { ?>
			<a
			 href="#"data-toggle="modal"
			 data-target="#info-petugas-detail"
			 onclick="setAjak('info-admin', '<?= base_url() ?>petugas/infoadmin?id=<?= $k->id ?>')"
			 class="btn btn-info"
			 title="Detail petugas">
				<i class="fas fa-info-circle"></i>
			</a>

			<a
			 href="?id=<?= $k->id ?>"
			 onclick="bekuAdmin(<?= $k->id ?>, '<?= $k->nama ?>')"
			 data-toggle="modal"
			 data-target="#bukaBeku"
			 class="btn btn-primary"
			 title="Aktifkan kembali petugas">
				<i class="fas fa-play-circle"></i>
			</a>
		<?php } ?>
	</td>
</tr>
<?php endforeach ?>
