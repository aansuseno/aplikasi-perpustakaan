<table class="table borderless">
	<tr>
		<th>NIS</th>
		<th>Nama</th>
		<th>Aksi</th>
	</tr>
	<?php foreach ($siswa as $s): ?>
		<tr>
			<td><?= $s->nis ?></td>
			<td><?= $s->nama ?></td>
			<td>
				<!-- reset -->
				<a
				 href="#"
				 class="btn btn-secondary"
				 onclick="
					docInnerHtml('namaSiswaArsipT', '<?= $s->nama ?>')
					idArsipkan = <?= $s->id ?>
				 "
				 data-toggle="modal"
				 data-target="#reset"
				 title="Reset password siswa">
					<i class="fas fa-unlock-alt"></i>
				</a>

				<!-- hapus -->
				<a href="#" onclick="hapusSiswa(<?= $s->id_kelas ?>,<?= $s->id ?>, '<?= $s->nama ?>')" class="btn btn-danger" data-toggle="modal" data-target="#hapus" title="Hapus siswa"><i class="fas fa-trash"></i></a>

				<!-- info -->
				<a href="#" class="btn btn-info" onclick="setDetail(<?= $s->nis?>,'<?= $s->nama ?>','<?= $s->nama_kelas ?>','<?= $s->jenis_kelamin ?>', '<?= $s->status ?>', '<?= $s->no_wa ?>')" data-toggle="modal" data-target="#info" title="Detail siswa"><i class="fas fa-info-circle"></i></a>

				<!-- edit -->
				<a href="#" class="btn btn-warning" onclick="setEdit(<?= $s->nis?>,'<?= $s->nama ?>','<?= $s->id_kelas ?>','<?= $s->jenis_kelamin ?>', '<?= $s->status ?>', <?= $s->id ?>)" data-toggle="modal" data-target="#edit" title="Edit siswa"><i class="fas fa-edit"></i></a>
			</td>
		</tr>
	<?php endforeach ?>
</table>

<!-- pagination -->
<div style="display: flex; flex-wrap: wrap; justify-content: space-around;">
<?php if ($jml_halaman > 1): ?>
	<ul class="pagination">
		<?php if ($halamanSekarang > 0): ?>
			<li class="page-item">
				<span
				class="page-link"
				style="cursor: pointer;"
				onclick="setAjak('tampilData', '<?= base_url() ?>siswa/cari?halaman=0&cari=<?= $cari ?>')"
				>First
				</span>
			</li>
			<li class="page-item">
				<span
				class="page-link"
				style="cursor: pointer;"
				<?php if ($halamanSekarang > 0): ?>
					onclick="setAjak('tampilData', '<?= base_url() ?>siswa/cari?halaman='+<?= $halamanSekarang-1 ?>+'&cari=<?= $cari ?>')"
				<?php endif ?>
				>&lt;
				</span>
			</li>
		<?php endif ?>
		<?php for ($i = $halamanSekarang - 2; $i <= $halamanSekarang + 4; $i++): ?>
			<?php if ($i > 0 && $i <= $jml_halaman): ?>
				<li
				class="page-item <?php if($halamanSekarang == $i - 1) { echo 'active'; }?>"
				style="cursor: pointer;">
					<span
					class="page-link"
					onclick="setAjak('tampilData', '<?= base_url() ?>siswa/cari?halaman='+<?= $i-1 ?>+'&cari=<?= $cari ?>')">
						<?= $i ?>
					</span>
				</li>
			<?php endif ?>
		<?php endfor ?>
		<?php if ($halamanSekarang < $jml_halaman-1): ?>
			<li class="page-item">
				<span
				class="page-link"
				style="cursor: pointer;"
				<?php if ($halamanSekarang < $jml_halaman-1): ?>
					onclick="setAjak('tampilData', '<?= base_url() ?>siswa/cari?halaman='+<?= $halamanSekarang+1 ?>+'&cari=<?= $cari ?>')"
				<?php endif ?>
				>&gt;
				</span>
			</li>
			<li class="page-item">
				<span
				class="page-link"
				style="cursor: pointer;"
				onclick="setAjak('tampilData', '<?= base_url() ?>siswa/cari?halaman='+<?= $jml_halaman-1 ?>+'&cari=<?= $cari ?>')"
				>Last
				</span>
			</li>
		<?php endif ?>
	</ul>
<?php endif ?>
</div>
