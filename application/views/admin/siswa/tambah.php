<script type="text/javascript">
	var namakelasbaru = ''
	var deskrisikelasbaru = ''
	function tambahkelasbaru() {
		namakelasbaru = document.getElementById('Kelas').value
		deskrisikelasbaru = document.getElementById('deskrisi').value
		setAjak('tambahSiswa', '<?= base_url() ?>siswa/tambahKelasProses?nama='+namakelasbaru+'&deskripsi='+deskrisikelasbaru)
	}
	function tambahSiswaProses() {
		nis = document.getElementById('nis').value
		nama = document.getElementById('nama').value
		jns_kelamin = document.getElementById('jns_kelamin').value
		kelas = document.getElementById('kelas').value
		setAjak('tambahSiswa', '<?= base_url() ?>siswa/tambahSiswaProses?nama='+nama+'&nis='+nis+'&jns_kelamin='+jns_kelamin+'&kelas='+kelas)
	}
</script>
<div class="container">
	<div class="judul-halaman">
		<h1><?= $judul_halaman ?></h1>
	</div>
	<br>
	<div id="tambahSiswa"></div>
</div>

<!-- pop up tambah kelas -->
<div class="modal fade bd-example-modal-lg" id="tambah_kelas" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-body" id="tambahKelas">
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?= base_url()?>/assets/js/ajax.js"></script>
<script type="text/javascript">
	document.getElementById('tambahSiswa').onload = setAjak('tambahSiswa', '<?= base_url() ?>siswa/tampilTambah')
</script>