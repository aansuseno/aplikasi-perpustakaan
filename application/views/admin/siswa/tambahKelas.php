<!-- nama -->
<div class="form-group row">
	<label for="Kelas" class="col-sm-2 col-form-label">Kelas</label>
	<div class="col-sm-10">
		<input type="text" autocomplete="off" class="form-control" id="Kelas" title="Inputkan nama kelas disini">
	</div>
</div>

<!-- deskrisi -->
<div class="form-group row">
	<label for="deskrisi" class="col-sm-2 col-form-label">Deskrisi</label>
	<div class="col-lg-10">
		<input type="text" title="Deskripsi kelas(opsional)" autocomplete="off" class="form-control" id="deskrisi">
	</div>
</div>

<!-- simpan -->
<a href="#" title="Simpan kelas" id="simpantambahkelabaru" onclick="tambahkelasbaru()" data-dismiss="modal" class="btn btn-primary">Simpan</a>
