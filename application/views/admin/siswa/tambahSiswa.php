<?php if ($pesan == 'berhasil') {?>
	<div class="alert alert-primary" role="alert">
		Siswa baru berhasil ditambahkan.
	</div>
<?php } else if ($pesan == 'nisAda') {?>
	<div class="alert alert-primary" role="alert">
		Siswa gagal ditambahkan. Karena NIS sudah digunakan.
	</div>
<?php } ?>

<!-- nis -->
<small id="niswarn" class="form-text text-danger"></small>
<div class="form-group row">
	<label for="nis" class="col-sm-2 col-form-label">NIS</label>
	<div class="col-sm-7">
		<input type="text" title="Nomor induk siswa disini" oninput="harusAngka('nis')" autocomplete="off" class="form-control" id="nis" placeholder="12345">
	</div>
</div>

<!-- nama -->
<div class="form-group row">
	<label for="nama" class="col-sm-2 col-form-label">Nama</label>
	<div class="col-lg-7">
		<input type="text" title="Inputkan nama siswa" oninput="janganKosong('nama')" autocomplete="off" class="form-control" id="nama" placeholder="">
	</div>
</div>

<!-- jenis kelamin -->
<div class="form-group row">
	<label class="col-sm-2 col-form-label">Jenis Kelamin</label>
	<div class="col-sm-10">
		<div class="input-group" title="Pilih jenis kelamin">
			<div class="input-group-append">
				<select class="form-control" id="jns_kelamin">
					<option value="l">Laki-laki</option>
					<option value="p">Perempuan</option>
				</select>
			</div>
		</div>
	</div>
</div>

<!-- Kelas -->
<div class="form-group row">
	<label class="col-sm-2 col-form-label">Kelas</label>
	<div class="col-sm-10">
		<div class="input-group">
			<div class="input-group-append" title="Pilih kelas">
				<select class="form-control" id="kelas">
					<?php foreach ($kelas as $k) { ?>
						<option value="<?= $k->id?>"><?= $k->nama?></option>
					<?php } ?>
				</select>
			</div>
			<a href="#" onclick="setAjak('tambahKelas', '<?= base_url() ?>siswa/tampilTambahKelas')" data-toggle="modal" title="Tambah kelas baru" data-target="#tambah_kelas"  class="nav-icon btn btn-success"><i class="fas fa-plus-circle"></i></a>
		</div>
	</div>
</div>

<!-- simpan -->
<button type="submit" onclick="tambahSiswaProses()" id="submit" disabled="disabled" class="btn btn-primary">Simpan</button>
