<script type="text/javascript" src="<?= base_url()?>/assets/js/ajax.js"></script>
<script type="text/javascript">
	var idkelas = 0
	var namakelasbaru = ''
	var deskrisikelasbaru = ''

	function tambahkelasbaru() {
		namakelasbaru = document.getElementById('Kelas').value
		deskrisikelasbaru = document.getElementById('deskrisi').value
		setAjak('tampilData', '<?= base_url() ?>siswa/tambahKelasProsesTampilSIswa?nama='+namakelasbaru+'&deskripsi='+deskrisikelasbaru)
	}

	document.getElementById('tampilData').onload = setAjak('tampilData', '<?= base_url() ?>siswa/tampilKelas')

	function tampilSiswa(id) {
		setAjak('tampilData', '<?= base_url() ?>siswa/tampilSiswaKelas?id='+id)
	}

	function tambahSiswaProses() {
		nis = document.getElementById('nis').value
		nama = document.getElementById('nama').value
		jns_kelamin = document.getElementById('jns_kelamin').value
		kelas = document.getElementById('kelas').value
		setAjak('tampilData', '<?= base_url() ?>siswa/tambahSiswaProses?nama='+nama+'&nis='+nis+'&jns_kelamin='+jns_kelamin+'&kelas='+kelas+'&pesan=tampil')
	}

	function setIdKelas(id) {
		document.getElementById('k'+id).selected = "selected"
	}

	function hapusSiswa(id_kelas, id_siswa, nama) {
		docInnerHtml('namaSiswa', nama)
		document.getElementById('hapusSiswaAlamat').innerHTML = '<button type="button" onclick="hapusYakin('+id_kelas+','+id_siswa+')" class="btn btn-danger"><i class="fas fa-trash"></i> Hapus</button>'
	}

	function hapusYakin(kelas, siswa) {
		setAjak('tampilData', '<?= base_url() ?>siswa/hapusSiswaProses?kelas='+kelas+'&siswa='+siswa)
	}

	function setDetail(nis, nama, kelas, jk, status, wa) {
		docInnerHtml('siswa_detail_nis', nis)
		docInnerHtml('siswa_detail_nama', nama)
		docInnerHtml('siswa_detail_kelas', kelas)
		if(jk == 'l') {
			docInnerHtml('siswa_detail_jk', 'Laki-laki')
		} else if(jk == 'p') {
			docInnerHtml('siswa_detail_jk', 'Perempuan')
		}
		if(status == 'n') {
			docInnerHtml('siswa_detail_status', 'Tidak aktif')
		} else if(status == 'y') {
			docInnerHtml('siswa_detail_status', 'Aktif')
		}
	}

	function setEdit(nis, nama, kelas, jk, status, id) {
		document.getElementById('siswa_edit_id').value = id
		document.getElementById('siswa_edit_nis').value = nis
		document.getElementById('siswa_edit_nama').value = nama
		document.getElementById('edit'+kelas).selected = "selected"
		document.getElementById('jk_'+jk).selected = "selected"
	}

	function editProses() {
		nis = getNilai('siswa_edit_nis')
		nama = getNilai('siswa_edit_nama')
		kelas = getNilai('siswa_edit_kelas')
		jk = getNilai('siswa_edit_jk')
		id = getNilai('siswa_edit_id')
		setAjak('tampilData', '<?= base_url() ?>siswa/editSiswaProses?nis='+nis+'&nama='+nama+'&kelas='+kelas+'&jk='+jk+'&id='+id)
	}

	function setNaikKelas(kelas) {
		document.getElementById('idKelasNaik').value = kelas
		document.getElementById('naikKelas'+kelas).selected = "selected"
	}

	function prosesNaik() {
		setAjak('tampilData', '<?= base_url() ?>siswa/prosesNaikKelas?dari='+getNilai('idKelasNaik')+'&ke='+getNilai('naikKelasId'))
	}
</script>

<div class="container">
	<div class="judul-halaman">
		<h1><?= $judul_halaman ?></h1>
	</div>
	<br>
	<input
	 type="text"
	 id="cari-siswa"
	 class="form-control"
	 oninput="setAjak('tampilData', '<?= base_url() ?>siswa/cari?cari='+document.getElementById('cari-siswa').value)"
	 style="border-radius: 24px;"
	 placeholder="Cari berdasar nama siswa atau NIS"
	 title="Cari siswa">
	<br>
	<div id="tampilData">

	</div>
</div>

<!-- pop up tambah kelas -->
<div class="modal fade" id="tambah_kelas" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="exampleModalLongTitle">Tambah Kelas!!</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="tambahKelas">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
			</div>
		</div>
	</div>
</div>

<!-- pop up tambah siswa -->
<div class="modal fade" id="tambah_siswa" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<!-- nis -->
				<small id="niswarn" class="form-text text-danger"></small>
				<div class="form-group row">
					<label for="nis" class="col-sm-2 col-form-label">NIS</label>
					<div class="col-sm-7">
						<input type="text" oninput="harusAngka('nis')" autocomplete="off" class="form-control" id="nis" placeholder="12345">
					</div>
				</div>

				<!-- nama -->
				<div class="form-group row">
					<label for="nama" class="col-sm-2 col-form-label">Nama</label>
					<div class="col-lg-7">
						<input type="text" oninput="janganKosong('nama')" autocomplete="off" class="form-control" id="nama" placeholder="">
					</div>
				</div>

				<!-- jenis kelamin -->
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Jenis Kelamin</label>
					<div class="col-sm-10">
						<div class="input-group">
							<div class="input-group-append">
								<select class="form-control" id="jns_kelamin">
									<option value="l">Laki-laki</option>
									<option value="p">Perempuan</option>
								</select>
							</div>
						</div>
					</div>
				</div>

				<!-- Kelas -->
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Kelas</label>
					<div class="col-sm-10">
						<div class="input-group">
							<div class="input-group-append">
								<select class="form-control" id="kelas">
									<?php foreach ($kelas as $k) { ?>
										<option id="k<?=$k->id?>" value="<?= $k->id?>"><?= $k->nama?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div>
				</div>

				<!-- simpan -->
				<button type="submit" onclick="tambahSiswaProses()" data-dismiss="modal" id="submit" disabled="disabled" class="btn btn-primary">Simpan</button>
			</div>
		</div>
	</div>
</div>

<!-- pop up hapus-->
<div class="modal fade" id="hapus" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title text-danger" id="exampleModalLongTitle">Peringatan !!</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
			Anda yakin untuk menghapus <b id="namaSiswa"></b>?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
				<a href="#" id="hapusSiswaAlamat" data-dismiss="modal">

				</a>
			</div>
		</div>
	</div>
</div>

<!-- pop up info -->
<div class="modal fade" id="info" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="exampleModalLongTitle">Info</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<table class="table borderless">
					<tr>
						<th>NIS</th>
						<td>: <span id="siswa_detail_nis"></span></td>
					</tr>
					<tr>
						<th>Nama</th>
						<td>: <span id="siswa_detail_nama"></span></td>
					</tr>
					<tr>
						<th>Kelas</th>
						<td>: <span id="siswa_detail_kelas"></span></td>
					</tr>
					<tr>
						<th>Jenis Kelamin</th>
						<td>: <span id="siswa_detail_jk"></span></td>
					</tr>
					<tr>
						<th>Status</th>
						<td>: <span id="siswa_detail_status"></span></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- pop up edit -->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="exampleModalLongTitle">Edit <i class="fas fa-edit"></i></h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<table class="table borderless">
					<input type="hidden" id="siswa_edit_id">
					<tr>
						<th>NIS</th>
						<td><input type="text" class="form-control" autocomplete="off" id="siswa_edit_nis"></td>
					</tr>
					<tr>
						<th>Nama</th>
						<td><input type="text" class="form-control" autocomplete="off" id="siswa_edit_nama"></span></td>
					</tr>
					<tr>
						<th>Kelas</th>
						<td>
							<select id="siswa_edit_kelas" class="form-control">
								<?php foreach ($kelas as $k) { ?>
									<option id="edit<?=$k->id?>" value="<?= $k->id?>"><?= $k->nama?></option>
								<?php } ?>
							</select>
						</td>
					</tr>
					<tr>
						<th>Jenis Kelamin</th>
						<td>
							<select id="siswa_edit_jk" class="form-control">
								<option id="jk_l" value="l">Laki-laki</option>
								<option id="jk_p" value="p">Perempuan</option>
							</select>
						</td>
					</tr>
				</table>
				<br>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
				<a href="#" class="btn btn-warning float-right" onclick="editProses()" data-toggle="modal" data-target="#edit">Edit <i class="fas fa-edit"></i></a>
			</div>
		</div>
	</div>
</div>

<!-- pop up naik kelas -->
<div class="modal fade" id="naikKelas" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title">Naik Kelas</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<input type="hidden" id="idKelasNaik">
				<b>Naik ke Kelas:</b>
				<select class="form-control" id="naikKelasId">
					<?php foreach($kelas as $k) { ?>
						<option id="naikKelas<?= $k->id ?>" value="<?= $k->id ?>"><?= $k->nama ?></option>
					<?php } ?>
					<option value="x" title="Klik jika siswa sudah lulus. Pastikan semua siswa kelas ini sedang tidak dalam meminjam buku.">Lulus</option>
				</select>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-dismiss="modal" onclick="prosesNaik()">Naik Kelas</a>
			</div>
		</div>
	</div>
</div>

<!-- pop up arsip-->
<div class="modal fade" id="arsip" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title text-danger" id="exampleModalLongTitle">Peringatan !!</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
			Anda yakin untuk mengarsipkan <b id="namaSiswaArsip"></b>? <br>
			Siswa tidak akan pernah muncul lagi.
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<a href="#" data-dismiss="modal">
					<button class="btn btn-primary" onclick="setAjak('tampilData', '<?= base_url() ?>siswa/arsipkan?id='+idArsipkan)" data-dismiss="modal">Arsipkan</button>
				</a>
			</div>
		</div>
	</div>
</div>

<!-- pop up reset-->
<div class="modal fade" id="reset" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title text-danger" id="exampleModalLongTitle">Peringatan !!</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
			Anda yakin untuk mereset password dari <b id="namaSiswaArsipT"></b>?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<a href="#" data-dismiss="modal">
					<button class="btn btn-primary" onclick="setAjak('tampilData', '<?= base_url() ?>siswa/reset?id='+idArsipkan)" data-dismiss="modal">Reset</button>
				</a>
			</div>
		</div>
	</div>
</div>
