<a
 href="#"
 class="btn btn-success float-right"
 onclick="setAjak('tambahKelas', '<?= base_url() ?>siswa/tampilTambahKelas')"
 data-toggle="modal"
 data-target="#tambah_kelas"
 title="Tambah kelas baru">Tambah Kelas</a>
<br><br>
<div class="row">
<?php $i=0; foreach ($kelas as $k) {?>
	<div class="col-sm-4" style="cursor: pointer;">
		<div class="card" onclick="tampilSiswa(<?= $k->id ?>)" title="Klik untuk menampilkan semua siswa kelas <?= $k->nama ?>">
			<div class="card-body">
				<div class="d-flex justify-content-between px-md-1">
					<div>
						<h3 class="text-success"><?= $k->jml_siswa?></h3>
						<p class="mb-0"><?= $k->nama?></p>
					</div>
					<div class="align-self-center">
						<i class="fas fa-user-graduate text-success fa-3x"></i>
					</div>
				</div>
				<div class="px-md-1">
					<div class="progress mt-3 mb-1 rounded" style="height: 12px;" title="<?= $jmlAktif[$i] ?> dari <?= $k->jml_siswa ?> siswa sudah mendaftar">
						<div class="progress-bar bg-success" role="progressbar" style="width:
							<?php
							if($k->jml_siswa != 0) {
								echo ($jmlAktif[$i]*100.00)/$k->jml_siswa;
							} else {echo 100;}
							$i++;
							?>%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>
</div>
