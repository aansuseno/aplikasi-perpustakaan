<?php if($pesan == 'berhasilTambah') { ?>
<div class="alert alert-primary">Data berhasil tambah</div>
<?php } else if($pesan == 'berhasilEdit') { ?>
<div class="alert alert-primary">Data berhasil diedit</div>
<?php } else if($pesan == 'berhasilHapus') { ?>
<div class="alert alert-primary">Data berhasil dihapus</div>
<?php } else if($pesan == 'gagalHapus') { ?>
<div class="alert alert-primary">Siswa gagal dihapus. Dikarenakan pernah melakukan transaksi. Alternatif nya diarsipkan</div>
<?php } else if($pesan == 'gagalArsip') { ?>
<div class="alert alert-primary">Siswa gagal diarsipkan. Dikarenakan sedang meminjam buku.</div>
<?php } else if($pesan == 'berhasilArsip') { ?>
<div class="alert alert-primary">Siswa berhasil diarsipkan.</div>
<?php } else if($pesan == 'berhasilNaik') { ?>
<div class="alert alert-primary">Siswa berhasil naik kelas.</div>
<?php } else if($pesan == 'berhasilLulus') { ?>
<div class="alert alert-primary">Siswa berhasil Lulus.</div>
<?php } else if($pesan == 'nisAda') { ?>
<div class="alert alert-primary">Siswa gagal ditambahkan. Karena nis sudah digunakan.</div>
<?php } else if($pesan == 'berhasilReset') { ?>
<div class="alert alert-primary">Password berhasi direset. Sekarang siswa bisa daftar ulang kembali.</div>
<?php } ?>
<br>

<a
 href="#"
 class="btn btn-secondary"
 onclick="setNaikKelas(<?= $idkelas ?>)"
 data-toggle="modal"
 data-target="#naikKelas"
 title="Klik untuk mengganti kelas semua siswa dalam kelas ini">
	Naik Kelas <i class="fas fa-sign-out-alt" style="transform: rotate(-90deg);"></i>
</a>

<a
 href="#"
 class="btn btn-success float-right"
 onclick="setIdKelas(<?= $idkelas?>)"
 data-toggle="modal"
 data-target="#tambah_siswa"
 title="Tambah siswa baru">
	Tambah Siswa <i class="fas fa-plus"></i>
</a>
<br><br>
<ul class="nav nav-tabs">
	<?php foreach ($kelas as $k): ?>
		<li class="nav-item" title="Tampil siswa kelas <?= $k->nama ?>">
			<a class="nav-link <?php if($idkelas == $k->id) : echo "active"; endif; ?>" href="#" onclick="tampilSiswa(<?= $k->id ?>)"><b><?= $k->nama ?></b></a>
		</li>
	<?php endforeach; ?>
</ul>
<br>
<div class="siswas">
	<table class="table borderless">
		<tr>
			<th>NIS</th>
			<th>Nama</th>
			<th>Aksi</th>
		</tr>
		<?php foreach ($siswa as $s): ?>
			<tr>
				<td><?= $s->nis ?></td>
				<td><?= $s->nama ?></td>
				<td>
					<!-- reset -->
					<a
					 href="#"
					 class="btn btn-secondary"
					 onclick="
						docInnerHtml('namaSiswaArsipT', '<?= $s->nama ?>')
						idArsipkan = <?= $s->id ?>
					 "
					 data-toggle="modal"
					 data-target="#reset"
					 title="Reset password siswa">
						<i class="fas fa-unlock-alt"></i>
					</a>

					<!-- hapus -->
					<a
					 href="#"
					 onclick="hapusSiswa(<?= $idkelas ?>,<?= $s->id ?>, '<?= $s->nama ?>')"
					 class="btn btn-danger"
					 data-toggle="modal"
					 data-target="#hapus"
					 title="Hapus siswa">
						<i class="fas fa-trash"></i>
					</a>

					<!-- info -->
					<a
					 href="#"
					 class="btn btn-info"
					 onclick="setDetail(<?= $s->nis?>,'<?= $s->nama ?>','<?= $namakelassiswa->nama ?>','<?= $s->jenis_kelamin ?>', '<?= $s->status ?>', '<?= $s->no_wa ?>')"
					 data-toggle="modal"
					 data-target="#info"
					 title="Detail siswa">
						<i class="fas fa-info-circle"></i>
					</a>

					<!-- edit -->
					<a
					 href="#"
					 class="btn btn-warning"
					 onclick="setEdit(<?= $s->nis?>,'<?= $s->nama ?>','<?= $namakelassiswa->id ?>','<?= $s->jenis_kelamin ?>', '<?= $s->status ?>', <?= $s->id ?>)"
					 data-toggle="modal"
					 data-target="#edit"
					 title="Edit siswa">
						<i class="fas fa-edit"></i>
					</a>

					<!-- arsip -->
					<a
					 href="#"
					 class="btn btn-secondary"
					 onclick="
						docInnerHtml('namaSiswaArsip', '<?= $s->nama ?>')
						idArsipkan = <?= $s->id ?>
					 "
					 data-toggle="modal"
					 data-target="#arsip"
					 title="Arsipkan siswa">
						<i class="fas fa-archive"></i>
					</a>
				</td>
			</tr>
		<?php endforeach ?>
	</table>
</div>
<!-- pagination -->
<div style="display: flex; flex-wrap: wrap; justify-content: space-around;">
<?php if ($jml_halaman > 1): ?>
	<ul class="pagination">
		<?php if ($halamanSekarang > 0): ?>
			<li class="page-item">
				<span
				class="page-link"
				style="cursor: pointer;"
				onclick="setAjak('tampilData', '<?= base_url() ?>siswa/tampilSiswaKelas?halaman=0&id=<?= $idkelas ?>')"
				>First
				</span>
			</li>
			<li class="page-item">
				<span
				class="page-link"
				style="cursor: pointer;"
				<?php if ($halamanSekarang > 0): ?>
					onclick="setAjak('tampilData', '<?= base_url() ?>siswa/tampilSiswaKelas?halaman='+<?= $halamanSekarang-1 ?>+'&id=<?= $idkelas ?>')"
				<?php endif ?>
				>&lt;
				</span>
			</li>
		<?php endif ?>
		<?php for ($i = $halamanSekarang - 2; $i <= $halamanSekarang + 4; $i++): ?>
			<?php if ($i > 0 && $i <= $jml_halaman): ?>
				<li
				class="page-item <?php if($halamanSekarang == $i - 1) { echo 'active'; }?>"
				style="cursor: pointer;">
					<span
					class="page-link"
					onclick="setAjak('tampilData', '<?= base_url() ?>siswa/tampilSiswaKelas?halaman='+<?= $i-1 ?>+'&id=<?= $idkelas ?>')">
						<?= $i ?>
					</span>
				</li>
			<?php endif ?>
		<?php endfor ?>
		<?php if ($halamanSekarang < $jml_halaman-1): ?>
			<li class="page-item">
				<span
				class="page-link"
				style="cursor: pointer;"
				<?php if ($halamanSekarang < $jml_halaman-1): ?>
					onclick="setAjak('tampilData', '<?= base_url() ?>siswa/tampilSiswaKelas?halaman='+<?= $halamanSekarang+1 ?>+'&id=<?= $idkelas ?>')"
				<?php endif ?>
				>&gt;
				</span>
			</li>
			<li class="page-item">
				<span
				class="page-link"
				style="cursor: pointer;"
				onclick="setAjak('tampilData', '<?= base_url() ?>siswa/tampilSiswaKelas?halaman='+<?= $jml_halaman-1 ?>+'&id=<?= $idkelas ?>')"
				>Last
				</span>
			</li>
		<?php endif ?>
	</ul>
<?php endif ?>
</div>
