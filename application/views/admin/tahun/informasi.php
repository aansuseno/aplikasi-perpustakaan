<div class="modal-body" style="max-height: 50vh; overflow:auto;">
	<ol>
		<li>
			<h4>Tambah Tahun</h4>
			<ol>
				<li>Klik tombol tambah</li>
				<li>Akan muncul popup</li>
				<li>Masukkan nama tahun. Harus berupa bilangan bulat 4 digit</li>
				<li>Masukkan deskripsi(opsional)</li>
				<li>Aturan: dilarang menggunakan simbol <b>&</b></li>
				<li>Klik tombol tambah untuk menyimpan</li>
			</ol>
		</li>
		<li>
			<h4>Detail Tahun</h4>
			<ol>
				<li>Untuk melihat detail tahun bisa klik ikon <i class="fas fa-info-circle"></i></li>
			</ol>
		</li>
		<li>
			<h4>Edit Tahun</h4>
			<ol>
				<li>Untuk mengedit tahun bisa klik ikon <i class="fas fa-edit"></i></li>
				<li>Ganti nama tahun dan deskripsi</li>
				<li>Klik tombol edit untuk menyimpan</li>
			</ol>
		</li>
		<li>
			<h4>Hapus Tahun</h4>
			<ol>
				<li>Untuk menghapus tahun bisa klik ikon <i class="fas fa-trash"></i></li>
				<li>Tidak disarankan untuk mnghapus tahun</li>
			</ol>
		</li>
	</ol>
</div>
<div class="modal-footer">
	<button class="btn btn-secondary" data-dismiss="modal">tutup</button>
</div>
