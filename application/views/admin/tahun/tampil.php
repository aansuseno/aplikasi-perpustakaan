<script type="text/javascript" src="<?php echo base_url('assets/js/adminTahun.js') ?>"></script>

<div class="container">
	<div class="judul-halaman">
		<h1>Tahun</h1>
	</div>
	<br>
	<!-- tutorial -->
	<button
	 class="btn btn-info"
	 data-toggle="modal"
	 data-target="#popup"
	 onclick="
		document.getElementById('judulpopup').innerHTML = 'Informasi pengelolaan tahun'
		setAjak('isipopup', '<?= base_url() ?>tahun/informasi')
	 "
	 title="Informasi pengelolaan">
		<i class="fas fa-info"></i>
	</button>
	<br>
	<span class="btn btn-success float-right" onclick="tambahTahunBaru('<?php echo base_url() ?>')" data-toggle="modal" data-target="#popup" title="Tambah tahun baru">Tambah</span><br><br>
	<div id="datatahun">
		<table class="table table-bordered table-hover">
			<thead>
					<th scope="col">No</th>
					<th scope="col">Nama</th>
					<th scope="col">aksi</th>
				</tr>
			</thead>
			<tbody >
				<?php  $no=0;foreach ($tahun as $l) {?>
				<tr>
					<?php $no++ ?>
					<td><?php echo $no ?></td>
					<td><?php echo $l->nama ?></td>
					<td>
						<span
						 class="btn btn-info"
						 onclick="infotahundatamaster('<?=base_url() ?>',<?php echo $l->id; ?>)"
						 data-toggle="modal"
						 data-target="#popup"
						 title="Detail tahun">
							<i class="fas fa-info-circle"></i>
						</span>
						<span
						 class="btn btn-warning"
						 onclick="edittahundatamaster('<?=base_url() ?>',<?php echo $l->id; ?>)"
						 data-toggle="modal"
						 data-target="#popup"
						 title="Edit tahun">
							<i class="fas fa-edit"></i>
						</span>
						<span
						 class="btn btn-danger"
						 onclick="hapustahundatamaster('<?= base_url() ?>', <?php echo $l->id ?>, '<?= $l->nama ?>')"
						 data-toggle="modal"
						 data-target="#popup"
						 title="Hapus tahun">
							<i class="fas fa-trash"></i>
						</span>
					</td>
				</tr>
				<?php }	 ?>
			</tbody>
		</table>

		<!-- pagination -->
		<div style="display: flex; flex-wrap: wrap; justify-content: space-around;">
		<?php if ($jml_halaman > 1): ?>
			<ul class="pagination">
				<?php if ($halamanSekarang > 0): ?>
					<li class="page-item">
						<span
						class="page-link"
						style="cursor: pointer;"
						onclick="setAjak('datatahun', '<?= base_url() ?>tahun/list?halaman=0')"
						>First
						</span>
					</li>
					<li class="page-item">
						<span
						class="page-link"
						style="cursor: pointer;"
						<?php if ($halamanSekarang > 0): ?>
							onclick="setAjak('datatahun', '<?= base_url() ?>tahun/list?halaman='+<?= $halamanSekarang-1 ?>)"
						<?php endif ?>
						>&lt;
						</span>
					</li>
				<?php endif ?>
				<?php for ($i = $halamanSekarang - 2; $i <= $halamanSekarang + 4; $i++): ?>
					<?php if ($i > 0 && $i <= $jml_halaman): ?>
						<li
						class="page-item <?php if($halamanSekarang == $i - 1) { echo 'active'; }?>"
						style="cursor: pointer;">
							<span
							class="page-link"
							onclick="setAjak('datatahun', '<?= base_url() ?>tahun/list?halaman='+<?= $i-1 ?>)">
								<?= $i ?>
							</span>
						</li>
					<?php endif ?>
				<?php endfor ?>
				<?php if ($halamanSekarang < $jml_halaman-1): ?>
					<li class="page-item">
						<span
						class="page-link"
						style="cursor: pointer;"
						<?php if ($halamanSekarang < $jml_halaman-1): ?>
							onclick="setAjak('datatahun', '<?= base_url() ?>tahun/list?halaman='+<?= $halamanSekarang+1 ?>)"
						<?php endif ?>
						>&gt;
						</span>
					</li>
					<li class="page-item">
						<span
						class="page-link"
						style="cursor: pointer;"
						onclick="setAjak('datatahun', '<?= base_url() ?>tahun/list?halaman='+<?= $jml_halaman-1 ?>)"
						>Last
						</span>
					</li>
				<?php endif ?>
			</ul>
		<?php endif ?>
		</div>
	</div>
</div>

<!-- pop up -->
<div class="modal fade" id="popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="judulpopup"></h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div id="isipopup"></div>
		</div>
	</div>
</div>
