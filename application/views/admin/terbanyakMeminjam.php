<table class="table">
	<tr>
		<th>Peringkat</th>
		<th>NIS</th>
		<th>Nama</th>
		<th>Kelas</th>
		<th>Jumlah buku yang dipinjam</th>
	</tr>
	<?php $no = 1;foreach($detailSiswa1Bln as $s) { ?>
		<tr>
			<th style="text-align: center">
				<?php
				if($no <= 1) {
					echo '<i class="fas fa-star" style="color: gold;"></i><i class="fas fa-star" style="color: gold;"></i><i class="fas fa-star" style="color: gold;"></i>';
				} else if($no == 2) {
					echo '<i class="fas fa-star" style="color: gold;"></i><i class="fas fa-star" style="color: gold;"></i>';
				} else if($no == 3) {
					echo '<i class="fas fa-star" style="color: gold;"></i>';
				} else {
					echo $no;
				}
				$no++; ?>
			</th>
			<td><?= $s->nis ?></td>
			<td><?= $s->nama ?></td>
			<td><?= $s->kelas ?></td>
			<td>
				<div class="progress mt-3 mb-1 rounded" style="height: 20px;" title="Sudah meminjam <?= $s->jmlBuku ?> buku">
				<div class="progress-bar bg-primary" role="progressbar" style="width:
					<?php
					echo ($s->jmlBuku*100.00)/$pinjamTerbanyak;
					?>%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
			</div>
			</td>
		</tr>
	<?php } ?>
</table>
