<div class="modal-body">
	Anda yakin ingin menghapus data secara <b>permanen</b>?
</div>
<div class="modal-footer">
	<span class="btn btn-secondary float-right" data-dismiss="modal">Batal</span>
	<span class="btn btn-danger" data-dismiss="modal" onclick="hapustransaksiProses('<?= base_url() ?>', <?= $id_transaksi_hapus ?>, '<?= $page ?>')">Hapus</span>
</div>