<script type="text/javascript" src="<?= base_url() ?>assets/js/adminTransaksi.js"></script>

<div class="container">
	<div class="judul-halaman">
		<h1><?= $judul_halaman ?></h1>
	</div>
	<br>
	<?php if($page != 'anu' ) : ?>
	<button
	 class="btn btn-info"
	 onclick="
		document.getElementById('judulpopup').innerHTML = 'Tentang Transaksi'
		setAjak('isipopup', '<?= base_url() ?>transaksi/informasiTransaksi')"
	 data-toggle="modal"
	 data-target="#popup"
	 title="Informasi tentang transaksi peminjaman">
		<i class="fas fa-info"></i>
	</button>
	<?php endif ?>
	<button
	 class="btn btn-success float-right"
	 onclick="tambahTransaksi('<?= base_url() ?>', '<?= $page ?>')"
	 data-toggle="modal"
	 data-target="#popup"
	 title="Buat transaksi peminjaman buku baru">
		<i class="fas fa-plus"></i>
	</button>
	<br><br>
	<div id="daftar_transaksi">
		<table class="table">
			<tr>
				<th>No</th>
				<th>Tgl Pinjam</th>
				<th>NIS</th>
				<th>Nama</th>
				<th>Kode Katalog</th>
				<th>Judul Buku</th>
				<th>Aksi</th>
			</tr>
			<?php $no=0; foreach ($transaksi as $t): ?>
				<?php $no++ ?>
				<tr
				<?php if ($t->target_kembali < date('Y-m-d') && $t->kembali == 0) { ?>
					class="table-danger"
				<?php } ?>
				>
						<td><?= $no ?></td>
						<td><?= $t->pinjam ?></td>
						<td><?= $t->nis ?></td>
						<td><?= $t->nama ?></td>
						<td><?= $t->katalog ?></td>
						<td><?= $t->judul ?></td>
						<td>
							<!-- hapus -->
							<?php if ($t->kembali == 0) { ?>
							<span
							 class="btn btn-danger"
							 onclick="hapustransaksi('<?= base_url() ?>', <?php echo $t->id ?>, '<?= $page ?>')"
							 data-toggle="modal"
							 data-target="#popup"
							 title="Hapus transaksi peminjaman secara permanen">
								<i class="fas fa-trash"></i>
							</span>
							<?php } ?>
							<!-- info -->
							<span
							 class="btn btn-info"
							 onclick="infoTransaksi('<?=base_url() ?>',<?php echo $t->id; ?>)"
							 data-toggle="modal"
							 data-target="#popup"
							 title="Detail transaksi. Seperti tgl pinjam, nama peminjam, buku dipinjam, dan lainnya.">
								<i class="fas fa-info-circle"></i>
							</span>
							<!-- kembali -->
							<?php if ($t->kembali == 0) { ?>
							<span
							 class="btn btn-primary"
							 onclick="kembaliTransaksi('<?=base_url() ?>',<?php echo $t->id; ?>, '<?= $page ?>')"
							 data-toggle="modal"
							 data-target="#popup"
							 title="Klik jika buku sudah atau akan dikembalikan.">
								<i class="fas fa-handshake"></i>
							</span>
							<?php } ?>
							<!-- tambah lama peminjaman -->
							<?php if ($t->kembali == 0) { ?>
							<span
							 class="btn btn-dark
							 <?php if($t->minta_tambah_lama_pinjam == 1) { ?>text-warning<?php } ?>"
							 onclick="
							 	document.getElementById('judulpopup').innerHTML = 'Tambah Lama Peminjaman'
								setAjak('isipopup', '<?= base_url() ?>transaksi/tambahLamaPeminjaman?id=<?= $t->id ?>&page=<?= $page ?>')"
							 data-toggle="modal"
							 data-target="#popup"
							 title="<?php if($t->minta_tambah_lama_pinjam == 1) { ?>Siswa meminta tambah waktu lama peminjaman. Silakan klik untuk menambah lama peminjam.<?php } else { ?>Mengedit atau menambah waktu peminjaman buku.<?php } ?>">
								<i class="fas fa-stopwatch"></i>
							</span>
							<?php } ?>
						</td>
				</tr>
			<?php endforeach ?>
		</table>
		<?php if($page == 'anu') : ?>
			<button
			 class="btn btn-dark float-right"
			 title="Tampil lebih banyak data peminjaman 30 hari sebelumnya."
			 onclick="
			 	setAjak('daftar_transaksi', '<?= base_url() ?>transaksi/tampilListTransaksi?page=<?= $page ?>&bulan=-1');
			 ">
				<i class="fas fa-plus"></i>
			</button>
		<?php endif ?>
	</div>
</div>

<!-- pop up -->
<div class="modal fade" id="popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="judulpopup"></h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div id="isipopup"></div>
		</div>
	</div>
</div>
