<div class="modal-body" style="max-height: 70vh; overflow: auto;">
	<h5>Peminjaman</h5>
	<table class="table table-hover table-borderless">
		<tr>
			<td>Tgl Pinjam</td>
			<td>: <?= $transaksi_info->tgl_pinjam ?></td>
		</tr>
		<tr>
			<td>Target Kembali</td>
			<td>: <?= $transaksi_info->target_kembali ?></td>
		</tr>
		<tr>
			<td>Tgl Kembali</td>
			<td>: <?= $transaksi_info->tgl_kembali ?></td>
		</tr>
		<tr>
			<td>Petugas</td>
			<td>: <?= $petugas_info->nama ?></td>
		</tr>
		<tr>
			<td>No WA Petugas</td>
			<td>: <?= $petugas_info->no_wa ?></td>
		</tr>
	</table>
	<hr>
	<br>
	<h5>Siswa</h5>
	<table class="table table-hover  table-borderless">
		<tr>
			<td>Nama</td>
			<td>: <?= $siswa_info->nama ?></td>
		</tr>
		<tr>
			<td>Kelas</td>
			<td>: <?= $kelas_info->nama ?></td>
		</tr>
	</table>
	<hr>
	<br>
	<h5>Buku</h5>
	<img src="<?= base_url() ?>gambar/<?= $bibliografi_info->sampul ?>"  style="max-width: 200px;border-radius: 10px; box-shadow: 0 2px 5px rgba(0,0,0,0.6)" class="sampul-detail">
	<br>
	<table class="table table-hover  table-borderless">
		<tr>
			<td>Judul</td>
			<td>: <?= $bibliografi_info->judul ?></td>
		</tr>
		<tr>
			<td>Lemari</td>
			<td>: <?= $lemari_info->nama ?></td>
		</tr>
		<tr>
			<td>Kode Katalog</td>
			<td>: <?= $katalog_info->kode_katalog ?></td>
		</tr>
	</table>
</div>
<div class="modal-footer">
	<span class="btn btn-secondary float-right" data-dismiss="modal">Tutup</span>
</div>