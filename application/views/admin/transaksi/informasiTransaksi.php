<div class="modal-body" style="max-height: 50vh; overflow: auto;">
	<h5>Tambah Transaksi</h5>
	<ol>
		<li>Klik tombol plus di sebelah kanan atas.</li>
		<li>Masukkan kode katalog yang tertera dibuku. Jangan ada kesalahan ketik.</li>
		<li>Masukkan NIS siswa.</li>
		<li>Masukkan tanggal kembali. Secari default lama peminjaman yaitu <?= $perpus->lama_peminjaman ?> hari(bisa diedit di <a  href="<?= base_url('admin/pengaturan') ?>">halaman edit data sekolah</a>.) Tanggal bisa diedit dengan cara klik tanggal.</li>
	</ol>
	<hr>
	<h5>Hapus Transaksi</h5>
	<ol>
		<li>Klik ikon tempat samppah.</li>
		<li>Klik tombol hapus. Maka transaksi akan dihapus secara <b>permanen</b></li>
	</ol>
	<hr>
	<h5>Info Transaksi</h5>
	<ol>
		<li>Klik ikon lingkaran <i>i</i> untuk melihat detail transaksi. Seperti: tgl pinjam, target tgl kembali, nama peminjam, dan buku yg dipinjam.</li>
	</ol>
	<hr>
	<h5>Kembali Transaksi</h5>
	<ol>
		<li>Pastikan buku mempunyai kode katalog yang sama seperti ketika dipinjam.</li>
		<li>Pastikan buku sudah dikembalikan di rak/lemari buku.(bisa dilihat di detail transaksi bagian bawah)</li>
		<li>Klik ikon jabat tangan.</li>
		<li>Jika pengembalian tepat waktu, klik <i>oke</i>.</li>
		<li>Untuk terlambat perhatikan jumlah hari, karena hari libur masih dihitung.</li>
		<li>Untuk denda secara default yaitu Rp. <?= $perpus->denda ?>.(bisa diedit di <a  href="<?= base_url('admin/pengaturan') ?>">halaman edit data sekolah</a>.)</li>
		<li>Klik ok jika sudah selesai langkah2 diatas.</li>
	</ol>
	<hr>
	<h5>Tambah lama peminjaman</h5>
	<ol>
		<li>Klik ikon stopwatch untuk mengedit lama peminjaman.</li>
		<li>Lama peminjaman diedit jika peminjam meminta diperpanjang lama peminjaman.</li>
		<li>Klik pada bagian input tanggal untuk memasukkan tanggal baru.</li>
		<li>Jika icon stopwatch berwarna jingga, siswa meminta untuk memperpanjang jangka pinjam.</li>
		<li>Silakan ganti lama jangka pinjam, mungkin selama 3 hari.</li>
		<li>Jika tidak dapat menambah jangka pinjam tetap klik ikon stopwatch, tetapi waktu pinjam jangan diubah, lalu simpan.</li>
	</ol>
</div>
<div class="modal-footer">
	<span
	 class="btn btn-secondary float-right"
	 data-dismiss="modal"
	 title="Tutup popup">Tutup</span>
</div>
