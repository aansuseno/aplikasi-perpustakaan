<div class="modal-body">
	Pastikan buku sudah dikembalikan di tempatnya.
</div>
<div class="modal-footer">
	<span class="btn btn-secondary float-right" data-dismiss="modal">Batal</span>
	<span class="btn btn-primary" data-dismiss="modal" onclick="kembalitransaksiProses('<?= base_url() ?>', <?= $id_transaksi_kembali ?>, '<?= $page ?>')">Oke</span>
</div>