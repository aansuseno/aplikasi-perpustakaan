<div class="modal-body">
	Peminjam terlambat mengembalikan. Dengan target kembali tanggal <b><?= $transaksi_kembali_terlambat->target_kembali ?></b>. Sementara sekarang tanggal <b><?= date('Y-m-d') ?></b>, peminjam terlambat
	<?php
		$target = date_create($transaksi_kembali_terlambat->target_kembali);
		$sekarang = date_create(date('Y-m-d'));
		$selisih = date_diff($target, $sekarang);
		echo $selisih->format("%a");
	?>
	hari <b>termasuk hari libur</b>.
	<hr>
	<h5>Hitung denda:</h5>
	<table class="table table-borderless">
		<tr>
			<td>Jumlah hari</td>
			<td>
				<input type="text" oninput="hitungdenda()" onfocus="hitungdenda()" class="form-control" id="jumlahhari" value="<?= $selisih->format("%a") ?>">
			</td>
		</tr>
		<tr>
			<td>Denda perhari</td>
			<td>
				<input type="text" value="<?= $perpus->denda ?>" onfocus="hitungdenda()" oninput="hitungdenda()" class="form-control" id="dendaperhari" >
			</td>
		</tr>
		<tr>
			<td>Jumlah denda</td>
			<td id="jumlahdenda">
			<?php
				$target = date_create($transaksi_kembali_terlambat->target_kembali);
				$sekarang = date_create(date('Y-m-d'));
				$selisih = $target->diff($sekarang)->days;
				echo $selisih * $perpus->denda;
			?></td>
		</tr>
	</table>
</div>
<div class="modal-footer">
	<span class="btn btn-secondary float-right" data-dismiss="modal">Batal</span>
	<span class="btn btn-primary" data-dismiss="modal" onclick="kembalitransaksiProses('<?= base_url() ?>', <?= $transaksi_kembali_terlambat->id ?>, '<?= $page ?>')">Oke</span>
</div>
