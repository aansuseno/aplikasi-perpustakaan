<?php if($pesan != '') { ?>
	<div class="alert alert-primary" data-dismiss="alert" style="cursor: pointer;" title="Tutup pemberitahuan.">
		<?php if($pesan == 'berhasilTambah') { ?>
			Transaksi baru berhasil ditambahkan.
		<?php } else if($pesan == 'berhasilHapus') { ?>
			Transaksi berhasil dihapus.
		<?php } else if($pesan == 'berhasilKembali') { ?>
			Buku berhasil dikembalikan.
		<?php } else if($pesan == 'berhasilTambahLamaPeminjaman') { ?>
			Lama peminjaman berhasil diedit.
		<?php } ?>
		<span class="float-right"><i class="fas fa-times"></i></span>
	</div>
<?php } ?>

<table class="table">
	<tr>
		<th>No</th>
		<th>Tgl Pinjam</th>
		<th>NIS</th>
		<th>Nama</th>
		<th>Kode Katalog</th>
		<th>Judul Buku</th>
		<th>Aksi</th>
	</tr>
	<?php $no=0; foreach ($transaksi as $t): ?>
		<?php $no++ ?>
		<tr
		<?php if ($t->target_kembali < date('Y-m-d') && $t->kembali == 0) { ?>
			class="table-danger"
		<?php } ?>
		>
				<td><?= $no ?></td>
				<td><?= $t->pinjam ?></td>
				<td><?= $t->nis ?></td>
				<td><?= $t->nama ?></td>
				<td><?= $t->katalog ?></td>
				<td><?= $t->judul ?></td>
				<td>
					<!-- hapus -->
					<?php if ($t->kembali == 0) { ?>
					<span class="btn btn-danger" onclick="hapustransaksi('<?= base_url() ?>', <?php echo $t->id ?>, '<?= $page ?>')" data-toggle="modal" data-target="#popup">
						<i class="fas fa-trash"></i>
					</span>
					<?php } ?>
					<!-- info -->
					<span class="btn btn-info" onclick="infoTransaksi('<?=base_url() ?>',<?php echo $t->id; ?>)" data-toggle="modal" data-target="#popup">
						<i class="fas fa-info-circle"></i>
					</span>
					<!-- kembali -->
					<?php if ($t->kembali == 0) { ?>
					<span class="btn btn-primary" onclick="kembaliTransaksi('<?=base_url() ?>',<?php echo $t->id; ?>)" data-toggle="modal" data-target="#popup">
						<i class="fas fa-handshake"></i>
					</span>
					<?php } ?>
					<!-- tambah lama peminjaman -->
					<?php if ($t->kembali == 0) { ?>
					<span
					 class="btn btn-dark
					 <?php if($t->minta_tambah_lama_pinjam == 1) { ?>text-warning<?php } ?>"
					 onclick="
					 	document.getElementById('judulpopup').innerHTML = 'Tambah Lama Peminjaman'
						setAjak('isipopup', '<?= base_url() ?>transaksi/tambahLamaPeminjaman?id=<?= $t->id ?>&page=<?= $page ?>')"
					 data-toggle="modal"
					 data-target="#popup"
					 title="<?php if($t->minta_tambah_lama_pinjam == 1) { ?>Siswa meminta tambah waktu lama peminjaman. Silakan klik untuk menambah lama peminjam.<?php } else { ?>Mengedit atau menambah waktu peminjaman buku.<?php } ?>">
						<i class="fas fa-stopwatch"></i>
					</span>
					<?php } ?>
				</td>
		</tr>
	<?php endforeach ?>
</table>
<?php if($page == 'anu') : ?>
	<button
	 class="btn btn-dark float-right"
	 title="Tampil lebih banyak data peminjaman 30 hari sebelumnya."
	 onclick="
	 	setAjak('daftar_transaksi', '<?= base_url() ?>transaksi/tampilListTransaksi?page=<?= $page ?>&bulan=<?= $bulan - 1 ?>');
	 ">
		<i class="fas fa-plus"></i>
	</button>
<?php endif ?>
