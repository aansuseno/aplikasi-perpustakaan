<div class="modal-body">
	<center><small class="text-danger" id="hasilcekkode"></small></center>
	<table class="table table-borderless">
		<tr>
			<th>Kode Katalog</th>
			<td><input type="text" autocomplete="off" oninput="cekKodeKatalog('<?= base_url() ?>')" class="form-control" id="kode_katalog"><td>
		</tr>
		<tr>
			<th>NIS</th>
			<td><input type="text" autocomplete="off" oninput="cekNisSiswa('<?= base_url() ?>')" id="nis_siswa" class="form-control"></td>
		</tr>
		<tr>
			<th>Target kembali</th>
			<td>
				<input
				 type="date"
				 value="<?php
				 	$tgl_sekarang = date_create(date('Y-m-d'));
				 	date_add($tgl_sekarang,date_interval_create_from_date_string($perpus->lama_peminjaman." days"));
					echo date_format($tgl_sekarang,"Y-m-d");?>"
				 id="target_kembali"
				 class="form-control">
			</td>
		</tr>
	</table>
</div>

<div class="modal-footer">
	<span
	 class="btn btn-success float-right"
	 data-dismiss="modal"
	 onclick="tambahTransaksiProses('<?= base_url() ?>', '<?= $page ?>')"
	 title="Simpan transaksi baru">
		Simpan
	</span>
</div>
