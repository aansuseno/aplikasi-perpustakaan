<div class="modal-body">
	<table class="table table-borderless">
		<tr>
			<th>Target kembali</th>
			<td>
				<input
				 type="date"
				 value="<?= $transaksi->target_kembali ?>"
				 id="target_kembali"
				 class="form-control">
			</td>
		</tr>
	</table>
</div>

<div class="modal-footer">
	<span
	 class="btn btn-success float-right"
	 data-dismiss="modal"
	 onclick="setAjak('daftar_transaksi', '<?= base_url() ?>transaksi/tambahLamaPeminjamanProses?id=<?= $transaksi->id ?>&target_kembali='+document.getElementById('target_kembali').value+'&page=<?= $page ?>')"
	 title="Simpan perubahan">
		Simpan
	</span>
</div>
