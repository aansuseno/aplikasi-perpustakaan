<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Perpustakaan <?= $perpus->nama_perpus ?></title>
	<!-- Google Font: Source Sans Pro -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	<!-- Font Awesome Icons -->
	<link rel="stylesheet" href="<?= base_url()?>/assets/plugins/fontawesome-free/css/all.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?= base_url()?>/assets/dist/css/adminlte.min.css">
	<!-- css tambahan -->
	<style>
		* {
			margin: 0;
		}

		.kartu {
			width: 200px;
			height: 300px;
			margin: 10px;
		}

		.kartu div {
			width: 100%;
		 	padding: 5px;
		 	color: #fff;
		 	text-align: center;
		 	background: rgba(0,0,0,0.8);
		 	transition: height 0.3s;
		}
	</style>
</head>
<body class="wrapper">
	<!-- navbar -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary" style="position: relative;">
		<a class="navbar-brand" href="#"><?= $perpus->nama_perpus ?></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="<?= base_url() ?>">Home</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="#">Cari</a>
				</li>
			</ul>
		</div>
	</nav>
	<!-- !navbar -->

	<div class="container" style="min-height: 100vh;">
		<br><br>
		<div class="input-group">
			<input
			 type="text" class="form-control"
			 id="cari-buku"
			 placeholder="Cari berdasar judul, penulis, atau kategori"
			 style="border-radius: 24px 0 0 24px;">
			<div class="input-group-append">
				<span
				 class="btn btn-outline-primary tombol-cari"
				 onclick="setAjak('hasil', '<?= base_url() ?>cari/cari?cari='+document.getElementById('cari-buku').value)"
				 style="border-radius: 0 24px 24px 0">
				<i class="fas fa-search"></i>
				</span>
			</div>
		</div>
		<div id="hasil"></div>
	</div>

	<!-- footer -->
	<footer class="text-center text-white bg-primary" style="margin-top: 50px;">
		<div class="text-center p-3">
			Dibuat dengan <i class="fas fa-heart" ></i> oleh siswa RPL SMK Batik Perbaik Purworejo 2021.
		</div>
	</footer>

 	<!-- REQUIRED SCRIPTS -->

 	<!-- jQuery -->
 	<script src="<?= base_url()?>/assets/plugins/jquery/jquery.min.js"></script>
 	<!-- Bootstrap 4 -->
 	<script src="<?= base_url()?>/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
 	<!-- AdminLTE App -->
 	<script src="<?= base_url()?>/assets/dist/js/adminlte.min.js"></script>
 	<!-- js ajax -->
 	<script type="text/javascript" src="<?= base_url()?>/assets/js/ajax.js"></script>
 	<!-- js tambahan -->
 	<script type="text/javascript">
 		var sementara = ''
		function detailBuku(link) {
			sementara = document.getElementById('hasil').innerHTML
			setAjak('hasil', link)
		}

		function kembaliDariDetailBuku() {
			document.getElementById('hasil').innerHTML = sementara
		}
 	</script>
</body>
</html>
