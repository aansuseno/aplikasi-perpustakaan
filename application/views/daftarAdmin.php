<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Daftar Admin - Perpustakaan <?= $nama_sekolah?></title>
	<!-- Google Font: Source Sans Pro -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	<!-- Font Awesome Icons -->
	<link rel="stylesheet" href="<?= base_url()?>/assets/plugins/fontawesome-free/css/all.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?= base_url()?>/assets/dist/css/adminlte.min.css">
	<!-- css tambahan -->
	<link rel="stylesheet" href="<?= base_url()?>/assets/css/style.css">

	<!-- REQUIRED SCRIPTS -->

	<!-- jQuery -->
	<script src="<?= base_url()?>/assets/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="<?= base_url()?>/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- AdminLTE App -->
	<script src="<?= base_url()?>/assets/dist/js/adminlte.min.js"></script>
	<!-- js tambahan -->
	<script type="text/javascript" src="<?= base_url() ?>assets/js/js.js"></script>
</head>
<body class="hold-transition register-page">
<div class="content bg-white" style="margin: 10px; padding: 20px; box-shadow: 0 0 10px rgba(0,0,0,.5)">
		<h2>Daftar <span id="role">Admin</span></h2>
		<hr>
		<small class="form-text text-danger"><?= validation_errors(); ?></small>
		<small class="form-text text-danger"><?= $pesan ?></small>
		<form action="<?= base_url().'daftar/adminProses' ?>" method="post">
			<table width="100%">
				<tr>
					<th>Nama Lengkap</th>
					<td>
						<div class="input-group mb-3">
							<input type="text" class="form-control" name="nama">
							<div class="input-group-append">
								<div class="input-group-text">
									<span class="fas fa-user-tie"></span>
								</div>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<th>No. WA</th>
					<td>
						<div class="input-group mb-3">
							<input type="text" class="form-control" name="no-wa" id="no_wa" oninput="noWaValidasi()">
							<div class="input-group-append">
								<div class="input-group-text">
									<span class="fab fa-whatsapp"></span>
								</div>
							</div>
						</div>
						<small id="nomorWa" class="form-text text-danger"></small>
					</td>
				</tr>
				<tr>
					<th>Username</th>
					<td>
						<div class="input-group mb-3">
							<input type="text" class="form-control" name="username">
							<div class="input-group-append">
								<div class="input-group-text">
									<span class="fas fa-user"></span>
								</div>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<th>Wewenang</th>
					<td>
						<div class="form-group">
							<select class="form-control" name="wewenang">
						    	<option value="admin">Admin</option>
						    	<option value="pustakawan">Pustakawan</option>
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<th>Password</th>
					<td>
						<div class="input-group mb-3">
							<input type="password" class="form-control" name="password" id="pass_awal">
							<div class="input-group-append">
								<div class="input-group-text">
									<span class="fas fa-lock"></span>
								</div>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<th>Konfirmasi Password</th>
					<td>
						<div class="input-group mb-3">
							<input type="password" class="form-control" name="konf-pass" id="konfPass" oninput="konfirPass()">
							<div class="input-group-append">
								<div class="input-group-text">
									<span class="fas fa-lock"></span>
								</div>
							</div>
						</div>
						<small id="harusSama" class="form-text text-danger"></small>
					</td>
				</tr>
			</table>
			<hr>
			<button type="submit" id="submit" disabled="disabled" class="btn btn-primary">Daftar</button>
		</form>
	</div>
</body>
</html>