<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Perpustakaan <?= $perpus->nama_perpus ?></title>
	<!-- Google Font: Source Sans Pro -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	<!-- Font Awesome Icons -->
	<link rel="stylesheet" href="<?= base_url()?>/assets/plugins/fontawesome-free/css/all.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?= base_url()?>/assets/dist/css/adminlte.min.css">
	<!-- css tambahan -->
	<style>
		* {
			margin: 0;
		}

		.jmbtron {
			height: 100vh;
			width: 100%;
			position: relative;
			background: url("<?= base_url('gambar') ?>/jmb.jpg");
			background-position: center;
			background-repeat: no-repeat;
			background-size: cover;
		}

		.jmbtron:after {
			display: block;
			content: '';
			height: 100vh;
			width: 100%;
			position: absolute;
			background: rgba(0,0,0,0.5);
		}

		.informasi {
			border-left: 5px solid #C4F039;
			padding: 10px;
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%,-50%);
			z-index: 1;
		}

		.informasi span {
			color: #eee;
		}

		.informasi img {
			max-width: 250px;
		}

		.kartu {
			max-width: 200px;
			background: #E0C9A6;
			box-shadow: rgba(0, 0, 0, 0.45) 0px 25px 20px -20px;
			padding: 10px;
			margin: 10px 5px;
			font-family: serif;
		}

		.pembungkus {
			display: flex;
			flex-wrap: wrap;
			justify-content: space-around;
		}

		@media (max-width: 700px) {
			.informasi img{
				max-width: 100px;
			}
		}
	</style>
</head>
<body style="background: #eee;">
	<div class="jmbtron">
		<div class="informasi">
			<img src="<?= base_url('gambar/'.$perpus->logo) ?>">
			<br>
			<span><b>- Perpustakaan</b></span>
			<h1 style="color: #C4F039"><?= $perpus->nama_perpus ?></h1>
			<span><?= $perpus->moto ?></span>
			<br>
			<a href="<?= base_url() ?>login" class="btn btn-primary float-right" style="border-radius: 0"><i class="fas fa-sign-in-alt"></i> Login</a>
		</div>
	</div>

	<!-- navbar -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary" style="position: relative;">
		<a class="navbar-brand" href="#"><?= $perpus->nama_perpus ?></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
				<li class="nav-item active">
					<a class="nav-link" href="<?= base_url() ?>">Home</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?= base_url('cari') ?>">Cari</a>
				</li>
			</ul>
		</div>
	</nav>
	<!-- !navbar -->
	<div class="container" style="">
		<br>
		<center>
			<p><i><?= $perpus->deskripsi ?></i></p>
		</center>
		<br>
		<div class="pembungkus">
			<div class="kartu">
				<img src="<?= base_url('gambar/open-book.png') ?>" style="width: 100%;">
				Perpustakaan <?= $perpus->nama_perpus ?> mempunyai koleksi <b><?= $jmlBuku ?></b> buku dengan judul berbeda.
			</div>
			<div class="kartu">
				<img src="<?= base_url('gambar/book.png') ?>" style="width: 100%;">
				Perpustakaan <?= $perpus->nama_perpus ?> mempunyai koleksi <b><?= $jmlExsemplar ?></b> eksemplar buku tersedia.
			</div>
			<div class="kartu">
				<img src="<?= base_url('gambar/graduates.png') ?>" style="width: 100%;">
				Aplikasi Perpustakaan <?= $perpus->nama_perpus ?> telah dikunjungi oleh <b><?= $perpus->jmlPengunjung ?></b> pengunjung.
			</div>
		</div>
	</div>
	<!-- footer -->
	<footer class="text-center text-white bg-primary" style="margin-top: 50px;">
		<div class="text-center p-3">
			Dibuat dengan <i class="fas fa-heart" ></i> oleh siswa RPL SMK Batik Perbaik Purworejo 2021.
		</div>
	</footer>

 	<!-- REQUIRED SCRIPTS -->

 	<!-- jQuery -->
 	<script src="<?= base_url()?>/assets/plugins/jquery/jquery.min.js"></script>
 	<!-- Bootstrap 4 -->
 	<script src="<?= base_url()?>/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
 	<!-- AdminLTE App -->
 	<script src="<?= base_url()?>/assets/dist/js/adminlte.min.js"></script>
 	<!-- js ajax -->
 	<script type="text/javascript" src="<?= base_url()?>/assets/js/ajax.js"></script>
 	<!-- js tambahan -->
 	<script type="text/javascript">

 	</script>
</body>
</html>
