<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Konfigurasi awal - Aplikasi Perpustakaan</title>
	<!-- Google Font: Source Sans Pro -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	<!-- Font Awesome Icons -->
	<link rel="stylesheet" href="<?= base_url()?>/assets/plugins/fontawesome-free/css/all.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?= base_url()?>/assets/dist/css/adminlte.min.css">
	<!-- css tambahan -->
	<link rel="stylesheet" href="<?= base_url()?>/assets/css/style.css">

	<!-- jQuery -->
	<script src="<?= base_url()?>/assets/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="<?= base_url()?>/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- AdminLTE App -->
	<script src="<?= base_url()?>/assets/dist/js/adminlte.min.js"></script>
	<!-- js tambahan -->
	<script type="text/javascript" src="<?= base_url() ?>assets/js/js.js"></script>
</head>
<body class="container bg-light">
	<div class="content bg-white" style="margin: 10px; padding: 20px; box-shadow: 0 0 10px rgba(0,0,0,.5)">
		<h2>Konfigurasi Aplikasi Perpustakaan</h2>
		<hr>
		<small class="form-text text-danger"><?= validation_errors(); ?></small>
		<form action="<?= base_url().'konfigurasi/aplikasiProses' ?>" method="post">
			<table width="100%">
				<tr>
					<th>Nama Sekolah</th>
					<td><input type="text" class="form-control" id="nama_sekolah" name="nama" oninput="janganKosong('nama_sekolah')"></td>
				</tr>
				<tr>
					<th>Moto Sekolah</th>
					<td><input type="text" id="moto" class="form-control" oninput="janganKosong('moto')" name="moto"></td>
				</tr>
			</table>
			<hr>
			<button type="submit" id="submit" class="btn btn-primary" disabled="disabled">Kirim</button>
		</form>
	</div>
</body>
</html>