<div style="display: flex;justify-content: space-around;flex-wrap: wrap;">
	<div class="input-group">
		<input type="text" class="form-control kolom-cari" autocomplete="off" id="cari-buku" placeholder="Cari berdasar judul, penulis, atau kategori">
		<div class="input-group-append">
			<span class="btn btn-outline-primary tombol-cari"
			onclick="cariBuku('<?= base_url() ?>user/cariBuku?cari=')">
			<i class="fas fa-search"></i>
			</span>
		</div>
	</div>
</div>
<div id="hasil-cari"></div>
