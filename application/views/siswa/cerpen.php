<h3>Cerpen</h3>
<div style="display: flex;flex-wrap: wrap;justify-content: space-around;">
	<!-- cari -->
	<div
	 class="card"
	 style="margin: 5px; min-width: 250px; cursor: pointer;"
	 onclick="detailBuku('<?= base_url() ?>user/cariCerpen')">
		<div
		 class="card-body">
			Cari <br>
			<i class="fas fa-search float-right" style="font-size: 50px;"></i>
		</div>
	</div>
	<!-- !cari -->
	<!-- terbaru -->
	<div
	 class="card"
	 style="margin: 5px; min-width: 250px; cursor: pointer;"
	 onclick="detailBuku('<?= base_url() ?>user/cerpenTerbaru')">
		<div
		 class="card-body">
			Terbaru <br>
			<i class="far fa-clock float-right" style="font-size: 50px;"></i>
		</div>
	</div>
	<!-- !terbaru -->
	<!-- sering dibaca -->
	<div
	 class="card"
	 style="margin: 5px; min-width: 250px; cursor: pointer;"
	 onclick="detailBuku('<?= base_url() ?>user/cerpenPopuler')">
		<div
		 class="card-body">
			Paling Sering Dibaca
			<br>
			<i class="fas fa-book-reader float-right" style="font-size: 50px;"></i>
		</div>
	</div>
	<!-- !sering dibaca -->
	<!-- bintang terbanyak -->
	<div
	 class="card"
	 style="margin: 5px; min-width: 250px; cursor: pointer;"
	 onclick="detailBuku('<?= base_url() ?>user/cerpenBerbintang')"
	 >
		<div
		 class="card-body">
			Bintang Terbanyak
			<br>
			<i class="fas fa-star float-right" style="font-size: 50px;"></i>
		</div>
	</div>
	<!-- !bintang terbanyak -->
</div>
