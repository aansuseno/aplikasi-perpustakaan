<span class="btn" onclick="kembaliDariDetailBuku()" style="border-bottom: 2px solid;border-radius: 0;"><i class="fas fa-arrow-left"></i></span>
<br>
<br>
<div class="card" style="width: 100%;" id="bacaCerpenAtas">
	<div class="card-header"><h3><?= $cerpen->judul ?></h3></div>
	<div class="card-body" style="overflow: visible;">
		<small><?= $cerpen->kategori ?></small>
		<br>

		<!-- isi -->
		<div style="font-family: chilanka;font-size: 19px;text-align: justify;">
			<?= $cerpen->isi ?>
		</div>

		<br>
		<button
		 class="btn bg-dark"
		 onclick="setAjak('container', '<?= base_url() ?>user/beriCerpenBintang?id=<?= $cerpen->id ?>&sudahBerbintang=<?= $berbintangkah ?>')">
			<i class="fa fa-star"  <?php if($berbintangkah == 1) : ?>
		 	style="color: gold !important;"
		 <?php endif ?>></i>suka
		</button>
	</div>
	<div class="card-footer">
		Dikarang oleh: <b><?= $cerpen->nama_pengarang ?></b><br>
		Kelas: <b><?= $cerpen->kelas_pengarang?></b><br>
		Kode Cerpen: <b><?= $cerpen->id?></b>
	</div>
</div>
<span class="btn" onclick="kembaliDariDetailBuku()" style="border-bottom: 2px solid;border-radius: 0;"><i class="fas fa-arrow-left"></i></span>
<a class="btn float-right" href="#" style="border-bottom: 2px solid;border-radius: 0;"><i class="fas fa-arrow-left" style="transform: rotate(90deg)"></i></a>
<br>
<br>
