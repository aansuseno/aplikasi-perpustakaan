<div style="display: flex;justify-content: space-around;flex-wrap: wrap;">
	<div class="input-group">
		<input type="text" class="form-control kolom-cari" autocomplete="off" id="cari-cerpen">
		<div class="input-group-append">
			<span class="btn btn-outline-primary tombol-cari"
			onclick="setAjak('hasil-cari', '<?= base_url() ?>user/cariCerpenProses?s='+getNilai('cari-cerpen'))">
			<i class="fas fa-search"></i>
			</span>
		</div>
	</div>
	<center><small>Cari cerpen berdasar kode, judul, penulis, atau kategori</small></center>
</div>
<div id="hasil-cari"></div>
