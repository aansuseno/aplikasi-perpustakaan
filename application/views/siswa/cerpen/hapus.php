<div class="modal-body">
	<?= $cerpen->judul ?> akan dihapus secara permanen, Anda yakin?
</div>
<div class="modal-footer">
	<span class="btn btn-secondary float-right" data-dismiss="modal">Batal</span>
	<span class="btn btn-danger float-right" data-dismiss="modal" onclick="setAjak('container', '<?= base_url() ?>user/hapusCerpenProses?id=<?= $cerpen->id ?>')"><i class="fas fa-trash-alt"></i>Hapus</span>
</div>

