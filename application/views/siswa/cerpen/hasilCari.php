<div class="card" style="width: 100%;">
	<div class="card-header"><h3>Cerpen Terbaru</h3></div>
	<div class="card-body" style="overflow: visible;">
		<?php foreach ($cerpen as $k): ?>
			<h4><b><?= $k->judul ?></b> by <?= $k->nama_pengarang ?></h4>
			<small><?= $k->kategori ?></small>
			<a href="#" class="text-dark">
			<p onclick="detailBuku('<?= base_url() ?>user/bacaCerpen?id=<?= $k->id ?>')" style="font-family: serif; cursor: pointer;text-align: justify;"><?= substr($k->isi, 0, 300) ?>...<kbd class="bg-primary">baca</kbd></p></a>
			<small>
                <!-- id cerpen -->
                <b class="text-secondary">#</b><b><?= $k->id ?></b> |

				<!-- bintang -->
				<i
				 class="fa fa-star"
				 <?php if($k->bintang >= 10) { ?>
				 	style="color: brown;"
				 <?php } else if($k->bintang >= 100) { ?>
				 	style="color: silver;"
				 <?php } else if($k->bintang >= 100) { ?>
				 	style="color: gold;"
				 <?php } ?>
				></i> <?= $k->bintang ?> |

				<!-- buka -->
				<i class="fas fa-eye"></i> <?= $k->dilihat ?>
			</small>
			<hr>
		<?php endforeach ?>
	</div>
</div>

<!-- pagination -->
<div style="display: flex; flex-wrap: wrap; justify-content: space-around;">
<?php if ($jml_halaman > 1): ?>
	<ul class="pagination">
		<?php if ($halamanSekarang > 0): ?>
			<li class="page-item">
				<span
				class="page-link"
				style="cursor: pointer;"
				onclick="setAjak('hasil-cari', '<?= base_url() ?>user/cariCerpenProses?halaman=0&s=<?= $cari ?>')"
				>First
				</span>
			</li>
			<li class="page-item">
				<span
				class="page-link"
				style="cursor: pointer;"
				<?php if ($halamanSekarang > 0): ?>
					onclick="setAjak('hasil-cari', '<?= base_url() ?>user/cariCerpenProses?halaman='+<?= $halamanSekarang-1 ?>+'&s=<?= $cari ?>')"
				<?php endif ?>
				>&lt;
				</span>
			</li>
		<?php endif ?>
		<?php for ($i = $halamanSekarang - 2; $i <= $halamanSekarang + 4; $i++): ?>
			<?php if ($i > 0 && $i <= $jml_halaman): ?>
				<li
				class="page-item <?php if($halamanSekarang == $i - 1) { echo 'active'; }?>"
				style="cursor: pointer;">
					<span
					class="page-link"
					onclick="setAjak('hasil-cari', '<?= base_url() ?>user/cariCerpenProses?halaman='+<?= $i-1 ?>+'&s=<?= $cari ?>')">
						<?= $i ?>
					</span>
				</li>
			<?php endif ?>
		<?php endfor ?>
		<?php if ($halamanSekarang < $jml_halaman-1): ?>
			<li class="page-item">
				<span
				class="page-link"
				style="cursor: pointer;"
				<?php if ($halamanSekarang < $jml_halaman-1): ?>
					onclick="setAjak('hasil-cari', '<?= base_url() ?>user/cariCerpenProses?halaman='+<?= $halamanSekarang+1 ?>+'&s=<?= $cari ?>')"
				<?php endif ?>
				>&gt;
				</span>
			</li>
			<li class="page-item">
				<span
				class="page-link"
				style="cursor: pointer;"
				onclick="setAjak('hasil-cari', '<?= base_url() ?>user/cariCerpenProses?halaman='+<?= $jml_halaman-1 ?>+'&s=<?= $cari ?>')"
				>Last
				</span>
			</li>
		<?php endif ?>
	</ul>
<?php endif ?>
</div>
