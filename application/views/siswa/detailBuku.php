<span class="btn" onclick="kembaliDariDetailBuku()" style="border-bottom: 2px solid;border-radius: 0;"><i class="fas fa-arrow-left"></i></span>
<br>
<div class="detail">
	<div class="sampul">
		<img src="<?= base_url() ?>gambar/<?= $b->sampul ?>" style="width: 100%; box-shadow: 0 0 5px rgba(0,0,0,0.7)">
	</div>
	<div class="info">
		<h3><?= $b->judul ?></h3>
		<hr>
		<table class="table table-borderless">
			<tr>
				<td>Penulis</td>
				<td>: <?= $b->penulis ?></td>
			</tr>
			<tr>
				<td>Penerbit</td>
				<td>: <?php if(isset($p->nama)) {echo $p->nama;} else {echo "error";}?></td>
			</tr>
			<tr>
				<td>Tahun terbit</td>
				<td>: <?php if(isset($t->nama)) {echo $t->nama;} else {echo "error";}?></td>
			</tr>
			<tr>
				<td>Kategori</td>
				<td>: <?= $b->kategori ?></td>
			</tr>
			<tr>
				<td>Deskripsi</td>
				<td>: <?= $b->deskripsi ?></td>
			</tr>
			<tr>
				<td>Lemari</td>
				<td>: <?php if(isset($l->nama)) {echo $l->nama;} else {echo "error";}?></td>
			</tr>
			<tr>
				<td>Tersedia</td>
				<td>: <?= $b->tersedia ?></td>
			</tr>
		</table>
	</div>
</div>
