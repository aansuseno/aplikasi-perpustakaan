<span class="btn" onclick="kembaliDariDetailBuku()" style="border-bottom: 2px solid;border-radius: 0;"><i class="fas fa-arrow-left"></i></span>
<br><br>
<?php if ($detail->target_kembali < date('Y-m-d') && $detail->kembali == 0) { ?>
<div class="alert alert-danger">Keterlambatan akan dikenakan denda sekitar Rp.<?= $perpus->denda ?> perhari</div>
<?php } ?>
<img src="<?= base_url() ?>gambar/<?= $detail->sampul ?>" style="width: 100%; box-shadow: 0 0 5px rgba(0,0,0,0.7); max-width: 300px; margin: auto;">
<table class="table table-borderless" <?php if($siswa->mode_gelap == 1) { ?>style="color: #eee"<?php } ?>>
	<tr>
		<td>Judul Buku</td>
		<th>: <?= $detail->judul ?></th>
	</tr>
	<tr>
		<td>Author</td>
		<th>: <?= $detail->penulis ?></th>
	</tr>
	<tr>
		<td>Penerbit</td>
		<th>: <?= $detail->penerbit ?></th>
	</tr>
	<tr>
		<td>Kategori</td>
		<th>: <?= $detail->kategori ?></th>
	</tr>
	<tr>
		<td>Tahun Terbit</td>
		<th>: <?= $detail->tahun ?></th>
	</tr>
	<tr>
		<td>Sinopsis</td>
		<th>: <?= $detail->deskripsi_buku ?></th>
	</tr>
	<tr>
		<td>Kode katalog</td>
		<th>: <?= $detail->kode_katalog ?></th>
	</tr>
	<tr>
		<td>Tgl Pinjam</td>
		<th>: <?= $detail->tgl_pinjam ?></th>
	</tr>
	<tr>
		<td>Target kembali</td>
		<th>: <?= $detail->target_kembali ?></th>
	</tr>
	<tr>
		<td>Tgl Kembali</td>
		<th>: <?= $detail->tgl_kembali ?></th>
	</tr>
</table>
<?php if($detail->kembali == 0) { ?>
<button class="btn btn-primary" onclick="setAjak('tambah-lama-peminjaman', '<?= base_url() ?>user/mintaPerlama?id=<?= $detail->id ?>')" id="tambah-lama-peminjaman">
	<?php if($detail->lamakan == 1) { ?>
	Sedang diproses.. Klik lagi untuk membatalkan
	<?php } else { ?>
	Tambah lama peminjaman
	<?php } ?>
</button>
<?php } ?>
<br>
<hr>
<?php if ($detail->kembali == 1 ) { ?>
	<div style="margin: auto;max-width: 300px;display: flex;flex-wrap: wrap;justify-content: space-around;">
		<h4>Beri nilai: </h4>
		<br>
		<i
		 class="fa fa-star"
		 id="bintang1"
		 <?php if($rating >= 1) { ?>
		 	style="color: #FFD700;"
		 <?php } ?>
		 onclick="bintang(1, '<?= base_url() ?>user/rating?nis=<?= $detail->nis ?>&id=<?= $detail->id_buku ?>&bintang=1')"></i>
		<i
		 class="fa fa-star"
		 id="bintang2"
		 <?php if($rating >= 2) { ?>
		 	style="color: #FFD700;"
		 <?php } ?>
		 onclick="bintang(2, '<?= base_url() ?>user/rating?nis=<?= $detail->nis ?>&id=<?= $detail->id_buku ?>&bintang=2')"></i>
		<i
		 class="fa fa-star"
		 id="bintang3"
		 <?php if($rating >= 3) { ?>
		 	style="color: #FFD700;"
		 <?php } ?>
		 onclick="bintang(3, '<?= base_url() ?>user/rating?nis=<?= $detail->nis ?>&id=<?= $detail->id_buku ?>&bintang=3')"></i>
		<i
		 class="fa fa-star"
		 id="bintang4"
		 <?php if($rating >= 4) { ?>
		 	style="color: #FFD700;"
		 <?php } ?>
		 onclick="bintang(4, '<?= base_url() ?>user/rating?nis=<?= $detail->nis ?>&id=<?= $detail->id_buku ?>&bintang=4')"></i>
		<i
		 class="fa fa-star"
		 id="bintang5"
		 <?php if($rating >= 5) { ?>
		 	style="color: #FFD700;"
		 <?php } ?>
		 onclick="bintang(5, '<?= base_url() ?>user/rating?nis=<?= $detail->nis ?>&id=<?= $detail->id_buku ?>&bintang=5')"></i>
	</div>
<?php } ?>
