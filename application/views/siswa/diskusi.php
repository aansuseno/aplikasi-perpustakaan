<h3 class="sub-judul">Diskusi</h3>
<small>*Gunakan bahasa yang sopan.</small><br>
<small>*Klik ikon refresh untuk memperbarui pesan. Bukan refresh halaman.</small><br>
<small>*Pesan otomatis dihapus setelah <?= $pengaturan->lama_pesan_diskusi ?> jam.</small><br>
<small>*Batas jumlah karakter <?= $pengaturan->max_pesan_diskusi ?>.</small><br>
<small>*Tambahkan <i><?= $pengaturan->panggil_pustakawan ?></i> untuk menandai pustakawan.</small>
<br>
<button
 class="btn btn-dark"
 document.getElementById('refresh').style.display = 'block'
 onclick="setAjak('container', '<?= base_url() ?>user/diskusi')">
	<i class="fas fa-sync-alt"></i>
</button>
<hr>
<br>

<div style="width: 100%;">
<center id="refresh" style="display: none;">menyegarkan...</center>
<center id="mengirim" style="display: none;">mengirim...</center>
<?php foreach($p as $p) { ?>
	<!-- buble pesan -->
	<div id="p<?= $p->id ?>" class="card" style="
	<?php if($p->id_pengirim == $siswa->id && $p->admin_kah == 0) { ?>
	width: max-content; border-radius: 15px 0 0 15px; margin-right: 0; margin-left: auto; max-width: 90%
	<?php } else if($p->admin_kah == 0) { ?>
	width: max-content; border-radius: 0 15px 15px 0; max-width: 90%;
	<?php } else if($p->admin_kah == 1) { ?>
	width: 90%; border-radius: 15px; margin-left: auto; margin-right: auto; cursor: pointer;
	<?php } ?>">
		<div class="card-body" style="padding: 10px;">
			<h5 class="card-title" style="width: 100%;">
				<?php
				if(!($p->id_pengirim == $siswa->id && $p->admin_kah == 0)) {
					echo $p->nama_pengirim;
				} ?>
			</h5>

			<p id="isiPesan<?= $p->id ?>" class="card-text"><?= $p->pesan ?></p>

			<!-- ygDibalas -->
			<?php
			if($p->membalas != 'n') { ?>
			<a
			 class="btn" style="font-size: 10px;"
			 href="#<?= $p->membalas ?>"
			 onclick="lihatYangDibalas('<?= $p->membalas ?>')"
			>
				<i class="fas fa-arrow-down"></i>
			</a>
			<?php } ?>
			<!-- end ygDibalas -->

			<!-- balas -->
			<button
			 class="btn" style="font-size: 10px;"
			 onclick="
			 	pesan = document.getElementById('isiPesan<?= $p->id ?>').innerHTML
			 	if(pesan.length > 33) pesan = pesan.substring(0,33)+'...';
			 	document.getElementById('pesanDibalas').style.display = 'block'
			 	document.getElementById('pesanDibalas').innerHTML = pesan
			 	document.getElementById('idYangdibalas').value = 'p<?= $p->id ?>'
			 	document.getElementById('ikonBatalBalas').style.display = 'block'
			 "
			>
				<i class="fas fa-comment-alt"></i>
			</button>
			<!-- end balas -->

			<!-- waktu kirim -->
			<small class="float-right" style="opacity: 0.7; font-size: 10px;">
				<?php
				$time = strtotime($p->dikirim);
				$newformat1 = date('D, M Y H:m:s',$time);
				echo $newformat1;
				?>
			</small>
			<!-- end waktu kirim -->

			<!-- hapus -->
			<?php
			if($p->id_pengirim == $siswa->id && $p->admin_kah == 0) { ?>
			<button
			 class="btn" style="font-size: 10px;"
			 onclick="setAjak('container', '<?= base_url() ?>user/hapusPesan?id=<?= $p->id ?>')">
				<i class="fas fa-trash text-danger"></i>
			</button>
			<?php } ?>
			<!-- end hapus -->
		</div>
	</div>
	<!-- end bable pesan -->
	<?php } ?>

</div>
<br><br>
<div
 style="
 	position: sticky;
 	left: 0;
 	bottom: 0;
 	padding: 15px;
 	background: var(--gray-dark);
 	border-top: 3px red solid;
 	box-shadow: 0 -5px 5px rgba(255,255,255,.5);
 ">

 	<!-- pesan dibalas -->
 	<i
 	 id="ikonBatalBalas"
 	 class="fas fa-times-circle text-light float-right"
 	 style="display: none; cursor: pointer"
 	 onclick="
 	 	document.getElementById('pesanDibalas').style.display = 'none'
 	 	document.getElementById('ikonBatalBalas').style.display = 'none'
 	 	document.getElementById('idYangdibalas').value = 'n'
 	 "></i>
 	<div
 	 id="pesanDibalas"
 	 style="
 	 	background: gray;
 	 	margin-bottom: 4px;
 	 	width: 100%;
 	 	padding: 5px;
 	 	border-left: 5px solid lightblue;
 	 	display: none;
 	 ">
 	</div>
 	<!-- end pesan dibalas -->

	<div class="input-group">
		<input type="hidden" id="idYangdibalas" value="n">
		<input
		 type="text"
		 class="form-control kolom-cari"
		 autocomplete="off"
		 id="diskusi"
		 placeholder="Pesan disini.."
		 maxlength="<?= $pengaturan->max_pesan_diskusi ?>">
		<div class="input-group-append">
			<a
			 href="#"
			 class="btn btn-outline-primary tombol-cari"
			 onclick="
			 	pesanNya = document.getElementById('diskusi').value
			 	pesan = pesanNya.split('&').join('00100110')
			 	if(pesan != '') {
			 		document.getElementById('mengirim').style.display = 'block'
				 	idBalas = document.getElementById('idYangdibalas').value
				 	setAjak('container', '<?= base_url() ?>user/diskusiKirim?pesan='+pesan+'&idBalas='+idBalas)
			 	}
			 ">
				<i class="far fa-paper-plane"></i>
			</a>
		</div>
	</div>
</div>
