<span class="btn" onclick="kembaliDariDetailBuku()" style="border-bottom: 2px solid;border-radius: 0;"><i class="fas fa-arrow-left"></i></span>
<br>
<br>
<div class="card" style="width: 100%;">
	<form method="post" action="<?= base_url('user') ?>/editCerpenProses">
		<div class="card-header"><input type="text" class="form-control" autocomplete="off" name="judul" value="<?= $cerpen->judul ?>"></div>
		<div class="card-body" style="overflow: visible;">
			<input type="text" class="form-control" autocomplete="off" name="kategori" value="<?= $cerpen->kategori ?>">
			<textarea class="form-control" rows="20" name="isi"><?= str_replace('<br>', '', $cerpen->isi) ?></textarea>
			<br>
			<input type="hidden" name="id" value="<?= $cerpen->id ?>">
			<span class="btn btn-danger" data-toggle="modal" data-target="#popup" onclick="popup('<?= base_url() ?>user/hapusCerpen?id=<?= $cerpen->id ?>', 'Hapus cerpen')"><i class="fas fa-trash"></i></span>
			<input type="submit" class="btn btn-success float-right" value="Edit">
		</div>
	</form>
	<div class="card-footer">
		Status: <b><?php if ($cerpen->status == 0) { ?>
			Belum Dikonfirmasi
		<?php } else { ?>
			Sudah Tayang
		<?php } ?>
		</b><br>
		Bintang: <b><?= $cerpen->bintang ?></b><br>
		Dilihat: <b><?= $cerpen->dilihat ?></b>
	</div>
</div>
