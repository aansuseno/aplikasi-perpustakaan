<?php if (isset($pesan)): ?>
	<div class="alert alert-dark"><?= $pesan ?></div>
<?php endif ?>

<table class="table table-borderless">
	<tr>
		<td>Password Baru</td>
		<td><input type="password" class="form-control" id="pw"></td>
	</tr>
	<tr>
		<td>Konfirmasi Password</td>
		<td><input type="password" class="form-control" id="pwKonf"></td>
	</tr>
</table>
<span class="btn btn-success float-right" onclick="gantiContainer('halamanPw', '<?= base_url() ?>user/editProfilProses?pw='+getNilai('pw')+'&pwKonf='+getNilai('pwKonf'))">Simpan</span>
