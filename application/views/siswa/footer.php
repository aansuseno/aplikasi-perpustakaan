
		</div>
		<!-- !container -->
		<br><br><br>
		<!-- footer -->
		<footer class="text-center text-white" style="position: relative" id="kakifooter">
			<!-- Copyright -->
			<div class="text-center p-3" style="background-color: #A90909;box-shadow: 5px 0 10px rgba(225,255,255,0.3);">
				Dibuat dengan <i class="fas fa-heart" ></i> oleh siswa RPL SMK Batik Perbaik Purworejo 2021.
			</div>
			<!-- Copyright -->
		</footer>

		<div id="numpang-lewat"></div>
		<!-- pop up -->
		<div class="modal fade" id="popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title" id="judulpopup"></h3>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div id="isipopup"></div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
