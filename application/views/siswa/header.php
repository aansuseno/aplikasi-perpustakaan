<!DOCTYPE html>
<html lang="id">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title><?= $nama_sekolah ?></title>
	<!-- Google Font: Source Sans Pro -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	<!-- Font Awesome Icons -->
	<link rel="stylesheet" href="<?= base_url()?>/assets/plugins/fontawesome-free/css/all.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?= base_url()?>/assets/dist/css/adminlte.min.css">
	<!-- css tambahan -->
	<link rel="stylesheet" href="<?= base_url()?>/assets/css/styleSiswa.css">
	<?php if($siswa->mode_gelap == 1) { ?>
		<link rel="stylesheet" href="<?= base_url()?>/assets/css/siswaDark.css">
	<?php } ?>
	<!-- REQUIRED SCRIPTS -->

	<!-- jQuery -->
	<script src="<?= base_url()?>/assets/plugins/jquery/jquery.min.js"></script>
 	<!-- Bootstrap 4 -->
 	<script src="<?= base_url()?>/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- AdminLTE App -->
	<script src="<?= base_url()?>/assets/dist/js/adminlte.min.js"></script>
	<!-- js ajax -->
	<script type="text/javascript" src="<?= base_url()?>/assets/js/ajax.js"></script>
	<!-- js tambahan -->
	<script type="text/javascript" src="<?= base_url()?>/assets/js/siswa.js"></script>
	<style type="text/css">
		@font-face {
			font-family: 'chilanka';
			src: url('<?= base_url() ?>assets/fonts/Chilanka-Regular.ttf');
		}
		@keyframes pesanDiBalas{
			0% {
				background: #0e7af1;
			}
		}
	</style>
</head>
<body style="background: #FFDEDE">
	<div class="wrapper">
		<!-- navbar -->
		<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
			<a class="navbar-brand" href="#"><?= $siswa->nama ?></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="fas fa-bars"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active" id="halamanBeranda" onclick="gantiContainer('halamanBeranda', '<?= base_url() ?>user/beranda')"
					class="navbar-toggler" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="true">
						<span class="nav-link"><i class="fas fa-home"></i>Beranda</span>
					</li>
					<li class="nav-item" id="halamanCari" onclick="gantiContainer('halamanCari', '<?= base_url() ?>user/cari')"
					class="navbar-toggler" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="true">
						<span class="nav-link"><i class="fas fa-search"></i>Cari</span>
					</li>
					<li class="nav-item" id="halamanCerpen" onclick="gantiContainer('halamanCerpen', '<?= base_url() ?>user/cerpen')"
					class="navbar-toggler" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="true">
						<span class="nav-link"><i class="fas fa-scroll"></i>Cerpen</span>
					</li>
					<li class="nav-item" id="halamanKoleksi" onclick="gantiContainer('halamanKoleksi', '<?= base_url() ?>user/koleksi')"
					class="navbar-toggler" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="true">
						<span class="nav-link"><i class="fas fa-th-list"></i>Koleksi</span>
					</li>
					<li class="nav-item" id="halamanRiwayat" onclick="gantiContainer('halamanRiwayat', '<?= base_url() ?>user/riwayat')"
					class="navbar-toggler" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="true">
						<span class="nav-link"><i class="fas fa-history"></i>Riwayat</span>
					</li>
					<li class="nav-item" id="halamanDiskusi"
					 onclick="gantiContainer('halamanDiskusi', '<?= base_url() ?>user/diskusi')"
					class="navbar-toggler" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="true">
						<span class="nav-link"><i class="far fa-comment"></i>Diskusi</span>
					</li>
				</ul>
				<ul class="navbar-nav navbar-right">
					<li class="nav-item">
						<a href="<?= base_url() ?>user/modeGelapTerang" class="nav-link">
							<?php if($siswa->mode_gelap == 0) { ?>
								<i class="fas fa-moon"></i> Mode Gelap
							<?php } else { ?>
								<i class="fas fa-sun"></i> Mode Terang
							<?php } ?>
						</a>
					</li>
					<li class="nav-item" id="halamanPw"
					class="navbar-toggler" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="true">
							<span class="nav-link" onclick="gantiContainer('halamanPw', '<?= base_url() ?>user/editProfil')"><i class="fas fa-cog"></i>Edit Password</span>
					</li>
					<li class="nav-item"
					class="navbar-toggler" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="true">
						<a href="<?= base_url() ?>user/keluar" class="nav-link" ><i class="fas fa-sign-out-alt"></i>Logout</a>
					</li>
				</ul>
			</div>
		</nav>
		<!-- !navbar -->

		<div class="container" id="container" style="margin-top: 20px; min-height: 100vh">
