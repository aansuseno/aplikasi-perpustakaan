<!-- rating tertinggi -->
<h3 class="sub-judul">Rating tertinggi</h3>
<div class="pembungkus-kartu">
	<?php foreach($urut_rating as $b) :?>
	<div class="kartu" onclick="detailBuku('<?= base_url() ?>user/detailBuku?id=<?= $b->id ?>')">
		<img src="<?= base_url() ?>gambar/<?= $b->sampul ?>">
		<div>
			<h5><?= $b->judul ?></h5>
		</div>
	</div>
	<?php endforeach ?>
</div>
<br><br>

<!-- baru ditambahkan -->
<h3 class="sub-judul">Baru ditambahkan</h3>
<div class="pembungkus-kartu">
	<?php foreach($urut_baru as $b) :?>
	<div class="kartu" onclick="detailBuku('<?= base_url() ?>user/detailBuku?id=<?= $b->id ?>')">
		<img src="<?= base_url() ?>gambar/<?= $b->sampul ?>">
		<div>
			<h5><?= $b->judul ?></h5>
		</div>
	</div>
	<?php endforeach ?>
</div>
<br><br>

<!-- baru rilis -->
<h3 class="sub-judul">Rilis terbaru</h3>
<div class="pembungkus-kartu">
	<?php foreach($urut_terbit as $b) :?>
	<div class="kartu" onclick="detailBuku('<?= base_url() ?>user/detailBuku?id=<?= $b->id ?>')">
		<img src="<?= base_url() ?>gambar/<?= $b->sampul ?>">
		<div>
			<h5><?= $b->judul ?></h5>
		</div>
	</div>
	<?php endforeach ?>
</div>
<br><br>
