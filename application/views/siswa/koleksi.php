<span
 class="btn btn-success float-right"
 onclick="detailBuku('<?= base_url() ?>user/tambahCerpen')"
 title="Buat cerpen">
	<i class="fa fa-plus"></i>
</span>
<br>
<br>
<div class="card" style="width: 100%;">
	<div class="card-header"><h3>Koleksi Saya</h3></div>
	<div class="card-body" style="overflow: visible;">
		<?php foreach ($koleksi as $k): ?>
			<h4><?= $k->judul ?> by <?= $k->nama_pengarang ?></h4>
			<small><?= $k->kategori ?></small>
			<p onclick="detailBuku('<?= base_url() ?>user/editCerpen?id=<?= $k->id ?>')" style="font-family: serif; cursor: pointer;"><?= substr($k->isi, 0, 300) ?>...<kbd class="bg-warning">edit</kbd></p>
			<hr>
		<?php endforeach ?>
	</div>
</div>
