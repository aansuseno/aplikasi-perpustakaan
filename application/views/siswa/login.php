<!DOCTYPE html>
<html>
<head>
	<title>Login Siswa</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<style type="text/css">
		.container {
			margin: 0;
			height: 100%;
			width: 100%;
			font-family: sans-serif;
			/*background: linear-gradient(to left, #FF2929, #3434FF, #FF2929)*/
			background: #eee;
		}
		.row {
			margin: 100px auto;
			max-width: 500px;
			color: #111;
			padding: 20px;
			text-align: center;
			margin-bottom: 0;
		}
		.input {
			background: none;
			display: block;
			text-align: center;
			border: 2px solid #222;
			padding: 10px 0;
			width: 100%;
			outline: none;
			color: #222;
			border-radius: 24px;
			transition: 0.25s
		}
		.input-bok {
			margin: auto;
			width: 70%;
			text-align: left;
		}

		.input:focus {
			border-radius: 12px;
		}

		.tombol {
			border: 2px solid #222;
			padding-top: 10px;
			padding-bottom: 10px;
			color: #222;
			border-radius: 24px;
			margin: auto;
			background: #ddd;
			transition: 0.25s;
			width: 100%;
		}
		.tombol:hover {
			background: #222;
			color: #eee;
		}
		.kecil {
			font-size: small;
		}
	</style>
</head>
<body class="container">
	<div class="row">
		<div class="card">
			<h1>Halaman Masuk Siswa</h1>
			<hr>
			<p class="kecil"><?= $pesan ?></p>
			<div class="input-bok">
				<form action="<?= base_url() ?>login/siswaProses" method="POST">
					<b>NIS:</b><br>
					<input type="text" class="input" name="nis" placeholder="NIS">
					<br><br>
					<b>Password:</b><br>
					<input type="password" class="input" name="pw" placeholder="Password..">
					<br><br>
					<button type="submit" class="tombol">Masuk</button>
				</form>
			</div>
			<br><br><br>
			<hr>
			<p class="kecil">Belum pernah mendaftar? <a href="<?= base_url() ?>daftar">Daftar</a></p>
		</div>
	</div>
</body>
</html>
