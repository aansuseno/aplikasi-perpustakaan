<?php if ($jml_halaman > 1): ?>
	<ul class="pagination">
		<?php if ($halamanSekarang > 0): ?>
			<li class="page-item">
				<span 
				class="page-link" 
				style="cursor: pointer;"
				onclick="setAjak('hasil-cari', '<?= base_url() ?>user/cariBuku?halaman=0&cari=<?= $cari ?>')"
				>First
				</span>
			</li>
			<li class="page-item">
				<span 
				class="page-link" 
				style="cursor: pointer;"
				<?php if ($halamanSekarang > 0): ?>
					onclick="setAjak('hasil-cari', '<?= base_url() ?>user/cariBuku?halaman='+<?= $halamanSekarang-1 ?>+'&cari=<?= $cari ?>')"
				<?php endif ?>
				>&lt;
				</span>
			</li>
		<?php endif ?>
		<?php for ($i = $halamanSekarang - 2; $i <= $halamanSekarang + 4; $i++): ?>
			<?php if ($i > 0 && $i <= $jml_halaman): ?>
				<li 
				class="page-item <?php if($halamanSekarang == $i - 1) { echo 'active'; }?>" 
				style="cursor: pointer;">
					<span 
					class="page-link" 
					onclick="setAjak('hasil-cari', '<?= base_url() ?>user/cariBuku?halaman='+<?= $i-1 ?>+'&cari=<?= $cari ?>')">
						<?= $i ?>
					</span>
				</li>
			<?php endif ?>
		<?php endfor ?>
		<?php if ($halamanSekarang < $jml_halaman-1): ?>
			<li class="page-item">
				<span 
				class="page-link" 
				style="cursor: pointer;"
				<?php if ($halamanSekarang < $jml_halaman-1): ?>
					onclick="setAjak('hasil-cari', '<?= base_url() ?>user/cariBuku?halaman='+<?= $halamanSekarang+1 ?>+'&cari=<?= $cari ?>')"
				<?php endif ?>
				>&gt;
				</span>
			</li>
			<li class="page-item">
				<span 
				class="page-link" 
				style="cursor: pointer;"
				onclick="setAjak('hasil-cari', '<?= base_url() ?>user/cariBuku?halaman='+<?= $jml_halaman-1 ?>+'&cari=<?= $cari ?>')"
				>Last
				</span>
			</li>
		<?php endif ?>
	</ul>
<?php endif ?>
