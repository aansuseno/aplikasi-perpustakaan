<div class="row">
	<?php foreach ($riwayat as $k) {?>
		<div class="col-sm-4" onclick="detailBuku('<?= base_url() ?>user/detailRiwayat?id=<?= $k->id ?>')">
			<div class="card
			<?php if ($k->target_kembali < date('Y-m-d') && $k->kembali == 0) { ?>
				bg-danger
			<?php } else if ($k->kembali == 0) {echo 'bg-warning';} ?>
			" style="max-width: 300px;">
				<div class="card-header"><?= $k->judul ?></div>
				<div class="card-footer">
					<?php
						$date = DateTime::createFromFormat('Y-m-d', $k->tgl_pinjam);
						echo date_format($date, 'D, d M Y');
					?>
				</div>
			</div>
		</div>
	<?php } ?>
</div>
