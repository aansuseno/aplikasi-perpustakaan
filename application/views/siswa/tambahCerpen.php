<span class="btn" onclick="kembaliDariDetailBuku()" style="border-bottom: 2px solid;border-radius: 0;"><i class="fas fa-arrow-left"></i></span>
<h3>Tambah Cerpen Baru</h3>
<form method="post" action="<?= base_url('user') ?>/tambahCerpenProses">
	<table class="table table-borderless">
		<tr>
			<td>Judul</td>
			<td><input type="text" autocomplete="off" class="form-control" name="judul"></td>
		</tr>
		<tr>
			<td>Kategori</td>
			<td><input type="text" autocomplete="off" placeholder="Drama, Misteri, Histori, Komedi," class="form-control" name="kategori"></td>
		</tr>
	</table>
	<h5>Isi</h5>
	<textarea class="form-control" name="isi" rows="12"></textarea>
	<br><br>
	<input type="submit" value="Kirim" class="btn btn-primary float-right">
</form>
