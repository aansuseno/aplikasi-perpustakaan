function tambahkategori(base_url) {
	docInnerHtml('judulpopup','Tambah Kategori')
	setAjak('isipopup',base_url+'kategori/tambah')
}
function prosestambahkategori(base_url){
	nama=getNilai('namakategori')
	deskripsi=getNilai('namadeskripsi')
	setAjak('datakategori', base_url+'kategori/prosesTambahKategori?nama='+nama+'&deskripsi='+deskripsi)
}
function hapuskategoridatamaster(base_url,id,nama){
	docInnerHtml('judulpopup','Peringatan ! ! !')
	setAjak('isipopup', base_url+'kategori/hapus?id='+id+'&nama='+nama)
}
function proseshapuskategoridatamaster(base_url,id){
	setAjak('datakategori', base_url+'kategori/proseshapus?id='+id)
}
function infokategoridatamaster(base_url,id){
	docInnerHtml('judulpopup','Info')
	setAjak('isipopup', base_url+'kategori/info?id='+id)
}
function editkategoridatamaster(base_url,id){
	docInnerHtml('judulpopup','Edit')
	setAjak('isipopup',base_url+'kategori/edit?id='+id)
}

function proseseditkategori(base_url,id){
	nama=getNilai('namakategoriedit')
	deskripsi=getNilai('deskripsikategoriedit')
	setAjak('datakategori', base_url+'kategori/editproses?nama='+nama+'&deskripsi='+deskripsi+'&id='+id)
}