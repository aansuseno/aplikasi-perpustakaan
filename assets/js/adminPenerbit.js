function tambahpenerbit(base_url) {
	docInnerHtml('judulpopup','Tambah Penerbit')
	setAjak('isipopup',base_url+'penerbit/tambah')
}
function prosestambahpenerbit(base_url){
	nama=getNilai('namapenerbit')
	deskripsi=getNilai('namadeskripsi')
	setAjak('datapenerbit', base_url+'penerbit/prosesTambahPenerbit?nama='+nama+'&deskripsi='+deskripsi)
}
function hapuspenerbitdatamaster(base_url,id,nama){
	docInnerHtml('judulpopup','Peringatan ! ! !')
	setAjak('isipopup', base_url+'penerbit/hapus?id='+id+'&nama='+nama)
}
function proseshapuspenerbitdatamaster(base_url,id){
	setAjak('datapenerbit', base_url+'penerbit/proseshapus?id='+id)
}
function infopenerbitdatamaster(base_url,id){
	docInnerHtml('judulpopup','Info')
	setAjak('isipopup', base_url+'penerbit/info?id='+id)
}
function editpenerbitdatamaster(base_url,id){
	docInnerHtml('judulpopup','Edit')
	setAjak('isipopup',base_url+'penerbit/edit?id='+id)
}

function proseseditpenerbit(base_url,id){
	nama=getNilai('namapenerbitedit')
	deskripsi=getNilai('deskripsipenerbitedit')
	setAjak('datapenerbit', base_url+'penerbit/editproses?nama='+nama+'&deskripsi='+deskripsi+'&id='+id)
}
