function tambahpenulis(base_url) {
	docInnerHtml('judulpopup','Tambah Penulis')
	setAjak('isipopup',base_url+'penulis/tambah')
}
function prosestambahpenulis(base_url){
	nama=getNilai('namapenulis')
	deskripsi=getNilai('namadeskripsi')
	setAjak('datapenulis', base_url+'penulis/prosesTambahPenulis?nama='+nama+'&deskripsi='+deskripsi)
}
function hapuspenulisdatamaster(base_url,id,nama){
	docInnerHtml('judulpopup','Peringatan ! ! !')
	setAjak('isipopup', base_url+'penulis/hapus?id='+id+'&nama='+nama)
}
function proseshapuspenulisdatamaster(base_url,id){
	setAjak('datapenulis', base_url+'penulis/proseshapus?id='+id)
}
function infopenulisdatamaster(base_url,id){
	docInnerHtml('judulpopup','Info')
	setAjak('isipopup', base_url+'penulis/info?id='+id)
}
function editpenulisdatamaster(base_url,id){
	docInnerHtml('judulpopup','Edit')
	setAjak('isipopup',base_url+'penulis/edit?id='+id)
}

function proseseditpenulis(base_url,id){
	nama=getNilai('namapenulisedit')
	deskripsi=getNilai('deskripsipenulisedit')
	setAjak('datapenulis', base_url+'penulis/editproses?nama='+nama+'&deskripsi='+deskripsi+'&id='+id)
}
