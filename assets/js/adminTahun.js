function tambahTahunBaru(base_url) {
	document.getElementById('judulpopup').innerHTML = "Tambah Tahun"
	setAjak("isipopup",base_url+"tahun/tambah")
}

function prosestambahtahun(base_url){
	nama=getNilai('namatahun')
	deskripsi=getNilai('namadeskripsi')
	setAjak('datatahun', base_url+'tahun/prosesTambahTahun?nama='+nama+'&deskripsi='+deskripsi)
}

function hapustahundatamaster(base_url,id,nama){
	document.getElementById('judulpopup').innerHTML= "Peringatan ! ! !"
	setAjak('isipopup', base_url+'tahun/hapus?id='+id+'&nama='+nama)
}

function proseshapustahundatamaster(base_url,id){
	setAjak('datatahun', base_url+'tahun/proseshapus?id='+id)
}

function infotahundatamaster(base_url,id){
	docInnerHtml('judulpopup','Info')
	setAjak('isipopup', base_url+'tahun/info?id='+id)
}

function edittahundatamaster(base_url,id){
	docInnerHtml('judulpopup','Edit')
	setAjak('isipopup',base_url+'tahun/edit?id='+id)
}

function prosesedittahun(base_url,id){
	nama=getNilai('namatahunedit')
	deskripsi=getNilai('deskripsitahunedit')
	setAjak('datatahun', base_url+'tahun/editproses?nama='+nama+'&deskripsi='+deskripsi+'&id='+id)
}