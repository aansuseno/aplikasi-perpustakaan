function tambahTransaksi(base_url, page) {
	document.getElementById('judulpopup').innerHTML = "Tambah Transaksi Baru"
	setAjak('isipopup', base_url+'transaksi/tambah?page='+page)
}

function tambahTransaksiProses(base_url, page) {
	setAjak('daftar_transaksi', base_url+'transaksi/tambahProses?kode_katalog='+getNilai('kode_katalog')+'&nis_siswa='+getNilai('nis_siswa')+'&target_kembali='+getNilai('target_kembali')+'&page='+page)
}

function cekKodeKatalog(base_url) {
	setAjak('hasilcekkode', base_url+'transaksi/cekKodeKatalog?kode='+getNilai('kode_katalog'))
}

function cekNisSiswa(base_url) {
	setAjak('hasilcekkode', base_url+'transaksi/cekNisSiswa?nis='+getNilai('nis_siswa'))
}

function infoTransaksi(base_url, id) {
	document.getElementById('judulpopup').innerHTML = "Detail"
	setAjak('isipopup', base_url+'transaksi/info?id='+id)
}

function hapustransaksi(base_url, id, page) {
	document.getElementById('judulpopup').innerHTML = "<span class='text-danger'>Peringatan !!!</span>"
	setAjak('isipopup', base_url+'transaksi/hapus?id='+id+'&page='+page)
}

function hapustransaksiProses(base_url, id, page) {
	setAjak('daftar_transaksi', base_url+'transaksi/hapusproses?id='+id+'&page='+page)
}

function kembaliTransaksi(base_url, id, page) {
	document.getElementById('judulpopup').innerHTML = "<span class='text-danger'>Peringatan !!!</span>"
	setAjak('isipopup', base_url+'transaksi/kembali?id='+id+'&page=blmKembali')
}

function kembalitransaksiProses(base_url, id, page) {
	setAjak('daftar_transaksi', base_url+'transaksi/kembaliProses?id='+id+'&page='+page)
}

function hitungdenda() {
	jmlhari = getNilai('jumlahhari')
	denda = getNilai('dendaperhari')
	docInnerHtml('jumlahdenda', jmlhari*denda)
}