function cariBuku(base_url) {
	setAjak('list-buku', base_url+'katalog/cariBuku?judul='+getNilai('cari-buku'))
}

function cariBukuId(base_url) {
	setAjak('list-buku', base_url+'katalog/cariBukuId?id='+getNilai('cari-buku-id'))
}

function cekInputan(base_url) {
	kode_katalog = getNilai('kode-katalog')
	id_buku = getNilai('list-buku')
	kondisi = getNilai('kondisi')	
	deskripsi = getNilai('deskripsi')

	if (kode_katalog == '' || id_buku == '') {
		docInnerHtml('pesanerror', '<span class="alert alert-danger">Semua kolom harus diisi.</span>')
	} else {
		docInnerHtml('pesanerror', '')
		setAjak('tambahKatalog', base_url+'katalog/tambahProses?kode_katalog='+kode_katalog+'&id_buku='+id_buku+'&kondisi='+kondisi+'&deskripsi='+deskripsi)
	}
}

function infokatalog(base_url, id) {
	docInnerHtml('judulpopup','Info')
	setAjak('isipopup', base_url+'katalog/info?id='+id)
}

// hapus
function hapusKatalog(base_url, id) {
	docInnerHtml('judulpopup', 'Peringatan !!!')
	setAjak('isipopup', base_url+'katalog/hapus?id='+id)
}

function proseshapuskatalog(base_url,id) {
	setAjak('data_katalog', base_url+'katalog/proseshapus?id='+id)
}

// edit
function editkatalog(base_url,id) {
	docInnerHtml('judulpopup','Edit')
	setAjak('isipopup',base_url+'katalog/edit?id='+id)
}

function cekUpdate(base_url, id) {
	kode_katalog = getNilai('kode-katalog')
	id_buku = getNilai('list-buku')
	kondisi = getNilai('kondisi')	
	deskripsi = getNilai('deskripsi')

	if (kode_katalog == '' || id_buku == '') {
		docInnerHtml('pesanerror', '<span class="alert alert-danger">Semua kolom harus diisi.</span>')
	} else {
		docInnerHtml('pesanerror', '')
		setAjak('data_katalog', base_url+'katalog/editproses?kode_katalog='+kode_katalog+'&id_buku='+id_buku+'&kondisi='+kondisi+'&deskripsi='+deskripsi+'&id='+id)
	}
}