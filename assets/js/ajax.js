function setAjak(idTarget, alamat) {
	var xhr = new XMLHttpRequest()
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && xhr.status == 200) {
			document.getElementById(idTarget).innerHTML = this.responseText
		}
	}

	xhr.open('GET', alamat, true)
	xhr.send()
}

function retAjakValue(tujuan, link) {
	var xhr = new XMLHttpRequest()
	xhr.onreadystatechange = function() {
		if(xhr.readyState == 4 && xhr.status == 200) {
			document.getElementById(tujuan).value += this.responseText + ', '
		}
	}

	xhr.open('GET', link, true)
	xhr.send()
}

function kembalian(nilai) {
	return nilai
}

var idA = 0

function bekuAdmin(id, nama) {
	idA = id
	document.getElementById('nama-petugas-terpilih').innerHTML = nama
}
