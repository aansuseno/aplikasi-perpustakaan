function docInnerHtml(id, isi) {
	document.getElementById(id).innerHTML = isi
}

function getNilai(id) {
	return document.getElementById(id).value
}

function domStyle(id, isi) {
	document.getElementById(id).style = isi
}

function gantiRole(roleSekarang) {
	if (roleSekarang == 'admin') {
		docInnerHtml('roleDaftar', "Admin")
	} else if (roleSekarang == 'pengurus') {
		docInnerHtml('roleDaftar', "Pengurus")
	}
}

function konfirPass() {
	awal = getNilai('pass_awal')
	konf = getNilai('konfPass')
	if (awal != konf) {
		docInnerHtml('harusSama', '*Kata sandi tidak sama..')
		document.getElementById('submit').disabled = "disabled"
	} else {
		docInnerHtml('harusSama', '')
		document.getElementById('submit').disabled = ""
	}
}

function noWaValidasi() {
	wa = getNilai('no_wa')
	if (isNaN(wa)) {
		docInnerHtml('nomorWa', '*Harus berupa bilangan bulat')
		document.getElementById('submit').disabled = "disabled"
	} else if(wa[0] != 6 || wa[1] != 2) {
		docInnerHtml('nomorWa', '*Nomor harus berformat 62')
		document.getElementById('submit').disabled = "disabled"
	} else {
		docInnerHtml('nomorWa', '')
		document.getElementById('submit').disabled = ""
	}
}

function harusAngka(id) {
	if (isNaN(getNilai(id))) {
		docInnerHtml('niswarn', '*NIS harus berupa angka')
		document.getElementById('submit').disabled = "disabled"
	} else {
		docInnerHtml('niswarn', '')
		document.getElementById('submit').disabled = ""
	}
}

function janganKosong(id) {
	sch = getNilai(id)
	if (sch == '') {
		document.getElementById('submit').disabled = "disabled"
	} else {
		document.getElementById('submit').disabled = ""
	}
}

function nonblok(idAwal, idTujuan) {
	domStyle(idAwal, "display: none;")
	domStyle(idTujuan, "display: block;")
}