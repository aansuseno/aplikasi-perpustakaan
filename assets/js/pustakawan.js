function setClass(id, kelas) {
	document.getElementById(id).className = kelas
}

function netralkanSidebar() {
	a = 'nav-link'
	setClass('halamanBeranda', a)
	setClass('halamanTahun', a)
	setClass('halamanKelas', a)
	setClass('halamanSiswa', a)
	setClass('halamanKategori', a)
	setClass('halamanPenulis', a)
	setClass('halamanPenerbit', a)
	setClass('halamanLemari', a)
	setClass('halamanBibliografi', a)
	setClass('halamanKatalog', a)
	setClass('halamanTransaksi', a)
	setClass('halamanCerpen', a)
}

function aktifkan(id) {
	netralkanSidebar()
	document.getElementById(id).className += ' active'
}

function gantiContainer(halaman, link) {
	aktifkan(halaman)
	setAjak('container', link)
}

// tambah //
function tambahDataMaster(judul, link) {
	docInnerHtml('judulpopup', 'Tambah '+judul)
	setAjak('isipopup', link)
}

function tambahDataMasterProses(list, id, link) {
	alamat = link+'?'
	for (i=0; i<id.length; i++) {
		alamat += id[i][0]+'='+getNilai(id[i][1])+'&'
	}
	setAjak(list, alamat)
}
// tambah end //

// hapus //
function hapusDataMaster(judul, link) {
	docInnerHtml('judulpopup', 'Hapus '+judul)
	setAjak('isipopup', link)
}

function hapusDataMasterProses(list, alamat) {
	setAjak(list, alamat)
}
// hapus end //

// info //
function infoDataMaster(judul, link) {
	docInnerHtml('judulpopup', 'Info '+judul)
	setAjak('isipopup', link)
}
// info end //

// edit //
function editDataMaster(judul, link) {
	docInnerHtml('judulpopup', 'Edit '+judul)
	setAjak('isipopup', link)
}

function editDataMasterProses(list, id, link) {
	alamat = link+'?'
	for (i=0; i<id.length; i++) {
		alamat += id[i][0]+'='+getNilai(id[i][1])+'&'
	}
	setAjak(list, alamat)
}
// edit end //

function harusAngka(awal, tujuan, submit) {
	if(isNaN(getNilai(awal)) || getNilai(awal) == '') {
		docInnerHtml(tujuan, 'Kolom harus diisi angka')
		document.getElementById(submit).disabled = "disabled"
	} else {
		docInnerHtml(tujuan, '')
		document.getElementById(submit).disabled = ''
	}
}

function filter(id, tujuan, link) {
	setAjak(tujuan, link+'&filter='+getNilai(id))
}

function getsetnilai(awal, tujuan) {
	document.getElementById(tujuan).value += getNilai(awal)+', '
}

function tambahMasterBuku(dari, table, ke, link) {
	retAjakValue(ke, link+'?table='+table+'&nama='+getNilai(dari))
}

function cekBibliografi(link) {
	judul = getNilai('inputjudul')
	penulis = getNilai('inputpenulis')
	id_penerbit = getNilai('inputid_penerbit')
	id_tahun = getNilai('inputid_tahun')
	jumlah_halaman = getNilai('inputjumlah_halaman')
	kategori = getNilai('inputkategori')
	id_lemari = getNilai('inputid_lemari')
	sampul = getNilai('inputsampul')

	if (judul == '' || penulis == '' || id_penerbit == 'null' || id_tahun == 'null' || jumlah_halaman == '' || kategori == '' || id_lemari == 'null' || sampul == '') {
		docInnerHtml('pesaninputbibliografi', 'Kolom Judul, Penulis, Penerbit, Tahun terbit, Jumlah halaman, Kategori, Lemari, dan Sampul <b>harus diisi</b>.')
	} else if(isNaN(jumlah_halaman)) {
		docInnerHtml('pesaninputbibliografi', 'Kolom Jumlah halaman <b>harus berupa angka</b>.')
	} else {
		docInnerHtml('pesaninputbibliografi', '')
		docInnerHtml('sumit', '<button class="btn btn-primary">Simpan</button>')
	}
}

function caribuku(link) {
	setAjak('inputtambahid_buku', link+'p_katalog/caribuku?judul='+getNilai('filterbuku'))
}

function caribukuid(link) {
	setAjak('inputtambahid_buku', link+'p_katalog/caribukuid?id='+getNilai('idbuku'))
}

function cari(asal, tujuan, link, para) {
	setAjak(tujuan, link+'?'+para+'='+getNilai(asal))
}

function kembaliBuku(link) {
	docInnerHtml('judulpopup', 'Kembali')
	setAjak('isipopup', link)
}

function hitungdenda() {
	docInnerHtml('jumlahdenda', getNilai('jumlahhari')*getNilai('dendaperhari'))
}

function setPagi(link, id, alamat) {
	setAjak('pagination', link)
	setAjak(id, alamat)
}

