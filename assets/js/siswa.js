// sering digunakan
function getNilai(id) {
	return document.getElementById(id).value
}


var sebelumDetailBuku = ''

function setClass(id, kelas) {
	document.getElementById(id).className = kelas
}

function netralkanSidebar() {
	a = 'nav-item'
	setClass('halamanBeranda', a)
	setClass('halamanCari', a)
	setClass('halamanRiwayat', a)
	setClass('halamanPw', a)
	setClass('halamanCerpen', a)
	setClass('halamanKoleksi', a)
	setClass('halamanDiskusi', a)
}

function aktifkan(id) {
	netralkanSidebar()
	document.getElementById(id).className += ' active'
}

function gantiContainer(halaman, link) {
	aktifkan(halaman)
	setAjak('container', link)
}

function detailBuku(link) {
	sebelumDetailBuku = document.getElementById('container').innerHTML
	setAjak('container', link)
}

function kembaliDariDetailBuku() {
	document.getElementById('container').innerHTML = sebelumDetailBuku
}

function cariBuku(link) {
	cari = getNilai('cari-buku')
	setAjak('hasil-cari', link+cari)
}

function bintang(star, link) {
	for (i = star; i >= 1; i--) {
		document.getElementById('bintang'+i).style = 'color: #FFD700;'
	}
	for (i = star+1; i <= 5; i++) {
		document.getElementById('bintang'+i).style = ''
	}
	setAjak('dum', link)
}

function popup(link, judul) {
	document.getElementById('judulpopup').innerHTML = judul
	setAjak('isipopup', link)
}

function lihatYangDibalas(id) {
	document.getElementById(id).style.animation = "pesanDiBalas 2s ease-in-out"
	setTimeout(function(){ document.getElementById(id).style.animation = "none"; }, 3000)
}
