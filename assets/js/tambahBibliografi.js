function filterPenulis(base_url) {
	cari = getNilai('filter-penulis')
	setAjak('list-penulis', base_url+'bibliografi/filterPenulis?cari='+cari)
}

function setPenulis() {
	document.getElementById('penulis').value += getNilai('list-penulis')+', '
}

function tambahPenulis(base_url) {
	nama = getNilai('tambah-penulis')
	setAjak('gak-Ono', base_url+'bibliografi/tambahPenulis?nama='+nama)
	document.getElementById('penulis').value += nama+', '
}

// kategori
function filterKategori(base_url) {
	cari = getNilai('filter-kategori')
	setAjak('list-kategori', base_url+'bibliografi/filterKategori?cari='+cari)
}

function setKategori() {
	document.getElementById('kategori').value += getNilai('list-kategori')+', '
}

function tambahKategori(base_url) {
	nama = getNilai('tambah-kategori')
	setAjak('gak-Ono', base_url+'bibliografi/tambahKategori?nama='+nama)
	document.getElementById('kategori').value += nama+', '
}

// lemari
function tambahLemari(base_url) {
	docInnerHtml('dataYgAkanDitambah', 'Lemari')
	setAjak('isiTambah', base_url+'bibliografi/tambahLemari')
}

function tambahLemariProses(base_url) {
	nama = getNilai('namaLemariBaru')
	deskripsi = getNilai('deskripsiLemariBaru')
	setAjak('lemari', base_url+'bibliografi/tambahLemariProses?nama='+nama+'&deskripsi='+deskripsi)
}


// penerbit
function tambahPenerbit(base_url) {
	docInnerHtml('dataYgAkanDitambah', 'Penerbit')
	setAjak('isiTambah', base_url+'bibliografi/tambahPenerbit')
}

function tambahPenerbitProses(base_url) {
	nama = getNilai('namaPenerbitBaru')
	deskripsi = getNilai('deskripsiPenerbitBaru')
	setAjak('penerbit', base_url+'bibliografi/tambahPenerbitProses?nama='+nama+'&deskripsi='+deskripsi)
}

// tahun
function tambahTahun(base_url) {
	docInnerHtml('dataYgAkanDitambah', 'Tahun')
	setAjak('isiTambah', base_url+'bibliografi/tambahTahun')
}

function tambahTahunProses(base_url) {
	nama = getNilai('namaTahunBaru')
	deskripsi = getNilai('deskripsiTahunBaru')
	setAjak('tahun', base_url+'bibliografi/tambahTahunProses?nama='+nama+'&deskripsi='+deskripsi)
}

function cekSimpan(base_url) {
	judul = getNilai('judul')
	penulis = getNilai('penulis')
	tahun = getNilai('tahun')
	penerbit = getNilai('penerbit')
	kategori = getNilai('kategori')
	lemari = getNilai('lemari')
	halaman = getNilai('halaman')
	deskripsi = getNilai('deskripsi')
	customFile = getNilai('customFile')

	if (judul == '' || penulis == '' || tahun == 'null' || penerbit == 'null'  || kategori == '' || lemari == 'null' || halaman == '' || customFile == '') {
		docInnerHtml('pesan_gagal', 'Semua kolom harus diisi!')
	} else {
		docInnerHtml('pesan_gagal', '')
		docInnerHtml('submit', '<input type="submit" value="Simpan" class="btn btn-primary float-right">')
	}
}