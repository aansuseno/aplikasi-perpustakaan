function detailBuku(base_url, id) {
	docInnerHtml('judulPopup', 'Detail Buku')
	setAjak('isiDetail', base_url+'bibliografi/detailBuku?id='+id)
}

// hapus
function hapusBuku(base_url, id) {
	docInnerHtml('judulPopup', 'Peringatan !!!')
	setAjak('isiDetail', base_url+'bibliografi/hapusBuku?id='+id)
}

function hapusBukuProses(base_url, id, jml) {
	console.log(jml)
	if (jml != 0) {
		tampilPesan('ada')
	} else {
		setAjak('listBuku', base_url+'bibliografi/hapusbukuProses?id='+id)
		tampilPesan('berhasil')
	}
}
// akhir hapus

function tampilPesan(pesan) {
	if (pesan == 'ada') {
		docInnerHtml('pesan', '<div class="alert alert-danger" data-dismiss="alert">Tidak dapat menghapus, dikarenakan data lebih dari 0.</div>')
	} else if (pesan == 'berhasil') {
		docInnerHtml('pesan', '<div class="alert alert-success" data-dismiss="alert">Data berhasil dihapus.</div>')
	}
}

// edit
function editBuku(base_url, id) {
	docInnerHtml('judulPopup', 'Edit Buku !!!')
	setAjak('isiDetail', base_url+'bibliografi/editBuku?id='+id)
}

// list kategori
// kategori
function filterKategori(base_url) {
	cari = getNilai('filter-kategori')
	setAjak('list-kategori', base_url+'bibliografi/filterKategori?cari='+cari)
}

function setKategori() {
	document.getElementById('kategori').value += getNilai('list-kategori')+', '
}

// penulis
function setPenulis() {
	document.getElementById('penulis').value += getNilai('list-penulis')+', '
}

function filterPenulis(base_url) {
	cari = getNilai('filter-penulis')
	setAjak('list-penulis', base_url+'bibliografi/filterPenulis?cari='+cari)
}

// edit simpan
function simpanEditBuku(base_url, id) {
	judul = getNilai('nilai-judul')
	penulis = getNilai('penulis')
	id_penerbit = getNilai('id_penerbit')
	kategori = getNilai('kategori')
	id_lemari = getNilai('id_lemari')
	id_tahun = getNilai('id_tahun')
	jumlah_halaman = getNilai('jumlah_halaman')
	deskripsi = getNilai('deskripsi')
	setAjak('listBuku', base_url+'bibliografi/editBukuProses?judul='+judul+'&penulis='+penulis+'&id_penerbit='+id_penerbit+'&kategori='+kategori+'&id_tahun='+id_tahun+'&jumlah_halaman='+jumlah_halaman+'&deskripsi='+deskripsi+'&id='+id+'&id_lemari='+id_lemari)
}