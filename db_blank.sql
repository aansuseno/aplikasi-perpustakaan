-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 12 Okt 2021 pada 12.17
-- Versi server: 10.4.19-MariaDB
-- Versi PHP: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `perpustakaan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `no_wa` varchar(20) NOT NULL,
  `alamat` text DEFAULT NULL,
  `wewenang` varchar(10) NOT NULL,
  `status` char(1) NOT NULL DEFAULT '0',
  `tgl_daftar` date NOT NULL,
  `bio` text DEFAULT NULL,
  `tgl_edit` date NOT NULL,
  `dikonfirmasi_oleh` int(11) NOT NULL,
  `dibekukan_oleh` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `baca_cerpen`
--

CREATE TABLE `baca_cerpen` (
  `id` bigint(15) NOT NULL,
  `id_siswa` int(9) NOT NULL,
  `id_cerpen` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `bibliografi`
--

CREATE TABLE `bibliografi` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `penulis` text NOT NULL,
  `id_penerbit` int(11) NOT NULL,
  `jumlah_halaman` int(11) NOT NULL,
  `id_tahun` int(11) NOT NULL,
  `kategori` text NOT NULL,
  `id_lemari` int(11) NOT NULL,
  `jumlah_buku` int(11) NOT NULL DEFAULT 0,
  `tersedia` int(10) NOT NULL DEFAULT 0,
  `deskripsi` text DEFAULT NULL,
  `sampul` varchar(200) NOT NULL,
  `rating` float NOT NULL DEFAULT 0,
  `jml_siswa_pemberi_nilai` int(11) NOT NULL DEFAULT 0,
  `tgl_buat` date NOT NULL,
  `tgl_edit` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `bintang_cerpen`
--

CREATE TABLE `bintang_cerpen` (
  `id_siswa` int(10) NOT NULL,
  `id_cerpen` int(10) NOT NULL,
  `id` bigint(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cerpen`
--

CREATE TABLE `cerpen` (
  `id` int(11) NOT NULL,
  `judul` varchar(200) NOT NULL,
  `isi` text NOT NULL,
  `nama_pengarang` varchar(255) NOT NULL,
  `kelas_pengarang` varchar(255) NOT NULL,
  `kategori` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `nis` int(11) NOT NULL,
  `bintang` int(9) NOT NULL,
  `dilihat` bigint(15) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `diskusi`
--

CREATE TABLE `diskusi` (
  `id` int(9) NOT NULL,
  `id_pengirim` int(9) NOT NULL,
  `admin_kah` tinyint(1) NOT NULL DEFAULT 0,
  `pesan` text NOT NULL,
  `dikirim` datetime NOT NULL,
  `nama_pengirim` varchar(255) NOT NULL,
  `membalas` varchar(255) DEFAULT NULL,
  `hapus` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `katalog`
--

CREATE TABLE `katalog` (
  `id_katalog` int(11) NOT NULL,
  `kode_katalog` varchar(100) NOT NULL,
  `id_buku` int(11) NOT NULL,
  `kondisi` varchar(10) NOT NULL,
  `status` varchar(11) NOT NULL DEFAULT 'ada',
  `deskripsi` text DEFAULT NULL,
  `arsip` tinyint(1) NOT NULL DEFAULT 0,
  `tgl_buat_katalog` date NOT NULL,
  `tgl_edit_katalog` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `deskripsi` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE `kelas` (
  `id` int(5) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `kepanjangan` varchar(100) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `jml_siswa` int(11) DEFAULT 0,
  `tgl_buat` date NOT NULL,
  `tgl_edit` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `lemari`
--

CREATE TABLE `lemari` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `deskripsi` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `penerbit`
--

CREATE TABLE `penerbit` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `deskripsi` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengaturan`
--

CREATE TABLE `pengaturan` (
  `id` int(11) NOT NULL,
  `nama_perpus` varchar(255) NOT NULL,
  `moto` varchar(255) NOT NULL,
  `tgl_buat` date NOT NULL,
  `jmlPengunjung` bigint(20) NOT NULL DEFAULT 0,
  `logo` varchar(200) NOT NULL DEFAULT 'logo.png',
  `deskripsi` text DEFAULT NULL,
  `denda` bigint(15) NOT NULL DEFAULT 500,
  `lama_peminjaman` int(10) NOT NULL DEFAULT 3,
  `alamat` varchar(255) DEFAULT 'Jl. kha Ahmad Dahlan NO. 14 PURWOREJO',
  `jabatan_penanda_tangan` varchar(255) DEFAULT 'Kepala Perpustakaan',
  `kota` varchar(255) NOT NULL DEFAULT 'Purworejo',
  `nama_penanda_tangan` varchar(255) NOT NULL DEFAULT 'Anonimus, Spd',
  `nip_penanda_tangan` varchar(255) NOT NULL DEFAULT '85827588838',
  `lama_pesan_diskusi` int(9) NOT NULL DEFAULT 24,
  `max_pesan_diskusi` int(9) NOT NULL DEFAULT 255,
  `panggil_pustakawan` varchar(255) NOT NULL DEFAULT '.kakpustaka'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `penulis`
--

CREATE TABLE `penulis` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `deskripsi` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rating`
--

CREATE TABLE `rating` (
  `id` int(9) NOT NULL,
  `nis_siswa` int(9) NOT NULL,
  `id_buku` int(9) NOT NULL,
  `rating` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE `siswa` (
  `id` int(11) NOT NULL,
  `nis` varchar(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `password` varchar(200) DEFAULT NULL,
  `id_kelas` int(11) NOT NULL,
  `jenis_kelamin` char(1) NOT NULL,
  `no_wa` varchar(20) NOT NULL,
  `status` varchar(200) NOT NULL,
  `mode_gelap` tinyint(1) NOT NULL DEFAULT 0,
  `terakhir_aktif` datetime NOT NULL,
  `aktivitas` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tahun`
--

CREATE TABLE `tahun` (
  `id` int(11) NOT NULL,
  `nama` int(4) NOT NULL,
  `deskripsi` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(11) NOT NULL,
  `kode_katalog` varchar(100) NOT NULL,
  `nis_siswa` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `tgl_pinjam` date NOT NULL,
  `target_kembali` date NOT NULL,
  `tgl_kembali` date DEFAULT NULL,
  `kembali` tinyint(1) NOT NULL DEFAULT 0,
  `tepat_waktu` int(2) NOT NULL DEFAULT 1,
  `minta_tambah_lama_pinjam` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indeks untuk tabel `baca_cerpen`
--
ALTER TABLE `baca_cerpen`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `bibliografi`
--
ALTER TABLE `bibliografi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `bintang_cerpen`
--
ALTER TABLE `bintang_cerpen`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `cerpen`
--
ALTER TABLE `cerpen`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `diskusi`
--
ALTER TABLE `diskusi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `katalog`
--
ALTER TABLE `katalog`
  ADD PRIMARY KEY (`id_katalog`),
  ADD UNIQUE KEY `kode_katalog` (`kode_katalog`);

--
-- Indeks untuk tabel `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `lemari`
--
ALTER TABLE `lemari`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `penerbit`
--
ALTER TABLE `penerbit`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pengaturan`
--
ALTER TABLE `pengaturan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `penulis`
--
ALTER TABLE `penulis`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nis` (`nis`);

--
-- Indeks untuk tabel `tahun`
--
ALTER TABLE `tahun`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `baca_cerpen`
--
ALTER TABLE `baca_cerpen`
  MODIFY `id` bigint(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `bibliografi`
--
ALTER TABLE `bibliografi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `bintang_cerpen`
--
ALTER TABLE `bintang_cerpen`
  MODIFY `id` bigint(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `cerpen`
--
ALTER TABLE `cerpen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `diskusi`
--
ALTER TABLE `diskusi`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `katalog`
--
ALTER TABLE `katalog`
  MODIFY `id_katalog` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `lemari`
--
ALTER TABLE `lemari`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `penerbit`
--
ALTER TABLE `penerbit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `pengaturan`
--
ALTER TABLE `pengaturan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `penulis`
--
ALTER TABLE `penulis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `rating`
--
ALTER TABLE `rating`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tahun`
--
ALTER TABLE `tahun`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
