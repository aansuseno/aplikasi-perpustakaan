-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 12 Okt 2021 pada 12.16
-- Versi server: 10.4.19-MariaDB
-- Versi PHP: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `perpustakaan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `no_wa` varchar(20) NOT NULL,
  `alamat` text DEFAULT NULL,
  `wewenang` varchar(10) NOT NULL,
  `status` char(1) NOT NULL DEFAULT '0',
  `tgl_daftar` date NOT NULL,
  `bio` text DEFAULT NULL,
  `tgl_edit` date NOT NULL,
  `dikonfirmasi_oleh` int(11) NOT NULL,
  `dibekukan_oleh` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `nama`, `no_wa`, `alamat`, `wewenang`, `status`, `tgl_daftar`, `bio`, `tgl_edit`, `dikonfirmasi_oleh`, `dibekukan_oleh`) VALUES
(2, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Farhan Hore', '90348834', 'Indonesia', 'admin', 'y', '2021-07-21', '', '0000-00-00', -1, NULL),
(9, 'aan', '4607ed4bd8140e9664875f907f48ae14', 'Aan Zo', '6285222278081', NULL, 'admin', 'y', '2021-07-22', NULL, '2021-07-22', 2, 2),
(17, 'aan123', '59c6f802008a2f455463881a9eec338d', 'Aan 123', '628522278081', NULL, 'admin', 'b', '2021-07-23', NULL, '2021-07-23', 9, 2),
(18, 'budi', '00dfc53ee86af02e742515cdcf075ed3', 'Budi', '628522278081', NULL, 'admin', 'y', '2021-07-23', NULL, '2021-07-23', 17, 2),
(19, 'anton', '784742a66a3a0c271feced5b149ff8db', 'Anton', '6285222278081', NULL, 'admin', 'n', '2021-07-23', NULL, '2021-07-23', 0, NULL),
(20, 'hola', 'e961b2ac40aac4cc36a8bf65bca9177e', 'Workplis', '6285222278081', 'Indonesia', 'pustakawan', 'y', '2021-08-11', 'Aku Seorang Kapiten', '2021-08-11', 9, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `baca_cerpen`
--

CREATE TABLE `baca_cerpen` (
  `id` bigint(15) NOT NULL,
  `id_siswa` int(9) NOT NULL,
  `id_cerpen` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `baca_cerpen`
--

INSERT INTO `baca_cerpen` (`id`, `id_siswa`, `id_cerpen`) VALUES
(1, 26, 5),
(2, 26, 13),
(3, 26, 6),
(4, 41, 12),
(5, 41, 14),
(6, 41, 6),
(7, 41, 8),
(8, 41, 17),
(9, 41, 15),
(10, 41, 13),
(11, 41, 5),
(12, 41, 10),
(13, 41, 7),
(14, 26, 17),
(15, 26, 12),
(16, 26, 10),
(17, 26, 8),
(18, 26, 11);

-- --------------------------------------------------------

--
-- Struktur dari tabel `bibliografi`
--

CREATE TABLE `bibliografi` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `penulis` text NOT NULL,
  `id_penerbit` int(11) NOT NULL,
  `jumlah_halaman` int(11) NOT NULL,
  `id_tahun` int(11) NOT NULL,
  `kategori` text NOT NULL,
  `id_lemari` int(11) NOT NULL,
  `jumlah_buku` int(11) NOT NULL DEFAULT 0,
  `tersedia` int(10) NOT NULL DEFAULT 0,
  `deskripsi` text DEFAULT NULL,
  `sampul` varchar(200) NOT NULL,
  `rating` float NOT NULL DEFAULT 0,
  `jml_siswa_pemberi_nilai` int(11) NOT NULL DEFAULT 0,
  `tgl_buat` date NOT NULL,
  `tgl_edit` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bibliografi`
--

INSERT INTO `bibliografi` (`id`, `judul`, `penulis`, `id_penerbit`, `jumlah_halaman`, `id_tahun`, `kategori`, `id_lemari`, `jumlah_buku`, `tersedia`, `deskripsi`, `sampul`, `rating`, `jml_siswa_pemberi_nilai`, `tgl_buat`, `tgl_edit`) VALUES
(10, 'Morning Pagi', ' Luna Lovegood, Tzuyu, ', 19, 100, 10, ' Drama, Fiksi Ilmiah, ', 12, 3, 2, '', 'images.jpeg', 4, 1, '2021-08-09', '2021-08-09'),
(11, 'Awesome Luarbiasa1', ' Scott Lang, Tony Stark, ', 16, 89, 7, ' Biografi, Drama, ', 5, 26, 22, '', 'f68443a9a72ed28ec403b005127c5962.jpg', 3.5, 2, '2021-08-09', '2021-09-12'),
(12, 'You Kamu', ' Bucky Burnes, Mark, Bill Gates, ', 10, 45, 13, ' Horror, Kriminal, ', 8, 2, 0, 'Deskripsi', 'terabithiaar01.jpg', 0, 0, '2021-08-09', '2021-08-09'),
(14, 'Siapa Is Who', 'Alastor Moody, Carol Danvers, ', 15, 2323, 1, 'Drama, ', 2, 0, 0, '</div>', 'wp8285214-landscape-painting-wallpapers.jpg', 0, 0, '2021-08-18', '2021-08-18'),
(15, 'Tertawa Kah', 'Bill Gates, ', 25, 12, 30, 'Biografi, ', 5, 6, 5, 'ANuan', 'websaya.svg', 3, 1, '2021-08-18', '2021-09-13'),
(16, 'Maybe Mungkin', 'James Sirius Potter, ', 23, 40, 7, 'Drama, ', 8, 1, 1, 'hehe', 'images1.jpeg', 3, 1, '2021-08-24', '2021-08-24'),
(17, 'Jangan Don\'t', 'Hermione Granger, ', 13, 78, 32, 'Komedi, ', 22, 1, 1, 'Lmao', 'images_(13).jpeg', 0, 0, '2021-08-24', '2021-08-24'),
(18, 'Mine Anuku', 'Fleur Delacour, Indra Otsutsuki, ', 12, 54, 14, 'Kriminal, ', 5, 1, 1, '', 'kilua.png', 0, 0, '2021-08-24', '2021-08-24'),
(19, 'Bu kanNot SayaMe ', 'Professor Minerva McGonagall, ', 14, 300, 1, 'Drama, ', 21, 0, 0, '', 'berhentibelakang.png', 0, 0, '2021-08-24', '2021-08-24');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bintang_cerpen`
--

CREATE TABLE `bintang_cerpen` (
  `id_siswa` int(10) NOT NULL,
  `id_cerpen` int(10) NOT NULL,
  `id` bigint(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `bintang_cerpen`
--

INSERT INTO `bintang_cerpen` (`id_siswa`, `id_cerpen`, `id`) VALUES
(26, 6, 6),
(41, 12, 7),
(41, 10, 8),
(41, 5, 9),
(26, 5, 11),
(26, 17, 13);

-- --------------------------------------------------------

--
-- Struktur dari tabel `cerpen`
--

CREATE TABLE `cerpen` (
  `id` int(11) NOT NULL,
  `judul` varchar(200) NOT NULL,
  `isi` text NOT NULL,
  `nama_pengarang` varchar(255) NOT NULL,
  `kelas_pengarang` varchar(255) NOT NULL,
  `kategori` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `nis` int(11) NOT NULL,
  `bintang` int(9) NOT NULL,
  `dilihat` bigint(15) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cerpen`
--

INSERT INTO `cerpen` (`id`, `judul`, `isi`, `nama_pengarang`, `kelas_pengarang`, `kategori`, `status`, `nis`, `bintang`, `dilihat`) VALUES
(5, 'Ayam Goreng', 'Sed commodo hendrerit mi quis imperdiet. Ut hendrerit dui ipsum, vel tincidunt ligula condimentum sed. In luctus ante scelerisque lectus tristique porta. Donec euismod convallis lacinia. Praesent sit amet convallis est. Nam a porta dolor. Cras sagittis, velit nec ultrices ullamcorper, neque felis accumsan risus, sit amet malesuada nunc lorem quis est. Mauris ut nisl tempor, maximus erat at, lobortis lectus.\r<br>\r<br>Mauris vitae semper lacus. Sed iaculis at tellus sit amet ornare. Cras eu ornare nunc. Sed ultricies lorem nibh, vel elementum velit pellentesque a. Nulla viverra enim vel leo suscipit ornare. Quisque ullamcorper malesuada massa, vitae elementum ipsum cursus in. Praesent tempor tellus ac velit auctor, in malesuada mi feugiat. Pellentesque posuere lacus ut neque tincidunt, id volutpat ex ornare. Donec sodales, augue varius ullamcorper ultricies, nisi ante varius ipsum, eu viverra neque dolor vel purus. Nunc eu odio id lectus rhoncus gravida. Morbi id nisl quis metus placerat efficitur in quis massa. Phasellus accumsan dui ut massa gravida, ut lacinia ante sollicitudin. Nunc consectetur est dui, vel mollis sem porta quis. Etiam commodo nulla vel enim consectetur maximus. Cras commodo dolor tortor, molestie mattis lorem efficitur at. Vivamus tellus arcu, accumsan eu accumsan lobortis, facilisis pharetra lacus.\r<br>\r<br>Nullam at felis feugiat, finibus metus ut, blandit odio. Suspendisse potenti. Nam aliquet, libero ac varius sollicitudin, ipsum nisl dignissim nisi, quis feugiat neque purus non felis. Proin elementum in sapien eu dapibus. Maecenas euismod non risus ac sollicitudin. Pellentesque lobortis lorem quis quam aliquam congue. Vivamus sodales arcu quis neque dapibus mollis. Duis egestas, ex sit amet volutpat ultrices, turpis libero hendrerit arcu, a ullamcorper neque purus et felis. Aliquam lacus tellus, eleifend sit amet tortor at, cursus tempor sapien. Sed nisl orci, condimentum eu consequat faucibus, fermentum quis quam. In hac habitasse platea dictumst. In libero dui, finibus a enim interdum, aliquet suscipit sem. Sed pellentesque, nibh at vehicula pretium, orci velit aliquam quam, a hendrerit eros erat faucibus sapien. Vivamus pulvinar ligula varius metus blandit, non suscipit orci venenatis. Praesent at dui nisi. Nullam metus arcu, dignissim sit amet faucibus eget, efficitur at nibh.\r<br>\r<br>Ut porta magna ut ligula mollis pharetra. Integer tincidunt turpis est, a porta metus hendrerit sit amet. Nunc nec nulla maximus, hendrerit eros ac, ornare libero. Vivamus malesuada nibh id arcu mollis tincidunt. Aliquam viverra nunc nec lectus interdum, vitae suscipit nunc pretium. Duis rhoncus gravida augue eget lacinia. Vestibulum nec lorem elit. Suspendisse nec turpis lacus. Suspendisse potenti. Nullam euismod diam lectus, at blandit magna consectetur vel. Aenean massa risus, feugiat a lectus eget, mattis convallis erat.\r<br>\r<br>Quisque id risus blandit, auctor arcu vitae, vehicula dui. Vivamus ut elementum lectus. Mauris sit amet malesuada diam, a cursus nibh. Donec interdum dui et augue finibus, ut laoreet tellus lobortis. Quisque id euismod sapien. Mauris sollicitudin purus non ullamcorper pretium. Etiam pharetra lectus ac erat malesuada mattis. Maecenas venenatis, velit porta congue tristique, neque justo auctor elit, iaculis sollicitudin odio ex ut dui. Cras gravida a nunc quis cursus. Nullam metus ipsum, viverra id euismod nec, blandit eget sem. Nullam leo est, euismod vel dolor non, aliquet viverra felis. Nullam placerat tellus nec justo dapibus, ut gravida leo convallis. Praesent sapien nibh, porttitor ac tempor sit amet, elementum nec justo. ', 'Asik', 'XI RPL', 'Horror, Drama', 1, 1111, 2, 1),
(6, 'Babi Kuah', 'Morbi euismod luctus urna eget fermentum. Etiam sodales justo ut mauris porta, non pulvinar lacus tincidunt. Nunc nisl dui, faucibus sed augue at, consequat tristique sem. Nulla facilisi. Fusce eget nibh consequat, fringilla felis sed, vehicula diam. In rutrum ornare quam, id efficitur dui interdum ac. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec ut libero quis est faucibus ultricies vitae lacinia eros. Cras dictum velit eu vulputate cursus. Duis nec cursus dolor. Nulla volutpat nibh non elit ullamcorper luctus id vitae neque.\r<br>\r<br>Integer odio mi, maximus et elit ut, euismod mollis felis. Donec pellentesque erat malesuada lectus interdum, vitae vestibulum lectus commodo. Etiam vel mi a ex accumsan gravida. Morbi faucibus sit amet orci sed commodo. Nullam sit amet commodo metus. Vestibulum molestie viverra purus ut pellentesque. Etiam sed lectus imperdiet, placerat sapien at, euismod arcu. Nam pretium diam risus, ac eleifend magna accumsan scelerisque. Praesent nec magna ante. Quisque eu ipsum bibendum, congue mauris id, aliquam dui. Etiam maximus risus at eros ultrices vulputate. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas ut sapien quis nulla cursus volutpat eu sed nisl. Nulla vitae felis venenatis, porta ligula vel, hendrerit nulla. Sed ultricies massa sit amet libero viverra euismod. Nam ut tortor eget turpis pretium bibendum.\r<br>\r<br>Aenean purus sem, tincidunt sit amet  ac, pretium ac est. Aliquam fermentum est iaculis, tincidunt augue sed, rutrum ante. Duis accumsan tortor placerat arcu commodo ullamcorper. In suscipit sagittis lectus id fermentum. Maecenas facilisis lorem nibh, ac congue lorem maximus et. Nunc vitae turpis eu dolor interdum iaculis. Praesent tincidunt tempus leo eu suscipit. Curabitur ultrices dapibus dolor, eu pharetra magna scelerisque in. Proin nibh eros, pretium nec mattis et, fermentum non tellus. Nam non efficitur mi. Phasellus tristique lorem vitae libero varius maximus. Quisque pulvinar malesuada arcu eget fringilla. Quisque in mauris vel arcu posuere pulvinar. Duis ac elit sit amet sapien porta aliquam.\r<br>\r<br>Etiam eleifend turpis non dapibus auctor. Proin et magna at mauris ultrices iaculis. Vivamus sodales nisl enim, quis malesuada justo blandit eu. Praesent ornare turpis ex, sit amet molestie nibh semper finibus. Vestibulum commodo imperdiet sapien sit amet finibus. Nullam ullamcorper, dolor eget viverra facilisis, dui leo mattis est, vel tempor diam ipsum a augue. Phasellus euismod sodales lacinia. In sodales elementum convallis. Nulla nec dapibus ligula, eu tincidunt erat. Vivamus dictum elit non sem placerat, at sollicitudin purus mattis. Nam elementum vulputate commodo. Fusce a risus quis mi tincidunt aliquet. Vestibulum iaculis interdum tristique. Nullam a pellentesque mauris. Suspendisse placerat erat a tristique faucibus. Vestibulum ac nulla leo.\r<br>\r<br>In tincidunt lectus quis posuere fringilla. Ut ut sollicitudin orci. Suspendisse laoreet diam diam, eget tempor dui dignissim et. Proin eleifend nibh in ipsum lacinia dapibus. Praesent laoreet sapien id massa blandit aliquet. Cras scelerisque et neque eget ornare. Fusce et pharetra dui. Praesent lacinia scelerisque sapien ac mollis.\r<br>\r<br>Ut vitae faucibus purus, ac lobortis justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In hac habitasse platea dictumst. Curabitur feugiat eleifend sapien vel auctor. Duis in lacinia urna. In dapibus sodales diam et sodales. Vestibulum aliquam at velit nec ullamcorper. Cras quis interdum diam. Sed quis dignissim justo, at porta magna. Suspendisse pulvinar finibus felis id tristique. In pellentesque imperdiet eleifend. Aliquam et nulla imperdiet arcu bibendum laoreet. Curabitur aliquam quam et condimentum congue. Vivamus pharetra risus sed finibus elementum.\r<br>\r<br>Suspendisse potenti. Fusce iaculis nunc neque, id tempor ex maximus sed. Aliquam erat volutpat. Aliquam erat volutpat. Donec dictum odio in mi convallis, vel molestie mauris vulputate. Integer scelerisque vitae enim et sollicitudin. Mauris mollis, nisl ut facilisis efficitur, arcu diam mollis neque, vel luctus ipsum metus ut sem. Aenean vel iaculis orci, id egestas mauris. In eros dui, tempus nec risus egestas, rhoncus dignissim nisi. Suspendisse potenti. ', 'Asik', 'XI RPL', 'Fantasi, Adventure', 1, 1111, 1, 2),
(7, 'Gila Mantap', 'Fusce ultrices sem commodo ante euismod, sit amet sodales nisi ullamcorper. Morbi tempus neque at turpis fringilla bibendum. Cras viverra arcu id diam sollicitudin, non commodo ipsum tincidunt. Curabitur hendrerit ultricies augue non tempus. Morbi quam mi, ornare nec aliquet sit amet, lacinia imperdiet est. Aliquam diam enim, elementum sit amet tincidunt ut, faucibus id est. Sed facilisis mattis elit ac fermentum. Vivamus lobortis hendrerit nulla, ac porta elit laoreet nec. Morbi elit odio, feugiat a libero sed, auctor iaculis urna. Aliquam tempor tellus commodo nibh facilisis, eget ullamcorper ipsum aliquam. Vestibulum pulvinar felis finibus leo blandit posuere. In hac habitasse platea dictumst. Sed venenatis ipsum vitae tincidunt posuere.\r<br>\r<br>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus feugiat metus tincidunt, aliquet ligula quis, venenatis urna. Aliquam nisi ligula, tincidunt sed finibus et, maximus eu lacus. Suspendisse ultrices, elit eget suscipit egestas, leo nulla placerat urna, nec tempor nibh elit in felis. Etiam eleifend tellus augue, sed euismod dui mollis a. Etiam eros metus, imperdiet vel elit vulputate, sagittis interdum velit. Pellentesque euismod malesuada massa, vel bibendum tellus aliquet non. Suspendisse id neque volutpat, eleifend nunc quis, lacinia ex. Duis in dictum neque. Mauris rhoncus tortor consectetur elit tincidunt consequat. In faucibus mi quis augue gravida faucibus. Ut mattis convallis eros.\r<br>\r<br>Praesent fringilla elit eu diam pharetra ultrices. Morbi vestibulum placerat elit convallis varius. Nunc mattis malesuada tincidunt. Sed commodo ligula et vehicula vestibulum. Duis et odio vel enim fringilla pharetra. Vivamus eleifend euismod dolor et efficitur. Maecenas non fermentum lacus. Donec eget dui rhoncus, fermentum sem nec, sagittis diam. Cras eget enim fringilla, eleifend massa vel, feugiat lorem. Suspendisse maximus sed ipsum ac tempor.\r<br>\r<br>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce tincidunt nisi eu orci vehicula, eget iaculis ipsum pretium. Etiam rutrum arcu a velit tempus posuere. Vestibulum laoreet purus quis lacus tempus tempus. Duis sed erat lobortis, vestibulum mi in, volutpat nunc. Nunc ac blandit nibh, sit amet malesuada felis. Sed ac lorem at urna venenatis congue ac quis mauris.\r<br>\r<br>Donec porttitor imperdiet eleifend. Nunc a nisl ut risus vehicula malesuada sit amet sed diam. Maecenas sed semper sem. Aliquam vehicula nulla id arcu iaculis rutrum. Ut auctor tellus eget dui auctor consectetur. Sed posuere metus a tincidunt imperdiet. Sed tristique hendrerit auctor. Nam ipsum urna, semper vel velit non, ornare luctus risus. Nulla dolor mi, eleifend et nisl vitae, malesuada volutpat mauris. Vestibulum mattis, velit a varius faucibus, libero augue sollicitudin lectus, id egestas est ante sit amet leo. Vestibulum commodo ipsum eu iaculis auctor. Curabitur suscipit ipsum ut ligula rhoncus egestas. Integer convallis volutpat lacus, vitae mattis orci accumsan non.\r<br>\r<br>Phasellus molestie justo lacus, et auctor velit faucibus sed. Sed vel purus bibendum, consectetur magna eu, dignissim orci. Curabitur cursus blandit viverra. In suscipit vehicula sollicitudin. In hac habitasse platea dictumst. Vestibulum venenatis dui ut lectus vestibulum, vitae aliquet nisi fringilla. Etiam varius nisl cursus felis tristique fringilla. Pellentesque vestibulum, tortor sollicitudin feugiat mollis, leo erat aliquet purus, nec vehicula nunc erat at turpis.\r<br>\r<br>Suspendisse pharetra ipsum dui, eget sagittis nisi feugiat vel. Proin gravida consequat venenatis. Praesent dictum lectus nisl, eu pharetra magna luctus ac. Curabitur in nibh et quam molestie tincidunt. Phasellus sit amet bibendum nisi. Aliquam enim dolor, tempus rhoncus tempor in, gravida ac quam. Maecenas sed erat purus. Quisque pharetra arcu a nibh placerat, quis sollicitudin ligula maximus. Fusce aliquam porttitor nisl ac vehicula. Cras at enim id metus sagittis posuere. Donec vitae elit ut sem pellentesque ultrices. Donec sit amet ultricies lacus, ac posuere ligula. Nulla facilisi. Morbi et gravida elit. Sed elementum augue ac risus scelerisque ornare. ', 'Asik', 'XI RPL', 'Biografi, Sejarah', 1, 1111, 0, 1),
(8, 'Mawar & Matahari', 'Morbi vel nisi eu mauris malesuada mollis vel eu augue. In pharetra lorem ut lacus luctus ullamcorper. Pellentesque a purus euismod, convallis enim nec, fringilla nibh. Etiam felis odio, tincidunt vel sodales et, tincidunt vel elit. Vivamus congue vehicula tellus sit amet tincidunt. Sed vitae ultricies turpis. Sed non sagittis ligula. Proin suscipit nunc sed odio vehicula faucibus. Etiam semper pharetra felis, ut faucibus tortor interdum in. Mauris et consectetur leo. Aliquam nulla elit, efficitur malesuada ligula id, congue pulvinar leo. Nunc egestas lorem a mattis cursus. Nunc eget commodo ex, nec volutpat sem.\r<br>\r<br>Fusce venenatis vestibulum pharetra. Integer vel lorem in metus cursus sagittis. Aliquam auctor, sapien in bibendum suscipit, enim leo finibus magna, quis posuere eros mi et massa. Morbi sit amet ipsum dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vestibulum lacus vitae dui convallis pulvinar. Vestibulum accumsan est at quam lobortis laoreet. Ut ullamcorper fermentum posuere. Nunc bibendum hendrerit lacus, non aliquam purus ullamcorper vel. Suspendisse venenatis neque sodales efficitur gravida. Cras sit amet consequat turpis, nec cursus ante. Suspendisse facilisis, odio non mollis rhoncus, nisl mi posuere nunc, ac malesuada nisl turpis ac mi. Ut facilisis pretium blandit.\r<br>\r<br>Curabitur rutrum neque odio, vel sodales ex ultrices eget. Mauris eget erat iaculis, vestibulum arcu non, lobortis orci. Praesent commodo hendrerit efficitur. Nunc id tempus neque, id fringilla nunc. Cras vestibulum risus lorem, quis interdum ligula laoreet sed. Quisque eu vulputate dui. Morbi nec enim pellentesque, tincidunt lorem a, ultricies turpis. Nunc vehicula non arcu id fermentum. In molestie quis augue id dapibus. Vivamus varius viverra justo eget placerat. Fusce porta augue in purus interdum, eget ornare ligula cursus. Aliquam fringilla, nisi aliquam commodo luctus, arcu nunc elementum lacus, non sollicitudin nulla risus et turpis. Nunc bibendum ullamcorper efficitur.\r<br>\r<br>Aenean tincidunt bibendum nibh id consequat. In feugiat purus sed ligula placerat maximus. Aliquam ut fermentum ex. Cras a libero sed sapien rutrum mattis. Suspendisse posuere orci velit, sit amet molestie odio aliquet at. Morbi eget lobortis mauris. Donec faucibus faucibus nisl, nec eleifend est congue in. Integer ultricies laoreet est, quis feugiat massa facilisis et. Integer scelerisque nibh sem, vel facilisis sapien pellentesque eget. Suspendisse quis odio ut mi varius dapibus vel et mauris. Nunc nec velit elit. Fusce condimentum condimentum purus, a viverra nulla sollicitudin vel.\r<br>\r<br>Vestibulum diam magna, ullamcorper at pretium eu, pharetra ornare orci. Vivamus vitae erat pharetra, porttitor nisi vel, consectetur neque. Suspendisse pharetra ligula nisi, vehicula rutrum augue volutpat ut. Nullam lacinia augue at consectetur aliquam. Nulla ultricies commodo nulla, vel pretium sapien dignissim vel. Duis eget interdum nibh, eget finibus felis. Ut faucibus ipsum tortor, et pulvinar justo sollicitudin quis. Nunc nunc libero, sodales sed nibh non, maximus ultricies leo. Fusce quis mauris id urna commodo tempor.\r<br>\r<br>Fusce feugiat est vitae tortor efficitur elementum. Aliquam accumsan ultrices lorem, malesuada vehicula leo porttitor et. Sed eget erat bibendum, malesuada est in, pellentesque eros. Mauris id molestie metus, nec vestibulum sapien. Sed eu purus elit. Maecenas imperdiet vestibulum nulla, vel hendrerit lorem venenatis at. Duis vitae dignissim odio, ac maximus justo. Ut sed egestas ipsum. Aliquam faucibus, massa a porttitor lobortis, tellus ipsum porttitor dolor, vel dictum magna elit ac sem. Integer et finibus ipsum. Pellentesque a scelerisque massa. Nullam malesuada viverra rhoncus.\r<br>\r<br>Mauris porttitor ex et lobortis imperdiet. Aliquam erat volutpat. Mauris lacus justo, tristique vel dolor et, vehicula viverra arcu. Maecenas libero felis, bibendum in faucibus eget, fringilla vel ex. Nunc faucibus purus viverra felis tincidunt fringilla. Etiam euismod dui ut eleifend maximus. Praesent vestibulum mauris non euismod venenatis. Mauris ac congue ligula, non cursus velit. Sed nisl odio, tristique sit amet tellus eget, interdum facilisis enim. Aliquam porttitor lacus quam, ut mollis felis mattis non. Ut tempus mattis metus, nec aliquet orci pellentesque non. Nullam varius nisl sit amet augue tristique accumsan. Nunc luctus egestas diam quis interdum. Cras venenatis sapien sapien, non mollis tortor dignissim vel. ', 'coco', 'XI TKJ', 'Drama, Komedi, Histori', 1, 5555, 0, 2),
(9, 'Melati Ora Umum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non sapien scelerisque, eleifend arcu non, fringilla velit. Nam vitae velit sit amet magna vehicula ultricies. Donec pretium convallis purus, sed feugiat sapien finibus a. Phasellus tellus urna, volutpat at augue et, rutrum vestibulum tortor. Integer scelerisque lectus eget ligula congue tristique. Cras dictum, lacus quis consectetur lobortis, dolor ante lobortis mi, et rhoncus orci ante in sem. Aenean pharetra lectus sit amet leo iaculis varius. Vivamus sagittis in mi a porta. Curabitur facilisis sapien sed pulvinar tempus.\r<br>\r<br>Aenean efficitur vehicula imperdiet. Sed mollis porttitor laoreet. Phasellus vehicula tincidunt massa. Integer eu odio eget velit facilisis hendrerit. Nullam sodales pharetra viverra. Integer dictum lobortis leo, non tristique ex. Ut blandit molestie risus, ut efficitur massa dictum imperdiet. Phasellus accumsan purus eu vestibulum convallis. Cras mollis, enim et ornare fermentum, leo nulla dapibus sem, quis rutrum sapien sem ac eros. Mauris in nibh auctor turpis laoreet porta non sed felis. Nullam semper libero et lacinia fringilla.\r<br>\r<br>Phasellus quis justo cursus risus pharetra sodales ultricies id eros. Vivamus gravida sapien sed mi maximus accumsan. Pellentesque sollicitudin bibendum mi, in viverra magna dignissim quis. Nam ac elementum ante. Nulla dapibus elementum sapien. Etiam imperdiet erat lectus, a posuere sem varius et. Vestibulum posuere purus in tortor rutrum sodales id ut neque. Maecenas in ullamcorper ex, non faucibus lorem.\r<br>\r<br>Nam ut enim eu sapien tincidunt pellentesque vel sed augue. Cras porttitor interdum leo. Nullam vulputate ut velit a ultricies. Aliquam erat volutpat. Aenean quam eros, rutrum vel neque vitae, convallis feugiat urna. Phasellus et felis id tortor dictum iaculis. Nam ligula lacus, congue id ultrices accumsan, consectetur a ante. Nunc finibus ut est ac accumsan.\r<br>\r<br>Nunc faucibus tortor quis quam lobortis, nec pellentesque quam volutpat. In finibus ultrices sapien, id ultricies magna pellentesque eget. Pellentesque tristique leo feugiat, tempor ante ac, consequat nulla. Aliquam vehicula libero ante, eget blandit est sodales sed. In hac habitasse platea dictumst. Mauris ultricies suscipit euismod. Mauris placerat felis ipsum, non fermentum felis interdum sit amet. Aenean sed malesuada lectus.\r<br>\r<br>Suspendisse potenti. Donec nec dui nec elit finibus venenatis. Curabitur lacus augue, efficitur at vestibulum eu, molestie dapibus tortor. Donec accumsan aliquet rutrum. In dapibus elementum mauris sit amet mattis. Suspendisse facilisis justo ac ipsum congue, id gravida eros scelerisque. Nam efficitur mauris ut ultrices vehicula. Sed aliquet rutrum placerat. Morbi malesuada lectus nec blandit tristique. Sed venenatis ut eros vitae cursus. Nullam euismod semper erat, eu maximus nulla eleifend sit amet. Pellentesque imperdiet risus ut suscipit finibus. Cras cursus nulla nisi, eu eleifend arcu congue vel. Nunc suscipit justo at iaculis convallis.\r<br>\r<br>Vivamus vitae velit vel lectus sodales congue vitae et odio. Duis nec nisl ex. Curabitur feugiat eget dui vel mattis. Vivamus sodales justo ac auctor aliquet. Vivamus quis congue purus, sed sagittis mauris. Aliquam sollicitudin, velit non tristique faucibus, urna nunc sodales orci, ac lacinia augue ipsum ut neque. Cras in condimentum massa. Fusce rutrum tortor eu neque posuere, quis ornare augue tempor. Aenean condimentum elit sed mauris tincidunt, non consequat massa ornare. Donec nulla turpis, vestibulum eu pharetra at, maximus et dui. Proin in dolor ut tortor semper pulvinar a ut turpis. Suspendisse imperdiet cursus tincidunt.\r<br>\r<br>Pellentesque eu sapien vitae felis dapibus dictum. Phasellus non lorem in urna cursus venenatis eget ac orci. Maecenas in turpis eu tellus pulvinar maximus vitae at nisl. Aliquam luctus elementum arcu, ac dapibus libero auctor ac. Aliquam erat volutpat. Interdum et malesuada fames ac ante ipsum primis in faucibus. Praesent sed lacinia metus. Etiam interdum viverra massa ut pretium. Etiam rutrum velit erat, in facilisis metus fermentum eu. Nam ornare molestie justo, feugiat porttitor risus convallis quis. Etiam vel hendrerit mauris.\r<br>\r<br>Proin sed maximus tortor. Nulla posuere feugiat risus pellentesque vestibulum. Vivamus aliquet massa tellus, ac maximus nulla scelerisque ac. Mauris finibus odio et mauris bibendum, eu consectetur mauris imperdiet. Cras euismod est ut cursus varius. Aliquam eget eros in ligula dapibus maximus. Duis nec viverra tellus, lobortis cursus ante. Mauris congue aliquet sollicitudin. ', 'coco', 'XI TKJ', 'Politik, Sosial', 1, 5555, 0, 0),
(10, 'Anggrek', 'Curabitur tincidunt dignissim tempor. Vestibulum sit amet arcu at elit euismod euismod ac nec erat. Proin eget laoreet justo. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam dapibus aliquam neque, non efficitur ipsum. Aenean sagittis sodales mauris vel cursus. Aliquam vel lectus dignissim, ullamcorper tortor semper, iaculis ex. Aenean placerat felis id tristique commodo. Vivamus condimentum bibendum tristique. Proin porttitor orci non dui aliquam, nec auctor velit viverra. Suspendisse consequat nisl eros, nec pellentesque sem interdum ac. Etiam hendrerit lectus nec venenatis dictum. Fusce lacinia libero erat, a euismod metus luctus ut. Integer maximus ornare lorem. Suspendisse potenti.\r<br>\r<br>Sed eu consectetur diam. Maecenas varius varius mauris, vehicula elementum velit tempus id. Curabitur quis nibh egestas, malesuada elit non, gravida risus. Vestibulum at orci orci. Aliquam erat volutpat. Sed semper, neque et bibendum pulvinar, mauris nibh euismod mauris, vitae tincidunt diam ex at ligula. Nullam vitae sapien neque. Cras vitae metus vitae ante sollicitudin tristique vitae quis purus.\r<br>\r<br>Suspendisse potenti. Fusce sed magna a orci varius pellentesque ac eget nisl. Praesent et felis in nisl porttitor aliquet vel vitae dui. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Ut ut sapien sapien. Quisque suscipit, magna id luctus laoreet, purus odio placerat sapien, ut sodales massa orci sit amet nisi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent pharetra erat vitae sapien semper, rhoncus fermentum leo laoreet. Cras nec diam eu leo aliquam rhoncus. Cras nec cursus odio. Etiam vitae eros non arcu vestibulum pharetra. Proin dapibus dapibus viverra. Nam ut bibendum orci. ', 'coco', 'XI TKJ', 'Kehidupan sehari-hari,', 1, 5555, 1, 2),
(11, 'Merah Muda itu Pink', 'Nam convallis sed nisl vel euismod. Fusce mattis enim sapien, sed consequat urna sagittis nec. In hac habitasse platea dictumst. Vivamus iaculis dui quis ligula egestas, non egestas justo egestas. Ut et justo lorem. Cras hendrerit ipsum eget enim congue, sed convallis lacus hendrerit. Duis imperdiet hendrerit mi, et hendrerit dui ultricies id. Fusce quis pharetra mauris. Quisque iaculis, turpis a hendrerit commodo, justo augue maximus leo, in molestie magna velit et odio. Quisque ornare facilisis nibh sit amet pretium.\r<br>\r<br>Ut lacinia maximus eros, quis semper velit vestibulum non. Curabitur posuere turpis ligula, dapibus consequat erat pharetra in. Nulla eget lectus sed tortor volutpat pellentesque. Ut quis sagittis lectus. Nam vestibulum pharetra lorem commodo fermentum. Nullam lobortis neque elit, ut fringilla magna fermentum a. Nullam at nulla vitae sem mattis iaculis.\r<br>\r<br>Phasellus vulputate eros nisi, quis interdum libero efficitur vitae. Phasellus venenatis mattis magna a lacinia. Ut sollicitudin metus et metus egestas, sit amet lacinia libero dictum. Aenean pellentesque eleifend velit, nec viverra velit faucibus vitae. In hac habitasse platea dictumst. Praesent et velit in ligula pharetra egestas. Mauris in ultrices massa. Integer cursus ex nec mauris vehicula, in facilisis magna venenatis.\r<br>\r<br>Nam nec risus at tortor faucibus ultricies ac a sem. Vestibulum sed vulputate augue. Sed convallis finibus feugiat. Integer pharetra, augue at varius consectetur, dolor metus semper augue, sed ullamcorper diam libero non odio. Fusce augue diam, cursus vitae molestie id, laoreet vitae mi. Morbi placerat lacus in commodo elementum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Pellentesque pharetra ipsum at risus congue rutrum. ', 'Suki', 'X AKL', 'Drama,', 1, 1004, 0, 1),
(12, 'Kunig dan Biru', 'Morbi tincidunt urna id lorem rhoncus, varius mollis augue aliquet. Donec commodo ex enim, id commodo felis aliquet quis. Aenean enim massa, elementum nec ullamcorper eu, volutpat a lectus. Sed ante ex, ultrices sit amet imperdiet a, laoreet eget dui. Praesent sit amet justo at lectus ornare venenatis nec non ligula. Ut et velit eros. Quisque elementum orci magna, eu gravida diam ornare vel. Pellentesque sed ultricies leo. Quisque ac suscipit felis. Praesent porttitor est ut odio porta maximus. Integer pretium quis nisl rutrum laoreet. Nullam fringilla et tortor eget elementum.\r<br>\r<br>Sed sit amet massa rutrum, feugiat erat vel, consectetur massa. Proin ante lectus, feugiat maximus dictum eu, euismod quis massa. Morbi at eros ut purus accumsan fermentum sed at sem. Nam ac ipsum nec ex vehicula congue. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam condimentum nec tellus in molestie. Mauris diam ex, dignissim sollicitudin mauris vel, porta dignissim sapien.\r<br>\r<br>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Fusce eget massa non orci imperdiet pharetra. Phasellus cursus efficitur elit. Maecenas eu tempor tellus. Cras blandit, diam auctor cursus tempus, lectus dolor dignissim elit, nec volutpat felis dui id erat. Nunc malesuada tellus at ligula congue feugiat. Sed lacinia, ligula vitae semper fermentum, sem dui convallis ipsum, ac feugiat metus enim vel mi. Duis rhoncus ac elit quis volutpat. Quisque eu pulvinar odio. Phasellus consequat ipsum velit, at egestas mauris pharetra nec. Pellentesque interdum vulputate tempor. Cras viverra mi non ex eleifend, non malesuada nunc sollicitudin.\r<br>\r<br>Sed rhoncus blandit felis, cursus rutrum urna condimentum sed. Aliquam semper porta pellentesque. Morbi non arcu sit amet orci semper elementum. Donec pellentesque, felis ut sollicitudin laoreet, risus lacus facilisis magna, id condimentum nisi dui et turpis. Phasellus gravida augue in ipsum laoreet, id efficitur lorem vehicula. In libero dui, iaculis ac ex elementum, semper accumsan eros. Nunc sed volutpat dui. Fusce augue tortor, sollicitudin in nulla sed, commodo consequat sem. Cras consequat velit ut nibh tincidunt posuere. Suspendisse commodo lorem nibh. Ut luctus aliquam ex vitae blandit. Nulla ac purus ac ipsum accumsan lobortis. Integer congue consectetur velit vel placerat. Sed lectus massa, rhoncus sed est sit amet, pretium suscipit enim. Phasellus auctor lectus justo, et faucibus ligula ornare sit amet.\r<br>\r<br>Morbi eget sodales erat, vitae fringilla eros. Suspendisse eu massa in mauris accumsan egestas. Nam vel nisi at elit venenatis tincidunt. Ut arcu nisi, pulvinar sit amet purus vitae, varius tempus dolor. Cras sodales dui lectus, vitae mollis purus varius non. Donec a sapien sit amet enim gravida commodo. Proin volutpat mi vitae nisi condimentum, sed aliquet arcu semper. Nam sagittis, nulla at condimentum egestas, nisi mauris iaculis elit, a tristique velit lacus sed nibh. Vestibulum pellentesque tincidunt mi vel suscipit. Nulla malesuada consectetur neque, a interdum metus dignissim id. Etiam bibendum vitae sem eget efficitur. Ut faucibus, ante at vestibulum ultrices, orci ante tempus urna, nec lacinia nulla urna in nibh. Ut vehicula tellus eu purus semper mattis. Duis lobortis quis arcu nec rutrum. Duis quis sagittis purus, vitae imperdiet dolor.\r<br>\r<br>Fusce erat elit, egestas in convallis eu, dignissim sit amet odio. Nullam ultrices purus leo, vel faucibus nisl mollis lobortis. Vestibulum venenatis leo nisi, at molestie ligula finibus vel. Vestibulum molestie rhoncus interdum. Vestibulum ac velit eu dui condimentum interdum. Fusce ornare nulla nisl, eu ultricies sapien ultricies sed. Sed suscipit augue in odio sagittis, et rhoncus quam lacinia. In in interdum ligula, eu placerat leo. Morbi at nulla ac lectus efficitur semper. Nam malesuada vitae elit viverra iaculis. Nulla facilisi. Cras imperdiet in tortor vitae ultricies. Donec tempus dictum convallis. Morbi a iaculis ante. Phasellus condimentum nisl ex, et consectetur mauris suscipit sed. Vestibulum interdum nisl vitae tellus maximus bibendum quis a enim.\r<br>\r<br>Morbi sagittis ut ipsum eget sagittis. Vestibulum dignissim eleifend ornare. Fusce euismod aliquet turpis. Cras feugiat leo nec elit iaculis, non dictum arcu ultrices. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse ut lobortis urna. Aliquam interdum ligula quis posuere consequat. Maecenas consectetur venenatis eleifend. Suspendisse interdum euismod volutpat. Sed leo augue, hendrerit vitae finibus at, finibus et metus. Nunc pharetra felis quis sagittis pretium. Pellentesque elementum mollis augue, sed faucibus neque sodales eget. Mauris suscipit nec diam eget laoreet. Vestibulum scelerisque magna at massa consequat fringilla. Vestibulum nec sem nisi.\r<br>\r<br>Aliquam sit amet lacus ultricies, dapibus lacus sit amet, malesuada sapien. Sed egestas placerat nisi ac ultricies. Morbi quis rutrum sem. Donec non libero sed neque tincidunt tincidunt. Sed fermentum nibh vel nisl vehicula tincidunt. Donec vel ornare mi, id sodales metus. Praesent in ligula dictum turpis ultricies pharetra ut sit amet justo. Quisque tincidunt libero tortor, vel dictum ipsum lobortis in. Duis sollicitudin est in lorem tristique, semper placerat neque dictum. Donec maximus molestie iaculis. Cras in iaculis sem, sed lobortis nisi. Morbi id malesuada quam. Pellentesque laoreet metus porta felis dignissim, ut bibendum nisl pretium. Nam eu posuere purus.\r<br>\r<br>In vitae mollis ipsum. Sed a orci a augue feugiat hendrerit. Curabitur in eleifend libero. Sed sollicitudin lorem in lectus aliquam, id rutrum lectus suscipit. Sed efficitur est sit amet elit convallis vehicula. Nulla tempus placerat mauris, at ornare arcu finibus venenatis. Cras augue tortor, tristique iaculis dignissim tincidunt, vehicula non enim. Sed id commodo risus. Integer auctor iaculis diam, at placerat velit egestas at. Morbi lacinia malesuada neque ut mollis. Vestibulum gravida erat eget risus porta, at porta urna efficitur. In porttitor, tortor vel dictum commodo, arcu dui dapibus sem, non cursus enim ipsum in justo. Quisque malesuada condimentum libero tincidunt condimentum. Donec leo mi, suscipit non accumsan non, facilisis ac enim. Donec maximus at arcu at posuere. Proin posuere tortor justo, eu mollis odio feugiat et.\r<br>\r<br>Ut varius, lectus eu imperdiet cursus, magna ligula consectetur enim, at aliquet metus massa eu diam. Sed ac felis consectetur, aliquet neque sed, accumsan nulla. Nam id venenatis nibh, at posuere velit. Duis efficitur arcu ligula, eget commodo arcu ultricies quis. Phasellus et sollicitudin sapien, a viverra odio. Vivamus lorem felis, condimentum non augue porttitor, dapibus pulvinar mauris. Proin lobortis dignissim urna, a pretium magna. Suspendisse potenti. Curabitur diam ligula, tincidunt eget ante eget, rhoncus scelerisque sem. Morbi orci risus, tristique ornare lacus et, eleifend consectetur ipsum. Fusce vehicula dignissim gravida. Nulla sodales libero orci, ac mattis quam gravida quis. Nulla et velit suscipit mi luctus pharetra. Sed nec est a ante tristique pulvinar eu sit amet nunc. Nunc vitae justo at velit sodales laoreet dictum ut erat. ', 'Suki', 'X AKL', 'Misteri,', 1, 1004, 1, 2),
(13, 'Biru Muda dan Tua', 'In lacus purus, semper id est vel, tristique viverra massa. Donec a leo vel turpis volutpat elementum. Maecenas tincidunt, est ut faucibus molestie, mi ipsum vulputate ipsum, ac tristique erat elit sed nisl. Mauris arcu diam, pellentesque nec rutrum efficitur, blandit id enim. Donec mollis nec quam in laoreet. Praesent quis rutrum tortor. Interdum et malesuada fames ac ante ipsum primis in faucibus.\r<br>\r<br>Nullam tortor sapien, iaculis elementum dui id, ornare iaculis enim. Sed cursus ligula quis tortor dapibus ornare eget nec velit. Maecenas ex ex, dapibus id metus bibendum, vehicula rutrum orci. Sed et ex congue, pulvinar lacus in, fringilla urna. Nam at dapibus metus. Nulla aliquet massa erat. Suspendisse viverra ullamcorper dignissim. Nulla ac fermentum sem. Curabitur at libero ut tellus accumsan vestibulum. Quisque nec dapibus purus.\r<br>\r<br>Nulla id augue feugiat, bibendum purus sit amet, molestie eros. Suspendisse porttitor consequat nunc non fermentum. Quisque convallis justo at dui fermentum, nec imperdiet dui tincidunt. Praesent eu nisi vel nibh sollicitudin aliquet. Sed maximus purus non consequat ornare. In nibh quam, pellentesque in malesuada sit amet, pretium eget ante. Fusce ultrices odio a magna placerat, in ornare justo aliquet. Phasellus non diam leo. Donec eleifend leo vitae cursus commodo. In tincidunt tellus ligula, at consequat diam dapibus eu. Phasellus magna nibh, placerat non ante vitae, imperdiet auctor quam. Ut vehicula ullamcorper ante, et facilisis ligula aliquet ut.\r<br>\r<br>Morbi auctor nisl in venenatis cursus. Nam ex justo, vulputate nec nulla vel, consectetur egestas quam. Nullam scelerisque dictum risus, ut viverra sapien finibus vitae. Suspendisse sollicitudin diam non ipsum suscipit vehicula. Nullam posuere odio tellus, eu finibus eros sollicitudin sed. Cras nec placerat elit, non pellentesque lectus. Sed vitae nunc vel ante aliquet dictum.\r<br>\r<br>Maecenas in est sem. Quisque ullamcorper ante vitae enim imperdiet egestas. Cras sit amet tellus ac dui congue iaculis. Pellentesque scelerisque dui id velit suscipit dapibus. Donec in libero a arcu lacinia pharetra vitae ac tortor. Curabitur ornare purus id eleifend pharetra. Nulla eleifend, neque sit amet pulvinar lacinia, urna felis tempus libero, quis tempus erat lectus at eros. Donec varius, tellus quis luctus maximus, risus mi accumsan lectus, ut congue velit ipsum in ante. Sed augue ipsum, dapibus nec nibh ac, posuere dignissim mi.\r<br>\r<br>Maecenas tincidunt scelerisque lectus, non aliquam velit fringilla quis. Nulla fermentum tempor ex quis euismod. Donec ac orci et ligula semper aliquam. Donec augue sapien, viverra vel lobortis sed, efficitur at erat. Mauris congue tellus magna. Quisque sodales sodales condimentum. Aenean luctus, turpis nec blandit luctus, urna sem fringilla neque, in elementum risus tortor in nulla. Donec tempor nisi tempus turpis dignissim ultrices. Praesent posuere ipsum sit amet neque mollis euismod.\r<br>\r<br>Sed posuere rutrum erat eget iaculis. Duis iaculis eros sem, sed lacinia elit lobortis quis. Mauris a magna feugiat, finibus arcu ut, vulputate ligula. Donec commodo, elit nec aliquet placerat, felis dolor dictum elit, sit amet volutpat nibh libero quis orci. Donec luctus eleifend lobortis. Nam sed dapibus ligula. Nulla eu aliquam velit, eu aliquam est. ', 'Suki', 'X AKL', 'Drama,', 1, 1004, 0, 1),
(14, 'Arab Tengah', 'Cras eu viverra metus, et molestie purus. Aenean maximus pharetra ex, vel ultrices nunc venenatis et. Pellentesque commodo dapibus dolor sed efficitur. Fusce ac felis sit amet diam ultrices convallis vitae aliquam ex. Integer pulvinar, nunc eu varius auctor, magna mi tempus tortor, ac eleifend metus enim et ante. Integer mollis quis purus quis efficitur. Nam pellentesque est et leo consequat viverra. Sed mollis finibus est. Duis malesuada, leo nec tempus suscipit, leo tellus mollis velit, eu condimentum metus metus nec nunc. Sed in imperdiet ligula. Nam maximus tincidunt nisi, id commodo risus tincidunt ultrices. Pellentesque dignissim euismod ante ut hendrerit. Vivamus lacinia eros lacus, ac sagittis lacus ornare a. Curabitur sed urna faucibus, ultricies est a, semper magna.\r<br>\r<br>Ut tempus massa sapien, vitae euismod turpis varius non. Donec ut quam id leo pellentesque dapibus. Morbi varius, felis in convallis rhoncus, erat nisi efficitur dolor, id varius dui tellus a velit. Fusce eget libero a ligula fringilla fringilla sed vel sapien. Nam fringilla nisi vel diam aliquam, in ullamcorper felis facilisis. Sed viverra odio nec augue efficitur rutrum. Suspendisse auctor, augue sollicitudin malesuada hendrerit, urna tortor elementum massa, sed malesuada sem ligula eu nisi. Nulla tristique pretium mi, sed viverra lorem varius in. Praesent hendrerit mollis mattis. Quisque ac accumsan nisi. Ut accumsan, tortor non congue hendrerit, mi ex pulvinar tortor, nec suscipit diam metus id metus. Nunc ut nisl lacus. Fusce ex quam, elementum non felis sit amet, tincidunt elementum orci. Cras sem sapien, tempus vel viverra id, aliquam vitae ex. Donec vehicula mollis fringilla. Maecenas consequat ut eros interdum hendrerit.\r<br>\r<br>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse dignissim ut massa eu rhoncus. Praesent fermentum ornare sem, id accumsan velit facilisis eget. Integer justo quam, varius at dignissim at, facilisis ac diam. Aenean elit nisi, ornare ac felis in, ullamcorper congue sem. Ut lobortis iaculis rutrum. Praesent sagittis in nibh quis ornare. Etiam id sem egestas, vehicula nisl vitae, finibus justo. Cras mi metus, tristique aliquet erat sed, rhoncus gravida est. Integer id ante accumsan, imperdiet sem vitae, molestie nibh.\r<br>\r<br>Vestibulum facilisis mattis ullamcorper. Ut porta, enim sit amet semper dignissim, enim mauris vestibulum purus, nec pellentesque mauris lorem id orci. Ut et lobortis neque, a ultricies leo. In ullamcorper molestie ipsum id rutrum. Mauris venenatis, mi ac tincidunt aliquam, metus nunc aliquam tellus, sed commodo urna quam et diam. Vestibulum ultricies quis nibh a tempor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.\r<br>\r<br>Aenean ullamcorper, orci eu pellentesque ullamcorper, eros arcu cursus libero, a venenatis diam metus eget nunc. Nullam accumsan in nunc mollis egestas. Quisque interdum metus ut nulla auctor pretium sit amet et massa. Nullam turpis metus, suscipit sit amet euismod nec, sagittis in urna. Donec est est, maximus in libero nec, imperdiet pulvinar est. Nunc in turpis erat. Donec porta urna vel purus hendrerit, et hendrerit arcu hendrerit. ', 'batman', 'XI RPL', 'Cerita Sebelum Tidur', 1, 444, 0, 1),
(16, 'Amerika dan Lainnya', '\n<br>\n<br>Morbi commodo felis vitae orci sagittis aliquet. Quisque eget dui vel enim ullamcorper auctor. Cras aliquet sed metus vel efficitur. Donec faucibus lacus vel mauris rutrum pharetra. Sed maximus eros a ultrices bibendum. Proin consectetur tristique magna, vitae varius mi iaculis in. Etiam sit amet ligula neque. In vehicula viverra mi vel dictum. Maecenas pretium eros at tortor varius convallis. Nam maximus leo id urna tempus facilisis. Etiam nec erat eu nibh cursus ornare eget vitae nisi.\n<br>\n<br>Duis sodales, lorem et semper vulputate, eros diam blandit nunc, non suscipit erat est eu ligula. Curabitur lobortis molestie ex. Mauris vel elit nec lectus dictum sagittis. Aliquam sed elit nulla. Aenean tellus mi, sollicitudin fringilla metus molestie, gravida aliquam odio. Duis massa sem, posuere in semper vel, volutpat eu est. Duis sed mauris euismod ex congue iaculis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis pulvinar sed sapien et consequat. Vestibulum consectetur imperdiet egestas.\n<br>\n<br>Donec euismod dui eget vehicula vulputate. Nunc pharetra tellus at eros mattis bibendum. Integer at augue tincidunt tellus placerat tempor at a quam. Donec rhoncus ultricies ex et accumsan. Maecenas egestas pretium dapibus. Nunc molestie ex sem, ut euismod mi egestas eu. Sed tincidunt ac turpis a rhoncus. Integer sit amet velit non enim ultricies sodales eget dapibus turpis. In suscipit lorem in elit vulputate eleifend. Donec non dui quis orci pretium dapibus et at nisl. In suscipit lectus enim, eu commodo libero consequat a. Aenean volutpat imperdiet ullamcorper. Pellentesque maximus purus vitae neque consectetur ultrices. Praesent sagittis leo ex, eget ultricies magna congue vel.\n<br>\n<br>Proin sed urna in nunc dignissim malesuada. Etiam mollis eleifend odio, a condimentum sapien laoreet eu. Morbi at efficitur eros. Vestibulum ac neque luctus, auctor tortor a, aliquam nibh. Vestibulum vel odio scelerisque, bibendum sapien eget, accumsan sem. In ultricies cursus tristique. Ut eu lacinia dui. Donec nec nulla pulvinar, pellentesque dolor eget, aliquam risus. In hac habitasse platea dictumst. Etiam quis velit vel risus suscipit sollicitudin placerat bibendum massa. Morbi volutpat scelerisque molestie. Mauris vitae scelerisque erat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent semper dui eget odio facilisis accumsan non ac ligula. Proin faucibus metus at orci finibus accumsan ut vel tellus. Nulla suscipit tortor pulvinar euismod varius.\n<br>\n<br>Nam pulvinar in purus eget porta. Morbi consequat, turpis eu maximus ultricies, dui urna luctus lorem, in luctus arcu nunc quis neque. Fusce iaculis in est sed ullamcorper. Maecenas semper, diam sit amet maximus molestie, felis velit faucibus ipsum, a scelerisque magna est sed odio. Duis leo quam, accumsan sit amet metus a, semper laoreet turpis. In sollicitudin orci ut justo blandit, eget mollis ante porttitor. Suspendisse non rutrum ligula. Proin cursus, eros in euismod pharetra, nulla sem euismod dui, eu lobortis lacus sem ac nunc. Aenean ultrices viverra elit, sit amet convallis sem maximus ac. Duis consequat arcu quis finibus mollis. Donec lacinia imperdiet quam, eget aliquet quam pretium in. Sed viverra efficitur quam ut vestibulum. Vivamus aliquam odio eget justo aliquam, eget elementum odio elementum. Etiam dapibus ultrices purus eu varius. Pellentesque fringilla purus quis semper ultricies. ', 'batman', 'XI RPL', 'Drama, Fantasi', 0, 444, 0, 0),
(17, 'Jawa Tengah de Bomm', '<b>Lorem</b> ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vitae quam nunc. <i>Ini miring</i>Curabitur euismod maximus tortor, egestas facilisis justo vestibulum vel. Donec blandit dapibus nisi, sit amet tempus tellus laoreet non. Proin quis tempus libero, eu ornare massa. Aenean scelerisque sem sit amet metus luctus, non laoreet arcu vulputate. Morbi fringilla quam pretium velit efficitur, scelerisque egestas urna cursus. \r<br>\r<br>Curabitur faucibus eros magna, non bibendum nisi aliquet id. Sed gravida felis non aliquam scelerisque. Duis in tellus vitae diam placerat porttitor vel at dolor. Phasellus ultrices ut leo sed eleifend. Vivamus fermentum arcu non tincidunt sodales. Proin mi nunc, tempus sed dui vitae, auctor pellentesque mi. Mauris vitae blandit tortor. Suspendisse gravida leo eget tortor convallis, vel elementum libero cursus. Nulla facilisis nisi ac nulla dignissim elementum.\r<br>\r<br>Nulla ut orci arcu. Nunc turpis mi, tempus ac bibendum sed, pulvinar vitae tortor. Nunc eget tincidunt nibh. Sed facilisis tellus eget odio sagittis iaculis. In hac habitasse platea dictumst. Vivamus sit amet quam elementum, facilisis sapien sed, vulputate urna. Fusce lorem turpis, dapibus vitae nisi dapibus, tempus ultrices erat. Mauris sit amet iaculis turpis. Cras eleifend ligula id ultricies malesuada. Cras sollicitudin volutpat lacus at efficitur. Proin sit amet ex pharetra, maximus enim eu, eleifend nibh. Etiam non venenatis metus, at sagittis mi. Donec vitae lectus quis mauris tincidunt posuere.\r<br>\r<br>Cras blandit gravida gravida. Mauris convallis, magna eget commodo efficitur, erat sem lobortis felis, in ultricies justo enim eget tellus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque eget suscipit eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam odio odio, elementum ut nunc ullamcorper, sagittis facilisis ipsum. Donec semper volutpat nunc sed accumsan.\r<br>\r<br>Aenean risus tortor, euismod non placerat ac, suscipit tempor massa. Pellentesque eleifend nibh tellus, in porta ante rhoncus rhoncus. Quisque libero nisl, ullamcorper sed pharetra ut, interdum et velit. Integer facilisis posuere eros vel tincidunt. Quisque ac tempus diam, quis finibus quam. Ut eget tincidunt mauris, vel malesuada nisl. Etiam nec eleifend ipsum, ut pellentesque lorem. Etiam non nisi luctus, porttitor sapien at, molestie purus.\r<br>\r<br>Donec at imperdiet risus. Nunc consectetur condimentum nulla sit amet tincidunt. In et sollicitudin purus, sed dictum nisi. Suspendisse finibus nec neque sed varius. Duis aliquet sem massa, eleifend laoreet lorem imperdiet ut. Cras consectetur elementum lacus vitae pulvinar. Aliquam enim neque, iaculis in sapien vitae, placerat feugiat ante. ', 'Songo Songo Songo', 'X AKL', 'Misteri, Sedih', 1, 9999, 1, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `diskusi`
--

CREATE TABLE `diskusi` (
  `id` int(9) NOT NULL,
  `id_pengirim` int(9) NOT NULL,
  `admin_kah` tinyint(1) NOT NULL DEFAULT 0,
  `pesan` text NOT NULL,
  `dikirim` datetime NOT NULL,
  `nama_pengirim` varchar(255) NOT NULL,
  `membalas` varchar(255) DEFAULT NULL,
  `hapus` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `diskusi`
--

INSERT INTO `diskusi` (`id`, `id_pengirim`, `admin_kah`, `pesan`, `dikirim`, `nama_pengirim`, `membalas`, `hapus`) VALUES
(40, 2, 1, 'Farhan Hore', '2021-10-12 08:14:45', 'Farhan Hore', 'n', '2021-10-16 12:14:45'),
(42, 2, 1, 'Pustakawan', '2021-10-12 08:18:19', 'Pustakawan', 'n', '9999-12-12 00:00:00'),
(43, 2, 1, 'Pustakawan', '2021-10-12 08:40:00', 'Pustakawan', 'n', '2021-10-21 00:00:00'),
(45, 2, 1, 'hola bang&manusia', '2021-10-12 09:02:33', 'Pustakawan', 'n', '9999-12-12 00:00:00'),
(51, 2, 1, 'hello world', '2021-10-12 11:22:50', 'Pustakawan', 'n', '9999-12-12 00:00:00'),
(53, 2, 1, 'iya apa bang', '2021-10-12 11:25:17', 'Pustakawan', 'n', '9999-12-12 00:00:00'),
(55, 2, 1, 'oing oing bang', '2021-10-12 11:26:17', 'Pustakawan', 'p53', '2021-10-13 11:26:17'),
(56, 26, 0, 'siap pak pustakawan', '2021-10-12 15:44:19', 'Asik', 'p53', '2021-10-13 15:44:19'),
(57, 26, 0, 'aku seorang kapiten mempunyai pedang panjang kalo berjalan prok prok prokprok. aku seorang kapiten. lah cuma hapal segitu doang mbah. lanjutannya apa ya bang. lah kok urung mandek mandek ki. wis piro karakter ini', '2021-10-12 15:52:59', 'Asik', 'n', '2021-10-13 15:52:59'),
(58, 26, 0, 'My name is Mary Katherine Blackwood. I am eighteen years old, and I live with my sister Constance. I have often thought that with any luck at all I could have been born a werewolf, because the two middle fingers on both my hands are the same length, but I', '2021-10-12 15:56:44', 'Asik', 'n', '2021-10-13 15:56:44'),
(59, 26, 0, 'panjang ya bang', '2021-10-12 15:57:29', 'Asik', 'p58', '2021-10-13 15:57:29'),
(60, 26, 0, '.kakpustaka', '2021-10-12 16:02:31', 'Asik', 'n', '2021-10-13 16:02:31'),
(61, 26, 0, 'siap pak', '2021-10-12 16:06:38', 'Asik', 'p42', '2021-10-13 16:06:38'),
(62, 26, 0, '.kakpustaka gimana cara meledakkan bumi sih', '2021-10-12 16:07:11', 'Asik', 'n', '2021-10-13 16:07:11'),
(63, 2, 1, 'mudah nak', '2021-10-12 16:14:34', 'Pustakawan', 'p62', '2021-10-13 16:14:34'),
(64, 2, 1, '.kakpustaka lain mungkin bisa membantu', '2021-10-12 16:15:23', 'Pustakawan', 'p63', '2021-10-13 16:15:23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `katalog`
--

CREATE TABLE `katalog` (
  `id_katalog` int(11) NOT NULL,
  `kode_katalog` varchar(100) NOT NULL,
  `id_buku` int(11) NOT NULL,
  `kondisi` varchar(10) NOT NULL,
  `status` varchar(11) NOT NULL DEFAULT 'ada',
  `deskripsi` text DEFAULT NULL,
  `arsip` tinyint(1) NOT NULL DEFAULT 0,
  `tgl_buat_katalog` date NOT NULL,
  `tgl_edit_katalog` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `katalog`
--

INSERT INTO `katalog` (`id_katalog`, `kode_katalog`, `id_buku`, `kondisi`, `status`, `deskripsi`, `arsip`, `tgl_buat_katalog`, `tgl_edit_katalog`) VALUES
(17, 'ayam.001', 11, 'Baru', 'ada', '', 0, '2021-08-11', '2021-08-11'),
(18, 'ayam.002', 11, 'Baru', 'dipinjam', 'Ini adalah deskripsi katalog ayam.002', 0, '2021-08-19', '2021-08-19'),
(19, 'ayam.003', 11, 'Baru', 'ada', '', 0, '2021-08-11', '2021-08-11'),
(20, 'bunglon.002', 10, 'Baik', 'ada', '', 0, '2021-08-11', '2021-08-11'),
(21, 'bunglon.003', 10, 'Baik', 'ada', '', 0, '2021-08-11', '2021-08-11'),
(22, 'cacing.1', 12, 'Baru', 'dipinjam', '', 0, '2021-08-11', '2021-08-11'),
(27, 'bunglon.001', 10, 'Baik', 'dipinjam', '', 0, '2021-08-19', '2021-08-19'),
(28, 'dino.001', 15, 'Baik', 'dipinjam', '', 0, '2021-08-19', '2021-08-19'),
(29, 'dino.003', 15, 'Baik', 'ada', '', 0, '2021-08-19', '2021-08-19'),
(30, 'cacing.2', 12, 'Rusak', 'ada', '', 1, '2021-08-21', '2021-08-21'),
(31, 'ayam.004', 11, 'Baik', 'dipinjam', '', 0, '2021-08-24', '2021-08-24'),
(32, 'ayam.0051', 11, 'Baik', 'ada', '', 0, '2021-08-24', '2021-09-22'),
(33, 'ayam.006', 11, 'Baik', 'dipinjam', '', 0, '2021-08-24', '2021-08-24'),
(34, 'ayam.007', 11, 'Baik', 'dipinjam', '', 0, '2021-08-24', '2021-08-24'),
(35, 'ayam.008', 11, 'Baik', 'ada', '', 0, '2021-08-24', '2021-08-24'),
(36, 'ayam.009', 11, 'Baik', 'ada', '', 0, '2021-08-24', '2021-08-24'),
(37, 'ayam.010', 11, 'Baik', 'ada', '', 0, '2021-08-24', '2021-08-24'),
(38, 'ayam.011', 11, 'Baik', 'ada', '', 0, '2021-08-24', '2021-08-24'),
(39, 'ayam.012', 11, 'Baik', 'ada', '', 0, '2021-08-24', '2021-08-24'),
(40, 'ayam.013', 11, 'Baik', 'ada', '', 0, '2021-08-24', '2021-08-24'),
(41, 'ayam.014', 11, 'Baik', 'ada', '', 0, '2021-08-24', '2021-08-24'),
(42, 'ayam.015', 11, 'Baik', 'ada', '', 0, '2021-08-24', '2021-08-24'),
(43, 'ayam.016', 11, 'Baik', 'ada', '', 0, '2021-08-24', '2021-08-24'),
(44, 'ayam.017', 11, 'Baik', 'ada', '', 0, '2021-08-24', '2021-08-24'),
(45, 'ayam.018', 11, 'Baik', 'ada', '', 0, '2021-08-24', '2021-08-24'),
(46, 'ayam.019', 11, 'Baik', 'ada', '', 0, '2021-08-24', '2021-08-24'),
(47, 'ayam.020', 11, 'Baik', 'ada', '', 0, '2021-08-24', '2021-08-24'),
(48, 'ayam.021', 11, 'Baik', 'ada', '', 0, '2021-08-24', '2021-08-24'),
(49, 'ayam.022', 11, 'Baik', 'ada', '', 0, '2021-08-24', '2021-08-24'),
(50, 'ayam.023', 11, 'Baik', 'ada', '', 0, '2021-08-24', '2021-08-24'),
(51, 'ayam.024', 11, 'Baik', 'ada', '', 0, '2021-08-24', '2021-08-24'),
(52, 'ayam.025', 11, 'Baik', 'ada', '', 0, '2021-08-24', '2021-08-24'),
(53, 'ayam.026', 11, 'Baik', 'ada', '', 0, '2021-08-24', '2021-08-24'),
(54, 'dino.002', 15, 'Baik', 'ada', '', 0, '2021-08-24', '2021-08-24'),
(55, 'dino.004', 15, 'Baik', 'ada', '', 0, '2021-08-24', '2021-08-24'),
(56, 'dino.005', 15, 'Baik', 'ada', '', 0, '2021-08-24', '2021-08-24'),
(57, 'dino.006', 15, 'Baik', 'ada', '', 0, '2021-08-24', '2021-08-24'),
(59, 'elang.001', 17, 'Baru', 'ada', 'Hola bang', 0, '2021-08-27', '2021-09-13'),
(60, 'fanda.001', 16, 'Baru', 'ada', '', 0, '2021-08-27', '2021-09-22'),
(61, 'gajah.001', 18, 'Baik', 'ada', '', 0, '2021-08-27', '2021-08-27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `deskripsi` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id`, `nama`, `deskripsi`) VALUES
(1, 'Komedi', NULL),
(2, 'Novel', NULL),
(3, 'Drama', NULL),
(4, 'Misteri', NULL),
(5, 'Fantasi', 'Fantasy'),
(6, 'Romance', NULL),
(7, 'Horror', NULL),
(8, 'Biografi', NULL),
(9, 'Fiksi Ilmiah', NULL),
(10, 'Kriminal', NULL),
(11, 'Biologi', NULL),
(12, 'Ho', ''),
(13, 'Hora umum', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE `kelas` (
  `id` int(5) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `kepanjangan` varchar(100) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `jml_siswa` int(11) DEFAULT 0,
  `tgl_buat` date NOT NULL,
  `tgl_edit` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelas`
--

INSERT INTO `kelas` (`id`, `nama`, `kepanjangan`, `deskripsi`, `jml_siswa`, `tgl_buat`, `tgl_edit`) VALUES
(1, 'XI RPL', 'XI REKAYASA PERANGKAT LUNAK', '', 9, '2021-07-21', '2021-09-14'),
(3, 'XI TKJ', 'XI TEKNIK KOMPUTER DAN JARINGAN', NULL, 3, '2021-07-21', '2021-07-21'),
(7, 'X RPL', NULL, '', 1, '0000-00-00', '0000-00-00'),
(18, 'X TKJ', NULL, '', 0, '0000-00-00', '0000-00-00'),
(19, 'X AKL', '10 Akuntansi Keuangan dan Lembaga', 'hehe', 4, '0000-00-00', '2021-08-15'),
(20, 'X BDP', NULL, '', 2, '0000-00-00', '0000-00-00'),
(21, 'XI AKL', NULL, '', 4, '0000-00-00', '0000-00-00'),
(22, 'XII AKL I', NULL, '', 1, '0000-00-00', '0000-00-00'),
(32, 'XII AKL', '12 Akuntansi Keuangan dan Lembaga', 'hoho', 0, '0000-00-00', '2021-08-15'),
(33, 'XI BDP', '11 Bisnis Daring dan Pemasaran', 'hola', 0, '2021-08-15', '2021-08-15'),
(36, 'X HEHE', NULL, 'hehe', 0, '0000-00-00', '0000-00-00'),
(37, 'X Haha', NULL, '', 0, '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `lemari`
--

CREATE TABLE `lemari` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `deskripsi` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `lemari`
--

INSERT INTO `lemari` (`id`, `nama`, `deskripsi`) VALUES
(2, 'A - 2', ''),
(3, 'A - 1', 'ha'),
(4, 'A - 3', ''),
(5, 'B - 1', ''),
(6, 'B - 2', ''),
(7, 'B - 3', ''),
(8, 'B - 4', ''),
(9, 'C - 2', ''),
(10, 'D - 3', ''),
(11, 'D - 2', ''),
(12, 'C - 1', ''),
(13, 'D - 1', ''),
(14, 'G - 8', ''),
(15, 'A - 9', ''),
(16, 'B - 7', ''),
(17, 'A - 4', ''),
(18, 'B - 6', ''),
(19, 'B - 7', ''),
(20, 'Y - 1', ''),
(21, 'A - 5', NULL),
(22, 'G - 5', 'Hi'),
(24, 'hfkdjf', 'jaf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penerbit`
--

CREATE TABLE `penerbit` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `deskripsi` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penerbit`
--

INSERT INTO `penerbit` (`id`, `nama`, `deskripsi`) VALUES
(1, 'PT Hoho', NULL),
(2, 'PT Kigo Hi', NULL),
(3, 'Pustaka N', NULL),
(4, 'Lorem Ipsum', NULL),
(5, 'Kolo', NULL),
(6, 'Gilbur', NULL),
(7, 'Boum', NULL),
(8, 'Artie', NULL),
(9, 'Hp', NULL),
(10, 'Asus', NULL),
(11, 'PT hiya', NULL),
(12, 'Alwas Ginm', 'Hehe'),
(13, 'JKjd Hury', NULL),
(14, 'Falk', NULL),
(15, 'CV Kjdjf', NULL),
(16, 'PT Yahoo', ''),
(17, 'Toyo', ''),
(18, 'Tes', ''),
(19, 'PT Gundam', ''),
(20, 'PT. Ajaro', ''),
(21, 'CV Wayne', ''),
(23, 'PT Katara', NULL),
(24, 'PT Sokka', NULL),
(25, 'PT Windos', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengaturan`
--

CREATE TABLE `pengaturan` (
  `id` int(11) NOT NULL,
  `nama_perpus` varchar(255) NOT NULL,
  `moto` varchar(255) NOT NULL,
  `tgl_buat` date NOT NULL,
  `jmlPengunjung` bigint(20) NOT NULL DEFAULT 0,
  `logo` varchar(200) NOT NULL DEFAULT 'logo.png',
  `deskripsi` text DEFAULT NULL,
  `denda` bigint(15) NOT NULL DEFAULT 500,
  `lama_peminjaman` int(10) NOT NULL DEFAULT 3,
  `alamat` varchar(255) DEFAULT 'Jl. kha Ahmad Dahlan NO. 14 PURWOREJO',
  `jabatan_penanda_tangan` varchar(255) DEFAULT 'Kepala Perpustakaan',
  `kota` varchar(255) NOT NULL DEFAULT 'Purworejo',
  `nama_penanda_tangan` varchar(255) NOT NULL DEFAULT 'Anonimus, Spd',
  `nip_penanda_tangan` varchar(255) NOT NULL DEFAULT '85827588838',
  `lama_pesan_diskusi` int(9) NOT NULL DEFAULT 24,
  `max_pesan_diskusi` int(9) NOT NULL DEFAULT 255,
  `panggil_pustakawan` varchar(255) NOT NULL DEFAULT '.kakpustaka'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengaturan`
--

INSERT INTO `pengaturan` (`id`, `nama_perpus`, `moto`, `tgl_buat`, `jmlPengunjung`, `logo`, `deskripsi`, `denda`, `lama_peminjaman`, `alamat`, `jabatan_penanda_tangan`, `kota`, `nama_penanda_tangan`, `nip_penanda_tangan`, `lama_pesan_diskusi`, `max_pesan_diskusi`, `panggil_pustakawan`) VALUES
(2, 'SMK Tes Dicoba', 'Membaca adalah kegiatan.', '2021-07-21', 72, 'SMK_BATIK_LOGO.png', 'Mollit labore nulla ut labore velit ad occaecat velit ullamco deserunt exercitation deserunt ea adipisicing ut ullamco velit ad est irure nulla in eiusmod reprehenderit quis.', 400, 3, 'Jl. kha Ahmad Dahlan NO. 14 PURWOREJO1', 'Kepala Perpustakaan1', 'Purworejo1', 'Anonimus, Spd1', '858275888381', 24, 255, '.oi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penulis`
--

CREATE TABLE `penulis` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `deskripsi` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penulis`
--

INSERT INTO `penulis` (`id`, `nama`, `deskripsi`) VALUES
(1, 'Kaguya', NULL),
(2, 'Naruto Uzumaki', NULL),
(3, 'Sasuke Uchiha', NULL),
(4, 'Hagaromo Otsutsuki', NULL),
(5, 'Hashirama Senju', NULL),
(6, 'Madara Uchiha', NULL),
(7, 'Nagato', NULL),
(8, 'Indra Otsutsuki', NULL),
(9, 'Obito Uchiha', NULL),
(10, 'Itachi Uchiha', NULL),
(11, 'Hiruzen Sarutobi', NULL),
(12, 'Orochimaru', NULL),
(13, 'Mighy Guy', NULL),
(14, 'Jiraiya', NULL),
(15, 'Killer Bee', NULL),
(16, 'Lady Tsunade', NULL),
(17, 'Kakashi', NULL),
(18, 'Gaara', NULL),
(19, 'Minato Kamikaze', NULL),
(20, 'Sakura Haruno', NULL),
(21, 'Mei Terumi', NULL),
(22, 'Yamato', NULL),
(23, 'Shikamaru Nara', NULL),
(24, 'Kabuto Yakushi', NULL),
(25, 'Tzuyu', NULL),
(26, 'Momo', NULL),
(27, 'Sana', NULL),
(28, 'Mina', NULL),
(29, 'Jihyo', NULL),
(30, 'Nayeon', NULL),
(31, 'Jeongneon', NULL),
(32, 'Chaeyoung', NULL),
(33, 'Dahyun', NULL),
(34, 'Tony Stark', NULL),
(35, 'Thor Odinson', NULL),
(36, 'Janet Van Dyne', NULL),
(37, 'Dr. Bruce Banner', NULL),
(38, 'Steve Rogers', NULL),
(39, 'Clinton Barton', NULL),
(40, 'Pietro Maximoff', NULL),
(41, 'Wanda Maximoff', NULL),
(43, 'Natasha Rumanoff', NULL),
(44, 'Carol Danvers', NULL),
(45, 'Samuel Wilson', NULL),
(46, 'Peter Parker', NULL),
(47, 'Scott Lang', NULL),
(48, 'Bucky Burnes', NULL),
(49, 'Hermione Granger', NULL),
(50, 'Harry Potter', NULL),
(51, 'Lord Voldemort', NULL),
(52, 'Draco Malfoy', NULL),
(53, 'Ron Wesley', NULL),
(54, 'Professor Albus Dumbledor', NULL),
(55, 'Professor Severus Snape', NULL),
(56, 'Rubeus Hagrid', NULL),
(57, 'Luna Lovegood', NULL),
(58, 'Dobby', NULL),
(59, 'Professor Minerva McGonagall', NULL),
(60, 'Neville Longbottom', NULL),
(61, 'Bellatrix Lestrange', NULL),
(62, 'Sirius Black', NULL),
(63, 'Fred Weasley', NULL),
(64, 'Remus Lupin', NULL),
(65, 'Astoria Greengrass', NULL),
(66, 'Gellert Gindelwald', NULL),
(67, 'Narcissa Malfoy', NULL),
(68, 'Dolores Umbridge', NULL),
(69, 'Pansy Parkinson', NULL),
(70, 'Lucius Malfoy', NULL),
(71, 'Nymphadora Tonks', NULL),
(72, 'Ginny Weasley', NULL),
(73, 'Newt Scamander', NULL),
(74, 'Lily Potter', NULL),
(75, 'Fleur Delacour', NULL),
(76, 'James Potter', NULL),
(77, 'Peter Pettigrew', NULL),
(78, 'Nagini', NULL),
(79, 'Alastor Moody', 'Jalankah'),
(80, 'Argus Filch', NULL),
(81, 'Sybill Trelawney', NULL),
(82, 'George Weasley', NULL),
(83, 'Moaning Myrtle', NULL),
(84, 'Molly Weasly', NULL),
(85, 'Dudly Dursley', NULL),
(86, 'James Sirius Potter', NULL),
(87, 'Dean Thomas', NULL),
(88, 'Albus Severus Potter', NULL),
(89, 'John Wick', NULL),
(90, 'Batman', NULL),
(91, 'Mark', NULL),
(92, 'Bill Gates', NULL),
(95, 'Dennis Ritchie', NULL),
(110, 'tes', ''),
(111, 'est', ''),
(112, 'anuan', ''),
(113, 'Hanako', ''),
(114, 'Dana', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rating`
--

CREATE TABLE `rating` (
  `id` int(9) NOT NULL,
  `nis_siswa` int(9) NOT NULL,
  `id_buku` int(9) NOT NULL,
  `rating` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rating`
--

INSERT INTO `rating` (`id`, `nis_siswa`, `id_buku`, `rating`) VALUES
(1, 1111, 10, 4),
(2, 9999, 11, 4),
(3, 1111, 11, 3),
(4, 1111, 16, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE `siswa` (
  `id` int(11) NOT NULL,
  `nis` varchar(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `password` varchar(200) DEFAULT NULL,
  `id_kelas` int(11) NOT NULL,
  `jenis_kelamin` char(1) NOT NULL,
  `no_wa` varchar(20) NOT NULL,
  `status` varchar(200) NOT NULL,
  `mode_gelap` tinyint(1) NOT NULL DEFAULT 0,
  `terakhir_aktif` datetime NOT NULL,
  `aktivitas` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`id`, `nis`, `nama`, `password`, `id_kelas`, `jenis_kelamin`, `no_wa`, `status`, `mode_gelap`, `terakhir_aktif`, `aktivitas`) VALUES
(18, '543', 'Dsd', NULL, 7, 'l', '', 'x', 0, '0000-00-00 00:00:00', ''),
(22, '4545', 'Guren', NULL, 0, 'p', '', 'n', 0, '0000-00-00 00:00:00', ''),
(23, '12', 'They We', NULL, 19, 'p', '', 'n', 0, '0000-00-00 00:00:00', ''),
(24, '56', 'Us', NULL, 19, 'l', '', 'n', 0, '0000-00-00 00:00:00', ''),
(25, '12121', 'Ass', NULL, 21, 'l', '', 'n', 0, '0000-00-00 00:00:00', ''),
(26, '1111', 'Asik', '10500f3abbd8cdd302408e3391eee0ad', 1, 'l', '', 'y', 0, '2021-10-12 16:07:20', 'Keluar/Logout'),
(27, '2222', 'Andi', NULL, 1, 'l', '', 'n', 0, '0000-00-00 00:00:00', ''),
(28, '99999', 'Diki', NULL, 1, 'l', '', 'n', 0, '0000-00-00 00:00:00', ''),
(29, '77777', 'Joker', NULL, 1, 'l', '', 'n', 0, '0000-00-00 00:00:00', ''),
(30, '444', 'batman', '29421e3dbf61f1ed93f2f8594131f6b6', 1, 'l', '', 'y', 0, '0000-00-00 00:00:00', ''),
(31, '777777', 'kiki', NULL, 3, 'l', '', 'n', 0, '0000-00-00 00:00:00', ''),
(39, '5555', 'coco', 'd642668f2d463c2ff3e603eba3df672c', 3, 'l', '', 'y', 0, '2021-10-09 18:12:44', 'Diskusi mengirim pesan.'),
(40, '8989', 'jfjf', NULL, 1, 'p', '', 'n', 0, '0000-00-00 00:00:00', ''),
(41, '9999', 'Songo Songo Songo', '', 19, 'l', '', 'n', 0, '0000-00-00 00:00:00', ''),
(42, '4441', 'tyni Asli', NULL, 22, 'l', '', 'n', 0, '0000-00-00 00:00:00', ''),
(45, '1001', 'Toph Baiifong', NULL, 21, 'p', '', 'n', 0, '0000-00-00 00:00:00', ''),
(46, '1002', 'Sokka', NULL, 21, 'p', '', 'n', 0, '0000-00-00 00:00:00', ''),
(47, '1004', 'Suki', 'd7e6b589d17cf7b1e2c7d6782e317885', 19, 'p', '', 'y', 0, '2021-10-08 20:51:17', 'Keluar/Logout'),
(49, '5678', 'Iman Nami', NULL, 1, 'p', '', 'n', 0, '0000-00-00 00:00:00', ''),
(50, '73', 'Bot12', NULL, 1, 'p', '', 'n', 0, '0000-00-00 00:00:00', ''),
(51, '74', 'Bot 34', NULL, 21, 'p', '', 'n', 0, '0000-00-00 00:00:00', ''),
(52, '67', 'Asdf', NULL, 3, 'l', '', 'n', 0, '0000-00-00 00:00:00', ''),
(53, '22008', 'Santa Kaos', NULL, 20, 'p', '', 'n', 0, '0000-00-00 00:00:00', ''),
(56, '67675', 'Ajai', NULL, 20, 'p', '', 'n', 0, '0000-00-00 00:00:00', ''),
(57, '6909', 'Nomaden', NULL, 1, 'l', '', 'n', 0, '0000-00-00 00:00:00', ''),
(58, '5234864', 'Hiki', NULL, 3, 'l', '', 'x', 0, '0000-00-00 00:00:00', ''),
(61, '634', 'Hoho', NULL, 7, 'p', '', 'n', 0, '0000-00-00 00:00:00', ''),
(62, '129017', 'AndiHi', NULL, 18, 'l', '', 'x', 0, '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tahun`
--

CREATE TABLE `tahun` (
  `id` int(11) NOT NULL,
  `nama` int(4) NOT NULL,
  `deskripsi` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tahun`
--

INSERT INTO `tahun` (`id`, `nama`, `deskripsi`) VALUES
(1, 2001, NULL),
(7, 2003, NULL),
(8, 2005, NULL),
(10, 2004, NULL),
(11, 2007, NULL),
(12, 2077, ''),
(13, 2010, 'tahun bagus'),
(18, 1004, 'Sewu papat'),
(19, 1014, 'Sewu papat'),
(21, 2011, ''),
(29, 2222, NULL),
(30, 2055, NULL),
(31, 2009, 'Ahe'),
(33, 3434, ''),
(40, 7676, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(11) NOT NULL,
  `kode_katalog` varchar(100) NOT NULL,
  `nis_siswa` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `tgl_pinjam` date NOT NULL,
  `target_kembali` date NOT NULL,
  `tgl_kembali` date DEFAULT NULL,
  `kembali` tinyint(1) NOT NULL DEFAULT 0,
  `tepat_waktu` int(2) NOT NULL DEFAULT 1,
  `minta_tambah_lama_pinjam` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id`, `kode_katalog`, `nis_siswa`, `id_petugas`, `tgl_pinjam`, `target_kembali`, `tgl_kembali`, `kembali`, `tepat_waktu`, `minta_tambah_lama_pinjam`) VALUES
(8, 'aaaaaaa.0003', 1111, 9, '2021-08-11', '2021-08-10', NULL, 0, 1, 0),
(10, 'ayam.001', 12121, 9, '2021-08-11', '2021-08-18', NULL, 1, 1, 0),
(15, 'cacing.1', 999, 9, '2021-08-11', '2021-08-04', '2021-08-13', 1, 1, 0),
(16, 'ayam.002', 4441, 9, '2021-08-11', '2021-08-10', NULL, 0, 1, 0),
(17, 'ayam.003', 12, 9, '2021-08-11', '2021-08-18', '2021-08-12', 1, 1, 0),
(18, 'bunglon.002', 56, 2, '2021-08-13', '2021-08-17', '2021-08-13', 1, 1, 0),
(19, 'ayam.001', 5555, 9, '2021-08-13', '2021-08-16', '2021-08-20', 1, 1, 0),
(31, 'ayam.003', 777777, 9, '2021-08-13', '2021-08-02', '2021-08-13', 1, 1, 0),
(32, 'ayam.003', 9999, 20, '2021-08-19', '2021-08-24', '2021-08-20', 1, 1, 0),
(33, 'bunglon.001', 77777, 20, '2021-08-19', '2021-09-30', NULL, 0, 1, 0),
(35, 'ayam.001', 1111, 20, '2021-08-27', '2021-08-28', '2021-08-27', 1, 1, 0),
(36, 'bunglon.002', 1111, 20, '2021-08-27', '2021-09-02', '2021-08-27', 1, 1, 0),
(37, 'cacing.1', 1111, 20, '2021-08-27', '2021-09-30', NULL, 0, 1, 1),
(38, 'dino.001', 1111, 20, '2021-08-27', '2021-08-31', NULL, 0, 1, 0),
(39, 'elang.001', 1111, 20, '2021-08-27', '2021-08-26', '2021-08-27', 1, 1, 0),
(40, 'fanda.001', 1111, 20, '2021-08-27', '2021-09-03', '2021-08-27', 1, 1, 0),
(41, 'ayam.0051', 9999, 20, '2021-08-31', '2021-09-03', '2021-09-11', 1, 0, 0),
(42, 'ayam.007', 5555, 2, '2021-09-10', '2021-09-13', NULL, 0, 1, 0),
(43, 'ayam.0051', 9999, 2, '2021-09-12', '2021-09-15', '2021-09-12', 1, 1, 0),
(44, 'ayam.004', 1111, 2, '2021-09-12', '2021-09-22', NULL, 0, 1, 0),
(45, 'ayam.006', 777777, 2, '2021-09-12', '2021-09-21', NULL, 0, 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indeks untuk tabel `baca_cerpen`
--
ALTER TABLE `baca_cerpen`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `bibliografi`
--
ALTER TABLE `bibliografi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `bintang_cerpen`
--
ALTER TABLE `bintang_cerpen`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `cerpen`
--
ALTER TABLE `cerpen`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `diskusi`
--
ALTER TABLE `diskusi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `katalog`
--
ALTER TABLE `katalog`
  ADD PRIMARY KEY (`id_katalog`),
  ADD UNIQUE KEY `kode_katalog` (`kode_katalog`);

--
-- Indeks untuk tabel `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `lemari`
--
ALTER TABLE `lemari`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `penerbit`
--
ALTER TABLE `penerbit`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pengaturan`
--
ALTER TABLE `pengaturan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `penulis`
--
ALTER TABLE `penulis`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nis` (`nis`);

--
-- Indeks untuk tabel `tahun`
--
ALTER TABLE `tahun`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `baca_cerpen`
--
ALTER TABLE `baca_cerpen`
  MODIFY `id` bigint(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `bibliografi`
--
ALTER TABLE `bibliografi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT untuk tabel `bintang_cerpen`
--
ALTER TABLE `bintang_cerpen`
  MODIFY `id` bigint(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `cerpen`
--
ALTER TABLE `cerpen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `diskusi`
--
ALTER TABLE `diskusi`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT untuk tabel `katalog`
--
ALTER TABLE `katalog`
  MODIFY `id_katalog` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT untuk tabel `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT untuk tabel `lemari`
--
ALTER TABLE `lemari`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT untuk tabel `penerbit`
--
ALTER TABLE `penerbit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT untuk tabel `pengaturan`
--
ALTER TABLE `pengaturan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `penulis`
--
ALTER TABLE `penulis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- AUTO_INCREMENT untuk tabel `rating`
--
ALTER TABLE `rating`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT untuk tabel `tahun`
--
ALTER TABLE `tahun`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
